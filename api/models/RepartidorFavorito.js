"use strict";
const { Modelo } = require("./Modelo");


class RepartidorFavorito extends Modelo {
  //Obligatorio. Indica la tabla que va a usar
  static get tableName() {
    return "repartidores_favoritos";
  }


  static get relationMappings() {
    const Repartidor = require('./Repartidor');

    return {
      'repartidor': {
        relation: Modelo.BelongsToOneRelation,
        modelClass: Repartidor,
        join: {
          from: 'repartidores_favoritos.id_repartidor',
          to: 'repartidores.id'
        },
      },
    };
  }

}
module.exports = RepartidorFavorito;
