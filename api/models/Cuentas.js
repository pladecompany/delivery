"use strict";

const { Modelo } = require("./Modelo");

class Cuentas extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName() {
        return "cuentas";
    }

}
module.exports = Cuentas;
