"use strict";

const { Modelo } = require("./Modelo");


class Horarios extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName() {
        return "horarios";
    }
}
module.exports = Horarios;
