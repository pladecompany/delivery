"use strict";

const { Modelo } = require("./Modelo");


class Tipo_Vehiculo extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName() {
        return "tipovehiculo";
    }
}
module.exports = Tipo_Vehiculo;
