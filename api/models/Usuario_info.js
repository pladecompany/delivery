"use strict";
const Restaurante = require("../models/Restaurante");
const { Modelo } = require("./Modelo");


class Usuario extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName() {
        return "usuarios";
    }

    $afterGet(queryContext) {
        return this.ag();
    }

    async restaurante() {
        if(this.id_restaurante){
            var res = await Restaurante.query()
                .where("id", this.id_restaurante);
            if (res[0]) var valor = res;
            else var valor = [];
            this.restaurante = valor;
        }
    }

    async ag() {
        await this.restaurante();
    }
}
module.exports = Usuario;
