"use strict";

const { Modelo } = require("./Modelo");

class RestauranteSubcategorias extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName() {
        return "restaurantes_subcategorias";
    }
}
module.exports = RestauranteSubcategorias;
