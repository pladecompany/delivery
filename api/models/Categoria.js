"use strict";

const { Modelo } = require("./Modelo");

class Categoria extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName() {
        return "categorias";
    }
}
module.exports = Categoria;
