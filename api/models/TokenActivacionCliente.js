"use strict";

const { Modelo } = require("./Modelo");

class TokenActivacionCliente extends Modelo {
	// Obligatorio. Indica la tabla a utilizar.
	static get tableName() {
		return "token_activacion_cliente";
	}
}

//---- BORRAR TOKENS EXPIRADOS.
let tokenExpiraEnDias = 2
let intervalo = 3600000 * tokenExpiraEnDias // convertir a milisegundos.

setInterval(async function(){
	await TokenActivacionCliente.query().delete()
		.whereRaw("creado < DATE_SUB(NOW(), INTERVAL ? DAY)", [tokenExpiraEnDias]);
}, intervalo);
//----


module.exports = TokenActivacionCliente;
