"use strict";

const { Modelo } = require("./Modelo");


class MenuAcompañante extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName() {
        return "menus_acompañantes";
    }
}
module.exports = MenuAcompañante;
