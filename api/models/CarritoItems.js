"use strict";

const { Modelo } = require("./Modelo");
const Menu = require("./Menu");
const PedidoAdicionales = require("./PedidoAdicionales");
const PedidoAcompañantes = require("./PedidoAcompañantes");

class CarritoItems extends Modelo {
    static get tableName() {
        return "carrito_items";
    }

    $afterGet(queryContext){
        return this.ag();
    }

    async adicionales(){
        if(this.id)
            this.adicionales = await PedidoAdicionales.query().where("id_item", this.id);
    }

    async acompañantes(){
        if(this.id)
            this.acompañantes = await PedidoAcompañantes.query().where("id_item", this.id);
    }

    async menu(){
        if(this.id_menu)
            this.menu = await Menu.query().findById(this.id_menu);
    }

    async ag(){
        await this.adicionales();
        await this.acompañantes();
        await this.menu();
    }
}

module.exports = CarritoItems;
