"use strict";

const { Modelo } = require("./Modelo");
const CarritoItems = require("./CarritoItems");

class Carrito extends Modelo {
    static get tableName() {
        return "carrito";
    }
}

module.exports = Carrito;
