"use strict";

const { Modelo } = require("./Modelo");
const PedidoAdicionales = require("./PedidoAdicionales");

class Menus extends Modelo {
    static get tableName() {
        return "menus";
    }
}

class PedidoOrden extends Modelo {
    static get tableName() {
        return "pedidos_ordenes";
    }

    $afterGet(queryContext){
        return this.ag();
    }

    async adicionales(){
        this.adicionales = await PedidoAdicionales.query().where("id_orden", this.id);
    }

    async menu(){
        this.menu = await Menus.query().findById(this.id_menu);
    }

    async ag(){
        await this.adicionales();
        await this.menu();
    }
}

module.exports = PedidoOrden;
