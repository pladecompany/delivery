"use strict";

const { Modelo } = require("./Modelo");


class ClienteFavoritos extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName() {
        return "clientes_favoritos";
    }


    static get relationMappings() {
    	const Restaurante = require("./Restaurante");
        return {
            'restaurante': {
                relation: Modelo.BelongsToOneRelation,
                modelClass: Restaurante,
                join: {
                    from: 'clientes_favoritos.id_restaurante',
                    to: 'restaurantes.id'
                }
            }
        }
    };

}
module.exports = ClienteFavoritos;
