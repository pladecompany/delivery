"use strict";

const { Modelo } = require("./Modelo");
const PedidoAdicionales = require("./PedidoAdicionales");
const PedidoAcompañantes = require("../models/PedidoAcompañantes");
const PedidoEstatus = require("./PedidoEstatus");
const Cliente = require("./Cliente");
const Cuentas = require("../models/Cuentas");
const Repartidor = require("../models/Repartidor");

const CarritoItems = require("./CarritoItems");


class Pedido extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName(){
        return "pedidos";
    }

    $afterGet(queryContext){
        return this.ag();
    }

    async estatus(){
        if(!this.id_estatus) return;
        this.estatus = await PedidoEstatus.query().findById(this.id_estatus);
    }

    async ordenes(){
        if(!this.id) return;
        this.ordenes = await CarritoItems.query()
            // .join('menus', 'menus.id', 'carrito_items.id_menu')
            // .leftJoin('pedidos_adicionales', 'pedidos_adicionales.id_item', 'carrito_items.id')
            // .leftJoin('menus_adicionales', 'menus_adicionales.id', 'pedidos_adicionales.id_adicional')
            .select(
                'carrito_items.*',
                // 'menus.precio_usd AS precio_usd',
            )
            .where("carrito_items.id_pedido", this.id);


        // Incluir ids de adicionales en cada item.
        /*for (let orden of this.ordenes){
            orden.adicionales = await PedidoAdicionales.query()
                .join('carrito_items', 'carrito_items.id', 'pedidos_adicionales.id_item')
                .join('menus_adicionales', 'menus_adicionales.id', 'pedidos_adicionales.id_adicional')
                .select('menus_adicionales.*', 'pedidos_adicionales.*')
                .where('id_item', orden.id);
            orden.acompanantes = await PedidoAcompañantes.query()
                .join('carrito_items', 'carrito_items.id', 'pedidos_acompañantes.id_item')
                .join('menus_acompañantes', 'menus_acompañantes.id', 'pedidos_acompañantes.id_acompañante')
                .join('menus', 'menus.id', 'menus_acompañantes.id_menu_acompañante')
                .select('menus.nombre','menus.precio_usd','menus_acompañantes.*', 'pedidos_acompañantes.*')
                .where('id_item', orden.id);
        }*/
    }

    async cuenta(){
        if(this.id_cuenta){
            this.cuenta = await Cuentas.query()
                .select(
                    'id_restaurante as idres',
                    'nombre as cuenta',
                    'retorno'
                )
                .findById(this.id_cuenta);
                //.where("metodos_en_cuentas.id", this.id_metodos_en_cuentas);
        }
    }

    async cliente(){
        if(!this.id_cliente) return;
        this.cliente = await Cliente.query().findById(this.id_cliente);
    }

    async repartidor(){
        if(this.id_repartidor)
            this.repartidor = await Repartidor.query().findById(this.id_repartidor);
    }

    async ag(){
        await this.estatus();
        await this.ordenes();
        await this.cuenta();
        await this.cliente();
        await this.repartidor();
    }
};

module.exports = Pedido;
