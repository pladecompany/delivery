"use strict";

const { Modelo } = require("./Modelo");


class PedidoAcompañantes extends Modelo {
    static get tableName() {
        return "pedidos_acompañantes";
    }
}

module.exports = PedidoAcompañantes;
