"use strict";

const { Modelo } = require("./Modelo");


class Usuario extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName() {
        return "usuarios";
    }
}
module.exports = Usuario;
