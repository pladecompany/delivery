"use strict";

const { Modelo } = require("./Modelo");

class PreguntasFrecuentes extends Modelo {
    static get tableName() {
        return "preguntas_frecuentes";
    }
}

module.exports = PreguntasFrecuentes;
