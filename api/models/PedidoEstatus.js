"use strict";

const { Modelo } = require("./Modelo");


class PedidoEstatus extends Modelo {
    static get tableName() {
        return "pedidos_estatus";
    }
}

module.exports = PedidoEstatus;
