"use strict";

const { Modelo } = require("./Modelo");


class HorariosProgramados extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName() {
        return "horarios_programados";
    }
}
module.exports = HorariosProgramados;
