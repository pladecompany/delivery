"use strict";

const { Modelo } = require("./Modelo");

class PagoPedido extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName() {
        return "pagar_pedidos";
    }

}
module.exports = PagoPedido;
