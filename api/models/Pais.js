"use strict";

const { Modelo } = require("./Modelo");

class Paises extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName() {
        return "pais";
    }

}

module.exports = Paises;
