"use strict";

const { Modelo } = require("./Modelo");
const MenuAdicionales = require("./MenuAdicionales");
const CategoriaMenu = require("./CategoriaMenu");
const MenuAcompañante = require('./MenuAcompañante');
const Restaurante = require('./Restaurante');


class Menu extends Modelo {
  //Obligatorio. Indica la tabla que va a usar
  static get tableName() {
    return "menus";
  }

  $afterGet(queryContext) {
    return this.ag();
  }

  async adicionales() {
    let adis = await MenuAdicionales.query().where("id_menu", this.id);
    this.adicionales = adis[0] ? adis : [];
  }

  async categoria() {
    let cat = await CategoriaMenu.query().where("id", this.id_categoria);
    this.categoria = cat[0] ? cat[0] : {};
  }

  async acompañantes(){
    let acom = await MenuAcompañante.query()
      .join('menus', 'menus.id', 'menus_acompañantes.id_menu_acompañante')
      .select('menus_acompañantes.*', 'menus.precio_usd', 'menus.nombre', 'menus.existencias')
      .where('id_menu', this.id);
    this['acompañantes'] = acom;
  }

  async ganancias() {
    const t2 = this;
    this.ganancias = await Restaurante.query()
      .select('restaurantes.ganancia AS ganancia_restaurante', 'menus.ganancia AS ganancia_menu')
      .join('menus', 'restaurantes.id', 'menus.id_restaurante')
      .where('menus.id', this.id)
      .first()
  }

  async ag() {
    await this.adicionales();
    await this.categoria();
    await this.acompañantes();
  }

  static get relationMappings(){
    return {
      'rm_acompañantes': {
        relation: Modelo.HasManyRelation,
        modelClass: MenuAcompañante,
        join: {
          from: 'menus.id',
          to: 'menus_acompañantes.id_menu'
        }
      }
    }
  };
}

module.exports = Menu;
