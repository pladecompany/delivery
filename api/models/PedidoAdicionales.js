"use strict";

const { Modelo } = require("./Modelo");


class PedidoAdicionales extends Modelo {
    static get tableName() {
        return "pedidos_adicionales";
    }
}

module.exports = PedidoAdicionales;
