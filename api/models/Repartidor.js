"use strict";
const Tipo_Vehiculo = require("../models/Tipo_Vehiculo");
const { Modelo } = require("./Modelo");


class Repartidor extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName() {
        return "repartidores";
    }

    $afterGet(queryContext) {
        return this.ag();
    }

    async tipovehiculo() {
        if(this.id_tipo_vehiculo){
            var veh = await Tipo_Vehiculo.query()
                .where("id", this.id_tipo_vehiculo);
            if (veh[0]) var tipoveh = veh;
            else var tipoveh = [];
            this.vehiculo = tipoveh;
        }
    }

    async ag() {
        await this.tipovehiculo();
    }
}
module.exports = Repartidor;
