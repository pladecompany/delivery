"use strict";

const { Modelo } = require("./Modelo");
const Categoria = require("../models/Categoria");


class Subcategoria extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName() {
        return "subcategorias";
    }

    $afterGet(queryContext) {
        return this.ag();
    }

    async categoria() {
        if (this.id_categoria) {
            var cat = await Categoria.query().where("id", this.id_categoria);
            // if (cat[0]) var valor = cat;
            // else var valor = [];
            // this.categoria = valor;
            this.categoria = cat[0] ? cat[0] : [];
        }
    }

    async ag() {
        await this.categoria();
    }
}
module.exports = Subcategoria;
