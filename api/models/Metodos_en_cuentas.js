"use strict";

const { Modelo } = require("./Modelo");

class Metodos_en_cuentas extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName() {
        return "metodos_en_cuentas";
    }

}
module.exports = Metodos_en_cuentas;
