"use strict";

const fs = require("fs");

const config = require("../config");

var fecha_act = new Date();
var ano_act = fecha_act.getFullYear();

module.exports = function(data) {
    return `
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <title>Document</title>
    </head>
    <body bgcolor="#E1E1E1" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
                        <center style="background-color:#E1E1E1;">
                            <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="table-layout: fixed;max-width:100% !important;width: 100% !important;min-width: 100% !important;">
                                <tr>
                                    <td align="center" valign="top" id="bodyCell">
                                        <!--espaciado superior-->
                                        <table bgcolor="#E1E1E1" border="0" cellpadding="0" cellspacing="0" width="500" id="emailHeader">
                                            <tr>
                                                <td align="center" valign="top">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td align="center" valign="top">
                                                                <table border="0" cellpadding="10" cellspacing="0" width="500" class="flexibleContainer">
                                                                    <tr>
                                                                        <td valign="top" width="500" class="flexibleContainerCell">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <!--espaciado superior-->
                                        <table bgcolor="#FFFFFF"  border="0" cellpadding="0" cellspacing="0" width="500" id="emailBody">
                                            <tr>
                                                <td align="center" valign="top">
                                                      <!--Header-->
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="color:#FFFFFF;" bgcolor="#3498db">
                                                        <tr>
                                                            <td align="center" valign="top">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
                                                                    <tr>
                                                                        <td align="center" valign="top" width="500" class="flexibleContainerCell">
                                                                            <table border="0" cellpadding="30" cellspacing="0" width="100%">
                                                                                <tr>
                                                                                    <td align="center" valign="top" style="padding:5px;justify-content: space-between;display: flex;"  class="textContent flex">
                                                                                        <div style="padding: 10px;display: flex;align-items: center;">
                                                                                            <div style="padding: 5px;border-radius: 50%;opacity: .5;" class="">
                                                                                                <p><i class="fas fa-mobile-alt"></i></p>
                                                                                               
                                                                                            </div>
                                                                                            <p style="margin: 0;" class="fuente" >App</p>
                                                                                        </div>
                                                                                        <div style="padding: 10px;display: flex;align-items: center;" >
                                                                                            <div style="padding: 5px;border-radius: 50%;opacity: .5;" class="">
                                                                                                <p><i class="far fa-bell"></i></p>
                                                                                            </div>
                                                                                            <p style="margin: 0;" class="fuente" >Notificaciones</p>
                                                                                        </div>
                                                                                       
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                      <!--Header-->
                                                     <!--Banner-->
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="color:#FFFFFF;" bgcolor="#3498db">
                                                        <tr>
                                                            <td align="center" valign="top">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
                                                                    <tr>
                                                                        <td align="center" valign="top" width="500" class="flexibleContainerCell">
                                                                            <table border="0" cellpadding="30" cellspacing="0" width="100%">
                                                                                <tr>
                                                                                    <td align="center" valign="top" style="padding:0;" class="textContent">
                                                                                        <center>
                                                                                        <!--Logo-->
                                                                                        <img src="${config.dominioWeb}static/img/logo.png" title="logo" alt="logo" width="200px" height="auto" style="max-width:50%;"><br><br>
                                                                                        <!--Logo-->
                                                                                            <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
                                                                                                <h2 style="color:#FFFFFF;font-size:18px;font-weight:bold;margin:5px;margin-bottom:50px;text-align:center!important" align="center">
                                                                                                   ${data.titulo}
                                                                                               </h2>
                                                                                        </center>
                                                                                       
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <!--Banner-->
                                                </td>
                                            </tr>
    
                                            <tr>
                                                <td align="center" valign="top">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#F8F8F8">
                                                        <tr>
                                                            <td align="center" valign="top">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
                                                                    <tr>
                                                                        <td align="center" valign="top" width="500" class="flexibleContainerCell">
                                                                            <table border="0" cellpadding="30" cellspacing="0" width="100%">
                                                                                <tr>
                                                                                    <td align="center" valign="top">
    
                                                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                            <tr>
                                                                                                <td>
                                                                                                   
                                                                                                </td>
                                                                                            </tr>
    
                                                                                            <tr>
                                                                                                <td valign="top" class="textContent">
    
                                                                                                   
                                                                                                    
                                                                                                    ${data.cuerpo}
                                                                                                    </div>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
    
                                            <tr>
                                                <td align="center" valign="top">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td align="center" valign="top">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
                                                                    <tr>
                                                                        <td align="center" valign="top" width="500" class="flexibleContainerCell">
                                                                            <table border="0" cellpadding="30" cellspacing="0" width="100%">
                                                                                <tr>
                                                                                    <td align="center" valign="top">
                                                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
    
                                                                                            <tr>
                                                                                                <td align="center" valign="top" style="border-top:1px solid #C8C8C8;"></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td valign="top" class="textContent">
                                                                                                    <div style="text-align:center;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;margin-top:3px;color:#5F5F5F;line-height:135%;"><a href="${config.dominioWeb}">www.doomi.app</a></div>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
    
                                        <table bgcolor="#E1E1E1" border="0" cellpadding="0" cellspacing="0" width="500" id="emailFooter">
                                            <tr>
                                                <td align="center" valign="top">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td align="center" valign="top">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
                                                                    <tr>
                                                                        <td align="center" valign="top" width="500" class="flexibleContainerCell">
                                                                            <table border="0" cellpadding="30" cellspacing="0" width="100%">
                                                                                <tr>
                                                                                    <td valign="top" bgcolor="#E1E1E1">
    
                                                                                        <div style="font-family:Helvetica,Arial,sans-serif;font-size:13px;color:#828282;text-align:center;line-height:120%;">
                                                                                            <div>Copyright &#169; ${ano_act} <a href="${config.dominioWeb}" style="text-decoration:none;color:#828282;">Doomi</a> Todos los derechos reservados.</div>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </center>
                        <style>
                        </style>
                    </body>
    </html>
`;
};