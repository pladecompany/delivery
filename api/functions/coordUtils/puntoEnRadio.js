const distanciaEntreCoordenadas = require("./distanciaEntreCoordenadas");

module.exports = function puntoEnRadio(punto, centro, radio) {
    // El radio se debe dar en kilometros.
    //console.log(punto, centro, radio);
    return distanciaEntreCoordenadas(punto, centro) < radio;
};
