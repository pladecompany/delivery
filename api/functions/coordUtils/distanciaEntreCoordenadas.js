function degreesToRadians(degrees) {
    return degrees * Math.PI / 180;
}

function distanciaEnKmEntreCoordenadasTerrestres(punto1, punto2) {
    // Regresa la distancia en kilometros.

    let earthRadiusKm = 6371;

    let lat1 = punto1.lat || punto1.latitud;
    let lon1 = punto1.lon || punto1.longitud;

    let lat2 = punto2.lat || punto2.latitud;
    let lon2 = punto2.lon || punto2.longitud;


    let dPhi = degreesToRadians(lat2-lat1);
    let dLambda = degreesToRadians(lon2-lon1);

    let phi1 = degreesToRadians(lat1);
    let phi2 = degreesToRadians(lat2);

    let a = Math.sin(dPhi/2) * Math.sin(dPhi/2) +
        Math.sin(dLambda/2) * Math.sin(dLambda/2) * Math.cos(phi1) * Math.cos(phi2); 
    let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    return earthRadiusKm * c;
}

module.exports = distanciaEnKmEntreCoordenadasTerrestres;
