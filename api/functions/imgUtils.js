const fs = require("fs-extra");
const crypto = require("crypto");
const config = require("../config");
const archivos = require("../Archivos");

const IMG_DIR = __dirname + "/../public/uploads/";
const IMG_EXT = "__.jpg";



module.exports.deleteImage = async(nombre) => {
	await fs.unlink(IMG_DIR+nombre, err => console.log("borrarImagen-err:", err));
};

module.exports.guardarImagenBase64 = (data, pref) => {
	let filename = crypto.randomBytes(18).toString("hex") + IMG_EXT;

	if(typeof pref == "string")
		filename = pref.concat(filename);

	fs.writeFile(
		IMG_DIR+filename,
		new Buffer.from(data.split(",")[1], "base64"),
		"base64",
		function(err){}
	);

	return config.rutaArchivo(filename);
};
