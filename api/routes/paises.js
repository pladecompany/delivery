//llamar al modelo de Usuarios
const Paises = require("../models/Pais");
const Estado = require("../models/Estado");
const Ciudad = require("../models/Ciudad");

//Llamar otras librerias
const AR = require("../ApiResponser");
const express = require("express");
const jwt = require("jsonwebtoken");
const config = require("../config");
var sesion = require("./sesion");
var sesi = new sesion();
var archivos = require("../Archivos");

const validarMail = require("../functions/validateMail");
//Obligatorio para trabajar con FormData
const multer = require("multer");
const upload = multer();

const moment = require("moment");

var router = express.Router();
 


router.get('/',upload.none(),async (req, res, next) => {
    const paises = await Paises.query()
    .catch(err => {
      res.send(err);
    });
  AR.enviarDatos(paises, res);
});

router.get("/estado/:id",archivos.none(), async (req, res) => {
  const pais = await Estado.query()
    .where({id_pais:req.params.id})
    .catch(err => {
      res.send(err);
    });

  AR.enviarDatos(pais, res);
});

router.get("/ciudad/:id",archivos.none(), async (req, res) => {
  const pais = await Ciudad.query()
    .where({id_estado:req.params.id})
    .catch(err => {
      res.send(err);
    });

  AR.enviarDatos(pais, res);
});

module.exports = router;
