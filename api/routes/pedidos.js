//llamar al modelo
const { raw, ref } = require('objection');
const Pedido = require("../models/Pedido");
const PedidoOrden = require("../models/PedidoOrden");
const PedidoAdicionales = require("../models/PedidoAdicionales");
const PedidoAcompañantes = require("../models/PedidoAcompañantes");
const PedidoEstatus = require("../models/PedidoEstatus");
const Menu = require("../models/Menu");
const MenuAdicionales = require("../models/MenuAdicionales");
const CarritoItems = require("../models/CarritoItems");
const Metodos_en_cuentas = require("../models/Metodos_en_cuentas");
const Restaurante = require("../models/Restaurante");
const Repartidor = require("../models/Repartidor");
const Cuentas = require("../models/Cuentas");
const Admin = require("../models/Admin");
const Notificaciones = require("../models/Notificacion");
const Cliente = require("../models/Cliente");
const PagoPedido = require("../models/PagoPedido");
var Firebase = require("../routes/firebase");
Firebase = new Firebase();
//Llamar otras librerias
const fs = require("fs-extra");
const AR = require("../ApiResponser");
const path = require("path");
const archivos = require("../Archivos");
const sesi = new (require("./sesion"))();
const upload = require("multer")();
const router = require("express").Router();
const config = require("../config");
const HTML_PDF = require("html-pdf");
const moment = require("moment");


// Crear pedido.
router.post("/", upload.none(), sesi.validar_cliente, async(req,res) => {
	/*
	Un pedido: { id_menu: Number , cantidad: Number, adicionales: Array[Number] }
	*/
	let data = req.body;
	const id_res = req.headers['id_res'];
	const id_cliente = req.headers['id_cliente'];

	// Verificar y limpiar datos.
	if(!data || !Array.isArray(data))
		return res.status(400).send('Se esperaba un Array');

	data = data.filter(p => p.id_menu && p.cantidad);

	// await calcularSubtotalAdicionales(data[0].adicionales);
	// Fin verificacion.

	const errs = [];
	const trans = [];

	/*
	pedido = await Pedido.query(trx).insert({
		id_restaurante: id_res,
		id_cliente: id_cliente,
		id_estatus: 1, // Enviado.
	})
	.then(async resp => {
		for (orden of data){

		}
	})
	.catch(e => console.log(e));
	*/
	let trx, pedido;
	for (orden of data)
	{
		// trx = await Pedido.transaction();

		pedido = await Pedido.query().insert({
			id_restaurante: id_res,
			id_cliente: id_cliente,
			id_estatus: 1, // Enviado.
		});

		for (orden of data) {
			let ord = await PedidoOrden.query().insert({
				cantidad: orden.cantidad,
				id_menu: orden.id_menu,
				id_pedido: pedido.id,
			});

			if(orden.adicionales && orden.adicionales.length > 0){
				let adis = await PedidoAdicionales.query()
					.insertGraph(orden.adicionales.map( a => ({id_adicional:a, id_orden:ord.id}) ));
			}
		}


		if(!pedido) return res.sendStatus(500);
	}

	let p = await Pedido.query().findById(pedido.id);
		// .join("pedidos_ordenes", "pedidos.id", "pedidos_ordenes.id_pedido")
		// .join("pedidos_adicionales", "pedidos_ordenes.id", "pedidos_adicionales.id_orden");

	AR.enviarDatos(p, res);
});

// Listar pedidos - Cliente.
router.get("/", sesi.validar_general, async(req,res) => {
	const id_cliente = req.headers.id_usuario || req.query.id_cliente || req.params.id_cliente;
	// const Query = Pedido.query()
	// 	.join("restaurantes", "restaurantes.id", "pedidos.id_restaurante")
	// 	.select("pedidos.*", "restaurantes.img as img_restaurante")
	// 	.orderBy("pedidos.creado", "desc");

	// if(id_cliente)
	// 	Query.where("id_cliente", id_cliente)

	pedidosDetallados(id_cliente).then(resp => AR.enviarDatos(resp, res));
});

// Filtrar los pedidos.
router.get("/filtrados", upload.none(), sesi.validar_general, async(req,res) => {

	const filtros = req.query;
	const tipo_usuario = req.body.tipo_usuario || req.query.tipo_usuario || req.headers.tipo_usuario;

	const Query = Pedido.query()
		.join('restaurantes', 'restaurantes.id', 'pedidos.id_restaurante')
		.select(
			'pedidos.*',
			'restaurantes.img as img_restaurante',
			'restaurantes.nombre as nombre_doomi',
			raw('pedidos.subtotal + IFNULL(pedidos.subtotal_adicionales, 0) + IFNULL(pedidos.subtotal_acompañantes, 0) + IFNULL(pedidos.pago_del, 0)').as('total'),
			raw('DATE_FORMAT(pedidos.creado, "%Y-%m-%d %H:%i:%s")').as('creado')
			)
		.orderBy('pedidos.creado', 'desc');


	let id_usuario = null;
	if(req.headers['tipo-cliente'] == 'app')
		id_usuario = req.headers.id_usuario


	/** APLICAR FILTROS **/

	if(tipo_usuario == 'cliente' && id_usuario)
		Query.where({id_cliente: id_usuario});


	if(filtros.estatus && +filtros.estatus)
		Query.where('pedidos.id_estatus', filtros.estatus);

	if(filtros.desde) {
		let desde = new Date(filtros.desde).toISOString();
		Query.where('pedidos.creado', '>=', desde);
	}

	if(filtros.hasta) {
		let hasta = new Date(filtros.hasta).toISOString();
		Query.where('pedidos.creado', '<=', hasta);
	}

	if(+filtros.repartidor)
		Query.where('pedidos.id_repartidor', filtros.repartidor);

	if(+filtros.doomi)
		Query.where('pedidos.id_restaurante', filtros.doomi);

	if(Array.isArray(filtros.estatus_in))
		Query.whereIn('pedidos.id_estatus', filtros.estatus_in);

	if(filtros.actual) {
		let hoy = moment().format('YYYY-MM-DD');
		Query.whereRaw('DATE(pedidos.creado) = ?', hoy);
	}

	if(filtros.modalidad)
		Query.where('pedidos.modalidad', filtros.modalidad);

	Query.havingRaw('DATE(pedidos.creado) >= ?', moment().subtract(1,'week').format('YYYY-MM-DD'));
	/** FIN FILTROS **/


	// Ejecutar query.
	await Query.then(resp => AR.enviarDatos(resp, res)).catch(console.log);
});

// Listar pedidos - Admin y Restaurante
router.get("/restaurante/:id", sesi.validar_general, async(req,res) => {
	const id_res = req.params.id;
	let datos = req.query;
	const Query = Pedido.query()
		.join('restaurantes', 'restaurantes.id', 'pedidos.id_restaurante')
		.select("pedidos.*", "restaurantes.img as img_restaurante", "restaurantes.estatus as st", "restaurantes.nombre as nom_res",raw('DATE_FORMAT(pedidos.creado, "%Y-%m-%d %H:%i:%s")').as('creado'),raw('DATE_FORMAT(pedidos.fecha_pro, "%Y-%m-%d %H:%i:%s")').as('fecha_pro'));

	if (id_res != 0) //0 es un admin y por eso no entra a esta condicion
		Query.where('pedidos.id_restaurante', id_res);

	if (datos.fecd && !datos.fech)
		Query.where('pedidos.creado', '>=', datos.fecd+' 00:00:00');

	if (!datos.fecd && datos.fech)
		Query.where('pedidos.creado', '<=', datos.fech+' 23:59:59');

	if (datos.fecd && datos.fech)
		Query.where('pedidos.creado', '>=', datos.fecd+' 00:00:00')
			.where('pedidos.creado', '<=', datos.fech+' 23:59:59');

	if (datos.cliente)
		Query.where('pedidos.id_cliente', datos.cliente);

	if (datos.sts)
		Query.where('pedidos.id_estatus', datos.sts);

	if (datos.modalidad)
		Query.where('pedidos.modalidad', datos.modalidad);

	if (datos.app)
		Query.orderByRaw('pedidos.id_estatus, pedidos.id DESC')
	else
		Query.orderByRaw('pedidos.id_estatus, pedidos.id')
	await Query.then(resp => AR.enviarDatos(resp, res));
});

// Listar estatus posibles de pedidos.
router.get("/estatus", sesi.validar_general, async(req, res) => {
	await PedidoEstatus.query().then(resp => AR.enviarDatos(resp, res));
});

//editar Estatus
router.put("/edit_sts", upload.none(), sesi.validar_general, async (req, res, next) => {
	data = req.body;
	let errr = false;
	var usu_id = data.id_usu;
	var usu_tipo = data.tipo_usu;
	// inicio validaciones
	if (!data.sts) errr = "Ingresa el estatus";
	if (!data.iden) errr = "Ingresa la id del Pedido";

	//fin validaciones
	if(errr){
		return AR.enviarDatos({r: false, msj: errr}, res);
	}

	let datos = {
		id_estatus: data.sts,
		motivo: data.nota
	}
	if(data.calificacion)
		datos.nota=data.calificacion;
	if(data.calificacion2){
		datos.nota_rep=data.calificacion2;
		//datos.obs_rep=data.nota2;
	}

	// Iniciar preparacion programado.
	if (req.query.ini_pro){
		delete datos.id_estatus;
		datos.preparando_programado = 1;
	}

	//if(datos.id_estatus==99 ||datos.id_estatus==97)
	//	datos.pago_del=0;

	// Si fallo los tres intentos.
	if(datos.id_estatus==97){ // Inhabilitado.
		let p = await Pedido.query().findById(data.iden).where('nr_pago', 3).catch(console.log);
		if(p) { datos.id_estatus = 99 }
	}

	await Pedido.query().updateAndFetchById(data.iden, datos)
	.then(async resp => {
		var id_sts = resp.id_estatus;
		var info = await PedidoEstatus.query().where({ id: id_sts });//OBTENER DATA DEL STATUS
		if(data.sts==99 || data.sts==97){
			let allitems = await CarritoItems.query().where('id_pedido',data.iden);
			if(allitems[0]){
				for (var i = 0; i < allitems.length; i++) {
					let menu_sel = await Menu.query().findById(allitems[i].id_menu);
					if(menu_sel.existencias==0){
						await Notificaciones.query()
							.insert({
								contenido: menu_sel.nombre+', existencia restablecida por pedido '+info[0].nombre,
								fecha: new Date(),
								estatus: 0,
								tipo_noti: 2,
								redireccionar_noti: '?op=menu&id=',
								id_usable: menu_sel.id,
								id_receptor: pedido.id_restaurante,
								id_emisor: pedido.id_restaurante,
								tabla_usuario_r: 'restaurantes',
								tabla_usuario_e: 'restaurantes'
							}).then(async noti => {
					        	IO.sockets.emit("RecibirNotificacion", noti);
					        }).catch(async(err)=>{console.log(err);});
					}
					var suma = parseFloat(menu_sel.existencias)  + parseFloat(allitems[i].cantidad);
					await Menu.query().findById(menu_sel.id).patch({existencias: suma}).catch(err => console.log(err));
				};
			}
		}

		if (resp.id_estatus == 98) {
			await Pedido.query().patchAndFetchById(data.iden, {id_repartidor:null}).catch(console.error);
		}

		if(resp.id_estatus==7 && resp.id_repartidor!=null){
			Pedido.query().updateAndFetchById(data.iden, {id_estatus: 8}).catch(async err => {console.log(err);});
	    	id_sts = 8;
	    }



	  	var txt_noti = "El pedido #" + resp.id + " cambio de estatus a: " + info[0].nombre;
	    if(resp.id_estatus==97 || resp.id_estatus==99)
	    	txt_noti += "<br>Motivo: " + resp.motivo;
	    if(resp.id_estatus==7 && resp.id_repartidor==null && resp.modalidad=='Delivery')
	    	txt_noti += ", en espera que un repartidor tome tu pedido ";
	    //console.log(resp);
	    if(resp.id_estatus==4 || resp.id_estatus==5 || resp.id_estatus==6 || resp.id_estatus==7 || resp.id_estatus==8 || resp.id_estatus==9 || resp.id_estatus==10 || resp.id_estatus==97 || resp.id_estatus==99){
	    	var data_noti = {
	            contenido: txt_noti,
	            fecha: new Date(),
	            estatus: 0,
	            tipo_noti: 2,
	            redireccionar_noti: 'Detallescliente',
	            id_usable: resp.id,
	            id_receptor: resp.id_cliente,
	            id_emisor: resp.id_restaurante,
	            tabla_usuario_r: 'clientes',
	            tabla_usuario_e: 'restaurantes'
	        }
	        if(resp.id_estatus==8 && resp.modalidad=='Pickup'){
	        	data_noti.redireccionar_noti = 'Aviso';
	        }
	        if(resp.id_estatus==5 && usu_tipo=='administrador'){
	        	data_noti.id_emisor = usu_id;
	        	data_noti.tabla_usuario_e = 'administradores';
	        }
	        if(resp.id_estatus==10 && usu_tipo=='cliente') {
				data_noti.id_emisor = usu_id;
				data_noti.tabla_usuario_e = 'clientes';
				data_noti.id_receptor = data.id_receptor;
				data_noti.tabla_usuario_r = 'restaurantes';
	        }
	        var data_noti_rep = {};
	        if(resp.id_repartidor && (resp.id_estatus==6 || resp.id_estatus==7)){
	        	data_noti_rep = {
				    contenido: txt_noti,
				    fecha: new Date(),
				    estatus: 0,
				    tipo_noti: 2,
				    redireccionar_noti: 'Vermashistorial1',
				    id_usable: resp.id,
				    id_receptor: resp.id_repartidor,
				    id_emisor: resp.id_restaurante,
				    tabla_usuario_r: 'repartidores',
				    tabla_usuario_e: 'restaurantes'
				};

	        }else if(resp.id_repartidor && resp.id_estatus==9){
	        	data_noti_rep = {
				    contenido: "El pedido #" + resp.id + " se te ha " + info[0].nombre,
				    fecha: new Date(),
				    estatus: 0,
				    tipo_noti: 1,
				    redireccionar_noti: 'Historial',
				    id_receptor: resp.id_repartidor,
				    id_emisor: resp.id_restaurante,
				    tabla_usuario_r: 'repartidores',
				    tabla_usuario_e: 'restaurantes'
				};
	        }
	        if(resp.id_repartidor && (resp.id_estatus==6 || resp.id_estatus==7 || resp.id_estatus==9)){
	        	await Notificaciones.query()
			    .insert(data_noti_rep).then(async noti => {
		        	IO.sockets.emit("RecibirNotificacionApp", noti);
		        	var datap = {
		                id_usuario: noti.id_receptor,
		                title: noti.contenido,
		                body: noti.contenido,
		                id_usable: noti.id_usable,
		                tipo_noti: noti.tipo_noti,
		                ruta: noti.redireccionar_noti,
		                tablausuario: noti.tabla_usuario_r
		            };
		            await Firebase.enviarPushNotification(datap, function(datan) { console.log(datan); });
		        }).catch(async(err)=>{console.log(err);});
	        }
	    }else if(resp.id_estatus==11){
	    	var data_noti = {
	            contenido: "El pedido #" + resp.id + " se ha " + info[0].nombre,
	            fecha: new Date(),
	            estatus: 0,
	            tipo_noti: 2,
	            redireccionar_noti: '?op=pedidos_panel&id=',
	            id_usable: resp.id,
	            id_receptor: resp.id_restaurante,
	            id_emisor: resp.id_cliente,
	            tabla_usuario_r: 'restaurantes',
	            tabla_usuario_e: 'clientes'
	        }
	        if(resp.modalidad=='Delivery' || resp.modalidad=='Programado'){
	        	await Notificaciones.query()
			    .insert({
				    contenido: "El pedido #" + resp.id + " se ha Entregado y " + info[0].nombre,
				    fecha: new Date(),
				    estatus: 0,
				    tipo_noti: 2,
				    redireccionar_noti: 'Vermashistorial1',
				    id_usable: resp.id,
				    id_receptor: resp.id_repartidor,
				    id_emisor: resp.id_cliente,
				    tabla_usuario_r: 'repartidores',
				    tabla_usuario_e: 'clientes'
				}).then(async noti => {
		        	IO.sockets.emit("RecibirNotificacionApp", noti);
		        	var datap = {
		                id_usuario: noti.id_receptor,
		                title: noti.contenido,
		                body: noti.contenido,
		                id_usable: noti.id_usable,
		                tipo_noti: noti.tipo_noti,
		                ruta: noti.redireccionar_noti,
		                tablausuario: noti.tabla_usuario_r
		            };
		            await Firebase.enviarPushNotification(datap, function(datan) { console.log(datan); });
		        }).catch(async(err)=>{console.log(err);});
	        }
	    }
	    if (data_noti) {
		    await Notificaciones.query()
	        .insert(data_noti)
	        .then(async noti => {
	        	var datap = {
	                id_usuario: noti.id_receptor,
	                title: noti.contenido,
	                body: noti.contenido,
	                id_usable: noti.id_usable,
	                tipo_noti: noti.tipo_noti,
	                ruta: noti.redireccionar_noti,
			        tablausuario: noti.tabla_usuario_r
	            };
	            if(noti.tabla_usuario_r=='restaurantes'){
	            	IO.sockets.emit("RecibirNotificacion", noti);
	            	let resp_n = await Pedido.query().findById(data.iden);
					IO.sockets.emit("Recibirpedido", resp_n);
	            }
	            else
	            	IO.sockets.emit("RecibirNotificacionApp", noti);
				await Firebase.enviarPushNotification(datap, function(datan) { console.log(datan); });
				//IO.sockets.emit("RecibirNotificacionApp", noti);
			});
	    }
		if((resp.id_estatus==5 || resp.id_estatus==6 || resp.id_estatus==7) && resp.id_repartidor==null){
			//console.log(resp.id_estatus);
			if(resp.modalidad!='Pickup')
				IO.sockets.emit("ActualizarAppRep", {tip_sts: resp.id_estatus});
			if((resp.id_estatus==6 && resp.modalidad=='Pickup') || (resp.id_estatus==6 && resp.modalidad=='Delivery') || (resp.id_estatus==6 && resp.modalidad=='Programado' && resp.preparando_programado)){
				let min_retardo = parseFloat(data.minutos);
				if(resp.modalidad!='Pickup')
					min_retardo = parseFloat(data.minutos)*0.7;
				let fec_retardo = moment().add(min_retardo, 'minutes').format('YYYY-MM-DD HH:mm:ss');
				//USE LOS MISMOS CAMPOS DE BUSCAR FAVORITOS YA QUE CUMPLE LA MISMA FUNCION, SOLO QUE 1 ES PARA BUSCAR DESPUES DE LOS FAVORITOS Y 2 DESPUES DE TERMINAR EL TEMPORIZADOR
				await Pedido.query().updateAndFetchById(resp.id, {buscar_favoritos:2,fecha_buscar_fav:fec_retardo,tiempo_buscar: data.minutos})
				/*const configuracion = JSON.parse(fs.readFileSync(__dirname+"/../configuracion.json"));
				const restaurante_info = await Restaurante.query().where("id",resp.id_restaurante).catch(async err => {console.log("error: "+err)});

				if(restaurante_info[0]){
					const lat = restaurante_info[0].lat;
					const lon = restaurante_info[0].lon;

					await Repartidor.query()
					.join('repartidores_favoritos','repartidores_favoritos.id_repartidor','repartidores.id')
					.select('repartidores.*',raw(`
						(acos(sin(radians(repartidores.lat)) *
						sin(radians(:lat)) + cos(radians(repartidores.lat)) *
						cos(radians(:lat)) *
						cos(radians(repartidores.lon) - radians(:lon))) * 6370.986)`, {lat,lon}).as('distancia'))
					.having('distancia', '<=', configuracion.kmd)
					.where('repartidores_favoritos.id_restaurante',restaurante_info[0].id)
					.where('repartidores.estatus',1).where('repartidores.aprobado',1)
					.then(async repartidores =>{
						if(repartidores.length>0){
							let fec_bus = moment().add(configuracion.minutos, 'minutes').format('YYYY-MM-DD HH:mm:ss');
							await Pedido.query().updateAndFetchById(resp.id, {buscar_favoritos:true,fecha_buscar_fav:fec_bus})
							for (var re = 0; re < repartidores.length; re++) {
								var datap = {
					                id_usuario: repartidores[re].id,
					                title: 'Nuevo pedido',
					                body: 'Nuevo pedido generado por un cliente',
					                id_usable: resp.id,
					                tipo_noti: 2,
					                ruta: 'Vermas1',
							        tablausuario: 'repartidores'
				            	};
				            	await Firebase.enviarPushNotification(datap, function(datan) { console.log(datan); });
							}
						}else{
							Repartidor.query()
							.select('*',raw(`
								(acos(sin(radians(lat)) *
								sin(radians(:lat)) + cos(radians(lat)) *
								cos(radians(:lat)) *
								cos(radians(lon) - radians(:lon))) * 6370.986)`, {lat,lon}).as('distancia'))
							.having('distancia', '<=', configuracion.kmd)
							.where('estatus',1).where('aprobado',1)
							.then(async repartidores =>{
								for (var re = 0; re < repartidores.length; re++) {
									var datap = {
						                id_usuario: repartidores[re].id,
						                title: 'Nuevo pedido',
						                body: 'Nuevo pedido generado por un cliente',
						                id_usable: resp.id,
						                tipo_noti: 2,
						                ruta: 'Vermas1',
								        tablausuario: 'repartidores'
					            	};
					            	await Firebase.enviarPushNotification(datap, function(datan) { console.log(datan); });
								}
							})
						}
					}).catch(console.error);
				}*/
			}
		}
		IO.sockets.emit("ActualizarPedidosAdmin", {id_restaurante_emisor: resp.id_restaurante});
		resp.msj = "Estatus actualizado con éxito";
		resp.r = true;
		AR.enviarDatos(resp, res);
	})
	.catch(async err => {
		console.log(err)
		err.r = false;
		err.msj = 'Error al actualizar estatus';
		res.send(err);
	});
});
router.get("/ultima-calificacion/:id(\\d+)/", upload.none(), sesi.validar_general, async(req,res) => {
	const id_cli = req.params.id;

	await Pedido.query().where('id_cliente',id_cli).where('id_estatus',11).orderBy('pedidos.id', 'desc')
		.then(async pedido => {
			AR.enviarDatos(pedido, res);
		})
		.catch(err => {
			console.log(err);
			res.sendStatus(500);
		});
});

// Devolver pedido rechazado.
router.put("/devolver", upload.none(), sesi.validar_admin, async(req, res) => {
	const data = req.body;
	let errr = false;
	const usu_id = data.id_usu;
	const usu_tipo = data.tipo_usu;
	const id_pedido = data.iden ||data.id_pedido;

	// inicio validaciones
	// if (!data.sts) errr = "Ingresa el estatus";
	if (!id_pedido) errr = "Ingresa la id del Pedido";

	//fin validaciones
	if(errr) {
		return AR.enviarDatos({r: false, msj: errr}, res);
	}

	let items = await CarritoItems.query().where({ id_pedido }).catch(console.error);
	if (!items || items.length==0) return res.sendStatus(404);

	for (let item of items) {
		let menu = await Menu.query().findById(item.id_menu).catch(console.error);

		// Asegurar que haya existencias.
		if (menu.existencias < item.cantidad) {
			return AR.enviarDatos({r: false, msj:'No hay existencias suficientes'}, res);
		}
		for (let adi of item.adicionales) {
			let adicional = await PedidoAdicionales.query().findById(adi.id).catch(console.error);
			if (adicional.existencias < adi.cantidad) {
				return AR.enviarDatos({r: false, msj:'No hay existencias suficientes'}, res)
			}
		}
		for (let aco of item.acompañantes) {
			let acompañante = await PedidoAcompañantes.query().findById(aco.id).catch(console.error);
			if (acompañante.existencias < aco.cantidad) {
				return AR.enviarDatos({r: false, msj: 'No hay existencias suficientes'}, res);
			}
		}
	}

	const datos = {
		id_estatus: 97,
		nr_pago: 1,
	}

	await Pedido.query()
		.updateAndFetchById(id_pedido, datos)
		.then(ped => {
			const datos_noti = {
			    contenido: 'El pedido #'+ped.id+' ha sido devuelto por administración',
			    fecha: new Date(),
			    estatus: 0,
			    tipo_noti: 2,
			    redireccionar_noti: 'Detallescliente',
			    id_usable: ped.id,
			    id_receptor: ped.id_cliente,
			    id_emisor: usu_id,
			    tabla_usuario_r: 'clientes',
			    tabla_usuario_e: 'administradores'
			}


			Notificaciones.query().insert(datos_noti)
			.then(async noti => {
	        	IO.sockets.emit("RecibirNotificacionApp", noti);
	        	// IO.sockets.emit("RecibirNotificacion", ped);
	        	let datap = {
	                id_usuario: noti.id_receptor,
	                title: noti.contenido,
	                body: noti.contenido,
	                id_usable: noti.id_usable,
	                tipo_noti: noti.tipo_noti,
	                ruta: noti.redireccionar_noti,
	                tablausuario: noti.tabla_usuario_r
	            };
	            await Firebase.enviarPushNotification(datap, function(datan) { console.log(datan); });
			})
			.catch(console.error)

			Notificaciones.query()
				.insert({
					contenido: 'El pedido #'+ped.id+' ha sido devuelto por administración',
					fecha: new Date(),
					estatus: 0,
					tipo_noti: 2,
					redireccionar_noti: '?op=pedidos_panel&id=',
					id_usable: ped.id,
					id_receptor: ped.id_restaurante,
					id_emisor: ped.id_cliente,
					tabla_usuario_r: 'restaurantes',
					tabla_usuario_e: 'clientes'
				}).then(async noti => {
					IO.sockets.emit("RecibirNotificacion", noti);
				}).catch(async(err)=>{console.log(err);});

			IO.sockets.emit("Recibirpedido", ped);
		})
		.catch(console.error);

	AR.enviarDatos({r:true}, res);
});

// Obtener un pedido.
router.get("/:id(\\d+)/", upload.none(), sesi.validar_general, async(req,res) => {
	const id_pedido = req.params.id;

	await Pedido.query().findById(id_pedido).select("*",raw('DATE_FORMAT(pedidos.creado, "%Y-%m-%d %H:%i:%s")').as('creado'),raw('DATE_FORMAT(pedidos.fecha_pro, "%Y-%m-%d %H:%i:%s")').as('fecha_pro'))
		.then(async pedido => {
			if(!pedido)
				return res.sendStatus(404);

			// pedido.subtotalAdicionales = await calcularSubtotalAdicionales({id_pedido});
			// pedido.subtotalAcompañantes = await calcularSubtotalAcompañantes({id_pedido});
			// pedido.subtotal = await calcularSubtotal({id_pedido});
			pedido.total = ((pedido.pago_del*100)+(pedido.subtotal * 100) + ((pedido.subtotal_adicionales+pedido.subtotal_acompañantes) * 100)) / 100;
			pedido.restaurante = await Restaurante.query().where("id",pedido.id_restaurante).catch(async err => {console.log("error: "+err)});
			if(pedido.id_repartidor)
				pedido.repartidor = await Repartidor.query().findById(pedido.id_repartidor).catch(console.error);
			AR.enviarDatos(pedido, res);
		})
		.catch(err => {
			console.log(err);
			res.sendStatus(500);
		});
});

// Cancelar un pedido.
router.delete("/:id(\\d+)/", upload.none(), sesi.validar_admin, async(req, res) => {
	const id_pedido = req.params.id;
	const pedido = await Pedido.query().findById(id_pedido);
	const id_admin = req.headers.id_usuario;
	const motivo = req.body.motivo;

	if (!motivo) return AR.enviarDatos({r:false, msj:'Por favor ingrese el motivo'});

	await Pedido.query().findById(id_pedido)
		.whereIn('id_estatus', [8,9,10,12]) // Por entregar, Entregado, En transito, Entregado al cliente.
		.patch({pago_del:0, id_estatus: 98, motivo}) //Cancelado.
		.then(async r => {
			let allitems = await CarritoItems.query().where({id_pedido});

			if(allitems[0]){
				for (let item of allitems){
					let menu_sel = await Menu.query().findById(item.id_menu);
					if(menu_sel.existencias==0){
						await Notificaciones.query()
						  .insert({
							  contenido: menu_sel.nombre+', existencia restablecida por pedido cancelado',
							  fecha: new Date(),
							  estatus: 0,
							  tipo_noti: 2,
							  redireccionar_noti: '?op=menu&id=',
							  id_usable: menu_sel.id,
							  id_receptor: pedido.id_restaurante,
							  id_emisor: id_admin,
							  tabla_usuario_r: 'restaurantes',
							  tabla_usuario_e: 'administradores'
						}).then(async noti => {
					       	IO.sockets.emit("RecibirNotificacion", noti);
					    }).catch(async(err)=>{console.log(err);});
					}
					let suma = parseFloat(menu_sel.existencias) + parseFloat(item.cantidad);
					await Menu.query().findById(menu_sel.id).patch({existencias: suma}).catch(err => console.log(err));
				}
			}
			await Notificaciones.query()
			  .insert({
				  contenido: 'Ha cancelado el pedido #'+pedido.id,
				  fecha: new Date(),
				  estatus: 0,
				  tipo_noti: 2,
				  redireccionar_noti: '?op=pedidos_panel&id=',
				  id_usable: pedido.id,
				  id_receptor: pedido.id_restaurante,
				  id_emisor: id_admin,
				  tabla_usuario_r: 'restaurantes',
				  tabla_usuario_e: 'administradores'
			}).then(async noti => {
				IO.sockets.emit("RecibirNotificacion", noti);
				let resp_n = await Pedido.query().findById(id_pedido);
				IO.sockets.emit("Recibirpedido", resp_n);
			}).catch(async(err)=>{console.log(err);});

			await Notificaciones.query()
				.insert({
					contenido: 'Ha cancelado el pedido #'+pedido.id,
					fecha: new Date(),
					estatus: 0,
					tipo_noti: 2,
					redireccionar_noti: 'Detallescliente',
					id_usable: pedido.id,
					id_receptor: pedido.id_cliente,
					id_emisor: id_admin,
					tabla_usuario_r: 'clientes',
					tabla_usuario_e: 'administradores'
			}).then(async noti => {
				IO.sockets.emit("RecibirNotificacion", noti);
			}).catch(async(err)=>{console.log(err);});

			if(pedido.id_repartidor){

				await Notificaciones.query()
				.insert({
					contenido: 'Ha cancelado el pedido #'+pedido.id,
					fecha: new Date(),
					estatus: 0,
					tipo_noti: 2,
					redireccionar_noti: 'Vermashistorial1',
					id_usable: pedido.id,
					id_receptor: pedido.id_repartidor,
					id_emisor: id_admin,
					tabla_usuario_r: 'repartidores',
					tabla_usuario_e: 'administradores'
				}).then(async noti => {
					IO.sockets.emit("RecibirNotificacionApp", noti);
					var datap = {
						id_usuario: noti.id_receptor,
						title: noti.contenido,
						body: noti.contenido,
						id_usable: noti.id_usable,
						tipo_noti: noti.tipo_noti,
						ruta: noti.redireccionar_noti,
		                tablausuario: noti.tabla_usuario_r
					};
					await Firebase.enviarPushNotification(datap, function(datan) { console.log(datan); });
				}).catch(async(err)=>{console.log(err);});
			}

			AR.enviarDatos({r:true, msj:'Pedido cancelado'}, res);
		})

	/*
	await Pedido.query().delete().findById(id_pedido)
		.then(async r => {
			await CarritoItems.raw(''+
				'DELETE carrito_items, pedidos_adicionales ' +
				'FROM carrito_items ' +
				'LEFT JOIN pedidos_adicionales ON pedidos_adicionales.id_item = carrito_items.id ' +
				'WHERE carrito_items.id_pedido = ?', id_pedido)
				.catch(e => console.log(e));

			AR.enviarDatos({r:true, msj:'Pedido cancelado'}, res)
		})
		.catch(err => console.log(err));
	*/
});

// Procesar pago.
router.post("/pago", archivos.fields([{ name: "img_pago", maxCount: 1 }]), sesi.validar_cliente, async(req, res) => {
	const data = req.body;

	let errr = false;
	// Verificar.
	if(!data.cuenta)
		errr = "No se especificó la cuenta";
	// if(!data.metodo)
	// 	errr = "No se especificó el método de pago";
	if(!data.id_pedido)
		errr = "No se especificó el pedido";
	//if(!data.referencia)
	//	errr = "No se especificó la referencia";

	if(errr)
	{
		if(req.files["img_pago"])
			await fs.unlink(__dirname + "/../public/uploads/" + req.files["img_pago"][0].filename, function (err) { });

		return AR.enviarDatos({r:false, msj:errr});
	}


	const datos = {
		referencia: data.referencia,
		id_estatus: 3,
		id_cuenta: data.cuenta,
		nr_pago: raw('nr_pago + 1')
	};


	if(req.files["img_pago"])
		datos.img_pago = config.rutaArchivo(req.files["img_pago"][0].filename);

	const Cuenta = await Cuentas.query().where({id: data.cuenta});
	const Info = await Pedido.query().where({id: data.id_pedido});

	await Pedido.query()
		.where({id: data.id_pedido})
		.patch(datos)
		.then(async resp => {
			AR.enviarDatos({r:true, msj:'Datos registrados'}, res);

			if(Cuenta[0]){
				if(Cuenta[0].id_restaurante){
					//enviar notificacion
			        await Notificaciones.query()
			            .insert({
				            contenido: 'Ha realizado el pago del pedido #'+Info[0].id,
				            fecha: new Date(),
				            estatus: 0,
				            tipo_noti: 2,
				            redireccionar_noti: '?op=pedidos_panel&id=',
				            id_usable: Info[0].id,
				            id_receptor: Info[0].id_restaurante,
				            id_emisor: Info[0].id_cliente,
				            tabla_usuario_r: 'restaurantes',
				            tabla_usuario_e: 'clientes'
				        }).then(async noti => {
					       	IO.sockets.emit("RecibirNotificacion", noti);
					       	let resp_n = await Pedido.query().findById(Info[0].id);
							IO.sockets.emit("Recibirpedido", resp_n);
							let cli = await Cliente.query().findById(Info[0].id_cliente);
							if (cli) {
								// console.log(pedido.id_restaurante, cli.correo);
								await Firebase.enviarPushNotification({
									id_usuario: "" + Info[0].id_restaurante + "",
									title: 'Nuevo pago del pedido #'+Info[0].id,
									body: `${cli.nombre ? cli.nombre : 'Nombre no configurado'} (${cli.correo}), ha realizado un nuevo pago al pedido #${Info[0].id}`,
									tablausuario: 'restaurantes'
								}, function(datan) { console.log(datan); });
							}
					    }).catch(async(err)=>{console.log(err);});
				}else{
					let enviar_n = true;
					const ADMIN = await Admin.query();
					for (var i = 0; i < ADMIN.length; i++) {
						await Notificaciones.query()
			            .insert({
				            contenido: 'Ha realizado el pago del pedido #'+Info[0].id,
				            fecha: new Date(),
				            estatus: 0,
				            tipo_noti: 2,
				            redireccionar_noti: '?op=pedidos_panel&id=',
				            id_usable: Info[0].id,
				            id_receptor: ADMIN[i].id,
				            id_emisor: Info[0].id_cliente,
				            tabla_usuario_r: 'administradores',
				            tabla_usuario_e: 'clientes'
				        }).then(async noti => {
							// Solo hacer emits una sola vez, ya que cuando hay mas admins se duplicaran los emits innecesariamente
							if (enviar_n) {
								IO.sockets.emit("RecibirNotificacion", noti);
								enviar_n = false;
								let resp_n = await Pedido.query().findById(Info[0].id);
								IO.sockets.emit("Recibirpedido", resp_n);
							}

							let cli = await Cliente.query().findById(Info[0].id_cliente);
							if (cli) {
								// console.log(pedido.id_restaurante, cli.correo);
								await Firebase.enviarPushNotification({
									id_usuario: "" + ADMIN[i].id + "",
									title: 'Nuevo pago del pedido #'+Info[0].id,
									body: `${cli.nombre ? cli.nombre : 'Nombre no configurado'} (${cli.correo}), ha realizado un nuevo pago al pedido #${Info[0].id}`,
									tablausuario: 'administradores'
								}, function(datan) { console.log(datan); });
							}
					    }).catch(async(err)=>{console.log(err);});
					}
					await Notificaciones.query()
			            .insert({
				            contenido: 'El pedido #'+Info[0].id+' ha sido pagado a la administración por motivo de morosidad',
				            fecha: new Date(),
				            estatus: 0,
				            tipo_noti: 2,
				            redireccionar_noti: '?op=pedidos_panel&id=',
				            id_usable: Info[0].id,
				            id_receptor: Info[0].id_restaurante,
				            id_emisor: Info[0].id_cliente,
				            tabla_usuario_r: 'restaurantes',
				            tabla_usuario_e: 'clientes'
				        }).then(async noti => {
					       	IO.sockets.emit("RecibirNotificacion", noti);
					       	let resp_n = await Pedido.query().findById(Info[0].id);
							IO.sockets.emit("Recibirpedido", resp_n);
							await Firebase.enviarPushNotification({
								id_usuario: "" + Info[0].id_restaurante + "",
								title: 'Pago enviado a la administración, pedido #'+Info[0].id,
								body:  'El pedido #'+Info[0].id+' ha sido pagado a la administración por motivo de morosidad',
								tablausuario: 'restaurantes'
							}, function(datan) { console.log(datan); });
					    }).catch(async(err)=>{console.log(err);});
				}
			}

			// ELiminar imagen anterior.
			if(Info[0].img_pago)
				await fs.unlink(__dirname + "/../public/uploads/" + Info[0].img_pago.split(`\\public\\uploads\\`)[1], function (err) { });

		})
		.catch(err => console.log(err));
});

router.get("/conteo_pedidos_por_tab", sesi.validar_cliente, async(req, res) => {
	const id_cliente = req.headers.id_usuario

	await Pedido.query()
		.select('id_estatus', raw('COUNT(*)').as('count'))
		.where({id_cliente})
		.whereRaw('DATE(creado) >= ?', moment().subtract(1,'week').format('YYYY-MM-DD'))
		.groupBy('id_estatus')
		.then(resp => AR.enviarDatos(resp, res))
		.catch(console.error);
});

// Editar calificacion.
router.put("/calificacion/:id(\\d+)/", upload.none(), sesi.validar_cliente, async(req, res) => {
	const rate = req.body.nueva_calificacion || req.body.calificacion;
	if(!rate) return res.sendStatus(400);

	await Pedido.query().patchAndFetchById(req.params.id, {nota: rate})
		.then(r => AR.enviarDatos({r:true}, res))
		.catch(e => { console.error(e); res.sendStatus(500) });
});

// Intervencion admin.
router.put("/admin-edit", upload.none(), sesi.validar_admin, async(req, res) => {
	const id_pedido = req.body.id_pedido;
	const id_admin = req.body.id_usuario || req.headers.id_usuario;
	const body = req.body;
	const data = {};

	if (+body.id_estatus)
		data.id_estatus = body.id_estatus;

	if (body.hasOwnProperty('motivo'))
		data.motivo = body.motivo;

	if (body.liberar_repartidor)
		data.id_repartidor = null;

	const info = await Pedido.query().findById(id_pedido).catch(console.error);

	// Programado pasa a preparando.
	if (data.id_estatus && data.id_estatus == 6) {
		data.preparando_programado = 1;
	}

	Pedido.query()
		.patchAndFetchById(id_pedido, data)
		.then(async ped => {

			if (ped.id_estatus == 98) {
				await Pedido.query().patchAndFetchById(ped.id, {id_repartidor:null}).catch(console.error);
				body.liberar_repartidor = true;
			}

			let datos_noti = {
			    contenido: 'Administración ha hecho cambios en el pedido #'+ped.id,
			    fecha: new Date(),
			    estatus: 0,
			    tipo_noti: 2,
			    redireccionar_noti: 'Detallescliente',
			    id_usable: ped.id,
			    id_receptor: ped.id_cliente,
			    id_emisor: id_admin,
			    tabla_usuario_r: 'clientes',
			    tabla_usuario_e: 'administradores'
			}
			if(ped.id_estatus==8 && ped.modalidad=='Pickup'){
	        	data_noti.redireccionar_noti = 'Aviso';
	        }
			Notificaciones.query().insert(datos_noti)
			.then(async noti => {
	        	IO.sockets.emit("RecibirNotificacionApp", noti);
	        	// IO.sockets.emit("RecibirNotificacion", ped);
	        	let datap = {
	                id_usuario: noti.id_receptor,
	                title: noti.contenido,
	                body: noti.contenido,
	                id_usable: noti.id_usable,
	                tipo_noti: noti.tipo_noti,
	                ruta: noti.redireccionar_noti,
	                tablausuario: noti.tabla_usuario_r
	            };
	            await Firebase.enviarPushNotification(datap, function(datan) { console.log(datan); });
			})
			.catch(console.error)


			Notificaciones.query()
				.insert({
					contenido: 'Administración ha hecho cambios en el pedido #'+ped.id,
					fecha: new Date(),
					estatus: 0,
					tipo_noti: 2,
					redireccionar_noti: '?op=pedidos_panel&id=',
					id_usable: ped.id,
					id_receptor: ped.id_restaurante,
					id_emisor: id_admin,
					tabla_usuario_r: 'restaurantes',
					tabla_usuario_e: 'administradores'
				}).then(async noti => {
					IO.sockets.emit("RecibirNotificacion", noti);
				}).catch(async(err)=>{console.log(err);});

			IO.sockets.emit("Recibirpedido", ped);

			if (!body.liberar_repartidor && info.id_repartidor) return;
        	let datos_noti_rep = {
			    contenido: 'Administración te ha desasignado el pedido #'+ped.id,
			    fecha: new Date(),
			    estatus: 0,
			    tipo_noti: 1,
			    redireccionar_noti: 'Historial',
			    id_usable: ped.id,
			    id_receptor: info.id_repartidor,
			    id_emisor: id_admin,
			    tabla_usuario_r: 'repartidores',
			    tabla_usuario_e: 'administradores'
			};


			Notificaciones.query()
			    .insert(datos_noti_rep).then(async noti => {
		        	IO.sockets.emit("RecibirNotificacionApp", noti);
		        	var datap = {
		                id_usuario: noti.id_receptor,
		                title: noti.contenido,
		                body: noti.contenido,
		                id_usable: noti.id_usable,
		                tipo_noti: noti.tipo_noti,
		                ruta: noti.redireccionar_noti,
		                tablausuario: noti.tabla_usuario_r
		            };
		            await Firebase.enviarPushNotification(datap, function(datan) { console.log(datan); });
		        }).catch(async(err)=>{console.log(err);});




			// IO.sockets.emit("ActualizarAppRep", {tip_sts: ped.id_estatus});

			if((ped.id_estatus==5 || ped.id_estatus==6 || ped.id_estatus==7) && !ped.id_repartidor){
				//console.log(resp.id_estatus);
				IO.sockets.emit("ActualizarAppRep", {tip_sts: ped.id_estatus});

				// if((ped.id_estatus==6 && ped.modalidad=='Delivery') || (ped.id_estatus==6 && ped.modalidad=='Programado' && ped.preparando_programado)){
				if(([5,6,7].includes(+ped.id_estatus) && ped.modalidad=='Delivery') || ([5,6,7].includes(+ped.id_estatus)  && ped.modalidad=='Programado' && ped.preparando_programado)){

					const configuracion = JSON.parse(fs.readFileSync(__dirname+"/../configuracion.json"));
					const restaurante_info = await Restaurante.query().where("id",ped.id_restaurante).catch(async err => {console.log("error: "+err)});

					if(restaurante_info[0]){
						const lat = restaurante_info[0].lat;
						const lon = restaurante_info[0].lon;
						Repartidor.query()
						.select('*',raw(`
							(acos(sin(radians(lat)) *
							sin(radians(:lat)) + cos(radians(lat)) *
							cos(radians(:lat)) *
							cos(radians(lon) - radians(:lon))) * 6370.986)`, {lat,lon}).as('distancia'))
						.having('distancia', '<=', configuracion.kmd)
						.where('estatus',1).where('aprobado',1)
						.then(async repartidores =>{
							//console.log(repartidores);
							for (var re = 0; re < repartidores.length; re++) {

								var datap = {
					                id_usuario: repartidores[re].id,
					                title: 'Pedido disponible',
					                body: 'Nuevo pedido disponible',
					                id_usable: ped.id,
					                tipo_noti: 2,
					                ruta: 'Vermas1',
							        tablausuario: 'repartidores'
				            	};
				            	await Firebase.enviarPushNotification(datap, function(datan) { console.log(datan); });
							}
						})
						.catch(console.error);
					}
				}
			}

			AR.enviarDatos({r:true}, res);
		})
		.catch(e => { res.sendStatus(500); console.error(e) });
});

/*** REPARTIDOR ***/

// Listar pedidos - Repartidor.
router.get("/repartidor", sesi.validar_repartidor, async(req,res) => {
	let id_repartidor = req.headers.id_usuario;
	const id_pedidos_rechazados = (req.query.no || '').split(',');
	const filtrar_doomi = req.query.doomi;
	const estatus_a_listar = [
		// 5, // En proceso
		6, // Preparando
		7, // Finalizado
		8 // Por entregar
	];

	const modalidades_a_listar = ['Delivery', 'Programado'];


	const lat = req.query.lat || req.params.lat;
	const lon = req.query.lon || req.params.lon;

	if(!lat || !lon)
		return AR.enviarDatos({r:false, msj:'No se especificó la ubicacion'}, res);

	const configuracion = JSON.parse(fs.readFileSync(__dirname+"/../configuracion.json"));

	const Query = Pedido.query()
	.join('restaurantes', 'restaurantes.id', 'pedidos.id_restaurante')
	.select(
		'pedidos.*',
		'restaurantes.nombre AS doomi_nombre',
		'restaurantes.img AS doomi_img',
		'restaurantes.ubicacion AS doomi_ubicacion',
		'restaurantes.lat AS lat_dir_r',
		'restaurantes.lon AS lon_dir_r',
		// raw('ST_Distance_Sphere(POINT(:lon, :lat), POINT(lon_dir_p, lat_dir_p), 6370.986)', {lon,lat}).as('distancia'),
		raw('(SELECT re.id FROM repartidores_favoritos re WHERE re.id_restaurante=restaurantes.id AND re.id_repartidor='+id_repartidor+' LIMIT 1)').as('favorito_rep'),
		raw(`
		(acos(sin(radians(restaurantes.lat)) *
			sin(radians(:lat)) + cos(radians(restaurantes.lat)) *
			cos(radians(:lat)) *
			cos(radians(restaurantes.lon) - radians(:lon))) * 6370.986)`, {lat,lon}).as('distancia')

	)
	// .having(  raw('pedidos.id_estatus IN (?)', [estatus_a_listar])  )
	.havingIn('pedidos.id_estatus', estatus_a_listar)
	.havingIn('pedidos.modalidad', modalidades_a_listar)
	.having('distancia', '<=', configuracion.km)
	.havingNull('pedidos.id_repartidor')
	.havingRaw('(pedidos.modalidad <> "Programado" OR (pedidos.modalidad="Programado" AND pedidos.preparando_programado IS NOT NULL)) AND ((pedidos.buscar_favoritos=0) OR (pedidos.buscar_favoritos=1 AND favorito_rep IS NOT NULL))')
	//.where('pedidos.buscar_favoritos',0)
	.orderBy('pedidos.creado', 'desc');


	// Busqueda por doomi.
	if(filtrar_doomi && typeof filtrar_doomi == 'string'){
		for (keyword of sanitizeString(filtrar_doomi).split(' '))
			Query.orWhere('restaurantes.nombre', 'LIKE', '%'+keyword+'%');
	}

	// No listar pedidos rechazados.
	if(id_pedidos_rechazados[0])
		Query.whereNotIn('pedidos.id', id_pedidos_rechazados);

	await Query.then(resp => AR.enviarDatos(resp, res)).catch(err => console.log(err));
});

// Obtener un pedido.
router.get("/repartidor/:id(\\d+)/", upload.none(), sesi.validar_repartidor, async(req,res) => {
	const id_pedido = req.params.id;

	await Pedido.query().findById(id_pedido)
		.join('restaurantes', 'restaurantes.id', 'pedidos.id_restaurante')
		.select(
			'pedidos.*',
			'restaurantes.nombre AS doomi_nombre',
			'restaurantes.telefono AS doomi_telefono',
			'restaurantes.ubicacion AS doomi_ubicacion',
			'restaurantes.img AS doomi_img',
			'restaurantes.lat AS doomi_lat',
			'restaurantes.lon AS doomi_lon'
			)
		.then(async pedido => {
			if(!pedido)
				res.sendStatus(404);

			pedido.subtotalAdicionales = await calcularSubtotalAdicionales({id_pedido});
			pedido.subtotal = await calcularSubtotal({id_pedido});
			pedido.total = ((pedido.subtotal * 100) + (pedido.subtotalAdicionales * 100)) / 100;

			AR.enviarDatos(pedido, res);
		})
		.catch(err => {
			console.log(err);
			res.sendStatus(500);
		});
});

// Tomar pedido.
router.patch("/repartidor/tomar_pedido", upload.none(), sesi.validar_repartidor, async(req,res) => {
	const id_pedido = req.body.id_pedido || req.body.idPedido;
	const id_repartidor = req.headers.id_usuario;

	// Verificar
	if(!id_pedido)
		return res.status(400).send('No se especificó el id del pedido');

	let errr = false;
	const repartidor = await Repartidor.query()
		.select('estatus','aprobado').findById(id_repartidor).catch(console.log);

	const asignado = await Pedido.query().findOne({id_repartidor}).where('id_estatus', '<>', '98').catch(console.log);
	const pedido = await Pedido.query().findById(id_pedido).catch(console.log);

	if(!repartidor.aprobado)
		errr = 'Tu registro aún no ha sido aprobado por el administrador';
	else if(!repartidor.estatus)
		errr = 'Te encuentras suspendido, comunicate con el administrador';
	else if(asignado && asignado.id_estatus != 11)//Completado
		errr = 'Ya tienes un pedido asignado';
	else if(pedido && pedido.id_repartidor)
		errr = 'Pedido asignado a otro repartidor';

	if(errr)
		return AR.enviarDatos({r:false, msj:errr}, res);

	const datos = {id_repartidor};

	if(pedido.id_estatus == 7 || pedido.id_estatus == 6) // Finalizado, Preparando.
		datos.id_estatus = 8 // Por entregar.

	await Pedido.query().findById(id_pedido).patch(datos)
		.then(async resp => {
			const info = await Pedido.query().findById(id_pedido).catch(console.log);
			await Notificaciones.query()
			  .insert({
				  contenido: 'Este repartidor a tomado el pedido #'+info.id,
				  fecha: new Date(),
				  estatus: 0,
				  tipo_noti: 2,
				  redireccionar_noti: '?op=pedidos_panel&id=',
				  id_usable: info.id,
				  id_receptor: info.id_restaurante,
				  id_emisor: info.id_repartidor,
				  tabla_usuario_r: 'restaurantes',
				  tabla_usuario_e: 'repartidores'
			}).then(async noti => {
				IO.sockets.emit("RecibirNotificacion", noti);
				let resp_n = await Pedido.query().findById(id_pedido);
				IO.sockets.emit("Recibirpedido", resp_n);
			}).catch(async(err)=>{console.log(err);});
			  await Notificaciones.query()
			  .insert({
				  contenido: 'Este repartidor a tomado su pedido #'+info.id,
				  fecha: new Date(),
				  estatus: 0,
				  tipo_noti: 2,
				  redireccionar_noti: 'Detallescliente',
				  id_usable: info.id,
				  id_receptor: info.id_cliente,
				  id_emisor: info.id_repartidor,
				  tabla_usuario_r: 'clientes',
				  tabla_usuario_e: 'repartidores'
			}).then(async noti => {
			   	IO.sockets.emit("RecibirNotificacionApp", noti);
			   	var datap = {
		                id_usuario: noti.id_receptor,
		                title: noti.contenido,
		                body: noti.contenido,
		                id_usable: noti.id_usable,
		                tipo_noti: noti.tipo_noti,
		                ruta: noti.redireccionar_noti,
		                tablausuario: noti.tabla_usuario_r
		        };
		        await Firebase.enviarPushNotification(datap, function(datan) { console.log(datan); });
			}).catch(async(err)=>{console.log(err);});

			AR.enviarDatos({r:true}, res);
		})
		.catch(console.log);
});

router.patch("/repartidor/estatus", upload.none(), sesi.validar_repartidor, async(req,res) => {
	const id_pedido = req.body.id_pedido;
	await Pedido.query()
		.updateAndFetchById(id_pedido, {id_estatus: 10}) // En transito.
		.then(async resp => {
			await Notificaciones.query()
			    .insert({
				    contenido: 'El repartidor va en transito con su pedido #'+resp.id,
				    fecha: new Date(),
				    estatus: 0,
				    tipo_noti: 2,
				    redireccionar_noti: 'Detallescliente',
				    id_usable: resp.id,
				    id_receptor: resp.id_cliente,
				    id_emisor: resp.id_repartidor,
				    tabla_usuario_r: 'clientes',
				    tabla_usuario_e: 'repartidores'
				}).then(async noti => {
					IO.sockets.emit("RecibirNotificacionApp", noti);
					let resp_n = await Pedido.query().findById(id_pedido);
					IO.sockets.emit("Recibirpedido", resp_n);
					var datap = {
		                id_usuario: noti.id_receptor,
		                title: noti.contenido,
		                body: noti.contenido,
		                id_usable: noti.id_usable,
		                tipo_noti: noti.tipo_noti,
		                ruta: noti.redireccionar_noti,
		                tablausuario: noti.tabla_usuario_r
		            };
		            await Firebase.enviarPushNotification(datap, function(datan) { console.log(datan); });
				}).catch(async(err)=>{console.log(err);});

			AR.enviarDatos({r:true, msj:'Estatus actualizado'}, res)
		})
		.catch(console.log);
});

router.patch("/repartidor/entregar", upload.none(), sesi.validar_general, async(req, res) => {
	const id_pedido = req.body.id_pedido || req.body.iden;
	await Pedido.query()
		.updateAndFetchById(id_pedido, {id_estatus: 12}) // Entregado al cliente.
		.then(async resp => {

			let contenido, tabla_r, tabla_e, id_receptor, id_emisor;

			if (resp.id_repartidor) {
				contenido = 'El repartidor entregó el pedido #'+resp.id;
				id_receptor = resp.id_cliente;
				id_emisor = resp.id_repartidor;
				tabla_r =  'clientes';
				tabla_e = 'repartidores';
			}
			else {
				contenido = 'El doomialiado entregó el pedido #'+resp.id;
				id_receptor = resp.id_cliente;
				id_emisor = resp.id_restaurante;
				tabla_r =  'clientes';
				tabla_e = 'restaurantes';
			}

			await Notificaciones.query()
			    .insert({
				    contenido: contenido,
				    fecha: new Date(),
				    estatus: 0,
				    tipo_noti: 2,
				    redireccionar_noti: 'Detallescliente',
				    id_usable: resp.id,
				    id_receptor: id_receptor,
				    id_emisor: id_emisor,
				    tabla_usuario_r: tabla_r,
				    tabla_usuario_e: tabla_e
				}).then(async noti => {
					IO.sockets.emit("RecibirNotificacionApp", noti);
					let resp_n = await Pedido.query().findById(id_pedido);
					IO.sockets.emit("Recibirpedido", resp_n);
					var datap = {
		                id_usuario: noti.id_receptor,
		                title: noti.contenido,
		                body: noti.contenido,
		                id_usable: noti.id_usable,
		                tipo_noti: noti.tipo_noti,
		                ruta: noti.redireccionar_noti,
		                tablausuario: noti.tabla_usuario_r
		            };
		            await Firebase.enviarPushNotification(datap, function(datan) { console.log(datan); });
				}).catch(async(err)=>{console.log(err);});

			AR.enviarDatos({r:true, msj:'Estatus actualizado'}, res)
		})
		.catch(console.log);
});

router.patch("/repartidor/estoy_afuera", upload.none(), sesi.validar_repartidor, async(req,res) => {
	const id_pedido = req.body.id_pedido;
	let resp = await Pedido.query().findById(id_pedido);
	if(resp){
		await Notificaciones.query()
			.insert({
			    contenido: '¡EL REPARTIDOR ESTÁ AFUERA!',
			    fecha: new Date(),
			    estatus: 0,
			    tipo_noti: 2,
			    redireccionar_noti: 'Detallescliente',
			    id_usable: resp.id,
			    id_receptor: resp.id_cliente,
			    id_emisor: resp.id_repartidor,
			    tabla_usuario_r: 'clientes',
			    tabla_usuario_e: 'repartidores'
			}).then(async noti => {
				IO.sockets.emit("RecibirNotificacionApp", noti);
				var datap = {
			        id_usuario: noti.id_receptor,
			        title: noti.contenido,
			        body: noti.contenido,
			        id_usable: noti.id_usable,
			        tipo_noti: noti.tipo_noti,
			        ruta: noti.redireccionar_noti,
			        tablausuario: noti.tabla_usuario_r
			    };
			    await Firebase.enviarPushNotification(datap, function(datan) { console.log(datan); });
				AR.enviarDatos({r:true, msj:'Aviso enviado'}, res)
			}).catch(async(err)=>{console.log(err);});
	}
});

// Historial de pedidos.
router.get("/repartidor/historial", upload.none(), sesi.validar_repartidor, async(req,res) => {
	const id_repartidor = req.headers.id_usuario;

	await Pedido.query().where({id_repartidor})
		.join('restaurantes', 'restaurantes.id', 'pedidos.id_restaurante')
		.select(
			'pedidos.*',
			'restaurantes.nombre AS doomi_nombre',
			'restaurantes.img AS doomi_img',
			'restaurantes.lat AS doomi_lat',
			'restaurantes.lon AS doomi_lon',
			'restaurantes.telefono AS doomi_telefono',
			'restaurantes.ubicacion AS doomi_ubicacion'
			)
		.orderBy('pedidos.creado', 'desc')
		.then(resp => AR.enviarDatos(resp, res))
		.catch(console.log);
});

// Historial de pedidos por filtro.
router.get("/repartidor/historial/filtrados", upload.none(), sesi.validar_repartidor, async(req,res) => {
	const id_repartidor = req.headers.id_usuario;
	const filtros = req.query;
	console.log(filtros,id_repartidor);
	const Query = Pedido.query().where({id_repartidor})
		.join('restaurantes', 'restaurantes.id', 'pedidos.id_restaurante')
		.select(
			'pedidos.*',
			'restaurantes.nombre AS doomi_nombre',
			'restaurantes.img AS doomi_img',
			'restaurantes.lat AS doomi_lat',
			'restaurantes.lon AS doomi_lon',
			'restaurantes.telefono AS doomi_telefono',
			'restaurantes.ubicacion AS doomi_ubicacion'
			);


	if(filtros.desde) {
		//let desde = new Date(filtros.desde).toISOString();
		let desde = filtros.desde;
		Query.whereRaw('((pedidos.creado>="'+desde+'" AND pedidos.fecha_pro IS NULL) OR pedidos.fecha_pro>="'+desde+'")');
	}

	if(filtros.hasta) {
		//let hasta = new Date(filtros.hasta).toISOString();
		let hasta = filtros.hasta;
		Query.whereRaw('((pedidos.creado<="'+hasta+'" AND pedidos.fecha_pro IS NULL) OR pedidos.fecha_pro<="'+hasta+'")');
	}
	Query.orderBy('pedidos.creado', 'desc');
	await Query.then(resp => AR.enviarDatos(resp, res)).catch(console.log);
});

/*** REPORTES ***/

router.get("/filtros_reporte", upload.none(), sesi.validar_general, async(req,res) => {
	const tipo_usu = req.body.tipo_usuario || req.query.tipo_usuario || req.headers.tipo_usuario;

	const resp = {};
	resp.repartidores = await Repartidor.query().select('id', 'nombre', 'apellido').catch(console.log);
	resp.doomialiados = await Restaurante.raw('SELECT ??,?? FROM ??', ['id', 'nombre', 'restaurantes']).catch(console.log);
	resp.doomialiados = resp.doomialiados[0];
	resp.estatus = await PedidoEstatus.query().catch(console.log);
	resp.modalidades = ['Pickup', 'Delivery', 'Programado'];
	AR.enviarDatos(resp, res);
});

router.post("/generar_reporte", upload.none(), sesi.validar_general, async(req,res) => {
	const filtros = req.body;
	const id_res = req.body.id_res;
	const tipo_usu = req.body.tipo_usuario || req.query.tipo_usuario || req.headers.tipo_usuario;


	const knex = Pedido.knex();
	const rep_nombre_completo = knex.raw('CONCAT(??, " ", ??)', ['repartidores.nombre', 'repartidores.apellido']);

	// const Query = Pedido.query()
	// 	.select(
	// 		'repartidores.id AS id_repartidor',
	// 		raw('IFNULL(CONCAT(??, " ", ??), "")', ['repartidores.nombre', 'repartidores.apellido']).as('repartidor'),
	// 		'restaurantes.id AS id_doomi',
	// 		'restaurantes.nombre AS nombre_doomi',
	// 		'restaurantes.img AS img_doomi',
	// 		'restaurantes.telefono AS telefono_doomi',
	// 		'restaurantes.ubicacion AS direccion_doomi',
	// 		'pedidos.id AS id_pedido',
	// 		'pedidos.modalidad AS modalidad',
	// 		'pedidos.creado AS fecha_pedido',
	// 		'pedidos_estatus.nombre AS estatus',
	// 		'pedidos.id_estatus AS id_estatus',
	// 		'pedidos.pago_del AS costo_delivery',
	// 		b => b.from('carrito_items')
	// 			.select(raw('SUM(:pre: * (:gan:/100) * :can:)', {pre:'carrito_items.precio_usd', gan:'carrito_items.ganancia', can:'carrito_items.cantidad'}))
	// 			.where('id_pedido', ref('pedidos.id')).as('ganancia')
	// 	)
	// 	.join('restaurantes', 'restaurantes.id', 'pedidos.id_restaurante')
	// 	.join('pedidos_estatus', 'pedidos_estatus.id', 'pedidos.id_estatus')
	// 	// Puede que no haya repartidores.
	// 	.leftJoin('repartidores', 'repartidores.id', 'pedidos.id_repartidor');


	const Query = knex('pedidos')
		.column({
			id_repartidor: 'repartidores.id',
			repartidor: knex.raw('IFNULL(??, "")', rep_nombre_completo),
			id_doomi: 'restaurantes.id',
			nombre_doomi: 'restaurantes.nombre',
			img_doomi: 'restaurantes.img',
			telefono_doomi: 'restaurantes.telefono',
			direccion_doomi: 'restaurantes.ubicacion',
			id_pedido: 'pedidos.id',
			modalidad: 'pedidos.modalidad',
			fecha_pedido: 'pedidos.creado',//knex.raw('DATE_FORMAT(??, "%e-%m-%Y")', ['pedidos.creado']),
			estatus: 'pedidos_estatus.nombre',
			id_estatus: 'pedidos.id_estatus',
			costo_delivery: 'pedidos.pago_del',
		})
		.join('restaurantes', 'restaurantes.id', 'pedidos.id_restaurante')
		.join('pedidos_estatus', 'pedidos_estatus.id', 'pedidos.id_estatus')
		// Puede que no haya repartidores.
		.leftJoin('repartidores', 'repartidores.id', 'pedidos.id_repartidor');


	/* APLICAR FILTROS*/
	if(+filtros.doomi)
		Query.having('id_doomi', '=', filtros.doomi);

	// if(tipo_usu=='restaurante')
	// 	Query.having('repartidor', '<>', '');

	if(+filtros.repartidor)
		Query.having('id_repartidor', '=', filtros.repartidor);

	if(+filtros.estatus)
		Query.having('id_estatus', '=', filtros.estatus);

	if(filtros.modalidad)
		Query.having('modalidad', '=', filtros.modalidad);

	if(filtros.desde){
		let desde = new Date(filtros.desde).toISOString();
		Query.having('fecha_pedido', '>=', desde);
	}

	if(filtros.hasta){
		let hasta = new Date(filtros.hasta).toISOString();
		Query.having('fecha_pedido', '<=', hasta);
	}
	/* FIN FILTROS */


	await Query.then(async data => {
		if(data.length==0){
			return res.sendStatus(204); // No hay datos.
		}

		for (let fila of data) {
			if (fila.id_estatus != 11) {
				fila.ganancia_total_acumulada = 0;
				continue;
			}
			let ganancia_total_acumulada = 0;

			// Ganancia sobre los menus.
			let ci = await CarritoItems.query()
				.select(
					'id AS id_item',
					raw('SUM( precio_usd * IFNULL(ganancia/100, 0) * cantidad )').as('ganancia_menus'),
				)
				.where('id_pedido', fila.id_pedido)
				.first()
				.catch(console.error);
			ganancia_total_acumulada += ci.ganancia_menus;

			// Ganancia sobre adicionales.
			let pa = await PedidoAdicionales.query()
				.select(
					raw('SUM( pedidos_adicionales.precio_usd * IFNULL(carrito_items.ganancia/100, 0) * pedidos_adicionales.cantidad )')
						.as('ganancia_adicionales')
				)
				.join('carrito_items', 'carrito_items.id', 'pedidos_adicionales.id_item')
				.where('carrito_items.id_pedido', fila.id_pedido)
				.first();
			ganancia_total_acumulada += pa.ganancia_adicionales;

			// Ganancia sobre acompañantes.
			let pac = await PedidoAcompañantes.query()
				.select(
					raw('SUM( pedidos_acompañantes.precio_usd * IFNULL(carrito_items.ganancia/100, 0) * pedidos_acompañantes.cantidad )')
						.as('ganancia_acompañantes')
				)
				.join('carrito_items', 'carrito_items.id', 'pedidos_acompañantes.id_item')
				.where('carrito_items.id_pedido', fila.id_pedido)
				.first();
			ganancia_total_acumulada += pac.ganancia_acompañantes;

			fila.ganancia_acumulada = ganancia_total_acumulada;
		}

		const renderTemplate = require("../functions/templateRender");
		const formatearPrecio = require("../functions/formatearPrecio");

		let template, html_reporte;

		const datos = {
			...data[0],
			data,
			tipo_usuario: tipo_usu,
			total_delivery: data.reduce((n, fila) => (n*100 + (fila.costo_delivery*100||0))/100,  0),
			total_ganancia: data.reduce((n, fila) => (n*100 + (fila.ganancia_acumulada*100||0))/100, 0),
			logo: path.join('file://', __dirname, '../../static/img', 'logo.jpg'),
			precio: (n) => formatearPrecio(n, '$'),
			fecha: require("../functions/extraerFecha"),
			filtro_modalidad: filtros.modalidad,
		};

		try {
			html_reporte = renderTemplate.TemplateEngine(
				fs.readFileSync(__dirname+'/../pdf/reporte_pedidos.html').toString(),
				datos
			);
		}
		catch (e){
			console.error(e);
			return res.sendStatus(500);
		}

		const pdfConfig = {
			orientation: "landscape",
			format: 'Letter',
			border: {
				top: '1cm',
				right: '0',
				bottom: '1cm',
				left: '0',
			},
			childProcessOptions: {
				detached: true
			},
		};

		HTML_PDF.create(html_reporte, pdfConfig).toStream((err, stream) => {
			if (err){
				res.sendStatus(500);
				return console.log(err);
			}

			let stat = fs.statSync(stream.path);
			res.setHeader('Content-Length', stat.size);
			res.setHeader('Content-Type', 'application/pdf');
			res.setHeader('Content-Disposition', 'attachment; filename="reporte_pedidos.pdf"');
			stream.pipe(res);
		});
	}).catch(e => {console.log(e); res.sendStatus(500)});
});

router.post("/generar_reporte_pago_pendiente", upload.none(), sesi.validar_general, async(req,res) => {
	const filtros = req.body;

	const Query = CarritoItems.query().join('pedidos','pedidos.id','carrito_items.id_pedido').join('clientes','clientes.id','pedidos.id_cliente')
	.whereRaw('(pedidos.id_estatus=11 || pedidos.id_estatus=12)')
	.whereRaw('pedidos.id_pago IS NULL')
  	.whereRaw('carrito_items.ganancia IS NOT NULL')
	.select('carrito_items.*','pedidos.id_restaurante',raw('DATE_FORMAT(pedidos.creado, "%d-%m-%Y %H:%i:%s")').as('fecha_pedido'),raw('DATE_FORMAT(pedidos.creado, "%Y-%m-%d %H:%i:%s")').as('creado'),'clientes.nombre as nom_cli','clientes.apellido as ape_cli','clientes.foto as img_cli',
		raw('SUM((carrito_items.cantidad*carrito_items.precio_usd)*(carrito_items.ganancia/100))').as('tot'),
		raw('(SELECT SUM((a.cantidad*a.precio_usd)*(c.ganancia/100)) from pedidos_acompañantes a, carrito_items c, pedidos p WHERE a.id_item=c.id AND c.id_pedido=p.id AND p.id=pedidos.id AND p.id_pago IS NULL)').as('tot_acom'),
		raw('(SELECT SUM((ad.cantidad*ad.precio_usd)*(c.ganancia/100)) from pedidos_adicionales ad, carrito_items c, pedidos p WHERE ad.id_item=c.id AND c.id_pedido=p.id AND p.id=pedidos.id AND p.id_pago IS NULL)').as('tot_adi'),
		raw('(SELECT pre_tasa from restaurantes r WHERE r.id=pedidos.id_restaurante)').as('pre_tasa'),
		raw('(SELECT nombre FROM restaurantes re WHERE re.id=pedidos.id_restaurante)').as('nombre_doomi'),
		raw('(SELECT SUM(p.subtotal+COALESCE(p.pago_del,0)+p.subtotal_adicionales+p.subtotal_acompañantes) FROM pedidos p WHERE p.id=carrito_items.id_pedido)').as('tot_ped')
	).groupBy("carrito_items.id_pedido")
	.orderBy("pedidos.creado", "desc");

	if(filtros.doomi)
		Query.where('pedidos.id_restaurante',filtros.doomi)

	if(filtros.desde){
		let desde = moment(filtros.desde+' 00:00:00').format('YYYY-MM-DD HH:mm:ss');
		Query.having('creado', '>=', desde);
	}

	if(filtros.hasta){
		let hasta = moment(filtros.hasta+' 23:59:59').format('YYYY-MM-DD HH:mm:ss');
		Query.having('creado', '<=', hasta);
	}

	await Query.then(resp => {
		if(resp.length==0){
			return res.sendStatus(204); // No hay datos.
		}
		let template, html_reporte;
		const renderTemplate = require("../functions/templateRender");
		const formatearPrecio = require("../functions/formatearPrecio");
		//console.log(resp);
		const datos = {
			data: resp,
			logo: path.join('file://', __dirname, '../../static/img', 'logo.jpg'),
			precio: (n, s='$') => formatearPrecio(n,s),
			fecha: require("../functions/extraerFecha")
		};

		try {
			html_reporte = renderTemplate.TemplateEngine(
				fs.readFileSync(__dirname+'/../pdf/reporte_pagos_pendientes.html').toString(),
				datos
			);
		}
		catch (e){
			console.error(e);
			return res.sendStatus(500);
		}

		const pdfConfig = {
			orientation: "landscape",
			format: 'Letter',
			border: {
				top: '1cm',
				right: '0',
				bottom: '1cm',
				left: '0',
			},
			childProcessOptions: {
				detached: true
			},
		};

		HTML_PDF.create(html_reporte, pdfConfig).toStream((err, stream) => {
			if (err){
				res.sendStatus(500);
				return console.log(err);
			}

			let stat = fs.statSync(stream.path);
			res.setHeader('Content-Length', stat.size);
			res.setHeader('Content-Type', 'application/pdf');
			res.setHeader('Content-Disposition', 'attachment; filename="reporte_pedidos.pdf"');
			stream.pipe(res);
		});
	}).catch(e => {console.log(e); res.sendStatus(500)});
});

router.post("/generar_reporte_pago_recibido", upload.none(), sesi.validar_general, async(req,res) => {
	const filtros = req.body;
	const Query = PagoPedido.query().join('restaurantes','restaurantes.id','pagar_pedidos.id_restaurante').join('cuentas','cuentas.id','pagar_pedidos.id_cuenta')
	.select('pagar_pedidos.*','cuentas.nombre as nom_cue','cuentas.detalles as det_cue',raw('DATE_FORMAT(pagar_pedidos.fecha_pago, "%d-%m-%Y %H:%i:%s")').as('fecha_pago2'),raw('DATE_FORMAT(pagar_pedidos.fecha_pago, "%Y-%m-%d %H:%i:%s")').as('pago'),'restaurantes.nombre as nombre_doomi',
		raw('(SELECT SUM(p.subtotal+COALESCE(p.pago_del,0)+p.subtotal_adicionales+p.subtotal_acompañantes) FROM pedidos p WHERE p.id_pago=pagar_pedidos.id)').as('tot_ped'),
	).orderBy("fecha_pago", "desc");

	if(filtros.doomi)
		Query.where('pagar_pedidos.id_restaurante',filtros.doomi)

	if(filtros.desde){
		let desde = moment(filtros.desde+' 00:00:00').format('YYYY-MM-DD HH:mm:ss');
		Query.having('pago', '>=', desde);
	}

	if(filtros.hasta){
		let hasta = moment(filtros.hasta+' 23:59:59').format('YYYY-MM-DD HH:mm:ss');
		Query.having('pago', '<=', hasta);
	}

	if(filtros.confirmado){
		Query.where('confirmado', filtros.confirmado);
	}

	await Query.then(async resp => {
		if(resp.length==0){
			return res.sendStatus(204); // No hay datos.
		}
		let template, html_reporte;
		const renderTemplate = require("../functions/templateRender");
		const formatearPrecio = require("../functions/formatearPrecio");

		for (var i = 0; i < resp.length; i++) {
			var pedidos = '';
			const PEDIDOS = await Pedido.query().select('id',raw('SUM(subtotal+COALESCE(pago_del,0)+subtotal_adicionales+subtotal_acompañantes)').as('tot')).where('id_pago',resp[i].id).groupBy('id').catch(e => {console.log(e);});
			if(PEDIDOS.length>0){
				for (var p = 0; p < PEDIDOS.length; p++) {
					var total_p = "("+formatearPrecio(PEDIDOS[p].tot, '$')+")";
					if(pedidos=='')
						pedidos=PEDIDOS[p].id+total_p;
					else
						pedidos+=', '+PEDIDOS[p].id+total_p;
				}
			}
			resp[i].id_pedidos = pedidos;
		}
		//console.log(resp);
		const datos = {
			data: resp,
			logo: path.join('file://', __dirname, '../../static/img', 'logo.jpg'),
			precio: (n) => formatearPrecio(n, '$'),
			precio2: (n) => formatearPrecio(n, 'Bs'),
			fecha: require("../functions/extraerFecha")
		};

		try {
			html_reporte = renderTemplate.TemplateEngine(
				fs.readFileSync(__dirname+'/../pdf/reporte_pagos_recibidos.html').toString(),
				datos
			);
		}
		catch (e){
			console.error(e);
			return res.sendStatus(500);
		}

		const pdfConfig = {
			orientation: "landscape",
			format: 'Letter',
			border: {
				top: '1cm',
				right: '0',
				bottom: '1cm',
				left: '0',
			},
			childProcessOptions: {
				detached: true
			},
		};

		HTML_PDF.create(html_reporte, pdfConfig).toStream((err, stream) => {
			if (err){
				res.sendStatus(500);
				return console.log(err);
			}

			let stat = fs.statSync(stream.path);
			res.setHeader('Content-Length', stat.size);
			res.setHeader('Content-Type', 'application/pdf');
			res.setHeader('Content-Disposition', 'attachment; filename="reporte_pedidos.pdf"');
			stream.pipe(res);
		});
	}).catch(e => {console.log(e); res.sendStatus(500)});
});

router.post("/generar_reporte_pago_recibido_repartidor", upload.none(), sesi.validar_admin, async(req, res) => {
	const filtros = req.body;

	const Query = Pedido.query()
    .select(
    	'pedidos.*',
    	'restaurantes.nombre AS nombre_restaurante',
    	raw('DATE_FORMAT(creado, "%Y-%m-%d %H:%i:%s")').as('creado'),
    	raw('DATE_FORMAT(fecha_pago_del, "%Y-%m-%d %H:%i:%s")').as('fecha_pago_del')
    )
	.leftJoin('restaurantes', 'restaurantes.id', 'pedidos.id_restaurante')
	// .where('id_restaurante',id_doomi)
	.where('delivery_pagado', filtros.delivery_pagado || 3)
	// .where('delivery_pagado',3)
	.whereRaw('(id_estatus=11 || id_estatus=12)')
  	.whereRaw('id_repartidor IS NOT NULL')
	.orderBy("pedidos.creado", "desc");

	if(filtros.desde) {
  		let desde = new Date(filtros.desde).toISOString();
  		desde = moment(desde).format('YYYY-MM-DD')
  		Query.whereRaw(' (pedidos.creado>="'+desde+' 00:00:00" OR pedidos.fecha_pago_del>="'+desde+' 00:00:00" )')
	}

	if(filtros.hasta) {
		let hasta = new Date(filtros.hasta).toISOString();
		hasta = moment(hasta).format('YYYY-MM-DD')
		Query.whereRaw(' (pedidos.creado<="'+hasta+' 23:59:59" OR pedidos.fecha_pago_del<="'+hasta+' 23:59:59" )')
	}

	if (filtros.repartidor) {
		Query.where('pedidos.id_repartidor', filtros.repartidor);
	}

	await Query.then(resp => {

		if(resp.length==0){
			return res.sendStatus(204); // No hay datos.
		}
		let template, html_reporte;
		const renderTemplate = require("../functions/templateRender");
		const formatearPrecio = require("../functions/formatearPrecio");

		const datos = {
			data: resp,
			logo: path.join('file://', __dirname, '../../static/img', 'logo.jpg'),
			precio: (n) => formatearPrecio(n, '$'),
			precio2: (n) => formatearPrecio(n, 'Bs'),
			fecha: require("../functions/extraerFecha")
		};

		try {
			html_reporte = renderTemplate.TemplateEngine(
				fs.readFileSync(__dirname+'/../pdf/reporte_pagos_recibidos_repartidor.html').toString(),
				datos
			);
		}
		catch (e){
			console.error(e);
			return res.sendStatus(500);
		}

		const pdfConfig = {
			orientation: "landscape",
			format: 'Letter',
			border: {
				top: '1cm',
				right: '0',
				bottom: '1cm',
				left: '0',
			},
			childProcessOptions: {
				detached: true
			},
		};

		HTML_PDF.create(html_reporte, pdfConfig).toStream((err, stream) => {
			if (err){
				res.sendStatus(500);
				return console.log(err);
			}

			let stat = fs.statSync(stream.path);
			res.setHeader('Content-Length', stat.size);
			res.setHeader('Content-Type', 'application/pdf');
			res.setHeader('Content-Disposition', 'attachment; filename="reporte_pedidos.pdf"');
			stream.pipe(res);
		});


	})
	.catch(console.log);
});

router.post("/generar_reporte_pago_pendiente_repartidor", upload.none(), sesi.validar_admin, async(req, res) => {
	const id_rep = req.body.repartidor;
	const filtros = {...req.query, ...req.body};
	const Query = Pedido.query()
	.select(
		'pedidos.*',
		'restaurantes.nombre AS nombre_restaurante',
		'restaurantes.img AS img_restaurante',
		raw('DATE_FORMAT(creado, "%Y-%m-%d %H:%i:%s")').as('creado'),
		raw('DATE_FORMAT(fecha_pago_del, "%Y-%m-%d %H:%i:%s")').as('fecha_pago_del')
	)
	.leftJoin('restaurantes', 'restaurantes.id', 'pedidos.id_restaurante')
	.where('id_repartidor',id_rep)
	.whereRaw('(id_estatus=11 || id_estatus=12)')
	.where('pedidos.delivery_pagado', 0)
  	.orderBy("pedidos.creado", "desc");

  	if(filtros.desde) {
  		let desde = new Date(filtros.desde).toISOString();
  		desde = moment(desde).format('YYYY-MM-DD')
  		// Query.whereRaw(' (pedidos.creado>="'+desde+' 00:00:00" OR pedidos.fecha_pago_del>="'+desde+' 00:00:00" )')
		Query.where('pedidos.creado', '>=', desde+' 00:00:00');
	}

	if(filtros.hasta) {
		let hasta = new Date(filtros.hasta).toISOString();
		hasta = moment(hasta).format('YYYY-MM-DD')
		// Query.whereRaw(' (pedidos.creado<="'+hasta+' 23:59:59" OR pedidos.fecha_pago_del<="'+hasta+' 23:59:59" )')
		Query.where('pedidos.creado', '<=', hasta+' 23:59:59');
	}

	await Query.then(resp => {
		if(resp.length==0){
			return res.sendStatus(204); // No hay datos.
		}
		let template, html_reporte;
		const renderTemplate = require("../functions/templateRender");
		const formatearPrecio = require("../functions/formatearPrecio");

		const datos = {
			data: resp,
			logo: path.join('file://', __dirname, '../../static/img', 'logo.jpg'),
			precio: (n) => formatearPrecio(n, '$'),
			precio2: (n) => formatearPrecio(n, 'Bs'),
			fecha: require("../functions/extraerFecha")
		};

		try {
			html_reporte = renderTemplate.TemplateEngine(
				fs.readFileSync(__dirname+'/../pdf/reporte_pagos_pendientes_repartidor.html').toString(),
				datos
			);
		}
		catch (e){
			console.error(e);
			return res.sendStatus(500);
		}

		const pdfConfig = {
			orientation: "landscape",
			format: 'Letter',
			border: {
				top: '1cm',
				right: '0',
				bottom: '1cm',
				left: '0',
			},
			childProcessOptions: {
				detached: true
			},
		};

		HTML_PDF.create(html_reporte, pdfConfig).toStream((err, stream) => {
			if (err){
				res.sendStatus(500);
				return console.log(err);
			}

			let stat = fs.statSync(stream.path);
			res.setHeader('Content-Length', stat.size);
			res.setHeader('Content-Type', 'application/pdf');
			res.setHeader('Content-Disposition', 'attachment; filename="reporte_pedidos.pdf"');
			stream.pipe(res);
		});
	}).catch(console.log);
});



async function calcularSubtotal(data) {

	const id_cliente = data.id_cliente;
	const id_pedido = data.id_pedido;

	if(!id_cliente && !id_pedido)
		return;

	const query = CarritoItems.query()
		.join('menus' ,'menus.id', 'carrito_items.id_menu')
		.join('pedidos', 'pedidos.id', 'carrito_items.id_pedido')
		.select( raw('sum(?? * ??)', 'menus.precio_usd', 'carrito_items.cantidad').as('subtotal') )
		.where( raw('?? = ??', 'menus.id', 'carrito_items.id_menu') );

	if(id_cliente)
		query.where('pedidos.id_cliente', id_cliente)
	else
		query.where('pedidos.id', id_pedido)

	const res = await query;
	return res.pop().subtotal || 0;
}

async function calcularSubtotalAdicionales(data) {
	// SUMA(precio_adicional * cantidad_adicional) //
	const id_pedido = data.id_pedido || data;

	if(!id_pedido) return;

	const Query = PedidoAdicionales.query()
		.join('carrito_items', 'carrito_items.id', 'pedidos_adicionales.id_item')
		.join('pedidos', 'pedidos.id', 'carrito_items.id_pedido')
		.where('pedidos.id', id_pedido)
		.select( raw('IFNULL(SUM(?? * ??), 0)', 'pedidos_adicionales.precio_usd', 'pedidos_adicionales.cantidad').as('subtotal') )

	const res = await Query;

	return res.pop().subtotal || 0;
}


async function calcularSubtotalAcompañantes(data) {
	// SUMA(precio_acompañante * cantidad_acompañante) //
	const id_pedido = data.id_pedido || data;

	if(!id_pedido) return;

	const Query = PedidoAcompañantes.query()
		.join('carrito_items', 'carrito_items.id', 'pedidos_acompañantes.id_item')
		.join('pedidos', 'pedidos.id', 'carrito_items.id_pedido')
		.select(
			raw('IFNULL(SUM(?? * ??), 0)', 'pedidos_acompañantes.cantidad', 'pedidos_acompañantes.precio_usd').as('subtotal')
		)

	const res = await Query;

	return res.pop().subtotal || 0;
}


async function pedidosDetallados(data){
	const id_cliente = data.id_cliente || data;

	if(!id_cliente) return;

	const Query = Pedido.query()
		.where({id_cliente})
		.whereRaw('DATE(pedidos.creado) >= ?', moment().subtract(1,'week').format('YYYY-MM-DD'))
		.join('restaurantes', 'restaurantes.id', 'pedidos.id_restaurante')
		.select(
			'pedidos.*',
			'restaurantes.nombre AS nombre_doomi',
			raw('subtotal + IFNULL(subtotal_adicionales,0) + IFNULL(subtotal_acompañantes,0) + IFNULL(pago_del,0)').as('total'),
			raw('DATE_FORMAT(pedidos.creado, "%Y-%m-%d %H:%i:%s")').as('creado')
		)

	// Recuperar los items y calcular sus totales.
	// const Query = CarritoItems.query()
	// 	.join("carrito", "carrito.id", "carrito_items.id_carrito")
	// 	.join('menus' ,'menus.id', 'carrito_items.id_menu')
	// 	.leftJoin('pedidos_adicionales', 'pedidos_adicionales.id_item', 'carrito_items.id')
	// 	.leftJoin('menus_adicionales', 'menus_adicionales.id', 'pedidos_adicionales.id_adicional')
	// 	.select(
	// 		// Info del item del carrito.
	// 		'carrito_items.*',

	// 		// El precio unitario.
	// 		raw('menus.precio_usd').as('precio_usd'),

	// 		// precio * cantidad = subtotal
	// 		raw('?? * ??', 'menus.precio_usd', 'carrito_items.cantidad').as('subtotal'),

	// 		// SUM(precio_adicional * cantidad) = subtotal_adicionales
	// 		raw('IFNULL(?? * ??, 0)', 'menus_adicionales.precio_usd', 'carrito_items.cantidad').as('subtotal_adicionales'),

	// 		// subtotal + subtotal_adicionales = total
	// 		raw('(?? * ??) + IFNULL((?? * ??), 0)', 'menus.precio_usd', 'carrito_items.cantidad', 'menus_adicionales.precio_usd', 'carrito_items.cantidad').as('total'),
	// 	);

	// if(id_carrito)
	// 	Query.where("carrito.id", id_carrito);
	// else
	// 	Query.where("carrito.id_cliente", id_cliente);

	return await Query;
};

function sanitizeString(str){
	/* Limpiar input */
	str = str.replace(/[^a-z0-9áéíóúñü \.,_-]/gim, "");
	return str.trim();
}

module.exports = router;
