//llamar al modelo
const { raw, ref } = require("objection");
const Pedido = require("../models/Pedido");
const Cliente = require("../models/Cliente");
const PedidoOrden = require("../models/PedidoOrden");
const PedidoAdicionales = require("../models/PedidoAdicionales");
const PedidoAcompañantes = require("../models/PedidoAcompañantes");
const PedidoEstatus = require("../models/PedidoEstatus");
const Menu = require("../models/Menu");
const MenuAdicionales = require("../models/MenuAdicionales");
const MenuAcompañante = require("../models/MenuAcompañante");
const Carrito = require("../models/Carrito");
const CarritoItems = require("../models/CarritoItems");
const Notificaciones = require("../models/Notificacion");
const Admin = require("../models/Admin");
var Firebase = require("../routes/firebase");
Firebase = new Firebase();
//Llamar otras librerias
const AR = require("../ApiResponser");
const express = require("express");
const path = require("path");
const archivos = require("../Archivos");
const sesi = new (require("./sesion"))();
const upload = require("multer")();
const config = require("../config");
const router = require("express").Router();
const fs = require("fs-extra");
const moment = require("moment");
moment.locale("es");

// Agregar item al carrito.
router.post("/item", upload.none(), sesi.validar_cliente, async(req,res) => {
	let data = req.body;
	const id_cliente = req.headers['id_usuario']
	const id_menu = data.id_producto || data.id_menu;
	const menu = await Menu.query().findById(id_menu).catch(console.error);
	const id_adicionales = (data.adicionales || []).map(a => parseInt(a[0], 10));
	const id_acompañantes = (data.acompañantes || []).map(a => parseInt(a[0], 10));
	let id_restaurante = menu.id_restaurante;
	let carrito = await Carrito.query().findOne('id_cliente', id_cliente).catch(console.error);


	// Verificar.
	let errr = 0;
	let msj = '';


	if(!id_menu){
		errr = 1;
		msj = "No se especifico la ID del producto";
	}
	else if(!data.cantidad)
		errr = 1;
		msj = "No se especifico la cantidad del producto";

	if(!errr) {
		if(!carrito)
			carrito = await Carrito.query().insert({id_cliente, id_restaurante});
		else if(!carrito.id_restaurante)
			await Carrito.query().patch({id_restaurante}).where('id_cliente', id_cliente);
		else if(carrito.id_restaurante != id_restaurante){
			msj = "Hay productos de otro doomialiado en el carrito";
			errr = -1;
		}

		if(!menu){
			msj = "El producto no existe";
			errr = -2;
		}
		else if(menu.existencias < 1){
			msj = "El producto está agotado";
			errr = -2;
		}
	}


	if(errr)
		return AR.enviarDatos({errr, msj, id_doomialiado_actual:carrito.id_restaurante}, res);

	// Fin verificacion.


	await CarritoItems.query()
	.insert({
		id_carrito: carrito.id,
		id_menu,
		cantidad: data.cantidad,
		notas: data.nota || data.notas,
		nombre: menu.nombre,
		precio_usd: menu.precio_usd,
		ganancia: menu.ganancia
	})
	.then(async resp => {

		// Recuperar menu para calcular totales.
		const menu = await Menu.query().findById(resp.id_menu);

		resp.subtotalAdicionales = 0;
		resp.subtotalAcompañantes = 0;
		resp.precio_usd = menu.precio_usd;

		// Guardar los adicionales.
		if(data.adicionales && data.adicionales.length > 0){
			let adicionales = [];
			for (let adi of data.adicionales){
				let menu_adi = await MenuAdicionales.query().findById(adi[0]).catch(console.log);
				adicionales.push({
					id_adicional: adi[0],
					cantidad: adi[1],
					id_item: resp.id,
					nombre: menu_adi.nombre,
					precio_usd: menu_adi.precio_usd
				});
			}
			await PedidoAdicionales.query()
				.insertGraph(
					adicionales
					// data.adicionales.map( adi => ({id_adicional: adi[0], cantidad: adi[1], id_item: resp.id}) )
				);
			/*
			// Calcular el subtotal de los adicionales.
			if(menu.adicionales && menu.adicionales.length > 0){

				// Filtrar solo los seleccionados.
				menu.adicionales = menu.adicionales.filter(a => id_adicionales.includes(parseInt(a.id, 10)))

				// Calcular subtotal.
				resp.subtotalAdicionales = menu.adicionales
					.reduce((n,adi) => (((n*100) + (adi.precio_usd * 100 * resp.cantidad)) / 100), 0);
			}*/
		}

		// Guardar los acompañantes.
		if(data.acompañantes && data.acompañantes.length > 0){
			let acompañantes = [];
			for (let aco of data.acompañantes) {
				let menu_aco = await MenuAcompañante.query()
					.select('menus.id AS id_menu_acompañante', 'menus.precio_usd', 'menus.nombre')
					.join('menus', 'menus.id', 'id_menu_acompañante')
					.findOne('menus_acompañantes.id', aco[0]).catch(console.log);

				acompañantes.push({
					id_acompañante: aco[0],
					cantidad: aco[1],
					id_item: resp.id,
					nombre: menu_aco.nombre,
					precio_usd: menu_aco.precio_usd
				});
			}
			await PedidoAcompañantes.query()
				.insertGraph(
					acompañantes
					// data.acompañantes.map( adi => ({id_acompañante: adi[0], cantidad: adi[1], id_item: resp.id}) )
				);
			/*
			// Calcular el subtotal de los acompañantes.
			if(menu.acompañantes && menu.acompañantes.length > 0){

				// Filtrar solo los seleccionados.
				menu.acompañantes = menu.acompañantes.filter(a => id_acompañantes.includes(parseInt(a.id, 10)))

				// Calcular subtotal.
				resp.subtotalAcompañantes = menu.acompañantes
					.reduce((n,aco) => (((n*100) + (aco.precio_usd * 100 * resp.cantidad)) / 100), 0);
			}*/
		}

		const items = await recuperarItemsDetallados({id_carrito: resp.id_carrito});

		/* Totales del carrito */
		const subtotal = await calcularSubtotal({id_carrito: resp.id_carrito});
		const subtotalAdicionales = await calcularSubtotalAdicionales({id_carrito: resp.id_carrito});
		const subtotalAcompañantes = await calcularSubtotalAcompañantes({id_carrito: resp.id_carrito});
		const total = ((subtotal*100) + (subtotalAdicionales*100) + (subtotalAcompañantes*100)) / 100;

		/* Totales de la orden */
		resp.subtotal = menu.precio_usd * 100 * resp.cantidad / 100;
		resp.total = ((resp.subtotal * 100) + (resp.subtotalAdicionales * 100) + (resp.subtotalAcompañantes * 100)) / 100;
		resp.adicionales = data.adicionales || [];

		/* Incluir [id, cantidad] de adicionales en cada item */
		for (let item of items){
			let adis = await PedidoAdicionales.query().where('id_item', item.id);
			item.adicionales = adis.map(a => [a.id_adicional, a.cantidad]);
		}

		let carrito = {
			items,
			subtotal,
			subtotalAdicionales,
			subtotalAcompañantes,
			total,
			// ultimo_item: resp,
			id_doomialiado_actual: id_restaurante,
			msj: 'Agregado al carrito',
			r: 1,
		};

		AR.enviarDatos(carrito, res);
	})
	.catch(err => {console.log(err);res.sendStatus(500)});
});

// Cambiar cantidad del item.
router.patch("/item/cantidad", upload.none(), sesi.validar_cliente, async(req, res) => {
	const id_cliente = req.headers['id_usuario'];
	const carrito = await Carrito.query().findOne({id_cliente});

	const id_item = req.body.id || req.body.idProducto || req.body.idItem;
	const nuevaCantidad = req.body.cantidad;

	await CarritoItems.query().where('id_carrito', carrito.id)
		.findById(id_item).patch({cantidad: nuevaCantidad})
		.then(r => AR.enviarDatos({r:true, msj: 'Datos actualizados'}, res))
		.catch(err => console.log(err));
});

// Obtener contenido del carrito.
router.get("/", sesi.validar_cliente, async(req,res) => {
	const id_cliente = req.headers['id_usuario'];
	const items = await recuperarItemsDetallados({id_cliente});

	// Totales del carrito.
	const subtotal = await calcularSubtotal({id_cliente});
	const subtotalAdicionales = await calcularSubtotalAdicionales({id_cliente});
	const subtotalAcompañantes = await calcularSubtotalAcompañantes({id_cliente});
	const total = ((subtotal*100) + (subtotalAdicionales*100) + (subtotalAcompañantes*100)) / 100;

	const rest = await Carrito.query()
		.select('restaurantes.nombre AS nombre_restaurante', 'carrito.id_restaurante')
		.leftJoin('restaurantes', 'restaurantes.id', 'carrito.id_restaurante')
		.where('carrito.id_cliente', id_cliente)
		.first()
		.catch(console.error);

	// Incluir [id, cantidad] de adicionales en cada item.
	for (let item of items){
		let adis = await PedidoAdicionales.query().where('id_item', item.id);
		// let acom = await PedidoAcompañantes.query()
		// 	.join('menus_acompañantes', 'menus_acompañantes.id', 'pedidos_acompañantes.id_acompañante')
		// 	.select('pedidos_acompañantes.*', 'menus_acompañantes.id_menu_acompañante')
		// 	.where('id_item', item.id);
		let acom = await PedidoAcompañantes.query()
			.where('id_item', item.id)
		item.id_adicionales = adis.map(a => [a.id_adicional, a.cantidad]);
		item.adicionales = adis;
		item.id_acompañantes = acom.map(a => [a.id_acompañante, a.cantidad, a.id_menu_acompañante]);
		item.acompañantes = acom;
	}

	return AR.enviarDatos({
		items,
		total,
		subtotal,
		subtotalAdicionales,
		subtotalAcompañantes,
		id_restaurante: rest ? rest.id_restaurante : null,
		nombre_restaurante: rest ? rest.nombre_restaurante : null,
	}, res);
});

// Procesar carrito.
router.get("/validad_productos_carrito", sesi.validar_cliente, async(req,res) => {
	const id_cliente = req.headers['id_usuario'];
	const carrito = await Carrito.query().findOne({id_cliente});
	let errr = 0;
	let msj = '';
	//let pedido = await Pedido.query().findOne('id_restaurante', carrito.id_restaurante);
	let allitems = await CarritoItems.query().select('*').sum('cantidad as total_can').where('id_carrito',carrito.id).groupBy('id_menu');
	if(allitems[0]){
		for (var i = 0; i < allitems.length; i++) {
			// console.log(allitems[i]);
			let menu_sel = await Menu.query().findById(allitems[i].id_menu);
			let acumacom = await PedidoAcompañantes.query()
				.join('carrito_items', 'carrito_items.id', 'pedidos_acompañantes.id_item')
				.join('menus_acompañantes', 'menus_acompañantes.id', 'pedidos_acompañantes.id_acompañante')
				.where('id_carrito', allitems[i].id_carrito)
				.where('id_menu_acompañante', allitems[i].id_menu)
				.sum('pedidos_acompañantes.cantidad as total_acum');
			var tot_acom = 0;
			if(acumacom[0].total_acum)
				tot_acom = acumacom[0].total_acum;

			let can_tot =allitems[i].total_can  + tot_acom;
			if(parseFloat(menu_sel.existencias)<parseFloat(can_tot)){
				errr = 1;
				if(msj=='')
					msj = '<b>'+menu_sel.nombre+'</b> tiene solamente disponible ('+menu_sel.existencias+')';
				else
					msj += '<br><br><b>'+menu_sel.nombre+'</b> tiene solamente disponible ('+menu_sel.existencias+')';
			}
		}
		let allitems2 = await CarritoItems.query().where('id_carrito',carrito.id);
		for (var i = 0; i < allitems2.length; i++) {
			let acom = await PedidoAcompañantes.query()
				.join('carrito_items', 'carrito_items.id', 'pedidos_acompañantes.id_item')
				.join('menus_acompañantes', 'menus_acompañantes.id', 'pedidos_acompañantes.id_acompañante')
				.join('menus', 'menus.id', 'menus_acompañantes.id_menu_acompañante')
				.select('menus.nombre','menus.precio_usd','menus_acompañantes.*', 'pedidos_acompañantes.*')
				.where('id_item', allitems2[i].id);
			if(acom[0]){
				for (var aco = 0; aco < acom.length; aco++) {
					let menu_sel = await Menu.query().findById(acom[aco]['id_menu_acompañante']);
					if(parseFloat(menu_sel.existencias)<parseFloat(acom[aco].cantidad)){
						errr = 1;
						if(msj=='')
							msj = '<b>'+menu_sel.nombre+'</b> tiene solamente disponible ('+menu_sel.existencias+')';
						else{
							if(msj.toLowerCase().indexOf(menu_sel.nombre) != -1)
								msj += '<br><br><b>'+menu_sel.nombre+'</b> tiene solamente disponible ('+menu_sel.existencias+')';
						}
					}
				}

			}
		};
	}
	if(errr){
		return AR.enviarDatos({errr, msj}, res);
	}else{
		return AR.enviarDatos({r:true}, res);
	}
});
router.post("/checkout", archivos.fields([{ name: "img_pago", maxCount: 1 }]), sesi.validar_cliente, async(req,res) => {
	const id_cliente = req.headers['id_usuario'];
	const data = req.body;
	// Verificar.
	let errr = 0;
	let msj = '';
	//console.log(data);

	const carrito = await Carrito.query().findOne({id_cliente});

	//let pedido = await Pedido.query().findOne('id_restaurante', carrito.id_restaurante);
	let allitems = await CarritoItems.query().select('*').sum('cantidad as total_can').where('id_carrito',carrito.id).groupBy('id_menu');
	if(allitems[0]){
		for (var i = 0; i < allitems.length; i++) {
			// console.log(allitems[i]);
			let menu_sel = await Menu.query().findById(allitems[i].id_menu);
			let acumacom = await PedidoAcompañantes.query()
				.join('carrito_items', 'carrito_items.id', 'pedidos_acompañantes.id_item')
				.join('menus_acompañantes', 'menus_acompañantes.id', 'pedidos_acompañantes.id_acompañante')
				.where('id_carrito', allitems[i].id_carrito)
				.where('id_menu_acompañante', allitems[i].id_menu)
				.sum('pedidos_acompañantes.cantidad as total_acum');
			var tot_acom = 0;
			if(acumacom[0].total_acum)
				tot_acom = acumacom[0].total_acum;

			let can_tot =allitems[i].total_can  + tot_acom;
			if(parseFloat(menu_sel.existencias)<parseFloat(can_tot)){
				errr = 1;
				if(msj=='')
					msj = '<b>'+menu_sel.nombre+'</b> tiene solamente disponible ('+menu_sel.existencias+')';
				else
					msj += '<br><br><b>'+menu_sel.nombre+'</b> tiene solamente disponible ('+menu_sel.existencias+')';
			}
			/*let adic = await PedidoAdicionales.query()
				.join('carrito_items', 'carrito_items.id', 'pedidos_adicionales.id_item')
				.join('menus_adicionales', 'menus_adicionales.id', 'pedidos_adicionales.id_adicional')
				.select('menus_adicionales.*', 'pedidos_adicionales.*')
				.where('id_item', allitems[i].id);
			if(adic[0]){
				for (var ac = 0; ac < adic.length; ac++) {
					let menu_sel = await MenuAdicionales.query().findById(adic[ac].id_adicional);
					if(parseFloat(menu_sel.existencias)<parseFloat(adic[ac].cantidad)){
						errr = 1;
						if(msj=='')
							msj = menu_sel.nombre+' tiene solamente DISPONIBLE('+menu_sel.existencias+')';
						else
							msj += '<br>'+menu_sel.nombre+' tiene solamente DISPONIBLE('+menu_sel.existencias+')';
					}
				}
			}*/
		}
		let allitems2 = await CarritoItems.query().where('id_carrito',carrito.id);
		for (var i = 0; i < allitems2.length; i++) {
			let acom = await PedidoAcompañantes.query()
				.join('carrito_items', 'carrito_items.id', 'pedidos_acompañantes.id_item')
				.join('menus_acompañantes', 'menus_acompañantes.id', 'pedidos_acompañantes.id_acompañante')
				.join('menus', 'menus.id', 'menus_acompañantes.id_menu_acompañante')
				.select('menus.nombre','menus.precio_usd','menus_acompañantes.*', 'pedidos_acompañantes.*')
				.where('id_item', allitems2[i].id);
			if(acom[0]){
				for (var aco = 0; aco < acom.length; aco++) {
					let menu_sel = await Menu.query().findById(acom[aco]['id_menu_acompañante']);
					if(parseFloat(menu_sel.existencias)<parseFloat(acom[aco].cantidad)){
						errr = 1;
						if(msj=='')
							msj = '<b>'+menu_sel.nombre+'</b> tiene solamente disponible ('+menu_sel.existencias+')';
						else{
							if(msj.toLowerCase().indexOf(menu_sel.nombre) != -1)
								msj += '<br><br><b>'+menu_sel.nombre+'</b> tiene solamente disponible ('+menu_sel.existencias+')';
						}
					}
				}

			}
		};
	}
	if(errr){
		if(req.files["img_pago"])
			await fs.unlink(__dirname + "/../public/uploads/" + req.files["img_pago"][0].filename, function (err) { });
		msj = '<p style="font-size:18px;"><b>Corrige el pedido para continuar</b></p><p style="font-size:16px;">'+msj+'</p>'
		return AR.enviarDatos({errr, msj}, res);
	}

	let subtotal = await calcularSubtotal({id_carrito: carrito.id});
	let subtotalAdicionales = await calcularSubtotalAdicionales({id_carrito: carrito.id});
	let subtotalAcompañantes = await calcularSubtotalAcompañantes({id_carrito: carrito.id});

	var id_restau = carrito.id_restaurante;
	const data_ped = {
		id_restaurante: carrito.id_restaurante,
		id_cliente,
		id_estatus: 3,
		referencia: data.refe,
		id_cuenta: data.cuenta,
		tasa: data.tasa_dia,
		nr_pago: 1,
		subtotal: subtotal,
		subtotal_adicionales: subtotalAdicionales,
		subtotal_acompañantes: subtotalAcompañantes
	};
	if(req.files["img_pago"])
		data_ped.img_pago = config.rutaArchivo(req.files["img_pago"][0].filename);

	pedido = await Pedido.query().insert(data_ped);

	await CarritoItems.query()
	// .leftJoin('pedidos_adicionales', 'pedidos_adicionales.id_item', 'carrito_items.id')
	.where('carrito_items.id_carrito', carrito.id)
	.patch({
		'carrito_items.id_pedido': pedido.id,
		'carrito_items.id_carrito': null,
	})
	.then(async() => {

		await Carrito.query().findById(carrito.id).patch({id_restaurante: null})
			.catch(err => console.log(err));


		var dataped = {modalidad:data.modalidad};
		if(data.modalidad=='Programado'){
			var fec_new = data.fecha_pro.split("/");
			var fec_1 = fec_new[2]+'-'+fec_new[1]+'-'+fec_new[0];
			var fec_2 = moment(fec_1+" "+data.hora_pro).format('HH:mm:ss');
			dataped.fecha_pro = fec_1+" "+fec_2;
		}
		if(data.modalidad=='Pickup'){
			// var fec_1 = moment().format('YYYY-MM-DD');
			// var fec_2 = moment(fec_1+" "+data.hora_pic).format('HH:mm:ss');
			// dataped.fecha_pro = fec_1+" "+fec_2;
			dataped.fecha_pro = null;
		}
		if(data.modalidad=='Delivery' || data.modalidad=='Programado'){
			dataped.direccion = data.dir;
			dataped.punto_referencia = data.ref;
			dataped.lat_dir_p = data.lat;
			dataped.lon_dir_p = data.lon;
			dataped.pago_del = data.precio_del;
		}
		await Pedido.query().findById(pedido.id).patch(dataped)
			.catch(err => console.log(err));

		let allitems = await CarritoItems.query().where('id_pedido',pedido.id);
		if(allitems[0]){
			for (var i = 0; i < allitems.length; i++) {
				let menu_sel = await Menu.query().findById(allitems[i].id_menu);
				var resta = parseFloat(menu_sel.existencias)  - parseFloat(allitems[i].cantidad);
				if(resta==0){
					await Notificaciones.query()
					  .insert({
						  contenido: menu_sel.nombre+' ha llegado a 0 de existencia',
						  fecha: new Date(),
						  estatus: 0,
						  tipo_noti: 2,
						  redireccionar_noti: '?op=menu&id=',
						  id_usable: menu_sel.id,
						  id_receptor: id_restau,
						  id_emisor: id_restau,
						  tabla_usuario_r: 'restaurantes',
						  tabla_usuario_e: 'restaurantes'
					}).catch(async(err)=>{console.log(err);});
				}
				await Menu.query().findById(menu_sel.id).patch({existencias: resta}).catch(err => console.log(err));

				/*let adic = await PedidoAdicionales.query()
				.join('carrito_items', 'carrito_items.id', 'pedidos_adicionales.id_item')
				.join('menus_adicionales', 'menus_adicionales.id', 'pedidos_adicionales.id_adicional')
				.select('menus_adicionales.*', 'pedidos_adicionales.*')
				.where('id_item', allitems[i].id);
				if(adic[0]){
					for (var ac = 0; ac < adic.length; ac++) {
						let menu_sel = await MenuAdicionales.query().findById(adic[ac].id_adicional);
						var resta = parseFloat(menu_sel.existencias)  - parseFloat(adic[ac].cantidad);
						await MenuAdicionales.query().findById(adic[ac].id_adicional).patch({existencias: resta}).catch(err => console.log(err));
					}

				}*/

				let acom = await PedidoAcompañantes.query()
					.join('carrito_items', 'carrito_items.id', 'pedidos_acompañantes.id_item')
					.join('menus_acompañantes', 'menus_acompañantes.id', 'pedidos_acompañantes.id_acompañante')
					.join('menus', 'menus.id', 'menus_acompañantes.id_menu_acompañante')
					.select('menus.nombre','menus.precio_usd','menus_acompañantes.*', 'pedidos_acompañantes.*')
					.where('id_item', allitems[i].id);
				if(acom[0]){
					for (var aco = 0; aco < acom.length; aco++) {
						let menu_sel = await Menu.query().findById(acom[aco]['id_menu_acompañante']);
						var resta = parseFloat(menu_sel.existencias)  - parseFloat(acom[aco].cantidad);
						await Menu.query().findById(menu_sel.id).patch({existencias: resta}).catch(err => console.log(err));
					}
				}

			};
		}

		// Desvincular id de los adicionales y acompañantes.
		await PedidoAdicionales.query()
			.whereIn('id_item', allitems.map(e => e.id))
			.patch({id_adicional: null}).catch(console.log);

		await PedidoAcompañantes.query()
			.whereIn('id_item', allitems.map(e => e.id))
			.patch({id_acompañante: null}).catch(console.log);

		// await PedidoAdicionales.query()
		// 	.join('pedidos_acompañantes', 'pedidos_acompañantes.id_item', 'pedidos_adicionales.id_item')
		// 	.whereIn('id_item', allitems.map(e => e.id))
		// 	.patch({
		// 		'pedidos_adicionales.id_adicionales': null,
		// 		'pedidos_acompañantes.id_acompañante': null
		// 	})
		// 	.catch(console.log);

		// await CarritoItems.query().where('id_pedido', pedido.id).patch({id_menu: null}).catch(console.log);
		// await PedidoAdicionales.query().where('id_item', pedido.id).patch({id_adicional: null}).catch(console.log);
		// await PedidoAcompañantes.query().where('id_item', pedido.id).patch({id_acompañante: null}).catch(console.log);

		//enviar notificacion
		await Notificaciones.query()
			.insert({
				contenido: 'Ha realizado el pedido #'+pedido.id,
				fecha: new Date(),
				estatus: 0,
				tipo_noti: 2,
				redireccionar_noti: '?op=pedidos_panel&id=',
				id_usable: pedido.id,
				id_receptor: pedido.id_restaurante,
				id_emisor: pedido.id_cliente,
				tabla_usuario_r: 'restaurantes',
				tabla_usuario_e: 'clientes'
			}).then(async noti => {
				IO.sockets.emit("RecibirNotificacion", noti);
			}).catch(async(err)=>{console.log(err);});

		await Notificaciones.query()
			.insert({
				contenido: 'Haz realizado el pedido #'+pedido.id,
				fecha: new Date(),
				estatus: 0,
				tipo_noti: 2,
				redireccionar_noti: 'Detallescliente',
				id_usable: pedido.id,
				id_receptor: pedido.id_cliente,
				id_emisor: pedido.id_restaurante,
				tabla_usuario_r: 'clientes',
				tabla_usuario_e: 'restaurantes'
			}).catch(async(err)=>{console.log(err);});

		let resp = await Pedido.query().findById(pedido.id);
		IO.sockets.emit("Recibirpedido", resp);
		AR.enviarDatos(resp, res);
		let cli = await Cliente.query().findById(pedido.id_cliente);
		if (cli) {
			// console.log(pedido.id_restaurante, cli.correo);
			await Firebase.enviarPushNotification({
				id_usuario: "" + pedido.id_restaurante + "",
				title: 'Nuevo pedido #'+pedido.id,
				body: `${cli.nombre ? cli.nombre : 'Nombre no configurado'} (${cli.correo}), ha realizado un pedido.`,
				tablausuario: 'restaurantes'
			}, function(datan) { console.log(datan); });
		}
		await Admin.query()
		.then(async admins => {
			for (let index = 0; index < admins.length; index++) {
				let element = admins[index];
				await Firebase.enviarPushNotification({
					id_usuario: "" + element.id + "",
					title: 'Nuevo pedido #'+pedido.id,
					body: `${cli.nombre ? cli.nombre : 'Nombre no configurado'} (${cli.correo}), ha realizado un pedido.`,
					tablausuario: 'administradores'
				}, function(datan) { console.log(datan); });
			}
		});
	})
	.catch(async err => {
		if(req.files["img_pago"])
			await fs.unlink(__dirname + "/../public/uploads/" + req.files["img_pago"][0].filename, function (err) { });
		console.log(err); res.sendStatus(500)});
});

// Quitar item del carrito.
router.delete("/item", sesi.validar_cliente, async(req,res) => {
	const id_cliente = req.headers['id_usuario'];
	const data = req.body;
	const id_item = data.id || data.id_item || data.idItem
	const carrito = await Carrito.query().findOne({id_cliente});

	const num_items = await CarritoItems.query()
		.where({id_carrito: carrito.id})
		.count('id AS conteo')
		.first();

	if(!id_item)
		return res.status(400).send("No se especifico la ID del producto");

	await PedidoAdicionales.query().delete().where({id_item}).catch(console.error)
	await PedidoAcompañantes.query().delete().where({id_item}).catch(console.error)


	await CarritoItems.query().delete()
		.where({id_carrito:carrito.id, id: id_item})
		.then(async resp => {
			// Si quedaba un solo item ahora el carrito esta vacio. Desligar del doomialiado.
			if(num_items.conteo == 1){
				await Carrito.query().findById(carrito.id).patch({id_restaurante: null});
			}

			// const items = await CarritoItems.query().where({id_carrito: carrito.id});
			AR.enviarDatos({r:1, msj:'Eliminado'}, res);
		})
		.catch(err => {console.log(err);res.sendStatus(500)});
});

// Vaciar carrito.
router.delete("/", upload.none(), sesi.validar_cliente, async(req,res) => {
	const id_cliente = req.headers.id_usuario;
	const carrito = await Carrito.query().findOne({id_cliente});

	await CarritoItems.query().where({id_carrito: carrito.id}).delete()
		.then(async resp => {
			await Carrito.query().findById(carrito.id).patch({id_restaurante: null})
				.catch(err => console.log(err));

			AR.enviarDatos({msj:'El carrito ha sido vaciado'}, res);
		})
		.catch(err => console.log(err));
});

// Sincronizar un carrito creado sin login.
router.post("/", upload.none(), sesi.validar_cliente, async(req,res) => {
	const payload = req.body;
	const data = payload.items;
	const id_restaurante = payload.id_restaurante || payload.id_doomialiado;
	const id_restaurante_nuevo = req.body.id_restaurante || req.body.id_doomialiado;
	const id_cliente = req.headers['id_usuario'];
	let carrito = await Carrito.query().findOne({id_cliente});

	// Verificar datos.
	if(!data || !Array.isArray(data))
		return AR.enviarDatos({r:false, msj:'error en los datos'});

	// Verificar que no haya items en el carrito.
	if(carrito.id_restaurante){
		return res.status(409)// 409: Conflicto.
		.send({
			r:false,
			id_restaurante_actual: carrito.id_restaurante,
		});
	}

	if(!carrito)
		carrito = await Carrito.query().insert({id_cliente});

	// Limpiar datos.
	data.forEach((el, i, items) => {
		if(!el.id_menu || !el.cantidad){
			delete items[i];
			return;
		}

		items[i].id_cliente = id_cliente;
		items[i].adicionales = items[i].adicionales || [];
		items[i].acompañantes = items[i].acompañantes || [];
	});


	await Carrito.query()
		.where({id_cliente})
		.patch({id_restaurante: id_restaurante || id_restaurante_nuevo})
		.catch(console.error);

	for (let i = data.length-1; i >= 0; i--) {
		let item = data[i];
		let menu = await Menu.query()
			.findById(item.id_menu)
			.catch(console.error);

		if(menu.existencias < item.cantidad){
			delete data[i];
			continue;
		}

		await CarritoItems.query()
			.insert({
				id_carrito: carrito.id,
				id_menu: item.id_menu,
				cantidad: item.cantidad,
				notas: item.notas,
				nombre: menu.nombre,
				precio_usd: menu.precio_usd,
				ganancia: menu.ganancia
			})
			.then(async db_item => {


				// Guardar los adicionales.
				if(item.adicionales && item.adicionales.length > 0){
					let adicionales = [];
					for (let adi of item.adicionales){
						let menu_adi = await MenuAdicionales.query().findById(adi[0]).catch(console.log);
						adicionales.push({
							id_adicional: adi[0],
							cantidad: adi[1],
							id_item: db_item.id,
							nombre: menu_adi.nombre,
							precio_usd: menu_adi.precio_usd
						});
					}
					await PedidoAdicionales.query().insertGraph(adicionales).catch(console.error);
				}

				// await PedidoAdicionales.query()
				// 	.insertGraph(
				// 		// item.adicionales.map(adi => ({id_item: db_item.id, id_adicional: adi}) )
				// 		item.adicionales.map( adi => ({id_item: db_item.id, id_adicional: adi[0], cantidad: adi[1]}) )
				// 	).catch(console.error);


				// Guardar los acompañantes.
				if(item.acompañantes && item.acompañantes.length > 0){
					let acompañantes = [];
					for (let aco of item.acompañantes) {
						let menu_aco = await MenuAcompañante.query()
							.select('menus.id AS id_menu_acompañante', 'menus.precio_usd', 'menus.nombre')
							.join('menus', 'menus.id', 'id_menu_acompañante')
							.findOne('menus_acompañantes.id', aco[0]).catch(console.log);

						acompañantes.push({
							id_acompañante: aco[0],
							cantidad: aco[1],
							id_item: db_item.id,
							nombre: menu_aco.nombre,
							precio_usd: menu_aco.precio_usd
						});
					}
					await PedidoAcompañantes.query().insertGraph(acompañantes).catch(console.error);
				}


				// await PedidoAcompañantes.query()
				// 	.insertGraph(
				// 		item.acompañantes.map( aco => ({id_item: db_item.id, id_acompañante: aco[0], cantidad: aco[1]}) )
				// 	).catch(console.error);
			})
			.catch(console.error);
	}


	AR.enviarDatos(true, res);
	// await CarritoItems.query().insertGraph()
});


async function calcularSubtotal(data) {
	// SUMA(precio_menu * cantidad) //
	const id_cliente = data.id_cliente;
	const id_carrito = data.id_carrito;

	if(!id_cliente && !id_carrito)
		return;

	const query = CarritoItems.query()
		.join('menus' ,'menus.id', 'carrito_items.id_menu')
		.join('carrito', 'carrito.id', 'carrito_items.id_carrito')
		.select( raw('sum((:pre: * IFNULL(:gan:/100,0) + :pre:) * :can:)', {pre:'carrito_items.precio_usd', gan:'carrito_items.ganancia', can:'carrito_items.cantidad'}).as('subtotal') )
		.where( raw('?? = ??', 'menus.id', 'carrito_items.id_menu') );

	if(id_cliente)
		query.where('carrito.id_cliente', id_cliente)
	else
		query.where('carrito.id', id_carrito)

	const res = await query;

	return res.pop().subtotal || 0;
}

async function calcularSubtotalAdicionales(data) {
	// SUMA(precio_adicional * cantidad_adicional) //
	const id_cliente = data.id_cliente;
	const id_carrito = data.id_carrito;

	if(!id_cliente && !id_carrito)
		return;

	// const Query = PedidoAdicionales.query()
	// 	.join('carrito_items', 'carrito_items.id', 'pedidos_adicionales.id_item')
	// 	.join('carrito', 'carrito.id', 'carrito_items.id_carrito')
	// 	.select( raw('IFNULL(SUM(?? * ??), 0)', 'pedidos_adicionales.precio_usd', 'pedidos_adicionales.cantidad').as('subtotal') )

	const Query = CarritoItems.query()
		.join('carrito', 'carrito.id', 'carrito_items.id_carrito')
		.join('pedidos_adicionales', 'pedidos_adicionales.id_item', 'carrito_items.id')
		.join('menus_adicionales', 'menus_adicionales.id', 'pedidos_adicionales.id_adicional')
		.select( raw('IFNULL(sum((:pre: * IFNULL(:gan:/100, 0) + :pre:) * :can:), 0)', {pre:'menus_adicionales.precio_usd', gan:'carrito_items.ganancia', can:'pedidos_adicionales.cantidad'}).as('subtotal') );

		// .select( raw('IFNULL(sum(?? * ??), 0)', 'menus_adicionales.precio_usd', 'carrito_items.cantidad').as('subtotal') );

	if(id_cliente)
		Query.where('carrito.id_cliente', id_cliente);
	else
		Query.where('carrito.id', id_carrito);

	const res = await Query;

	return res.pop().subtotal || 0;
}

async function calcularSubtotalAcompañantes(data) {
	// SUMA(precio_acompañante * cantidad_acompañante) //
	const { id_cliente, id_carrito } = data;

	if(!id_cliente && !id_carrito) return;

	// const Query = PedidoAcompañantes.query()
	// 	.join('carrito_items', 'carrito_items.id', 'pedidos_acompañantes.id_item')
	// 	.join('carrito', 'carrito.id', 'carrito_items.id_carrito')
	// 	.select(
	// 		raw('IFNULL(SUM(?? * ??), 0)', 'pedidos_acompañantes.cantidad', 'pedidos_acompañantes.precio_usd').as('subtotal')
	// 	)

	const Query = PedidoAcompañantes.query()
		.join('carrito_items', 'carrito_items.id', 'pedidos_acompañantes.id_item')
		.join('carrito', 'carrito.id', 'carrito_items.id_carrito')
		.join('menus_acompañantes', 'menus_acompañantes.id', 'pedidos_acompañantes.id_acompañante')
		.join('menus', 'menus.id', 'menus_acompañantes.id_menu_acompañante')
		.select( raw('IFNULL(sum((:pre: * IFNULL(:gan:/100, 0) + :pre:) * :can:), 0)', {pre:'menus.precio_usd', gan:'carrito_items.ganancia', can:'pedidos_acompañantes.cantidad'}).as('subtotal') )

	if(id_cliente)
		Query.where('carrito.id_cliente', id_cliente);
	else
		Query.where('carrito.id', id_carrito);

	const res = await Query;

	return res.pop().subtotal || 0;
}

async function recuperarItemsDetallados(data){
	const id_cliente = data.id_cliente;
	const id_carrito = data.id_carrito;

	if(!id_cliente && !id_carrito)
		return;

	const subtotal_adicionales = PedidoAdicionales.query()
		.where('id_item', ref('carrito_items.id'))
		.join('menus_adicionales', 'menus_adicionales.id', 'pedidos_adicionales.id_adicional')
		// .join('carrito_items')
		.sum(raw('IFNULL((:pre: * IFNULL(:gan:/100, 0) + :pre:) * :can:, 0)', {pre:'menus_adicionales.precio_usd', gan:ref('carrito_items.ganancia'), can:'pedidos_adicionales.cantidad'})).as('subtotal_adicionales');

	// const subtotal_adicionales = PedidoAdicionales.query()
	// 	.where('id_item', ref('carrito_items.id'))
	// 	.sum(raw('IFNULL(?? * ??, 0)', 'pedidos_adicionales.precio_usd', 'pedidos_adicionales.cantidad')).as('subtotal_adicionales');

	const subtotal_acompañantes = PedidoAcompañantes.query()
		.where('id_item', ref('carrito_items.id'))
		.join('menus_acompañantes', 'menus_acompañantes.id', 'pedidos_acompañantes.id_acompañante')
		.join('menus', 'menus.id', 'menus_acompañantes.id_menu_acompañante')
		.sum(raw('IFNULL((:pre: * IFNULL(:gan:/100, 0) + :pre:) * :can:, 0)', {pre:'menus.precio_usd', gan:ref('carrito_items.ganancia'), can:'pedidos_acompañantes.cantidad'})).as('subtotal_acompañantes');

	// const subtotal_acompañantes = PedidoAcompañantes.query()
	// 	.where('id_item', ref('carrito_items.id'))
	// 	.sum(raw('IFNULL(?? * ??, 0)', 'pedidos_acompañantes.precio_usd', 'pedidos_acompañantes.cantidad')).as('subtotal_acompañantes');

	// Recuperar los items y calcular sus totales.
	const Query = CarritoItems.query()
		.join("carrito", "carrito.id", "carrito_items.id_carrito")
		.join('menus' ,'menus.id', 'carrito_items.id_menu')
		.select(
			// Info del item del carrito.
			'carrito_items.*',

			// El precio unitario.
			raw('??', 'menus.precio_usd').as('precio_usd'),

			// subtotal = precio * cantidad
			raw('(:pre: * IFNULL(:gan:/100, 0) + :pre:) * :can:', {pre:'carrito_items.precio_usd', gan:'carrito_items.ganancia', can:'carrito_items.cantidad'}).as('subtotal'),

			subtotal_adicionales,

			subtotal_acompañantes,

			builder => builder.sum(raw('?? + IFNULL(??,0) + IFNULL(??,0)','subtotal','subtotal_acompañantes', 'subtotal_adicionales')).as('total')
		)
		.whereRaw('carrito_items.id IS NOT NULL');

	if(id_carrito)
		Query.where("carrito.id", id_carrito);
	else
		Query.where("carrito.id_cliente", id_cliente);

	return await Query;
};




module.exports = router;
