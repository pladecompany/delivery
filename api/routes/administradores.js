//llamar al modelo
const Admin = require("../models/Admin");
const Restaurante = require("../models/Restaurante");
const Repartidor = require("../models/Repartidor");
const Cliente = require("../models/Cliente");
const Pedido = require("../models/Pedido");

//Llamar otras librerias
const AR = require("../ApiResponser");
const express = require("express");
const moment = require("moment");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const config = require("../config");
var uuid = require("uuid");
var sesion = require("./sesion");
var sesi = new sesion();

const mailer = require("../mails/Mailer");
const mailp = require("../mails/correo");
const validarMail = require("../functions/validateMail");
//Obligatorio para trabajar con FormData
const multer = require("multer");
const upload = multer();

//Manejador de Errores
const { AuthError, ValidationError } = require("../Errores");

var router = express.Router();

//registrar un admin
router.post("/add", upload.none(), sesi.validar_admin, async (req, res, next) => {
    data = req.body;

    let errr = false;

    // inicio validaciones
    if (!data.identificacion) errr = "Selecciona un tipo de identificación";
    if (!data.cedula) errr = "Ingresa un número de identificación";
    if (!data.nombre) errr = "Ingresa un nombre";
    if (!data.apellido) errr = "Ingresa un apellido";
    if (!data.usuario) errr = "Ingresa un usuario";
    if (!data.correo) errr = "Ingresa un correo";
    if (!data.pass) errr = "Ingresa una contraseña";

    const verificar_c = await Admin.query().where({ cedula: data.cedula });
    const verificar_u = await Admin.query().where({ usuario: data.usuario });
    const verificar_e = await Admin.query().where({ correo: data.correo });
    if (verificar_c[0]) errr = "El número de identificación ya existe";
    if (verificar_u[0]) errr = "El usuario ya existe";
    if (verificar_e[0]) errr = "El correo ya existe";
    //fin validaciones
    if(errr) return AR.enviarDatos({r: false, msj: errr}, res);

    await Admin.query()
    .insert({
      nombre: data.nombre,
      apellido: data.apellido,
      identificacion: data.identificacion,
      usuario: data.usuario,
      cedula: data.cedula,
      correo: data.correo,
      pass: bcrypt.hashSync(data.pass, 8)
    })
    .then(async resp => {
      resp.msj = "Administrador registrado";
      resp.r = true;
      
      /*var cuerpo = `Hola, ${data.nombre} ${data.apellido}
        <br> Ahora formas parte
        de la administración de nuestra plataforma. Tus datos de acceso a
        la plataforma de DOOMI son: <br>
        <b>Usuario: </b> ${data.iden}${data.cedula} <br><b>Contraseña: </b> ${data.pass}
      `;
      mailer.enviarMail({
          direccion: data.correo,
          titulo: "BIENVENIDO A DOOMI.app",
          mensaje: mailp({
              cuerpo: cuerpo,
              titulo: "BIENVENIDO A DOOMI.app"
          })
      });
      await Notificaciones.query()
      .insert({
          descripcion: "Bienvenido a DOOMI.app",
          contenido: "Bienvenido <b>"+data.nombre+" "+data.apellido+"</b> formas parte de nuestra administración en MEYERLANDONLINE.COM",
          estatus: 0,
          id_receptor: resp.id,
          tabla_usuario: 'administradores',
          tipo_noti: 15,
          id_usable: resp.id,
          fecha: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
      }).catch(err => {console.log(err)});
      */
      AR.enviarDatos(resp, res);
    })
    .catch(err => {
      console.log(err)
      err.r = false;
      err.msj = 'Error al registrar Administrador';
      res.send(err);
    });

});

//editar admin
router.put("/edit", upload.none(), sesi.validar_admin, async (req, res, next) => {
  data = req.body;
  let errr = false;

  // inicio validaciones
  if (!data.identificacion) errr = "Selecciona un tipo de identificación";
  if (!data.cedula) errr = "Ingresa un número de identificación";
  if (!data.nombre) errr = "Ingresa un nombre";
  if (!data.apellido) errr = "Ingresa un apellido";
  if (!data.usuario) errr = "Ingresa un usuario";
  if (!data.correo) errr = "Ingresa un correo electrónico";
  if (!data.iden) errr = "Ingresa la id";
  const verificar_c = await Admin.query().where({ cedula: data.cedula }).where("id","<>",data.iden);
  const verificar_u = await Admin.query().where({ usuario: data.usuario }).where("id","<>",data.iden);
  const verificar_e = await Admin.query().where({ correo: data.correo }).where("id","<>",data.iden);
  if (verificar_c[0]) errr = "El número de identificación ya existe";
  if (verificar_u[0]) errr = "El usuario ya existe";
  if (verificar_e[0]) errr = "El correo ya existe";
  //fin validaciones
  if(errr) return AR.enviarDatos({r: false, msj: errr}, res);

  var datos = {
    nombre: data.nombre,
    apellido: data.apellido,
    identificacion: data.identificacion,
    usuario: data.usuario,
    cedula: data.cedula,
    correo: data.correo,
  }
  if (data.pass.length > 0) {
    datos.pass= bcrypt.hashSync(data.pass, 8);
  }

  await Admin.query().updateAndFetchById(data.iden, datos)
  .then(async resp => {
    resp.msj = "Datos actualizados con éxito";
    resp.r = true;
    AR.enviarDatos(resp, res);
  })
  .catch(err => {
    console.log(err)
    err.r = false;
    err.msj = 'Error al actualizar datos';
    res.send(err);
  });

});

//listar todos los admins
router.get("/", sesi.validar_admin, async(req, res) => {
  await Admin.query()
      .then(ret => {
      AR.enviarDatos(ret, res);
  });
});

router.get("/escritorio", sesi.validar_admin, async(req, res) => {
  dataall = {repartidores:0,clientes:0,restaurantes:0,solicitudes:0};

  restaurantes = await Restaurante.query()
  .catch(async err => {console.log("error restaurante: "+err)});

  repartidores = await Repartidor.query()
  .catch(async err => {console.log("error repartidores: "+err)});

  num_clientes = (await Cliente.query().count())[0];

  pedidos = await Pedido.query()
  .catch(async err => {console.log("error pedidos: "+err)});
  
  if(pedidos)
    dataall.solicitudes=pedidos.length;
  if(restaurantes)
    dataall.restaurantes=restaurantes.length;
  if(repartidores)
    dataall.repartidores=repartidores.length;
  if(num_clientes)
    dataall.clientes=num_clientes['count(*)'];

  AR.enviarDatos(dataall, res);

});

//ver un admin
router.get("/:id", sesi.validar_admin, async(req, res) => {
  await Admin.query().where("id",req.params.id).then(ret => {
      AR.enviarDatos(ret[0], res);
  });

});

//eliminar admin
router.delete("/delete/:id", upload.none(), sesi.validar_admin, async(req, res) => {
  data = req.query;
  var  info = await Admin.query().where({ id: req.params.id })
  const eliminado = await Admin.query().delete().where({ id: req.params.id })
    .catch(err => {
      console.log(err);
      res.send(err);
    });

    if (eliminado){
      AR.enviarDatos({ msj: "Registro eliminado exitosamente" }, res);
    }
    else{ AR.enviarDatos({ msj: "No se puede eliminar el admin" }, res);}
});

module.exports = router;
