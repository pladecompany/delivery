const FAQ = require('../models/PreguntasFrecuentes');

const AR = require('../ApiResponser');
const sesi = new (require('./sesion'))();
const upload = require('multer')();
const router = require('express').Router();

// Listar preguntas.
router.get('/', (req, res) => {
	FAQ.query().then(r => AR.enviarDatos(r, res)).catch(console.error);
});

// Obtener una pregunta.
router.get('/:id(\\d+)/', (req, res) => {
	FAQ.query().findById(req.params.id).then(r => AR.enviarDatos(r, res)).catch(console.error);
});

// Crear pregunta.
router.post('/', upload.none(), sesi.validar_admin, async(req, res) => {
	const pregunta = req.body.pregunta;
	const respuesta = req.body.respuesta;

	/** Validar **/
	if(!pregunta || !respuesta)
		return AR.enviarDatos({r:false, msj:'Datos incompletos'}, res);

	await FAQ.query()
		.insert({pregunta, respuesta})
		.then(resp => {
			resp.r = true;
			resp.msj = 'Pregunta creada';
			AR.enviarDatos(resp, res)
		})
		.catch(e => {
			console.error(e);
			AR.enviarDatos({r:false, msj:'Ocurrió un error'}, res)
		});
});

// Editar pregunta.
router.put('/:id(\\d+)/', upload.none(), sesi.validar_admin, async(req, res) => {
	const id = req.params.id;
	const pregunta = req.body.pregunta;
	const respuesta = req.body.respuesta;

	/** Validar **/
	if(!pregunta || !respuesta)
		return AR.enviarDatos({r:false, msj:'Datos incompletos'}, res);

	FAQ.query().updateAndFetchById(id, {pregunta, respuesta})
		.then(resp => {
			resp.r = true;
			resp.msj = 'Datos actualizados';
			AR.enviarDatos(resp, res);
		})
		.catch(e => {
			console.error(e);
			AR.enviarDatos({r:false, msj:'Ocurrió un error'}, res)
		});
});

// Cambiar estatus.
router.patch('/:id(\\d+)/estatus', upload.none(), sesi.validar_admin, async(req, res) => {
	const id = req.params.id;
	const estatus = req.body.estatus ? 1 : 0;

	await FAQ.query().patchAndFetchById(id, {estatus})
		.then(r => AR.enviarDatos(r, res))
		.catch(e => {
			console.error(e);
			AR.enviarDatos({r:false, msj:'Ocurrió un error'}, res)
		});
});

// Eliminar pregunta.
router.delete('/:id(\\d+)/', sesi.validar_admin, (req, res) => {
	FAQ.query().findById(req.params.id).delete()
		.then(() => AR.enviarDatos({r:true}, res))
		.catch(e => {
			console.error(e);
			AR.enviarDatos({r:false, msj:'Ocurrió un error'}, res)
		});
});


module.exports = router;
