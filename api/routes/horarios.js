2//llamar al modelo
const Horarios = require("../models/Horarios");
const HorariosProgramados = require("../models/HorariosProgramados");
const Restaurante = require("../models/Restaurante");

//Llamar otras librerias
const { raw } = require('objection');
const AR = require("../ApiResponser");
const express = require("express");
const moment = require("moment");
// const bcrypt = require("bcryptjs");
// const jwt = require("jsonwebtoken");
// const config = require("../config");
// var uuid = require("uuid");
var sesion = require("./sesion");
var sesi = new sesion();
// var archivos = require("../Archivos");
// var fs = require("fs-extra");

//Obligatorio para trabajar con FormData
const multer = require("multer");
const upload = multer();

//Manejador de Errores
// const { AuthError, ValidationError } = require("../Errores");

var router = express.Router();

//registrar
router.post("/add", upload.none(), sesi.validar_restaurante, async (req, res, next) => {
    data = req.body;
    let errr = false;
    let falta = false;
    hor_1_i = data.hora_ini_1;
    hor_1_f = data.hora_fin_1;
    sts_1 = data.status_h_1;
    hor_2_i = data.hora_ini_2;
    hor_2_f = data.hora_fin_2;
    sts_2 = data.status_h_2;
    hor_3_i = data.hora_ini_3;
    hor_3_f = data.hora_fin_3;
    sts_3 = data.status_h_3;
    let f_a = moment().format('e');
    let h_a = moment().format('HH:mm:ss');

    //var horario = ["LUNES", "MARTES", "MIERCOLES", "JUEVES", "VIERNES", "SABADO", "DOMINGO"];
    if(!sts_1 && !sts_2 && !sts_3) return AR.enviarDatos({r: false, msj: "Debes seleccionar al menos un horario activo."}, res);
    var horario = ["Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo"];

    //VALIDACIONES PARA CAMPOS VACIOS AL SELECCIONAR LA HORA
    for (var i = 0; i < hor_1_i.length; i++) {
      er1 = "horario 1 del dia ("+horario[i]+")";
      er2 = "horario 2 del dia ("+horario[i]+")";
      er3 = "horario 3 del dia ("+horario[i]+")";
      if((hor_1_i[i]!='' && hor_1_f[i]=='') || (hor_1_i[i]=='' && hor_1_f[i]!='') || (hor_1_i[i]=='' && hor_1_f[i]=='' && (sts_1 && sts_1[i]))){
        if(falta)
          falta += "<br>"+er1;
        else
          falta = er1;
      }

      if((hor_2_i[i]!='' && hor_2_f[i]=='') || (hor_2_i[i]=='' && hor_2_f[i]!='') || (hor_2_i[i]=='' && hor_2_f[i]=='' && (sts_2 && sts_2[i]))){
        if(falta)
          falta += "<br>"+er2;
        else
          falta = er2;
      }



      if((hor_3_i[i]!='' && hor_3_f[i]=='') || (hor_3_i[i]=='' && hor_3_f[i]!='') || (hor_3_i[i]=='' && hor_3_f[i]=='' && (sts_3 && sts_3[i]))){
        if(falta)
          falta += "<br>"+er3;
        else
          falta = er3;
      }

    }
    if(falta) return AR.enviarDatos({r: false, msj: "Falta completar las horas de los horarios:<br>"+falta}, res);

    //VALIDACIONES PARA LOS CAMPOS SELECCIONADOS
    if(sts_1){
      for (var i = 0; i < sts_1.length; i++) {
        if(sts_1[i]){
          er = false;
          if(hor_1_i[i]=='' || hor_1_f[i]==''){
            er = "Falta el horario 1 del dia ("+horario[i]+")";
          }
          if(er){
            if(falta)
              falta += "<br>"+er;
            else
              falta = er;
          }
        }
      }
    }
    if(sts_2){
      for (var i = 0; i < sts_2.length; i++) {
        if(sts_2[i]){
          er = false;
          fv = new Date('2/1/2020 '+hor_2_i[i]);//darle una fecha especifica para las validaciones
          f1 = new Date('2/1/2020 '+hor_1_i[i]);//darle una fecha especifica para las validaciones
          f2 = new Date('2/1/2020 '+hor_1_f[i]);//darle una fecha especifica para las validaciones

          if(f2<f1){ //si la fecha desde es pm y el hasta am, se valida con fechas diferentes
            f1 = new Date('1/1/2020 '+hor_1_i[i]);
            f2 = new Date('2/1/2020 '+hor_1_f[i]);
          }

          if(sts_1 && sts_1[i] && hor_1_i[i]!='' && hor_1_f[i]!='' && fv<f2){
            er = "El horario 2 del dia ("+horario[i]+") no debe estar en el rango del horario 1";
          }

          if(er){
            if(falta)
              falta += "<br>"+er;
            else
              falta = er;
          }
        }
      }
    }
    if(sts_3){
      for (var i = 0; i < sts_3.length; i++) {
        if(sts_3[i]){
          er = false;
          fv = new Date('2/1/2020 '+hor_3_i[i]);//darle una fecha especifica para las validaciones
          f1 = new Date('2/1/2020 '+hor_1_i[i]);//darle una fecha especifica para las validaciones
          f2 = new Date('2/1/2020 '+hor_1_f[i]);//darle una fecha especifica para las validaciones
          f11 = new Date('2/1/2020 '+hor_2_i[i]);//darle una fecha especifica para las validaciones
          f22 = new Date('2/1/2020 '+hor_2_f[i]);//darle una fecha especifica para las validaciones

          if(f2<f1){ //si la fecha desde es pm y el hasta am, se valida con fechas diferentes
            f1 = new Date('1/1/2020 '+hor_1_i[i]);
            f2 = new Date('2/1/2020 '+hor_1_f[i]);
          }
          if(f22<f11){ //si la fecha desde es pm y el hasta am, se valida con fechas diferentes
            f11 = new Date('1/1/2020 '+hor_2_i[i]);
            f22 = new Date('2/1/2020 '+hor_2_f[i]);
          }

          if(sts_2 && sts_2[i] && hor_2_i[i]!='' && hor_2_f[i]!='' && fv<f22){
            er = "El horario 3 del dia ("+horario[i]+") no debe estar en el rango del horario 2";
          }else if(sts_1 && sts_1[i] && hor_1_i[i]!='' && hor_1_f[i]!='' && fv<f2){
            er = "El horario 3 del dia ("+horario[i]+") no debe estar en el rango del horario 1";
          }

          if(er){
            if(falta)
              falta += "<br>"+er;
            else
              falta = er;
          }
        }
      }
    }
    if(falta) return AR.enviarDatos({r: false, msj: "Se presentan los siguientes errores en los horarios a mostrar:<br>"+falta}, res);

    await Horarios.query().delete().where({ id_restaurante: data.iden });
    for (var i = 0; i < hor_1_i.length; i++) {
      if(hor_1_i[i]!='' && hor_1_f!=''){
        val_s_1 = 0;
        if(sts_1 && sts_1[i])
          val_s_1 = sts_1[i];
        await Horarios.query()
          .insert({
            id_restaurante: data.iden,
            dia: horario[i],
            inicio_hor: hor_1_i[i],
            fin_hor: hor_1_f[i],
            estatus: val_s_1,
            horario: '1'
          });
      }
      if(hor_2_i[i]!='' && hor_2_f!=''){
        val_s_2 = 0;
        if(sts_2 && sts_2[i])
          val_s_2 = sts_2[i];
        await Horarios.query()
          .insert({
            id_restaurante: data.iden,
            dia: horario[i],
            inicio_hor: hor_2_i[i],
            fin_hor: hor_2_f[i],
            estatus: val_s_2,
            horario: '2'
          });
      }
      if(hor_3_i[i]!='' &&  hor_3_f!=''){
        val_s_3 = 0;
        if(sts_3 && sts_3[i])
          val_s_3 = sts_3[i];
        await Horarios.query()
          .insert({
            id_restaurante: data.iden,
            dia: horario[i],
            inicio_hor: hor_3_i[i],
            fin_hor: hor_3_f[i],
            estatus: val_s_3,
            horario: '3'
          });
      }
    }

    let c = await Horarios.query()
      .select( raw('COUNT(*)').as('count') )
      .first()
      .where({id_restaurante: data.iden, estatus: 1}).catch(console.error);

    let on = await Horarios.query()
      .where({id_restaurante: data.iden, dia: horario[f_a]})
      .where('inicio_hor','<=',h_a)
      .where('fin_hor','>=',h_a)
      .where('estatus',1)
      .catch(console.error);
    /*
    if(on[0]){
      Restaurante.query().updateAndFetchById(data.iden, {laborando: 1}).catch(async err => {console.log("error categoria: "+err)});
    }
    */
    AR.enviarDatos({r:true, msj: "Horario actualizado correctamente!", horarios_activos:c.count, veri_on: on}, res);

});

//listar
router.get("/", sesi.validar_general, async(req, res) => {
  await Horarios.query()
      .then(ret => {
      AR.enviarDatos(ret, res);
  });
});

//ver
router.get("/:id(\\d+)", sesi.validar_general, async(req, res) => {
  await Horarios.query().where("id_restaurante",req.params.id).then(ret => {
      AR.enviarDatos(ret, res);
  });
});




/** PROGRAMADOS **/

// Listar
router.get("/programados", sesi.validar_general, async(req, res) => {
  const modalidad = req.query.modo || req.query.modalidad;
  const id_restaurante = req.query.id_res || req.query.id_restaurante;
  if(!id_restaurante)
    return AR.enviarDatos({r:false}, res);

  let fec_act = moment().format('YYYY-MM-DD HH:mm:ss');
  let fec_act_d = moment().format('YYYY-MM-DD');

  const Query = HorariosProgramados.query()
    .where({id_restaurante})
    // .where('fin', '>=', new Date().toISOString()); // Que no haya finalizado.
    .where('fin','>',fec_act)//.where('inicio','>=',fec_act_d+' 00:00:00').where('fin','<=',fec_act_d+' 23:59:59');

  if(modalidad)
    Query.where({modalidad})
  Query.select('*',raw('DATE_FORMAT(inicio, "%Y-%m-%d %H:%i:%s")').as('fec_ini'),raw('DATE_FORMAT(fin, "%Y-%m-%d %H:%i:%s")').as('fec_fin'))
  Query.orderBy('inicio')
  await Query.then(horarios => {
    AR.enviarDatos(horarios, res)
  }).catch(console.error)
});

router.get("/programados-app", sesi.validar_general, async(req, res) => {
  const modalidad = req.query.modo || req.query.modalidad;
  const id_restaurante = req.query.id_res || req.query.id_restaurante;
  let fec_act = moment().format('YYYY-MM-DD HH:mm:ss');
  let fec_act_d = moment().format('YYYY-MM-DD');
  if(!id_restaurante)
    return AR.enviarDatos({r:false}, res);

  const Query = HorariosProgramados.query().where({id_restaurante})
  if(modalidad=='pickup')
    Query.where('fin','>',fec_act).where('inicio','>=',fec_act_d+' 00:00:00').where('fin','<=',fec_act_d+' 23:59:59');
  if(modalidad=='programado')
    Query.where('inicio','>',fec_act_d+' 23:59:59');

  if(modalidad)
    Query.where({modalidad})

  await Query.then(horarios => AR.enviarDatos(horarios, res)).catch(console.error)
});
router.get("/programados-horas", sesi.validar_general, async(req, res) => {
  const modalidad = req.query.modo || req.query.modalidad;
  const id_restaurante = req.query.id_res || req.query.id_restaurante;
  const fec_obt = req.query.fec || req.query.fec;
  let fec_act = moment(fec_obt).format('YYYY-MM-DD');
  if(!id_restaurante)
    return AR.enviarDatos({r:false}, res);

  const Query = HorariosProgramados.query().where({id_restaurante}).where('modalidad','programado').where('inicio','>=',fec_act+' 00:00:00').where('inicio','<=',fec_act+' 23:59:59');
  Query.select('*',raw('DATE_FORMAT(inicio, "%Y-%m-%d %H:%i:%s")').as('inicio'),raw('DATE_FORMAT(fin, "%Y-%m-%d %H:%i:%s")').as('fin'))
  await Query.then(horarios => AR.enviarDatos(horarios, res)).catch(console.error)
});
router.get("/verificar-horarios", sesi.validar_general, async(req, res) => {
  var horario_ver = ["Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo"];
  const del = req.query.del || req.query.del;
  const pic = req.query.pic || req.query.pic;
  const pro = req.query.pro || req.query.pro;
  const id_restaurante = req.query.id_res || req.query.id_restaurante;
  let fec_act = moment().format('YYYY-MM-DD HH:mm:ss');
  let fec_act_d = moment().format('YYYY-MM-DD');
  let h_a = moment().format('HH:mm:ss');
  let f_a = moment().format('e');
  if(!id_restaurante)
    return AR.enviarDatos({r:false}, res);

  // const Query_pick = await HorariosProgramados.query().where({id_restaurante}).where('modalidad','pickup').where('fin','>',fec_act).where('inicio','>=',fec_act_d+' 00:00:00').where('fin','<=',fec_act_d+' 23:59:59').catch(console.error);
  const Query_pro = await HorariosProgramados.query().where({id_restaurante}).where('modalidad','programado').where('inicio','>',fec_act_d+' 23:59:59').catch(console.error);
  const Query_del = await Restaurante.query().join("horarios","horarios.id_restaurante","restaurantes.id").where({id_restaurante}).where('restaurantes.laborando',1).where('restaurantes.vacaciones',0).where('horarios.dia',horario_ver[f_a]).where('horarios.estatus',1).catch(console.error);
  const Query_pick = await Restaurante.query().join("horarios","horarios.id_restaurante","restaurantes.id").where({id_restaurante}).where('restaurantes.laborando',1).where('restaurantes.vacaciones',0).where('horarios.dia',horario_ver[f_a]).where('horarios.estatus',1).catch(console.error);
  AR.enviarDatos({r:true,pic:Query_pick,pro:Query_pro,del:Query_del,fecha_act:fec_act}, res);
});
// Crear
router.post("/programados", upload.none(), sesi.validar_general, async(req, res) => {
  const id_restaurante = req.body.id_res || req.body.id_restaurante;
  const inicio = new Date(req.body.fecha_inicio);
  const fin = new Date(req.body.fecha_fin);
  const modalidad = (req.body.modalidad || '').toLowerCase();

  let err = false;

  if(!inicio || !fin || !id_restaurante) return res.sendStatus(400);

  if ( isNaN(inicio.getTime()) )
    err = 'Ingrese fecha inicial';

  else if ( isNaN(fin.getTime()) )
    err = 'Ingrese fecha final';

  else if (inicio > fin)
    err = 'La fecha inicial no puede ser mayor que la final';

  else if (fin < new Date())
    err = 'Introduzca una fecha posterior a la actual';

  else {
    const DIAS = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];
    let ini_dia = inicio.getDay();
    //let ini_fecha = moment(inicio).format('YYYY-MM-DD');
    //let ini_hora = moment(inicio).format('HH:mm:ss');
    let ini_fecha = moment(req.body.fecha_inicio).format('YYYY-MM-DD');
    let ini_hora = moment(req.body.fecha_inicio).format('HH:mm:ss');
    let fin_dia = fin.getDay();
    //let fin_fecha = moment(fin).format('YYYY-MM-DD');
    //let fin_hora = moment(fin).format('HH:mm:ss');
    let fin_fecha = moment(req.body.fecha_fin).format('YYYY-MM-DD');
    let fin_hora = moment(req.body.fecha_fin).format('HH:mm:ss');
    let dias_en_rango = [];
    if(ini_dia==fin_dia)
      dias_en_rango.push(DIAS[ini_dia]);
    else
      DIAS.forEach((dia, i, DIAS) => {
        if(i >= ini_dia && i < fin_dia) dias_en_rango.push(DIAS[i])
      });

    // Verificar que no haya colisiones de horario.
    let q_horarios_p = HorariosProgramados.query()
      .whereNot( raw('(DATE(inicio) > ? OR DATE(fin) < ?)', [fin_fecha,ini_fecha]) )
      .whereNot( raw('(TIME(inicio) > ? OR TIME(fin) < ?)', [fin_hora,ini_hora]) )
      .where({id_restaurante})

    if(modalidad=='pickup')
      q_horarios_p.where('modalidad', 'pickup');
    else
      q_horarios_p.where('modalidad', 'programado');
      // .catch(console.error)

    let horarios_p = await q_horarios_p.catch(console.error);

    // Verificar que este dentro del horario general.
    let horarios = await Horarios.query()
      .where('estatus', 1)
      .where('inicio_hor', '<=', ini_hora)
      .where('fin_hor', '>=', fin_hora)
      .whereIn('dia', dias_en_rango)
      .where({id_restaurante})
      .catch(console.error);

    if(horarios_p.length > 0) err = 'Existe un horario en este rango con la misma hora';
    if(modalidad=='pickup' && horarios.length == 0) err = 'Este horario está fuera del horario general';
  }

  if(err) return AR.enviarDatos({r: false, msj: err}, res);

  HorariosProgramados.query()
    .insert({ modalidad, id_restaurante, inicio, fin })
    .then(r => AR.enviarDatos(r, res))
    .catch(err => { console.error(err); res.sendStatus(500) })
});

router.put("/programados/:id(\\d+)", upload.none(), sesi.validar_restaurante, async(req, res) => {
  const id_restaurante = req.body.id_res || req.body.id_restaurante;
  const inicio = new Date(req.body.fecha_inicio);
  const fin = new Date(req.body.fecha_fin);
  const modalidad = (req.body.modalidad || '').toLowerCase();

  let err = false;

  if(!inicio || !fin || !id_restaurante) return res.sendStatus(400);

  if ( isNaN(inicio.getTime()) )
    err = 'Ingrese fecha inicial';

  else if ( isNaN(fin.getTime()) )
    err = 'Ingrese fecha final';

  else if (inicio > fin)
    err = 'La fecha inicial no puede ser mayor que la final';

  else if (fin < new Date())
    err = 'Introduzca una fecha posterior a la actual';

  else {
    const DIAS = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];
    let ini_dia = inicio.getDay();
    //let ini_fecha = moment(inicio).format('YYYY-MM-DD');
    //let ini_hora = moment(inicio).format('HH:mm:ss');
    let ini_fecha = moment(req.body.fecha_inicio).format('YYYY-MM-DD');
    let ini_hora = moment(req.body.fecha_inicio).format('HH:mm:ss');
    let fin_dia = fin.getDay();
    //let fin_fecha = moment(fin).format('YYYY-MM-DD');
    //let fin_hora = moment(fin).format('HH:mm:ss');
    let fin_fecha = moment(req.body.fecha_fin).format('YYYY-MM-DD');
    let fin_hora = moment(req.body.fecha_fin).format('HH:mm:ss');
    let dias_en_rango = [];
    if(ini_dia==fin_dia)
      dias_en_rango.push(DIAS[ini_dia]);
    else
      DIAS.forEach((dia, i, DIAS) => {
        if(i >= ini_dia && i < fin_dia) dias_en_rango.push(DIAS[i])
      });

    // Verificar que no haya colisiones de horario.
    let q_horarios_p = HorariosProgramados.query()
      .whereNot( raw('(DATE(inicio) > ? OR DATE(fin) < ?)', [fin_fecha,ini_fecha]) )
      .whereNot( raw('(TIME(inicio) > ? OR TIME(fin) < ?)', [fin_hora,ini_hora]) )
      .whereNot('id', req.params.id)
      .where({id_restaurante})

    if(modalidad=='pickup')
      q_horarios_p.where('modalidad', 'pickup');
    else
      q_horarios_p.where('modalidad', 'programado');
      // .catch(console.error)

    let horarios_p = await q_horarios_p.catch(console.error);

    // Verificar que este dentro del horario general.
    let horarios = await Horarios.query()
      .where('estatus', 1)
      .where('inicio_hor', '<=', ini_hora)
      .where('fin_hor', '>=', fin_hora)
      .whereIn('dia', dias_en_rango)
      .where({id_restaurante})
      .catch(console.error);

    if(horarios_p.length > 0) err = 'Existe un horario en este rango con la misma hora';
    if(modalidad=='pickup' && horarios.length == 0) err = 'Este horario está fuera del horario general';
  }

  if(err) return AR.enviarDatos({r: false, msj: err}, res);

  await HorariosProgramados.query()
    .findById(req.params.id)
    .delete()
    .then()
    .catch(console.error)

  await HorariosProgramados.query()
    .insert({ modalidad, id_restaurante, inicio, fin })
    .then(r => AR.enviarDatos(r, res))
    .catch(err => { console.error(err); res.sendStatus(500) })
});

// Eliminar
router.delete("/programados/:id(\\d+)", sesi.validar_general, async(req, res) => {
  const hor = await HorariosProgramados.query().findById(req.params.id).catch(console.error);

  await HorariosProgramados.query()
    .findById(req.params.id)
    .delete()
    .then(async r => {
      const verif = await HorariosProgramados.query()
        .count('* AS conteo')
        .first()
        .where('id_restaurante', hor.id_restaurante)

      if (!verif.conteo) {
        await Restaurante.query()
          .patchAndFetchById(hor.id_restaurante, {modalidad_programados:0}).catch(console.error);
      }

      res.sendStatus(200)
    })
    .catch(err => { console.error(err); res.sendStatus(500) });
})

// Verificar que hayan horarios.
router.search("/", sesi.validar_general, async (req, res) => {
  const id_restaurante = req.body.id_res || req.body.id_restaurante;
  const modalidad = req.body.modalidad;

  if(!id_restaurante) return AR.enviarDatos({r:false}, res);

  let count_pickup = await HorariosProgramados.query()
    .select( raw('COUNT(*)').as('count') )
    .where({id_restaurante, modalidad:'pickup'})
    .first()
    .catch(console.error);

  let count_programados = await HorariosProgramados.query()
    .select( raw('COUNT(*)').as('count') )
    .where({id_restaurante, modalidad:'programado'})
    .first()
    .catch(console.error);

  let count_general = await Horarios.query()
    .select( raw('COUNT(*)').as('count') )
    .first()
    .where({id_restaurante, estatus:1}).catch(console.error);

  let datos = {
    pickup: count_pickup.count || 0,
    programado: count_programados.count || 0,
    general: count_general.count || 0
  };

  AR.enviarDatos(datos, res);
});

module.exports = router;
