// llamar al modelo.
const Menu = require("../models/Menu");
const CarritoItems = require("../models/CarritoItems");
const CategoriaMenu = require("../models/CategoriaMenu");
const MenuAdicionales = require("../models/MenuAdicionales");
const MenuAcompañante = require("../models/MenuAcompañante");
const PedidoAcompañantes = require("../models/PedidoAcompañantes");
const Notificaciones = require("../models/Notificacion");
const Admin = require("../models/Admin");
const Restaurante = require("../models/Restaurante");
var Firebase = require("../routes/firebase");
Firebase = new Firebase();

// Otras librerias.
const AR = require("../ApiResponser");
const config = require("../config");
var sesion = require("./sesion");
var sesi = new sesion();
var archivos = require("../Archivos");
const jsonfile = require('jsonfile');
var fs = require("fs-extra");

// Obligatorio para trabajar con FormData.
const multer = require("multer");
const upload = multer();
const crypto = require("crypto");

const express = require("express");
var router = express.Router();


// Registrar un menu.
router.post("/add", archivos.fields([{ name: "img", maxCount: 1 }]), sesi.validar_restaurante, async function(req, res, next){
    data = req.body;

    // Validar los datos.
    let errr = false;
    if (!data.nom_men) errr = "Ingrese el nombre del menú";
    if (!data.descripcion) errr = "Ingrese la descripción del menú";
    if (!data.usd_men) errr = "Ingrese el precio del menú";
    if (!data.cat_men) errr ="Seleccione la categoría del menú";
    if (!data.est_men) errr = "Seleccione el estatus del menú"
    if (!data.id_res) errr = "Ingrese id del restaurante";
    if (!data.exi_men) errr = "Ingrese las existencias";

    const verificar_n = await Menu.query().where({nombre: data.nom_men, id_restaurante:data.id_res});
    if (verificar_n[0]) errr = "El menú ya existe";

    if (errr) {
        if(!data.imgweb){
            if(req.files["img"])
                await fs.unlink(__dirname + "/../public/uploads/" + req.files["img"][0].filename, function (err) { });
        }
        return AR.enviarDatos({r: false, msj: errr}, res);
    }
    // Fin de validacion.

    const adicionales = {
        nombres: data.adicionales_nombres,
        precios: data.adicionales_precios,
        existencias: data.adicionales_existencias,
    };
    if(data.adicionales_nombres)
        elementos_adicionales = data.adicionales_nombres.length;
    else
        elementos_adicionales = 0;

    data.acom_ids = data.acom_ids || [];

    var file = __dirname+ '/../configuracion.json'
  	var files = jsonfile.readFileSync(file);
  	const rest = await Restaurante.query().findById(data.id_res).catch(console.log);
  	var ganancia = files.ganancia;
  	if(rest.ganancia!=null)
  		var ganancia = rest.ganancia;
    datos = {
        nombre: data.nom_men,
        descripcion: data.descripcion,
        precio_usd: data.usd_men,
        estatus: data.est_men,
        id_categoria: data.cat_men,
        id_restaurante: data.id_res,
        existencias: data.exi_men,
        'rm_acompañantes': data.acom_ids.map(id => ({'id_menu_acompañante':id})),
        ganancia: ganancia,
    };

    // Guardar la ruta a la imagen.
    if(!data.imgweb){
        if(req.files["img"])
            datos.img = config.rutaArchivo(req.files["img"][0].filename);
    }else if(data.imgweb){
      if(data.img){
        var file = "img_per__" + crypto.randomBytes(18).toString("hex") + "__.jpg";
        await require("fs").writeFile(__dirname + "/../public/uploads/" + file, new Buffer.from(data.img.split(",")[1], 'base64'), 'base64', function(err) {
          //console.log(err);
        });
        datos.img = config.rutaArchivo(file);
      }
    }

    await Menu.query()
        .insertGraph(datos)
        .then(async resp => {
            resp.msj = "Menú agregado";
            resp.r = true;

            // Evitar que se elimine categoria
            //  si hay menus que pertenecen a ella.
            await CategoriaMenu.query()
                .where("id", datos.id_categoria).increment("veces_usada", 1);

            for (let i = 0; i < elementos_adicionales; i++) {
                let adi_nombre = adicionales.nombres[i];
                let adi_precio = adicionales.precios[i];
                let adi_existencia = adicionales.existencias[i];

                await MenuAdicionales.query()
                    .insert({
                        nombre: adi_nombre,
                        precio_usd: adi_precio,
                        existencias: adi_existencia,
                        id_menu: resp.id
                    })
                    .catch(err => console.log(err));
            }
            AR.enviarDatos(resp, res);
            // NOTIFICACIONES
            const ADMIN = await Admin.query();
            for (var i = 0; i < ADMIN.length; i++) {
                //enviar notificacion
                await Notificaciones.query()
                  .insert({
                      contenido: '¡El Doomi a registrado "'+resp.nombre+'" en su menú!',
                      fecha: new Date(),
                      estatus: 0,
                      tipo_noti: 2,
                      redireccionar_noti: '?op=listadomenu&id=',
                      id_usable: resp.id_restaurante,
                      id_receptor: ADMIN[i].id,
                      id_emisor: resp.id_restaurante,
                      tabla_usuario_r: 'administradores',
                      tabla_usuario_e: 'restaurantes'
                  }).catch(async(err)=>{console.log(err);});
                  await Firebase.enviarPushNotification({
                    id_usuario: "" + ADMIN[i].id + "",
                    title: 'Nuevo menú registrado',
                    body: '¡El Doomi a registrado "'+resp.nombre+'" en su menú!',
                    tablausuario: 'administradores'
                }, function(datan) { console.log(datan); });
            }
            IO.sockets.emit("RecibirNotificacion", {resp});
        })
        .catch(async err => {
            //ELIMINAR IMAGENES POR SI EXISTE ALGUN ERROR
            if(!req.body.imgweb){
                if(req.files["imagen"])
                    await fs.unlink(__dirname + "/../public/uploads/" + req.files["imagen"][0].filename, function (err) { });
            }
            console.log(err);
            err.r = false;
            err.msj = "Error al crear menú";
            res.send(err);
        });
});

// Editar menu.
router.put("/edit", archivos.fields([{ name: "img", maxCount: 1 }]), sesi.validar_restaurante, async (req,res,next)=>{
    data = req.body;

    // Validar los datos.
    let errr = false;
    if (!data.nom_men) errr = "Ingrese el nombre del menú";
    if (!data.descripcion) errr = "Ingrese la descripción del menú";
    if (!data.usd_men) errr = "Ingrese el precio del menú";
    if (!data.cat_men) errr ="Seleccione la categoría del menú";
    if (!data.est_men) errr = "Seleccione el estatus del menú"
    if (!data.id_res) errr = "Ingrese id del restaurante";
    if (!data.exi_men) errr = "Ingrese las existencias";

    // Fin de validacion.

    if (errr){
        //ELIMINAR IMAGENES SI NO PASA LAS VALIDACIONES.
        if(!data.imgweb){
            if(req.files["img"])
                await fs.unlink(__dirname + "/../public/uploads/" + req.files["img"][0].filename, function (err) { });
        }
        return AR.enviarDatos({r: false, msj: errr}, res);
    }

    // Actualizar datos.
    let datos = {nombre:   data.nom_men
            ,descripcion:  data.descripcion
            ,precio_usd:   data.usd_men
            ,estatus:      data.est_men
            ,id_categoria: data.cat_men
            ,existencias:    data.exi_men
    };

    const adicionales = {
        nombres: data.adicionales_nombres,
        precios: data.adicionales_precios,
        existencias: data.adicionales_existencias,
        ids: data.adicionales_ids
    };
    if(data.adicionales_ids)
        elementos_adicionales = data.adicionales_ids.length;
    else
        elementos_adicionales = 0;

    data.acom_ids = data.acom_ids || [];

    // Guardar ruta de la imagen.
    if(!data.imgweb){
        if(req.files["img"])
            datos.img = config.rutaArchivo(req.files["img"][0].filename);
    }else if(data.imgweb){
      if(data.img){
        var file = "img_per__" + crypto.randomBytes(18).toString("hex") + "__.jpg";
        await require("fs").writeFile(__dirname + "/../public/uploads/" + file, new Buffer.from(data.img.split(",")[1], 'base64'), 'base64', function(err) {
          //console.log(err);
        });
        datos.img = config.rutaArchivo(file);
      }
    }
    var info = await Menu.query().where({ id: data.iden });
    await Menu.query().updateAndFetchById(data.iden, datos)
        .then(async resp => {
            //ELIMINAR IMAGENES ANTERIOR SI HA GUARDADO UNA NUEVA.
            if((req.files["img"] || data.img) && + info[0].img)
                await fs.unlink(__dirname + "/../public/uploads/" + info[0].img.split(`\\public\\uploads\\`)[1], function (err) { });
            resp.msj = "Datos actualizados con éxito";
            resp.r = true;

            for (let i = 0; i < elementos_adicionales; i++) {
                let adi_nombre = adicionales.nombres[i];
                let adi_precio = adicionales.precios[i];
                let adi_existencia = adicionales.existencias[i];
                let adi_id = adicionales.ids[i];

                if (adi_id == 0){
                    await MenuAdicionales.query()
                        .insert({
                            nombre: adi_nombre,
                            precio_usd: adi_precio,
                            existencias: adi_existencia,
                            id_menu: resp.id
                        })
                        .catch(err => console.log(err));
                }
                else {
                    await MenuAdicionales.query().updateAndFetchById(adi_id, {nombre: adi_nombre, precio_usd: adi_precio, id_menu: resp.id, existencias: adi_existencia})
                        .catch(err => console.log(err));
                }
            }

            await MenuAcompañante.query().delete().where({id_menu: resp.id});
            for (acom of data.acom_ids){
                await MenuAcompañante.query()
                    .insert({ id_menu: resp.id, id_menu_acompañante: acom }).catch(console.error);
            }

            AR.enviarDatos(resp, res);
        })
        .catch(async (err) => {
            if(!req.body.imgweb){
                if(req.files["img"])
                    await fs.unlink(__dirname + "/../public/uploads/" + req.files["img"][0].filename, function (err) { });
            }
            console.log(err);
            err.r = false;
            err.msj = "Error al actualizar datos";
            res.send(err);
        });
});

// Cambiar estatus de un menu.
router.put("/edit_estatus", upload.none(), sesi.validar_restaurante, async(req, res)=>{
    data = req.body;

    await Menu.query().updateAndFetchById(data.iden, {estatus: data.nuevo_estatus})
        .then((resp)=>{
            if (resp) {
                resp.estatus_actual = data.nuevo_estatus;
                resp.msj = "Estatus actualizado con éxito";
                resp.r = true;
                AR.enviarDatos(resp, res);
            }
            else {
                res.send(null);
            }
        })
        .catch(async(err)=>{
            console.log(err);
            err.estatus_actual = (await Menu.query().where("id", data.iden)).estatus
            err.msj = "No se pudo actualizar el estatus";
            err.r = false;
            res.send(err);
        });
});

//Editar ganacias de un doomi
router.put("/edit_ganancia", upload.none(), sesi.validar_admin, async (req, res, next) => {
  data = req.body;
  let errr = false;
  // inicio validaciones
  if (data.val.length <1) {
    errr = 'Introduzca la ganancia'
  }

  //fin validaciones
  if(errr){
    return AR.enviarDatos({r: false, msj: errr}, res);
  }

  await Menu.query().updateAndFetchById(data.iden, {ganancia: data.val})
    .then(async resp => {
      resp.msj = "Ganancia actualizada con éxito";
      resp.r = true;
      AR.enviarDatos(resp, res);
    })
    .catch(async err => {
      console.log(err)
      err.r = false;
      err.msj = 'Error al actualizar ganancia';
      res.send(err);
    });
});
// Listar todos los menus.
router.get("/", async(req, res) => {
    data = req.query;
    query = Menu.query();

    if (data.id_res)
        query.where("id_restaurante", data.id_res);

    if (data.id_cat)
        query.where("id_categoria", data.id_cat);

    if (data.q)
        query.where('nombre', 'LIKE', `%${data.q}%`);

    query.orderBy("nombre");
    await query.then(menus => AR.enviarDatos(menus, res))
        .catch(err => {
            console.log(err);
            AR.enviarDatos({r:false}, res);
        });

    // await Menu.query().where("id_restaurante", data.id_res)
    //     .then(async menus => {

    //         // Incluir los nombres de categoria y estatus segun su id.
    //         for (var i = 0; i < menus.length; i++) {
    //             categoria = await CategoriaMenu.query().where("id", menus[i].id_categoria);
    //             menus[i].categoria = categoria[0].nombre;
    //         }
    //         AR.enviarDatos(menus, res);
    //     });
});

// Ver un menu.
router.get("/:id", sesi.validar_general, async(req, res) =>{
    await Menu.query().findById(req.params.id)
        .then(menu => {
            AR.enviarDatos(menu, res);
        });
});

// Listar menus por categoria.
router.get("/categoria/:id", sesi.validar_general, async(req, res) => {
    await Menu.query().where("id_categoria", req.params.id)
        .then(data => { data.r=true; AR.enviarDatos(data, res) })
        .catch(err => AR.enviarDatos({r:false, msj:"Error al recuperar categoria"}, res));
});

// Eliminar menu.
router.delete("/delete/:id", upload.none(), sesi.validar_restaurante, async(req,res)=>{
    data = req.body;

    const verif_ped = await CarritoItems.query()
        .where('id_menu', req.params.id)
        .whereNotNull('id_carrito')
        .catch(console.error)

    const verif_aco = await PedidoAcompañantes.query()
        .join('menus_acompañantes', 'menus_acompañantes.id', 'pedidos_acompañantes.id_acompañante')
        .where('menus_acompañantes.id_menu_acompañante', req.params.id)
        .catch(console.error)

    if (verif_ped.length || verif_aco.length)
        return AR.enviarDatos({ r:false, msj:'No se puede eliminar' }, res);

    // Decrementar las veces que se usa la categoria del menu.
    await Menu.query().where("id", req.params.id)
        .then(async menus => {
            await CategoriaMenu.query()
                .where("id", menus[0].id_categoria)
                .where("veces_usada",  ">", 0)
                .decrement("veces_usada", 1);
        });
    info = await Menu.query().where({ id: req.params.id });
    const eliminado = await Menu.query().delete().where({id:req.params.id})
        .catch(err => {
            console.log(err);
            res.send(err);
        });

    if (eliminado)
    {
        if(info[0])
        {
            MenuAdicionales.query().delete().where("id_menu", info[0].id).catch(err=>console.log(err));
            if(info[0].img){
                await fs.unlink(
                    __dirname + "/../public/uploads/" + info[0].img.split(`\\public\\uploads\\`)[1],
                    function (err){}
                );
            }
        }
        AR.enviarDatos({msj: "Menú eliminado exitosamente"}, res);
    }
    else {
        AR.enviarDatos({msj: "No se pudo eliminar el menú"}, res);
    }
});

// Eliminar adicional.
router.delete("/adicionales/:id", upload.none(), sesi.validar_restaurante, async(req,res)=>{
    let eliminado = await MenuAdicionales.query().delete().where("id", req.params.id)
        .catch(err=>console.log(err));

    if(eliminado)
        return AR.enviarDatos({r:true, msj:"Registro eliminado exitosamente"}, res);

    return AR.enviarDatos({r:false, msj:"Error al eliminar registro"}, res);
});

module.exports = router;