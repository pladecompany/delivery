const Notificaciones = require("../models/Notificacion");
const Firebase = require("../models/Firebase");
var FirebaseB = require("../routes/firebase");
FirebaseB = new FirebaseB();

//Llamar otras librerias
const AR = require("../ApiResponser");
const express = require("express");
const moment = require("moment");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
var archivos = require("../Archivos");
const config = require("../config");
var uuid = require("uuid");
var sesion = require("./sesion");
var sesi = new sesion();

const mailer = require("../mails/Mailer");
const mailp = require("../mails/correo");
const validarMail = require("../functions/validateMail");
//Obligatorio para trabajar con FormData
const multer = require("multer");
const upload = multer();

//Manejador de Errores
const { AuthError, ValidationError } = require("../Errores");

var router = express.Router();


//Obtener usuarios
/*router.get('/',upload.none(),async (req, res, next) => {
    var data = req.query;
    //console.log(data);
    if(data.limit!=null && data.limit!="")
            var limit = data.limit;
        else
            var limit ="5";

    var string = " ORDER BY ID DESC LIMIT "+limit+" ";

    if(data.limit=="todas")
        string = "";
    var notificaciones = await Notificaciones.raw('select * from notificacion where id_receptor='+data.id+' and tabla_usuario="'+data.tipo_usuario+'" '+string+';')
  AR.enviarDatos(notificaciones[0], res);
});*/

router.get('/',upload.none(),async (req, res, next) => {
    var data = req.query;
    if(data.limit!=null && data.limit!="")
        var limit = data.limit;
    else
        var limit ="5";

    var string = " ORDER BY ID DESC LIMIT "+limit+" ";

    if(data.limit=="todas")
        string = " ORDER BY ID DESC";

    var notificaciones = await Notificaciones.raw('select *, DATE_FORMAT(fecha, "%Y-%m-%d %H:%i:%s") as fecha,(SELECT CONCAT("#",a.id," ",a.nombre) FROM restaurantes a WHERE a.id=id_emisor and tabla_usuario_e="restaurantes") as nombre_r, (SELECT CONCAT("#",a.id," ",a.nombre," ",a.apellido) FROM clientes a WHERE a.id=id_emisor and tabla_usuario_e="clientes") as nombre_c, (SELECT CONCAT("#",a.id," ",a.nombre," ",a.apellido) FROM repartidores a WHERE a.id=id_emisor and tabla_usuario_e="repartidores") as nombre_re,(SELECT a.img FROM restaurantes a WHERE a.id=id_emisor and tabla_usuario_e="restaurantes") as img_r, (SELECT a.foto FROM clientes a WHERE a.id=id_emisor and tabla_usuario_e="clientes") as img_c, (SELECT a.img FROM repartidores a WHERE a.id=id_emisor and tabla_usuario_e="repartidores") as img_re from notificacion where id_receptor='+data.id+' and tabla_usuario_r="'+data.tipo_usuario+'" and estatus=0 '+string+';')

    var notificaciones2 = await Notificaciones.raw('select count(*) as count from notificacion where id_receptor='+data.id+' and tabla_usuario_r="'+data.tipo_usuario+'" and  estatus=0;')

    AR.enviarDatos({noti : notificaciones[0], count: notificaciones2[0]}, res);
});

router.get('/vistaall',upload.none(),async (req, res, next) => {
    var data = req.query;
    await Notificaciones.query()
    .update({ estatus:1})
    .where("id_receptor",data.id)
    .where("tabla_usuario_r",data.tipo_usuario)
    .catch(err => {
    console.log(err);
        res.send(err);
    });
    datos = {};
    datos.r = true;
    AR.enviarDatos(datos, res);
});

router.post('/vista/:id',upload.none(),async (req, res, next) => {
    var datos = {
        estatus:1
    }
    await Notificaciones.query()
    .updateAndFetchById(req.params.id, datos)
    .then(notificacion => {
        notificacion.r=true;
        notificacion.msj = "Se editó correctamente";
        AR.enviarDatos(notificacion, res);
    })
    .catch(err => {
    console.log(err);
        res.send(err);
    });
});

router.post('/count',upload.none(),async (req, res, next) => {
    data = req.body;
    var notificaciones = await Notificaciones.raw('select count(*) as count from notificacion where id_receptor='+data.id+' and tabla_usuario_r="'+data.tipo_usuario+'" and  estatus=0;')

    AR.enviarDatos(notificaciones[0], res);
});

router.post('/agregar-eliminar-firebase', upload.none(), async (req, res) => {
    console.log(req.body);
    var obj = {
        'id_usuario' : req.body.id_usuario,
        'token' : req.body.tokenFirebase,
        'tablausuario' : req.body.tablausuario,
        'device': req.body.device ? req.body.device : 2
        };
        await Firebase.query().delete()
        .where({token: obj.token})
        .catch(err => {
            console.log(err);
            res.send(err);
        });
        Firebase.query()
        .insert(obj)
        .then(fire => {
            AR.enviarDatos(fire, res);
        })
        .catch(async err => {

                        Firebase.query()
                        .patchAndFetchById(
                            obj.token,{
                                id_usuario: obj.id_usuario
                            })
                        .then(firebase => {
                            firebase.msj = "Se edito correctamente!";
                            AR.enviarDatos(firebase, res);
                        })
                        .catch(err => {
                            res.send(err);
                        });
        });
});

router.post('/pruebas', upload.none(), async (req, res) => {
   var datap = {
        id_usuario: req.query.id_usuario || req.body.id_usuario,
        title: req.query.texto || 'Pruebas noti',
        body: req.query.texto || 'Notificaciones push',
        tablausuario: req.query.tabla || req.body.tablausuario
    };
    console.log('Push Test: ', datap);
    await FirebaseB.enviarPushNotification(datap, function(datan) { console.log('Push Res:', datan); });
    AR.enviarDatos({msj: true}, res);
});

module.exports = router;
