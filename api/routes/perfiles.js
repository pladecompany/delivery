//llamar al modelo
const Restaurante = require("../models/Restaurante");
const Usuario = require("../models/Usuario");
const Pedido = require("../models/Pedido");

// Otras librerias.
const verificarRIF = require("../functions/verificarRIF");
const AR = require("../ApiResponser");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
var archivos = require("../Archivos");
const config = require("../config");
var fs = require("fs-extra");
var sesion = require("./sesion");
var sesi = new sesion();

// Obligatorio para trabajar con FormData.
const multer = require("multer");
const upload = multer();
const crypto = require("crypto");

const express = require("express");
var router = express.Router();


// Eliminar usuario.
router.delete("/delete_usu/:id", upload.none(), sesi.validar_restaurante, async(req, res) => {
  data = req.query;
  const eliminado = await Usuario.query().delete().where({ id: req.params.id })
    .catch(err => {
      console.log(err);
      res.send(err);
    });

    if (eliminado){
      AR.enviarDatos({ msj: "Usuario eliminado exitosamente" }, res);
    }
    else{ AR.enviarDatos({ msj: "No se puede eliminar el usuario" }, res);}
});

// Obtener restaurante y usuarios.
router.get("/", upload.none(), sesi.validar_restaurante, async(req, res)=>{
	await Restaurante.query().where("id", req.query.id_res)
		.then(async(rest) => {
			rest[0].usuarios = await Usuario.query().where("id_restaurante", rest[0].id);
			//OBTENER CALIFICACION DEL DOOMI
			var lista_ped = await Pedido.query().where("id_restaurante", rest[0].id).where("id_estatus", 11);
			rest[0].pedidos_completados = lista_ped.length;
			var califi_ped = await Pedido.raw("SELECT sum(nota) as num FROM `pedidos` WHERE id_restaurante="+rest[0].id+" and id_estatus=11");
			if(califi_ped[0][0]){
				var tot = 5*rest[0].pedidos_completados;
				rest[0].calificacion = parseFloat((califi_ped[0][0].num / tot)*5);
			}
			else
				rest[0].calificacion = 0;
			//FIN OBTENER CALIFICACION
			AR.enviarDatos(rest[0], res);
		});
});

// Obtener datos del encargado.
router.get("/encargado/:id", upload.none(), sesi.validar_restaurante, async(req, res)=>{
	await Usuario.query().where("id", req.params.id)
		.then(usu=>{
			AR.enviarDatos(usu[0], res);
		})
		.catch(err=>{console.log(err)});
});

// Editar usuario.
router.put("/edit_encargado", archivos.fields([{ name: "imagen", maxCount: 1 }]), sesi.validar_restaurante, async(req, res)=>{
	data = req.body;

	let errr = false;

	// Validar datos.
	if (!data.iden) errr = "ERROR INTERNO: no se especifico el ID del usuario";

	if ((!data.cedula) || (!data.correo))
	{
		errr = "Se deben completar todos los datos del usuario"
	};
	// Fin de validaciones.
	if (errr){
		//ELIMINAR IMAGENES SI NO PASA LAS VALIDACIONES
		if(!data.imgweb){ 
			if(req.files["imagen"])
				await fs.unlink(__dirname + "/../public/uploads/" + req.files["imagen"][0].filename, function (err) { });
		}
		return AR.enviarDatos({r: false, msj: errr}, res);
	}

	datos = {
		id_restaurante: data.id_res,
		nombre_apellido: data.nombre_apellido,
		cedula: data.cedula,
		correo: data.correo,
		telefono: data.telefono,
		whatsapp: data.whatsapp,
		pass: bcrypt.hashSync(data.pass, 8),
	};

	// Guardar ruta de la imagen.
	if(!data.imgweb){ 
		if(req.files["imagen"])
			datos.img = config.rutaArchivo(req.files["imagen"][0].filename);
	}else if(data.imgweb){
      if(data.img){
        var file = "img_per__" + crypto.randomBytes(18).toString("hex") + "__.jpg";
        await require("fs").writeFile(__dirname + "/../public/uploads/" + file, new Buffer.from(data.img.split(",")[1], 'base64'), 'base64', function(err) {
          //console.log(err);
        });
        datos.img = config.rutaArchivo(file);
      }
    }
    var info = await Usuario.query().where({ id: data.iden });//OBTENER DATA ANTES DE ACTUALIZAR
	await Usuario.query().updateAndFetchById(data.iden, datos)
		// .insert(datos)
		.then(async resp => {
			if((req.files["imagen"] || data.img) && + info[0].img)
      			await fs.unlink(__dirname + "/../public/uploads/" + info[0].img.split(`\\public\\uploads\\`)[1], function (err) { });
    
			resp.msj = "Datos actualizados";
			resp.r = true;

			AR.enviarDatos(resp, res);
		})
		.catch(async err => {
			//ELIMINAR IMAGENES POR SI EXISTE ALGUN ERROR
			if(!req.body.imgweb){
				if(req.files["imagen"])
					await fs.unlink(__dirname + "/../public/uploads/" + req.files["imagen"][0].filename, function (err) { });
			}
			console.log(err);
			err.r = false;
			err.msj = "Error al actualizar lso datos";
			res.send(err);
		});
});


module.exports = router;
