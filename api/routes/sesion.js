const config = require("../config");
const Admin = require("../models/Admin");
const Usuario = require("../models/Usuario");
const Cliente = require("../models/Cliente");
const Repartidor = require("../models/Repartidor");

var Sesion = function(){

    this.validar_app = async function(req, res, next) {
        const tipocliente = req.headers['tipo-cliente'];
        if(tipocliente !== 'app'){
            return res.sendStatus(401);
        }
        next();
    }

    this.validar_repartidor = async function(req, res, next) {
        var tok = req.body.token || req.query.token || req.headers.token;
        if (!tok)
            return res.sendStatus(401);

        await Repartidor.query().where("token", tok)
            .then(usuario=>{
                if(usuario.length>0)
                    return next();
                else
                    return res.sendStatus(401);
            })
            .catch(err=>{
                return res.sendStatus(401)
            });
    },

    this.validar_cliente = async function(req, res, next){
        
        var tok = req.body.token || req.query.token || req.headers.token;
        if(!tok)
            return res.sendStatus(401);

        await Cliente.query().where("token", tok)
            .then(usuario=>{
                if(usuario.length>0)
                    return next();
                else
                    return res.sendStatus(401);
            })
            .catch(err=>{
                return res.sendStatus(401);
            });
    },
    this.validar_general = async function(req, res, next){
        var tok = req.body.token   || req.query.token || req.headers.token;
        var tipo = req.body.tipo_usuario   || req.query.tipo_usuario || req.headers.tipo_usuario;

        if(!tipo)
            return res.sendStatus(401);
        if(tipo=='administrador'){
            await Admin.query().where("token",tok).then(usuario => {
                if(usuario.length>0)
                    return next();
                else
                    return res.sendStatus(401);   
            })
            .catch(err => {
                return res.sendStatus(401);
            });
        }
        else if(tipo=='restaurante'){
            await Usuario.query().where("token",tok).then(usuario => {
                if(usuario.length>0)
                    return next();
                else
                    return res.sendStatus(401);   
            })
            .catch(err => {
                return res.sendStatus(401);
            });
        }
        else if(tipo=='cliente'){
            await Cliente.query().where("token", tok)
                .then(usuario=>{
                    if(usuario.length>0)
                        return next();
                    else
                        return res.sendStatus(401);
                })
                .catch(err=>{
                    return res.sendStatus(401);
                });
        }
        else if(tipo=='repartidor'){
            await Repartidor.query().where("token", tok)
                .then(usuario=>{
                    if(usuario.length>0)
                        return next();
                    else
                        return res.sendStatus(401);
                })
                .catch(err=>{
                    return res.sendStatus(401);
                });
        }
        else{
            return res.sendStatus(401);
        }
    },
    this.validar_admin = async function(req, res, next){

        var tok = req.body.token   || req.query.token || req.headers.token;
        if(!tok)
            return res.sendStatus(401);
        await Admin.query().where("token",tok).then(usuario => {
            if(usuario.length>0)
                return next();
            else
                return res.sendStatus(401);   
        })
        .catch(err => {
            return res.sendStatus(401);
        });   
    },
    this.validar_restaurante = async function(req, res, next){

        var tok = req.body.token   || req.query.token || req.headers.token;
        if(!tok)
            return res.sendStatus(401);
        await Usuario.query().where("token",tok).then(usuario => {
            if(usuario.length>0)
                return next();
            else
                return res.sendStatus(401);   
        })
        .catch(err => {
            return res.sendStatus(401);
        });   
    }
}

module.exports = Sesion;
