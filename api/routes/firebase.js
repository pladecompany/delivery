//llamar al modelo
const Firebase = require("../models/Firebase");
const express = require("express");
const config = require("../knexfile");


var FCMN = require('fcm-node');
var serverKey = config.production.firebase.firebase_key_server;
var fcmn = new FCMN(serverKey);
var fcm = require('fcm-notification');
var FCM = new fcm(__dirname.replace("firebase") + '/../servicios/privatekey.json');

var htmlToText = require('html-to-text');
function html_a_text(html) {
    var text = htmlToText.fromString(html, {
      format: {
        heading: function (elem, fn, options) {
          var h = fn(elem.children, options);
          return '====\n' + h.toUpperCase() + '\n====';
        }
      }
    });

    return text;
}

var firebase = function(){


    this.enviarPushNotification = async function(data, retorno){
                data.id_usuario = "" + data.id_usuario + "";
                // console.log(data);
                let sql = `SELECT * FROM firebases WHERE id_usuario='${data.id_usuario}' and tablausuario='${data.tablausuario}' GROUP BY token;`;

                await Firebase.raw(sql
                ).then(async Usuarios => {
                    // console.log("------ tokens One " + Usuarios[0].length + "----------");
                    if(Usuarios[0].length >= 1 || Usuarios[0].length >= "1") {
                      let resul = Usuarios[0] || [];
                      // console.log("------ tokens Two" + resul.length + "----------");
                        if (!data.icon) {
                          data.icon = "https://doomi.app/static/img/logopush.jpg";
                        }
                        
                        for(i = 0 ; i < resul.length; i++){
                            let fila = resul[i];
                            
                            let messaged = { 
                              registration_ids: [fila.token],
                              notification: {
                                  title: html_a_text(data.title),
                                  body: html_a_text(data.body),
                                  icon: data.icon,
                                  sound : "default",
                                  badge: "1"
                              },
                              data: data,
                              priority: 'high',
                              timeToLive: 86400,
                              time_to_live: 86400,
                          };  

                          await fcmn.send(messaged, async function(errr, responsee){
                              if (errr) {
                                console.log("Error push", data.id_usuario);
                                let message = {
                                    token: fila.token, 
                                    notification: {
                                        title: html_a_text(data.title), 
                                        body: html_a_text(data.body)
                                    },
                                    data: data
                                };
                                if (fila.device == 2 || fila.device == 3) {
                                    message.android = {
                                      ttl: 3600 * 1000,
                                      priority: 'normal',
                                      notification: {
                                        title: html_a_text(data.title), 
                                        body: html_a_text(data.body)
                                      }
                                    };
                                    if (data.icon) {
                                      message.android.notification.icon = data.icon;
                                    }
                                  } else if (fila.device == 1) {
                                    message.webpush = {
                                      notification: {
                                        title: html_a_text(data.title), 
                                        body: html_a_text(data.body)
                                      }
                                    };
                                    if (data.icon) {
                                      message.webpush.notification.icon = data.icon;
                                    }
                                }
                                await FCM.send(message, async function(err, response){
                                  if (err) {
                                      console.log("Error push", data.id_usuario);
                                  } else {
                                      console.log("--------------------------------Envio push bien---------------------------");
                                  }
                              });
                              } else {
                                console.log("--------------------------------Envio push bien---------------------------");
                              }
                          });
                            
                        }
                        retorno({r: true});
                    }
                })
                .catch(err => {
                     retorno({r: true});
                });
    }
}

module.exports = firebase;
