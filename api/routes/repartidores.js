//llamar al modelo
const Repartidor = require("../models/Repartidor");
const Pedido = require("../models/Pedido");
const Notificaciones = require("../models/Notificacion");
const Admin = require("../models/Admin");
const { raw, ref } = require("objection");
var Firebase = require("../routes/firebase");
Firebase = new Firebase();

//Llamar otras librerias
const AR = require("../ApiResponser");
const express = require("express");
const moment = require("moment");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const config = require("../config");
var uuid = require("uuid");
var sesion = require("./sesion");
var sesi = new sesion();
var archivos = require("../Archivos");
var fs = require("fs-extra");

const mailer = require("../mails/Mailer");
const mailp = require("../mails/correo");
const validarMail = require("../functions/validateMail");
//Obligatorio para trabajar con FormData
const multer = require("multer");
const upload = multer();
const crypto = require("crypto");

//Manejador de Errores
const { AuthError, ValidationError } = require("../Errores");

var router = express.Router();

// Obtener un nuevo ID de repartidor.
router.get("/nuevo_id", upload.none(), /*sesi.validar_admin, */async(req, res) => {
	await Repartidor.query().orderBy("id_repartidor", "desc")
		.then(repartidores=>{
			let nuevoID = 1;
			if (repartidores[0])
				nuevoID = repartidores[0].id_repartidor + 1;
			AR.enviarDatos({r:true, nuevoID:nuevoID}, res);
		})
		.catch(err=>{console.log(err)});

});

//registrar un Repartidor
router.post("/add", archivos.fields([{ name: "img", maxCount: 1 },{ name: "img1", maxCount: 1 },{ name: "img2", maxCount: 1 },{ name: "img3", maxCount: 1 },{ name: "img_rif", maxCount: 1 }]), /*sesi.validar_admin,*/ async (req, res, next) => {
    data = req.body;
    let errr = false;

    // inicio validaciones
    if (!data.identificacion) errr = "Selecciona un tipo de identificación";
    if (!data.cedula) errr = "Ingresa un número de identificación";
    if (!data.nombre) errr = "Ingresa un nombre";
    if (!data.apellido) errr = "Ingresa un apellido";
    if (!data.tipo_veh) errr = "Selecciona el tipo de vehiculo";
    if (!data.correo) errr = "Ingresa un correo";
    if (!data.sexo) errr = "Selecciona el sexo";
    if (!data.telefono) errr = "Ingresa el teléfono";
    if (!data.whatsapp) errr = "Ingresa el whatsapp";
    if (!data.usuario) errr = "Ingresa un usuario";
    if (!data.pass) errr = "Ingresa una contraseña";
    // if (!data.id_nuevo_repartidor) errr = "Error interno: no se especificó id_repartidor";
    if (req.files && !req.files["img1"] && !req.files["img2"] && !req.files["img3"] && !data.imgweb) errr = "Selecciona una foto del vehículo";
    if (!data.img1 && !data.img2 && !data.img3 && data.imgweb) errr = "Selecciona una foto del vehículo";

    if(data.cedula){
      const verificar_c = await Repartidor.query().where({ cedula: data.cedula });
      if (verificar_c[0]) errr = "El número de cédula ya existe";
    }
    if(data.usuario){
      const verificar_u = await Repartidor.query().where({ usuario: data.usuario });
      if (verificar_u[0]) errr = "El usuario ya existe";
    }
    if(data.correo){
      const verificar_e = await Repartidor.query().where({ correo: data.correo });
      if (verificar_e[0]) errr = "El correo ya existe";
    }
    if(data.telefono){
      const verificar_t = await Repartidor.query().where({ telefono: data.telefono });
      if (verificar_t[0]) errr = "El teléfono ya existe";
    }
    if(data.whatsapp){
      const verificar_w = await Repartidor.query().where({ whatsapp: data.whatsapp });
      if (verificar_w[0]) errr = "El whatsapp ya existe";
    }
    //fin validaciones
    if(errr){
      //ELIMINAR IMAGENES SI NO PASA LAS VALIDACIONES
      if(!data.imgweb){ //saber si se cargan las imagenes desde movil
        if(req.files && req.files["img"])
          await fs.unlink(__dirname + "/../public/uploads/" + req.files["img"][0].filename, function (err) { });
        if(req.files && req.files["img1"])
          await fs.unlink(__dirname + "/../public/uploads/" + req.files["img1"][0].filename, function (err) { });
        if(req.files && req.files["img2"])
          await fs.unlink(__dirname + "/../public/uploads/" + req.files["img2"][0].filename, function (err) { });
        if(req.files && req.files["img3"])
          await fs.unlink(__dirname + "/../public/uploads/" + req.files["img3"][0].filename, function (err) { });
        if(req.files && req.files["img_rif"])
          await fs.unlink(__dirname + "/../public/uploads/" + req.files["img_rif"][0].filename, function (err) { });
      }
      return AR.enviarDatos({r: false, msj: errr}, res);
    }

    let ultimaID = await Repartidor.query().orderBy("id_repartidor", "desc")
      .catch(err=>console.log(err));

    let tipo_cliente = req.headers['tipo-cliente'];

    var datos = {
      nombre: data.nombre,
      apellido: data.apellido,
      identificacion: data.identificacion,
      usuario: data.usuario,
      cedula: data.cedula,
      correo: data.correo,
      pass: bcrypt.hashSync(data.pass, 8),
      id_tipo_vehiculo: data.tipo_veh,
      sexo: data.sexo,
      telefono: data.telefono,
      whatsapp: data.whatsapp,
      domicilio: data.domicilio,
      id_repartidor: ultimaID[0] ? ultimaID[0].id_repartidor+1 : 1,
      estatus: tipo_cliente === 'app' ? false : true,
      metodos_pagos: data.metodo_pago
    };
    //guardar ruta imagen
    if(!data.imgweb){
      if(req.files && req.files["img"])
        datos.img = config.rutaArchivo(req.files["img"][0].filename);
      if(req.files && req.files["img1"])
        datos.foto_1 = config.rutaArchivo(req.files["img1"][0].filename);
      if(req.files && req.files["img2"])
        datos.foto_2 = config.rutaArchivo(req.files["img2"][0].filename);
      if(req.files && req.files["img3"])
        datos.foto_3 = config.rutaArchivo(req.files["img3"][0].filename);
      if(req.files && req.files["img_rif"])
        datos.img_rif = config.rutaArchivo(req.files["img_rif"][0].filename);
    }else if(data.imgweb){
      if(data.img){
        var file = "img_per__" + crypto.randomBytes(18).toString("hex") + "__.jpg";
        await require("fs").writeFile(__dirname + "/../public/uploads/" + file, new Buffer.from(data.img.split(",")[1], 'base64'), 'base64', function(err) {
          //console.log(err);
        });
        datos.img = config.rutaArchivo(file);
      }
      if(data.img1){
        var file1 = "fot_veh__" + crypto.randomBytes(18).toString("hex") + "__.jpg";
        await require("fs").writeFile(__dirname + "/../public/uploads/" + file1, new Buffer.from(data.img1.split(",")[1], 'base64'), 'base64', function(err) {
          // console.log(err);
        });
        datos.foto_1 = config.rutaArchivo(file1);
      }
      if(data.img2){
        var file2 = "fot_veh__" + crypto.randomBytes(18).toString("hex") + "__.jpg";
        await require("fs").writeFile(__dirname + "/../public/uploads/" + file2, new Buffer.from(data.img2.split(",")[1], 'base64'), 'base64', function(err) {
          //console.log(err);
        });
        datos.foto_2 = config.rutaArchivo(file2);
      }
      if(data.img3){
        var file3 = "fot_veh__" + crypto.randomBytes(18).toString("hex") + "__.jpg";
        await require("fs").writeFile(__dirname + "/../public/uploads/" + file3, new Buffer.from(data.img3.split(",")[1], 'base64'), 'base64', function(err) {
          // console.log(err);
        });
        datos.foto_3 = config.rutaArchivo(file3);
      }
      if(data.img4){
        var file4 = "fot_rif__" + crypto.randomBytes(18).toString("hex") + "__.jpg";
        await require("fs").writeFile(__dirname + "/../public/uploads/" + file4, new Buffer.from(data.img4.split(",")[1], 'base64'), 'base64', function(err) {
          // console.log(err);
        });
        datos.img_rif = config.rutaArchivo(file4);
      }
    }

    await Repartidor.query()
    .insert(datos)
    .then(async resp => {
      //enviar notificacion
      resp.msj = "Repartidor registrado";
      resp.r = true;
      AR.enviarDatos(resp, res);
      if (tipo_cliente === 'app') {
        await Notificaciones.query()
        .insert({
            contenido: '¡Bienvenido a Doomi.app!, debes esperar que el administrador apruebe tu cuenta.',
            fecha: new Date(),
            estatus: 0,
            tipo_noti: 0,
            id_receptor: resp.id,
            id_emisor: null,
            tabla_usuario_r: 'repartidores',
            tabla_usuario_e: 'administradores'
        }).catch(async(err)=>{console.log(err);});
        const ADMIN = await Admin.query();
        for (var i = 0; i < ADMIN.length; i++) {
          await Notificaciones.query()
            .insert({
              contenido: 'Esperando verificación de la cuenta',
              fecha: new Date(),
              estatus: 0,
              tipo_noti: 1,
              redireccionar_noti: '?op=repartidores',
              id_usable: resp.id,
              id_receptor: ADMIN[i].id,
              id_emisor: resp.id,
              tabla_usuario_r: 'administradores',
              tabla_usuario_e: 'repartidores'
            }).catch(async(err)=>{console.log(err);});
            await Firebase.enviarPushNotification({
              id_usuario: "" + ADMIN[i].id + "",
              title: 'Nuevo repartidor registrado',
              body: `${data.nombre} ${data.apellido} (${data.correo}), está esperando verificación de la cuenta`,
              tablausuario: 'administradores'
            }, function(datan) { console.log(datan); });
        }
        IO.sockets.emit("RecibirNotificacion", {resp});
      }

      /*var cuerpo = `Hola, ${data.nombre} ${data.apellido}
        <br> Ahora formas parte
        de la Repartidoristración de nuestra plataforma. Tus datos de acceso a
        la plataforma de DOOMI son: <br>
        <b>Usuario: </b> ${data.iden}${data.cedula} <br><b>Contraseña: </b> ${data.pass}
      `;
      mailer.enviarMail({
          direccion: data.correo,
          titulo: "BIENVENIDO A DOOMI.app",
          mensaje: mailp({
              cuerpo: cuerpo,
              titulo: "BIENVENIDO A DOOMI.app"
          })
      });
      await Notificaciones.query()
      .insert({
          descripcion: "Bienvenido a DOOMI.app",
          contenido: "Bienvenido <b>"+data.nombre+" "+data.apellido+"</b> formas parte de nuestra Repartidoristración en MEYERLANDONLINE.COM",
          estatus: 0,
          id_receptor: resp.id,
          tabla_usuario: 'Repartidoristradores',
          tipo_noti: 15,
          id_usable: resp.id,
          fecha: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
      }).catch(err => {console.log(err)});
      */
    })
    .catch(async err => {
      //ELIMINAR IMAGENES POR SI EXISTE ALGUN ERROR
      if(!req.body.imgweb){
        if(req.files && req.files["img"])
          await fs.unlink(__dirname + "/../public/uploads/" + req.files["img"][0].filename, function (err) { });
        if(req.files && req.files["img1"])
          await fs.unlink(__dirname + "/../public/uploads/" + req.files["img1"][0].filename, function (err) { });
        if(req.files && req.files["img2"])
          await fs.unlink(__dirname + "/../public/uploads/" + req.files["img2"][0].filename, function (err) { });
        if(req.files && req.files["img3"])
          await fs.unlink(__dirname + "/../public/uploads/" + req.files["img3"][0].filename, function (err) { });
        if(req.files && req.files["img_rif"])
          await fs.unlink(__dirname + "/../public/uploads/" + req.files["img_rif"][0].filename, function (err) { });
      }
      console.log(err)
      err.r = false;
      err.msj = 'Error al registrar Repartidor';
      res.send(err);
    });

});

//editar Repartidor
router.put("/edit", archivos.fields([{ name: "img", maxCount: 1 },{ name: "img1", maxCount: 1 },{ name: "img2", maxCount: 1 },{ name: "img3", maxCount: 1 },{ name: "img_rif", maxCount: 1 }]), sesi.validar_general, async (req, res, next) => {
  data = req.body;
  let errr = false;
  let verificar;

  // inicio validaciones
  if (!data.identificacion) errr = "Selecciona un tipo de identificación";
  else if (!data.cedula) errr = "Ingresa un número de identelse ificación";
  else if (!data.nombre) errr = "Ingresa un nombre";
  else if (!data.apellido) errr = "Ingresa un apellido";
  else if (!data.tipo_veh) errr = "Selecciona el tipo de vehiculo";
  else if (!data.correo) errr = "Ingresa un correo";
  else if (!data.sexo) errr = "Selecciona el sexo";
  else if (!data.telefono) errr = "Ingresa el teléfono";
  else if (!data.whatsapp) errr = "Ingresa el whatsapp";
  else if (!data.usuario) errr = "Ingresa un usuario";
  else if (!data.iden) errr = "Ingresa la id";

  else if(data.cedula && data.iden){
    verificar = await Repartidor.query().whereNot('id', data.iden).where({ cedula: data.cedula });
    if (verificar[0]) errr = "El número de cédula ya existe";
  }
  else if(data.usuario && data.iden){
    verificar = await Repartidor.query().whereNot('id', data.iden).where({ usuario: data.usuario });
    if (verificar[0]) errr = "El usuario ya existe";
  }
  else if(data.correo && data.iden){
    verificar = await Repartidor.query().whereNot('id', data.iden).where({ correo: data.correo });
    if (verificar[0]) errr = "El correo ya existe";
  }
  else if(data.telefono && data.iden){
    verificar = await Repartidor.query().whereNot('id', data.iden).where({ telefono: data.telefono });
    if (verificar[0]) errr = "El teléfono ya existe";
  }
  else if(data.whatsapp && data.iden){
    verificar = await Repartidor.query().whereNot('id', data.iden).where({ whatsapp: data.whatsapp });
    if (verificar[0]) errr = "El whatsapp ya existe";
  }

  //fin validaciones
  if(errr){
    //ELIMINAR IMAGENES SI NO PASA LAS VALIDACIONES
    if(!data.imgweb){
      if(req.files["img"])
        await fs.unlink(__dirname + "/../public/uploads/" + req.files["img"][0].filename, function (err) { });
      if(req.files["img1"])
        await fs.unlink(__dirname + "/../public/uploads/" + req.files["img1"][0].filename, function (err) { });
      if(req.files["img2"])
        await fs.unlink(__dirname + "/../public/uploads/" + req.files["img2"][0].filename, function (err) { });
      if(req.files["img3"])
        await fs.unlink(__dirname + "/../public/uploads/" + req.files["img3"][0].filename, function (err) { });
      if(req.files["img_rif"])
        await fs.unlink(__dirname + "/../public/uploads/" + req.files["img_rif"][0].filename, function (err) { });
    }
    return AR.enviarDatos({r: false, msj: errr}, res);
  }

  // En caso de estar aprobado, no puede cambiar su foto.
  var info = await Repartidor.query().where({ id: data.iden });//OBTENER DATA ANTES DE ACTUALIZAR
  // if(info[0].aprobado && req.files["img"]){
  //   await fs.unlink(__dirname + "/../public/uploads/" + req.files["img"][0].filename, function (err) { });
  //   delete req.files["img"];
  // }



  var datos = {
    nombre: data.nombre,
    apellido: data.apellido,
    identificacion: data.identificacion,
    usuario: data.usuario,
    cedula: data.cedula,
    correo: data.correo,
    id_tipo_vehiculo: data.tipo_veh,
    sexo: data.sexo,
    telefono: data.telefono,
    whatsapp: data.whatsapp,
    domicilio: data.domicilio,
    metodos_pagos: data.metodo_pago
  }
  if (data.pass && data.pass.length > 0) {
    datos.pass = bcrypt.hashSync(data.pass, 8);
  }
  //guardar ruta imagen
  if(!data.imgweb){
    if(req.files["img"])
      datos.img = config.rutaArchivo(req.files["img"][0].filename);
    if(req.files["img1"])
      datos.foto_1 = config.rutaArchivo(req.files["img1"][0].filename);
    if(req.files["img2"])
      datos.foto_2 = config.rutaArchivo(req.files["img2"][0].filename);
    if(req.files["img3"])
      datos.foto_3 = config.rutaArchivo(req.files["img3"][0].filename);
    if(req.files["img_rif"])
      datos.img_rif = config.rutaArchivo(req.files["img_rif"][0].filename);
  }else if(data.imgweb){
      if(data.img){
        var file = "img_per__" + crypto.randomBytes(18).toString("hex") + "__.jpg";
        await require("fs").writeFile(__dirname + "/../public/uploads/" + file, new Buffer.from(data.img.split(",")[1], 'base64'), 'base64', function(err) {
          //console.log(err);
        });
        datos.img = config.rutaArchivo(file);
      }
      if(data.img1){
        var file1 = "fot_veh__" + crypto.randomBytes(18).toString("hex") + "__.jpg";
        await require("fs").writeFile(__dirname + "/../public/uploads/" + file1, new Buffer.from(data.img1.split(",")[1], 'base64'), 'base64', function(err) {
          // console.log(err);
        });
        datos.foto_1 = config.rutaArchivo(file1);
      }
      if(data.img2){
        var file2 = "fot_veh__" + crypto.randomBytes(18).toString("hex") + "__.jpg";
        await require("fs").writeFile(__dirname + "/../public/uploads/" + file2, new Buffer.from(data.img2.split(",")[1], 'base64'), 'base64', function(err) {
          //console.log(err);
        });
        datos.foto_2 = config.rutaArchivo(file2);
      }
      if(data.img3){
        var file3 = "fot_veh__" + crypto.randomBytes(18).toString("hex") + "__.jpg";
        await require("fs").writeFile(__dirname + "/../public/uploads/" + file3, new Buffer.from(data.img3.split(",")[1], 'base64'), 'base64', function(err) {
          // console.log(err);
        });
        datos.foto_3 = config.rutaArchivo(file3);
      }
      if(data.img_rif){
        var file4 = "fot_rif__" + crypto.randomBytes(18).toString("hex") + "__.jpg";
        await require("fs").writeFile(__dirname + "/../public/uploads/" + file4, new Buffer.from(data.img4.split(",")[1], 'base64'), 'base64', function(err) {
          // console.log(err);
        });
        datos.img_rif = config.rutaArchivo(file4);
      }
    }

  await Repartidor.query().updateAndFetchById(data.iden, datos)
  .then(async resp => {
    //ELIMINAR IMAGENES ANTERIOR SI HA GUARDADO UNA NUEVA
    if (!req.body.imgweb){
      if((req.files["img"] || data.img) && info[0].img)
        await fs.unlink(__dirname + "/../public/uploads/" + info[0].img.split(`\\public\\uploads\\`)[1], function (err) { });
      if((req.files["img1"] || data.img1) && info[0].foto_1)
        await fs.unlink(__dirname + "/../public/uploads/" + info[0].foto_1.split(`\\public\\uploads\\`)[1], function (err) { });
      if((req.files["img2"] || data.img2) && info[0].foto_2)
        await fs.unlink(__dirname + "/../public/uploads/" + info[0].foto_2.split(`\\public\\uploads\\`)[1], function (err) { });
      if((req.files["img3"] || data.img3) && info[0].foto_3)
        await fs.unlink(__dirname + "/../public/uploads/" + info[0].foto_3.split(`\\public\\uploads\\`)[1], function (err) { });
      if((req.files["img_rif"] || data.img4) && info[0].img_rif)
        await fs.unlink(__dirname + "/../public/uploads/" + info[0].img_rif.split(`\\public\\uploads\\`)[1], function (err) { });
    }
    await Notificaciones.query()
      .insert({
          contenido: '¡Perfil actualizado!',
          fecha: new Date(),
          estatus: 0,
          tipo_noti: 1,
          redireccionar_noti: 'ProfileRepartidor',
          id_receptor: data.iden,
          id_emisor: data.iden,
          tabla_usuario_r: 'repartidores',
          tabla_usuario_e: 'repartidores'
      }).catch(async(err)=>{console.log(err);});
    resp.msj = "Datos actualizados con éxito";
    resp.r = true;
    AR.enviarDatos(resp, res);
  })
  .catch(async err => {
    if(!req.body.imgweb){
      if(req.files["img"])
        await fs.unlink(__dirname + "/../public/uploads/" + req.files["img"][0].filename, function (err) { });
      if(req.files["img1"])
        await fs.unlink(__dirname + "/../public/uploads/" + req.files["img1"][0].filename, function (err) { });
      if(req.files["img2"])
        await fs.unlink(__dirname + "/../public/uploads/" + req.files["img2"][0].filename, function (err) { });
      if(req.files["img3"])
        await fs.unlink(__dirname + "/../public/uploads/" + req.files["img3"][0].filename, function (err) { });
      if(req.files["img_rif"])
        await fs.unlink(__dirname + "/../public/uploads/" + req.files["img_rif"][0].filename, function (err) { });
    }
    console.log(err)

    res.send({r:false, msj:'Error al actualizar datos'});
  });
});

//editar Estatus
router.put("/edit_sts", upload.none(), sesi.validar_admin, async (req, res, next) => {
  data = req.body;
  let errr = false;

  // inicio validaciones
  if (!data.sts) errr = "Ingresa el estatus";
  if (!data.iden) errr = "Ingresa la id del Repartidor";

  //fin validaciones
  if(errr){
    return AR.enviarDatos({r: false, msj: errr}, res);
  }

  var datos = {
    estatus: data.sts
  }


  await Repartidor.query().updateAndFetchById(data.iden, datos)
  .then(async resp => {
    var txt_n = '¡Tu cuenta esta Suspendida!';
    if(resp.estatus==1)
      txt_n = '¡Ya tu cuenta no esta Suspendida!';
    await Notificaciones.query()
      .insert({
          contenido: txt_n,
          fecha: new Date(),
          estatus: 0,
          tipo_noti: 0,
          id_receptor: resp.id,
          id_emisor: null,
          tabla_usuario_r: 'repartidores',
          tabla_usuario_e: 'administradores'
      }).then(async noti => {
        IO.sockets.emit("RecibirNotificacionApp", noti);
        var datap = {
          id_usuario: noti.id_receptor,
          title: noti.contenido,
          body: noti.contenido,
          id_usable: noti.id_usable,
          tipo_noti: noti.tipo_noti,
          tablausuario: noti.tabla_usuario_r
        };
        await Firebase.enviarPushNotification(datap, function(datan) { console.log(datan); });
      }).catch(async(err)=>{console.log(err);});

    resp.msj = "Estatus actualizado con éxito";
    resp.r = true;
    AR.enviarDatos(resp, res);
  })
  .catch(async err => {
    console.log(err)
    err.r = false;
    err.msj = 'Error al actualizar estatus';
    res.send(err);
  });

});

//editar cordenadas
router.put("/cambiar_ubicacion", upload.none(), sesi.validar_repartidor, async (req, res, next) => {
  data = req.body;
  let errr = false;

  // inicio validaciones
  if (!data.lat || !data.lon) errr = "Ingresa las cordenadas";
  if (!data.iden) errr = "Ingresa la id del Repartidor";

  //fin validaciones
  if(errr){
    return AR.enviarDatos({r: false, msj: errr}, res);
  }

  var datos = {
    lat: data.lat,
    lon: data.lon
  }

  await Repartidor.query().updateAndFetchById(data.iden, datos)
  .then(async resp => {
    var rutaapp = "RecibirUbicacionRepApp"+resp.id;
    IO.sockets.emit(rutaapp, resp);
    resp.msj = "Cordenadas actualizada con éxito";
    resp.r = true;
    AR.enviarDatos(resp, res);
  })
  .catch(async err => {
    console.log(err)
    err.r = false;
    err.msj = 'Error al actualizar cordenadas';
    res.send(err);
  });

});

// Aprobar o rechazar.
router.patch("/aprobacion", upload.none(), sesi.validar_admin, async(req, res) => {
  const id = req.body.id || req.body.iden;
  const observaciones = req.body.observaciones;
  let aprobado = req.body.aprobado || req.body.aprobacion;

  // Verificar.
  if(!id || !aprobado)
    return res.status(400).send({r:false, msj:'Datos incompletos'});

  aprobado = (aprobado==1 || aprobado=="true") ? 1 : 0;

  await Repartidor.query()
    .findById(id)
    .patch({
      aprobado,
      observaciones,
      estatus: aprobado ? 1 : undefined})
    .catch(err => console.log(err));



  if(aprobado==1){
    var txt = 'Su cuenta ha sido aprobada, puede disfrutar de la aplicación.';
  }else{
    var txt = 'Su cuenta ha sido rechazada por: '+observaciones;
    await Repartidor.query()
      .findById(id).del()
      .catch(console.error);
  }
  await Notificaciones.query()
    .insert({
      contenido: txt,
      fecha: new Date(),
      estatus: 0,
      tipo_noti: 0,
      id_receptor: id,
      id_emisor: null,
      tabla_usuario_r: 'repartidores',
      tabla_usuario_e: 'administradores'
    }).then(async noti => {
      IO.sockets.emit("RecibirNotificacionApp", noti);
      var datap = {
        id_usuario: noti.id_receptor,
        title: noti.contenido,
        body: noti.contenido,
        id_usable: noti.id_usable,
        tipo_noti: noti.tipo_noti,
        tablausuario: noti.tabla_usuario_r
      };
      await Firebase.enviarPushNotification(datap, function(datan) { console.log(datan); });
    }).catch(async(err)=>{console.log(err);});
  return AR.enviarDatos({r:true, msj:'Datos actualizados'}, res);
});

//editar Estatus a trabajar
router.put("/edit_sts_tra", upload.none(), sesi.validar_repartidor, async (req, res, next) => {
  data = req.body;
  let errr = false;

  // inicio validaciones
  if (!data.sts) errr = "Ingresa el estatus";
  if (!data.iden) errr = "Ingresa la id del Repartidor";

  //fin validaciones
  if(errr){
    return AR.enviarDatos({r: false, msj: errr}, res);
  }

  var datos = {
    trabajar: data.sts=="true" ? 1 : 0
  }
  await Repartidor.query().updateAndFetchById(data.iden, datos)
  .then(async resp => {
    resp.msj = "Estatus actualizado con éxito";
    resp.r = true;
    AR.enviarDatos(resp, res);
  })
  .catch(async err => {
    console.log(err)
    err.r = false;
    err.msj = 'Error al actualizar estatus';
    res.send(err);
  });

});

router.get("/buscar-trabajando", sesi.validar_general, async(req, res) => {
  await Repartidor.query()
    .where('estatus',1)
    .where('aprobado',1)
    .where('trabajar',1)
    .then(ret => {
      AR.enviarDatos(ret, res);
    });
});

router.get("/acumular-trabajando", sesi.validar_general, async(req, res) => {
  await Repartidor.query()
    .where('estatus',1)
    .where('aprobado',1)
    .where('trabajar',1)
    .count()
    .then(ret => {
      AR.enviarDatos(ret, res);
    });
});
//listar todos los Repartidor
router.get("/", sesi.validar_general, async(req, res) => {
  await Repartidor.query()
    .select(
      'repartidores.*',
      b => b.from('pedidos').count().where('id_repartidor', ref('repartidores.id')).as('bloqueado')
      )
    .then(ret => {
    AR.enviarDatos(ret, res);
  });
});

// Listar restaurantes
router.get("/buscar", async(req, res) => {
  var data = req.query;
  const resp = await Repartidor
  .raw("SELECT id, CONCAT(nombre,' ',apellido) as text FROM repartidores WHERE CONCAT(nombre,' ',apellido) LIKE '%"+data.q+"%' ORDER BY nombre,apellido LIMIT 10")
  .catch(console.error);
  AR.enviarDatos(resp[0], res);
});

//ver un Repartidor
router.get("/:id(\\d+)/", sesi.validar_general, async(req, res) => {
  // const tipo_usu = req.body.tipo_usuario   || req.query.tipo_usuario || req.headers.tipo_usuario;
  // if(tipo_usu == 'cliente')
  await Repartidor.query()
    .leftJoin('pedidos', 'pedidos.id_repartidor', 'repartidores.id')
    .select(
      'repartidores.*',

      //REDONDEAR (  PROMEDIO  ||  0     )
      raw('ROUND(  IFNULL(AVG(??), 0)  ,1)', 'pedidos.nota_rep').as('nota')
      )
    .findById(req.params.id).then(ret => {
      AR.enviarDatos(ret, res);
  });
});

// Verificar si el repartidor esta libre de pedidos.
router.get("/esta_libre", sesi.validar_repartidor, async(req, res) => {
  const id_repartidor = req.headers.id_usuario;

  const ped = await Pedido.query()
    .findOne({ id_repartidor })
    .whereIn('id_estatus', [6,7,8,9,10,12])
    .catch(console.error);

  // if (ped && [8, 9, 10, 12].includes(+ped.id_estatus))
  //   return AR.enviarDatos({r:false}, res);

  if (ped){
    return AR.enviarDatos({r:false}, res);
  }

  AR.enviarDatos({r:true}, res);
});

// Liberar repartidor.
router.patch("/liberar/:id(\\d+)", sesi.validar_admin, async(req, res) => {
  const id_admin = req.headers.id_usuario || req.headers.id_admin;

  const estatus_a_listar = [
    // 5, // En proceso
    6, // Preparando
    7, // Finalizado
    8, // Por entregar
    9, // Entregado
    10, // en transito
    12 // entregado al cliente
  ];

  await Pedido.query()
    // .findOne({ id_repartidor: req.params.id })
    .where('id_repartidor', req.params.id)
    .whereIn('id_estatus', estatus_a_listar)
    .patch({ id_repartidor: null })
    .then(() => {

      let datos_noti_rep = {
        contenido: 'Administración te ha liberado de pedidos',
        fecha: new Date(),
        estatus: 0,
        tipo_noti: 1,
        redireccionar_noti: 'Historial',
        id_usable: 0,
        id_receptor: req.params.id,
        id_emisor: id_admin,
        tabla_usuario_r: 'repartidores',
        tabla_usuario_e: 'administradores'
      };

      Notificaciones.query()
        .insert(datos_noti_rep)
        .then(async noti => {
          IO.sockets.emit("RecibirNotificacionApp", noti);
          var datap = {
            id_usuario: noti.id_receptor,
            title: noti.contenido,
            body: noti.contenido,
            id_usable: noti.id_usable,
            tipo_noti: noti.tipo_noti,
            ruta: noti.redireccionar_noti,
            tablausuario: noti.tabla_usuario_r
          };
          await Firebase.enviarPushNotification(datap, function(datan) { console.log(datan); });
        })
        .catch(async(err)=>{console.log(err);});

      AR.enviarDatos({r:true}, res)
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(500);
    });

});

//eliminar Repartidor
/*router.delete("/delete/:id", upload.none(), sesi.validar_admin, async(req, res) => {
  data = req.query;
  var info = await Repartidor.query().where({ id: req.params.id })
  const eliminado = await Repartidor.query().delete().where({ id: req.params.id })
    .catch(err => {
      console.log(err);
      res.send(err);
    });

    if (eliminado){
      if(info[0]){
        if(info[0].img)
          await fs.unlink(__dirname + "/../public/uploads/" + info[0].img.split(`\\public\\uploads\\`)[1], function (err) { });
        if(info[0].foto_1)
          await fs.unlink(__dirname + "/../public/uploads/" + info[0].foto_1.split(`\\public\\uploads\\`)[1], function (err) { });
        if(info[0].foto_2)
          await fs.unlink(__dirname + "/../public/uploads/" + info[0].foto_2.split(`\\public\\uploads\\`)[1], function (err) { });
        if(info[0].foto_3)
          await fs.unlink(__dirname + "/../public/uploads/" + info[0].foto_3.split(`\\public\\uploads\\`)[1], function (err) { });
      }
      AR.enviarDatos({ msj: "Registro eliminado exitosamente" }, res);
    }
    else{ AR.enviarDatos({ msj: "No se puede eliminar el Repartidor" }, res);}
});*/

module.exports = router;
