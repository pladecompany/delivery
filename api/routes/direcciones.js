//llamar al modelo
const Direccion = require("../models/Direccion");

//Llamar otras librerias
const AR = require("../ApiResponser");
const express = require("express");
const moment = require("moment");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const config = require("../config");
var uuid = require("uuid");
var sesion = require("./sesion");
var sesi = new sesion();
var archivos = require("../Archivos");
var fs = require("fs-extra");

//Obligatorio para trabajar con FormData
const multer = require("multer");
const upload = multer();

//Manejador de Errores
const { AuthError, ValidationError } = require("../Errores");

var router = express.Router();

//registrar 
router.post("/add", upload.none(), /*sesi.validar_admin,*/ async (req, res, next) => {
    data = req.body;

    let errr = false;

    // inicio validaciones
    if (!data.nombre_dir) errr = "Ingresa el nombre de tu dirección";
    if (!data.direc_dir) errr = "Ingresa la dirección";
    if (!data.ref_dir) errr = "Ingresa una referencia";
    if (!data.lat_dir || !data.lon_dir) errr = "Debes seleccionar la ubicación en el mapa";
    //fin validaciones
    if(errr) return AR.enviarDatos({r: false, msj: errr}, res);

    await Direccion.query()
    .insert({
        nom_dir: data.nombre_dir,
        dir_dir: data.direc_dir,
        lat_dir: data.lat_dir,
        lon_dir: data.lon_dir,
        id_cliente: data.id_cli,
        ref_dir: data.ref_dir
    })
    .then(async resp => {
      resp.msj = "Dirección registrada";
      resp.r = true;
      AR.enviarDatos(resp, res);
    })
    .catch(err => {
      console.log(err)
      err.r = false;
      err.msj = 'Error al registrar Dirección';
      res.send(err);
    });

});

//editar 
router.put("/edit", upload.none(), /*sesi.validar_admin,*/ async (req, res, next) => {
  data = req.body;
  let errr = false;

  // inicio validaciones
  if (!data.nombre_dir) errr = "Ingresa el nombre de tu dirección";
  if (!data.direc_dir) errr = "Ingresa la dirección";
  if (!data.ref_dir) errr = "Ingresa una referencia";
  if (!data.lat_dir || !data.lon_dir) errr = "Debes seleccionar la ubicación en el mapa";
  if (!data.iden) errr = "Ingresa la id";
  //fin validaciones
  if(errr) return AR.enviarDatos({r: false, msj: errr}, res);

  var datos = {
    nom_dir: data.nombre_dir,
    dir_dir: data.direc_dir,
    lat_dir: data.lat_dir,
    lon_dir: data.lon_dir,
    ref_dir: data.ref_dir
  }

  await Direccion.query().updateAndFetchById(data.iden, datos)
  .then(async resp => {
    resp.msj = "Datos actualizados con éxito";
    resp.r = true;
    AR.enviarDatos(resp, res);
  })
  .catch(err => {
    console.log(err)
    err.r = false;
    err.msj = 'Error al actualizar datos';
    res.send(err);
  });

});
//listar 
router.get("/", async(req, res) => {
  var id_cli = 0;
  data = req.query;
  if(data.id_cli)
    id_cli = data.id_cli;
  await Direccion.query().where('id_cliente',id_cli)
      .then(ret => {
      AR.enviarDatos(ret, res);
  });
});

//ver 
router.get("/:id", async(req, res) => {
  await Direccion.query().where("id",req.params.id).then(ret => {
      AR.enviarDatos(ret[0], res);
  });

});

//eliminar 
router.delete("/delete/:id", upload.none(), /*sesi.validar_admin,*/ async(req, res) => {
  data = req.query;
  const eliminado = await Direccion.query().delete().where({ id: req.params.id })
    .catch(err => {
      console.log(err);
      res.send(err);
    });

    if (eliminado){
      AR.enviarDatos({ msj: "Registro eliminado exitosamente",r: true }, res);
    }
    else{ AR.enviarDatos({ msj: "No se puede eliminar la dirección",r: false }, res);}
});


module.exports = router;
