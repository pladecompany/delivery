//llamar al modelo
const Metodos = require("../models/Metodos");


//Llamar otras librerias
const AR = require("../ApiResponser");
const express = require("express");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const config = require("../config");
var uuid = require("uuid");
var sesion = require("./sesion");
var sesi = new sesion();


//Obligatorio para trabajar con FormData
const multer = require("multer");
const upload = multer();

//Manejador de Errores
const { AuthError, ValidationError } = require("../Errores");

var router = express.Router();

//registrar un aula
router.post("/add", upload.none(), sesi.validar_general, async (req, res, next) => {
    data = req.body;

    let errr = false;

    // inicio validaciones
    if (!data.nombre) errr = "Ingresa un nombre";
    if (!data.id_res && data.tipo_usu!='administrador') errr = "Ingresa id del restaurante";
    //fin validaciones
    if(errr) return AR.enviarDatos({r: false, msj: errr}, res);

    var datos = {
      nombre: data.nombre
    };
    if(data.tipo_usu!='administrador')
      datos.id_restaurante = data.id_res;

    await Metodos.query()
    .insert(datos)
    .then(resp => {

      resp.msj = "Método de pago registrado";
      resp.r = true;
      AR.enviarDatos(resp, res);

    })
    .catch(err => {
      console.log(err)
      err.r = false;
      err.msj = 'Error al registrar metodo';
      res.send(err);
    });

});
router.put("/edit", upload.none(), sesi.validar_general, async (req, res, next) => {
    data = req.body;

    let errr = false;

    // inicio validaciones
    if (!data.nombre) errr = "Ingresa un nombre";
    if (!data.iden) errr = "Ingresa la id";
    //fin validaciones
    if(errr) return AR.enviarDatos({r: false, msj: errr}, res);
    var datos = {
        nombre: data.nombre,
    };
  
   const aula = await Metodos.query()
    .updateAndFetchById(data.iden, datos)
    .then(resp => {
      
      resp.msj = "Método de pago actualizado";
      resp.r = true;
      AR.enviarDatos(resp, res);

    })
    .catch(err => {
      console.log(err)
      err.r = false;
      err.msj = 'Error al editar metodo';
      res.send(err);
    });

});

router.delete("/delete/:id", upload.none(), sesi.validar_general, async(req, res) => {
        data = req.query;
        const eliminado = await Metodos.query()
            .delete()
            .where({ id: req.params.id })
            .catch(err => {
                console.log(err);
                res.send(err);
            });
        if (eliminado){
          AR.enviarDatos({ msj: "Registro eliminado exitosamente" }, res);}
        else{ AR.enviarDatos("0", res);}
});

router.get("/", async(req, res) => {
  data = req.query;
  if(data.tipo_usu=='administrador'){
    await Metodos.query().where("id_restaurante",null)
    .then(metodos => {
      AR.enviarDatos(metodos, res);
    });
  }else{
    await Metodos.query().where("id_restaurante",data.id_res)
    .then(metodos => {
      AR.enviarDatos(metodos, res);
    });
  }
});
router.get("/:id", async(req, res) => {
    await Metodos.query()
        .findById(req.params.id)
        .then(metodos => {
        AR.enviarDatos(metodos, res);
    });
});

module.exports = router;