const { raw } = require('objection');
const moment = require("moment");
moment.locale("es");
const Restaurante = require("../models/Restaurante");
const Horarios = require("../models/Horarios");
const HorariosProgramados = require("../models/HorariosProgramados");

const Pedido = require("../models/Pedido");
const Repartidor = require("../models/Repartidor");
const PedidoEstatus = require("../models/PedidoEstatus");
const Notificaciones = require("../models/Notificacion");
var Firebase = require("../routes/firebase");
Firebase = new Firebase();
const fs = require("fs-extra");

//const Notificaciones = require("../models/Notificacion");

var horario_ver = ["Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo"];
arr = []
class Verificar {

  static abrir() {
    let h_a = moment().format('HH:mm:ss');
    let f_a = moment().format('e');
      //console.log('entre',h_a,horario_ver[f_a]);
      Restaurante.query()
        .join("horarios","horarios.id_restaurante","restaurantes.id")
        .where('restaurantes.laborando',0)
        .where('restaurantes.vacaciones',0)
        .where('horarios.inicio_hor','<=',h_a)
        .where('horarios.fin_hor','>=',h_a)
        .where('horarios.dia',horario_ver[f_a])
        .where('horarios.estatus',1)
        .where('restaurantes.dia_cerrado','<>',horario_ver[f_a])
        .then(async ret => {
          //console.log(ret);
          if(ret.length>0){
            for (var i = 0; i < ret.length; i++) {
              await Restaurante.query().updateAndFetchById(ret[i].id, {laborando: 1, dia_cerrado:0}).catch(err => {console.log(err)});
            }
          }
        }).catch(err => {console.log(err)});
  }

  static cerrar() {
    let h_a = moment().format('HH:mm:ss');
    let f_a = moment().format('e');
      //console.log('entre_ce',h_a,horario_ver[f_a]);
      Restaurante.query()
        .where('laborando',1)
        .where('vacaciones',0)
        .where('dia_cerrado','<>',horario_ver[f_a])
        .then(async ret => {
          //console.log(ret);
          if(ret.length>0){
            for (var i = 0; i < ret.length; i++) {
              let res = await Horarios.query()
              .where({id_restaurante: ret[i].id, dia: horario_ver[f_a]})
              .where('inicio_hor','<=',h_a)
              .where('fin_hor','>=',h_a)
              .where('dia',horario_ver[f_a])
              .where('estatus',1)
              .catch(err => {console.log(err)});
              if(res.length==0){
                await Restaurante.query().updateAndFetchById(ret[i].id, {laborando: 0, dia_cerrado:0}).catch(err => {console.log(err)});
              }
            }
          }
        }).catch(err => {console.log(err)});
  }

  static horarios_programados() {
    HorariosProgramados.query()
      .where('fin', '<', moment().format('YYYY-MM-DD HH:mm'))
      .delete().then(() => {}).catch(console.error);
  }

  static quitarfavorito() {
    let fec_b_f = moment().format('YYYY-MM-DD HH:mm:ss');

    Pedido.query()
        .where('fecha_buscar_fav','<',fec_b_f)
        .where('buscar_favoritos',1)
        .havingNull('id_repartidor')
        .then(async ped => {
          //console.log(ped);
          if(ped.length>0){
            const configuracion = JSON.parse(fs.readFileSync(__dirname+"/../configuracion.json"));
            for (var i = 0; i < ped.length; i++) {
              const restaurante_info = await Restaurante.query().where("id",ped[i].id_restaurante).catch(async err => {console.log("error: "+err)});
              if(restaurante_info[0]){
                const lat = restaurante_info[0].lat;
                const lon = restaurante_info[0].lon;
                await Repartidor.query()
                .select('*',raw(`
                  (acos(sin(radians(lat)) *
                  sin(radians(:lat)) + cos(radians(lat)) *
                  cos(radians(:lat)) *
                  cos(radians(lon) - radians(:lon))) * 6370.986)`, {lat,lon}).as('distancia'))
                .having('distancia', '<=', configuracion.kmd)
                .where('estatus',1).where('aprobado',1)
                .then(async repartidores =>{
                  for (var re = 0; re < repartidores.length; re++) {
                    var datap = {
                      id_usuario: repartidores[re].id,
                      title: 'Nuevo pedido',
                      body: 'Nuevo pedido generado por un cliente',
                      id_usable: ped[i].id,
                      tipo_noti: 2,
                      ruta: 'Vermas1',
                      tablausuario: 'repartidores'
                    };
                    //console.log(datap);
                    await Firebase.enviarPushNotification(datap, function(datan) { console.log(datan); });
                  }
                })
              }
              await Pedido.query().updateAndFetchById(ped[i].id, {buscar_favoritos:false})
            }
            IO.sockets.emit("ActualizarAppRep", {act_ped: 1});
          }
        }).catch(err => {console.log(err)});
  }

  static terminartemporizador() {
    let fec_b_f = moment().format('YYYY-MM-DD HH:mm:ss');

    Pedido.query()
        .where('fecha_buscar_fav','<',fec_b_f)
        .where('buscar_favoritos',2)
        .where('modalidad','<>','Pickup')
        .havingNull('id_repartidor')
        .then(async ped => {

          if(ped.length>0){
            const configuracion = JSON.parse(fs.readFileSync(__dirname+"/../configuracion.json"));

            for (var i = 0; i < ped.length; i++) {
              const restaurante_info = await Restaurante.query().where("id",ped[i].id_restaurante).catch(async err => {console.log("error: "+err)});

              if(restaurante_info[0]){
                const lat = restaurante_info[0].lat;
                const lon = restaurante_info[0].lon;
                await Repartidor.query()
                .join('repartidores_favoritos','repartidores_favoritos.id_repartidor','repartidores.id')
                .select('repartidores.*',raw(`
                  (acos(sin(radians(lat)) *
                  sin(radians(:lat)) + cos(radians(lat)) *
                  cos(radians(:lat)) *
                  cos(radians(lon) - radians(:lon))) * 6370.986)`, {lat,lon}).as('distancia'))
                .having('distancia', '<=', configuracion.kmd)
                .where('repartidores_favoritos.id_restaurante',restaurante_info[0].id)
                .where('repartidores.estatus',1).where('repartidores.aprobado',1)
                .then(async repartidores =>{
                  if(repartidores.length>0){
                    let fec_bus = moment().add(configuracion.minutos, 'minutes').format('YYYY-MM-DD HH:mm:ss');
                    await Pedido.query().updateAndFetchById(ped[i].id, {buscar_favoritos:1,fecha_buscar_fav:fec_bus})
                    for (var re = 0; re < repartidores.length; re++) {
                      var datap = {
                                id_usuario: repartidores[re].id,
                                title: 'Nuevo pedido',
                                body: 'Nuevo pedido generado por un cliente',
                                id_usable: ped[i].id,
                                tipo_noti: 2,
                                ruta: 'Vermas1',
                            tablausuario: 'repartidores'
                            };
                            await Firebase.enviarPushNotification(datap, function(datan) { console.log(datan); });
                    }
                  }else{
                    await Repartidor.query()
                    .select('*',raw(`
                      (acos(sin(radians(lat)) *
                      sin(radians(:lat)) + cos(radians(lat)) *
                      cos(radians(:lat)) *
                      cos(radians(lon) - radians(:lon))) * 6370.986)`, {lat,lon}).as('distancia'))
                    .having('distancia', '<=', configuracion.kmd)
                    .where('estatus',1).where('aprobado',1)
                    .then(async repartidores =>{
                      await Pedido.query().updateAndFetchById(ped[i].id, {buscar_favoritos:0})
                      for (var re = 0; re < repartidores.length; re++) {
                        var datap = {
                                  id_usuario: repartidores[re].id,
                                  title: 'Nuevo pedido',
                                  body: 'Nuevo pedido generado por un cliente',
                                  id_usable: ped[i].id,
                                  tipo_noti: 2,
                                  ruta: 'Vermas1',
                              tablausuario: 'repartidores'
                              };
                              await Firebase.enviarPushNotification(datap, function(datan) { console.log(datan); });
                      }
                    })
                  }
                }).catch(console.error);
              }
              IO.sockets.emit("Recibirpedido", ped[i]);
            }

            IO.sockets.emit("ActualizarAppRep", {act_ped: 1});
          }
        }).catch(err => {console.log(err)});
  }

  static terminartemporizadorpickup() {
    let fec_b_f = moment().format('YYYY-MM-DD HH:mm:ss');

    Pedido.query()
        .where('fecha_buscar_fav','<',fec_b_f)
        .where('id_estatus',6)
        .where('buscar_favoritos',2)
        .where('modalidad','Pickup')
        .then(async ped => {
          if(ped.length>0){
            for (var i = 0; i < ped.length; i++) {
              await Pedido.query().updateAndFetchById(ped[i].id, {buscar_favoritos:0,id_estatus:8});
              var info = await PedidoEstatus.query().where({ id: 8 });//OBTENER DATA DEL STATUS
              var txt_noti = "El pedido #" + ped[i].id + " cambio de estatus a: " + info[0].nombre;

              var data_noti = {
                contenido: txt_noti,
                fecha: new Date(),
                estatus: 0,
                tipo_noti: 2,
                redireccionar_noti: 'Aviso',
                id_usable: ped[i].id,
                id_receptor: ped[i].id_cliente,
                id_emisor: ped[i].id_restaurante,
                tabla_usuario_r: 'clientes',
                tabla_usuario_e: 'restaurantes'
              }
              await Notificaciones.query()
                .insert(data_noti)
                .then(async noti => {
                  var datap = {
                      id_usuario: noti.id_receptor,
                      title: noti.contenido,
                      body: noti.contenido,
                      id_usable: noti.id_usable,
                      tipo_noti: noti.tipo_noti,
                      ruta: noti.redireccionar_noti,
                      tablausuario: noti.tabla_usuario_r
                    };
                      IO.sockets.emit("RecibirNotificacion", noti);
                      IO.sockets.emit("Recibirpedido", ped[i]);
                      IO.sockets.emit("RecibirNotificacionApp", noti);
                      await Firebase.enviarPushNotification(datap, function(datan) { console.log(datan); });
                });
            }

            //IO.sockets.emit("ActualizarAppRep", {act_ped: 1});
          }
        }).catch(err => {console.log(err)});
  }
}

module.exports = Verificar;
