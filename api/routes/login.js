//llamar al modelo
const Admin = require("../models/Admin");
const Usuario_info = require("../models/Usuario_info");
const Usuario = require("../models/Usuario");
const Cliente = require("../models/Cliente");
const Repartidor = require("../models/Repartidor");
const Notificaciones = require("../models/Notificacion");
const Pedido = require("../models/Pedido");

//Llamar otras librerias
const AR = require("../ApiResponser");
const express = require("express");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const config = require("../config");
const uuid = require("uuid");
const sesion = require("./sesion");
const sesi = new sesion();
const genToken = require('../functions/genToken');

const mailer = require("../mails/Mailer");
const mailp = require("../mails/correo");
//Obligatorio para trabajar con FormData
const multer = require("multer");
const upload = multer();

//Manejador de Errores
const { AuthError, ValidationError } = require("../Errores");

const router = express.Router();


//hacer login - mediante web.
router.post("/", upload.none(), async (req, res, next) => {
  data = req.body;
  clavecorrecta = false;
  dataall = [];

  let errr = false;
  // inicio validaciones
  if (!data.correo) errr = "Ingresa el correo";
  if (!data.pass) errr = "Ingresa una contraseña";
  //fin validaciones
  if(errr) return AR.enviarDatos({r: false, msj: errr}, res);


  admin = await Admin.query().where({'correo':data.correo}).catch(async err => {console.log("error admin: "+err)})

  if(admin[0]){
    admin[0].token  = uuid.v1();
    admin[0].r=true;
    admin[0].tipo_usu  = 'administrador';
    admin[0].text_tipo_usu  = 'Administrador';
    dataall.push(admin[0]);

    let passwordIsValid = bcrypt.compareSync(data.pass, admin[0].pass);
    if (passwordIsValid == true) {
      clavecorrecta = true;
    }
  }

  user = await Usuario_info.query().where({'correo':data.correo}).catch(async err => {console.log("error usuario: "+err)})

  if(user[0]){
    user[0].token  = uuid.v1();
    user[0].r=true;
    user[0].tipo_usu  = 'restaurante';
    user[0].text_tipo_usu  = 'Doomialiado';
    dataall.push(user[0]);

    let passwordIsValid = bcrypt.compareSync(data.pass, user[0].pass);
    if (passwordIsValid == true) {
      clavecorrecta = true;
    }
  }

  if(clavecorrecta){
    //GUARDAR TOKEN SI HAY CUENTAS EN LOS DIFERENTES NIVELES DE USUARIOS
    if(admin[0])
      await Admin.query().updateAndFetchById(admin[0].id, {token:admin[0].token});
    else if(user[0])
      await Usuario_info.query().updateAndFetchById(user[0].id, {token:user[0].token});
    AR.enviarDatos(dataall, res);

  }else{
    dataall = {}
    dataall.msj = "Datos incorrectos";
    dataall.r = false;
    AR.enviarDatos(dataall, res);
  }
});

// Hacer login - mediante app.
router.post("/app", upload.none(), async (req, res, next) => {
  let data = req.body;
  let clavecorrecta = false;
  let dataall = [];
  let tipocliente = req.headers['tipo-cliente'];
  let recordar = data.recordar;

  let errr = false;
  // inicio validaciones
  if (!data.correo) errr = "Ingresa el correo";
  if (!data.pass) errr = "Ingresa una contraseña";
  //fin validaciones
  if(errr) return AR.enviarDatos({r: false, msj: errr}, res);

  client = await Cliente.query().where({'correo':data.correo})
    .catch(async err => {console.log("error usuario: "+err)});

  // if(client[0] && !client[0].activado)
  //   return AR.enviarDatos({r: false, msj: "El correo no ha sido activado"}, res);

  if(client[0]){
    client[0].token  = uuid.v1();
    client[0].r=true;
    client[0].tipo_usu  = 'cliente';
    client[0].text_tipo_usu  = 'Cliente';
    client[0].refresh_token = recordar ? uuid.v4() : null;
    dataall.push(client[0]);

    let passwordIsValid = bcrypt.compareSync(data.pass, client[0].pass);
    if (passwordIsValid == true) {
      clavecorrecta = true;
    }
  }

  if(clavecorrecta){
    //GUARDAR TOKEN SI HAY CUENTAS EN LOS DIFERENTES NIVELES DE USUARIOS
    // if(admin[0])
    //   await Admin.query().updateAndFetchById(admin[0].id, {token:admin[0].token});
    if(client[0])
      await Cliente.query().updateAndFetchById(client[0].id, {token:client[0].token, refresh_token:client[0].refresh_token});

    AR.enviarDatos(dataall, res);

  }else{
    dataall = {}
    dataall.msj = errr || "Datos incorrectos";
    dataall.r = false;
    AR.enviarDatos(dataall, res);
  }
});

router.post("/app-re", upload.none(), async (req, res, next) => {
  let data = req.body;
  let clavecorrecta = false;
  let dataall = [];
  let tipocliente = req.headers['tipo-cliente'];
  let recordar = data.recordar;

  let errr = false;
  // inicio validaciones
  if (!data.correo) errr = "Ingresa el correo";
  if (!data.pass) errr = "Ingresa una contraseña";
  //fin validaciones
  if(errr) return AR.enviarDatos({r: false, msj: errr}, res);


  repar = await Repartidor.query().where({'correo':data.correo}).catch(async err => {console.log("error repartidor: "+err)})

  if(repar[0]){
    repar[0].token  = uuid.v1();
    repar[0].r=true;
    repar[0].tipo_usu  = 'repartidor';
    repar[0].text_tipo_usu  = 'Repartidor';
    repar[0].refresh_token = recordar ? uuid.v4() : null;

    let ped = await Pedido.query()
      .select('id', 'id_estatus')
      .where({id_repartidor: repar[0].id})
      .whereIn('id_estatus', [6,7,8,9,10,12])
      .first();

    repar[0].pedido_actual = ped ? ped.id : null;
    repar[0].pedido = ped ? { id: ped.id, id_estatus: ped.id_estatus } : null;

    dataall.push(repar[0]);

    let passwordIsValid = bcrypt.compareSync(data.pass, repar[0].pass);
    if (passwordIsValid == true) {
      clavecorrecta = true;
    }

    // if(tipocliente && tipocliente == 'app' && !repar[0].aprobado){
    //   errr = 'El usuario no ha sido aprobado';
    //   clavecorrecta = false;
    // }
  }

  if(clavecorrecta){
    //GUARDAR TOKEN SI HAY CUENTAS EN LOS DIFERENTES NIVELES DE USUARIOS
    // if(admin[0])
    //   await Admin.query().updateAndFetchById(admin[0].id, {token:admin[0].token});
    if(repar[0])
      await Repartidor.query().updateAndFetchById(repar[0].id, {token:repar[0].token, refresh_token:repar[0].refresh_token});
    AR.enviarDatos(dataall, res);

  }else{
    dataall = {}
    dataall.msj = errr || "Datos incorrectos";
    dataall.r = false;
    AR.enviarDatos(dataall, res);
  }
});

router.post("/cambiar-clave", upload.none(), sesi.validar_general, async(req, res, next) => {
    data = req.body;
    //console.log(data.usuario);
    try {
        if (!data.pass) {
            AR.enviarError('Por favor introduzca la contraseña actual', res, 200);
            return;
        }
        if (!data.pass_new) {
            AR.enviarError('Por favor introduzca la nueva contraseña', res, 200);
            return;
        }
        if (!data.pass_new_con) {
            AR.enviarError('Por favor introduzca el confirmar contraseña', res, 200);
            return;
        }
        if (data.pass_new == data.pass) {
            AR.enviarError('La contraseña nueva no debe coincidir con la actual', res, 200);
            return;
        }
        if (data.pass_new != data.pass_new_con) {
            AR.enviarError('Las contraseñas no coinciden', res, 200);
            return;
        }

        if(data.usuario=='administrador')
          usuario = await Admin.query().findOne("id", data.id).throwIfNotFound().catch(err => console.log(err));
        if(data.usuario=='restaurante')
          usuario = await Usuario.query().findOne("id", data.id).throwIfNotFound().catch(err => console.log(err));
        if(data.usuario=='repartidor')
          usuario = await Repartidor.query().findOne("id", data.id).throwIfNotFound().catch(err => console.log(err));
        if(data.usuario=='cliente')
          usuario = await Cliente.query().findOne("id", data.id).throwIfNotFound().catch(err => console.log(err));

        let passwordIsValid = bcrypt.compareSync(data.pass, usuario.pass);

        if (!passwordIsValid) {
          AR.enviarError('Su contraseña actual es incorrecta.', res, 200);
          return;
        }
        if(data.usuario=='administrador'){
            var obj = {
              pass: bcrypt.hashSync(req.body.pass_new, 8)
            }
            await Admin.query()
              .patchAndFetchById(
                data.id, obj)
              .then(usuario => {
                usuario.r = true;
                usuario.msg = "Ha cambiado su contraseña satisfactoriamente.";
                return AR.enviarDatos(usuario, res);
              })
              .catch(err => {
                console.log(err);
                res.send(err);
              });
        }else if(data.usuario=='restaurante'){
            var obj = {
              pass: bcrypt.hashSync(req.body.pass_new, 8)
            }
            await Usuario.query()
              .patchAndFetchById(
                data.id, obj)
              .then(usuario => {
                usuario.r = true;
                usuario.msg = "Ha cambiado su contraseña satisfactoriamente.";
                return AR.enviarDatos(usuario, res);
              })
              .catch(err => {
                console.log(err);
                res.send(err);
              });
        }else if(data.usuario=='repartidor'){
            var obj = {
              pass: bcrypt.hashSync(req.body.pass_new, 8)
            }
            await Repartidor.query()
              .patchAndFetchById(
                data.id, obj)
              .then(usuario => {
                usuario.r = true;
                usuario.msg = "Ha cambiado su contraseña satisfactoriamente.";
                return AR.enviarDatos(usuario, res);
              })
              .catch(err => {
                console.log(err);
                res.send(err);
              });
        }else if(data.usuario=='cliente'){
            var obj = {
              pass: bcrypt.hashSync(req.body.pass_new, 8)
            }
            await Cliente.query()
              .patchAndFetchById(
                data.id, obj)
              .then(usuario => {
                usuario.r = true;
                usuario.msg = "Ha cambiado su contraseña satisfactoriamente.";
                return AR.enviarDatos(usuario, res);
              })
              .catch(err => {
                console.log(err);
                res.send(err);
              });
        }
    } catch (err) {
        console.log(err)
        next(err);
    }
});

router.post("/recuperar", upload.none(), async (req, res, next) => {
  data = req.body;


  let errr = false;
  // inicio validaciones
  if (!data.correo) errr = "Ingrese el correo";

  //fin validaciones
  if(errr) return  res.send({r: false, msj: errr}, res);

  // Verificar usuario.
  const usu = await Usuario.query()
    .findOne({correo: data.correo})
    .catch(console.error);

  if (usu) {
    let newpass = await genToken(10);
    newpass = newpass.toLowerCase();
    usu.pass = bcrypt.hashSync(newpass, 8);

    await Usuario.query()
      .patchAndFetchById(usu.id, {pass: usu.pass})
      .catch(console.error);

    let cuerpo = `
      Hola ${usu.nombre_apellido}.<br>
      Tus datos de acceso a la plataforma de Deliveryapp son:<br>
      <b>Usuario:</b> ${usu.correo}<br>
      <b>Contraseña:</b> ${newpass}
    `;

    mailer.enviarMail({
      direccion: usu.correo,
      titulo: 'Doomi | Nueva contraseña generada',
      mensaje: mailp({
        cuerpo: cuerpo,
        titulo: 'Doomi | Nueva contraseña generada'
      })
    });

    return AR.enviarDatos({r:true, msj:'Verifique el correo que se ha enviado'}, res);
  }


  // Verificar admin.
   const ced = await Admin.query()
      .where({ correo: data.correo })
      .then(async resp => {
        if (resp[0]) {
          if (resp[0].correo == data.correo) {
            admin = resp[0];
            admin.token  = uuid.v1();

            await Admin.query().updateAndFetchById(admin.id, {token:admin.token, pass: bcrypt.hashSync(admin.token.substr(0,10), 8)});
            var cuerpo = `Hola, ${admin.nombre} ${admin.apellido}
              <br> Tus datos de acceso a
              la plataforma de Deliveryapp son: <br>
              <b>Usuario: </b> ${admin.correo} <br><b>Contraseña: </b> ${admin.token.substr(0,10)}
            `;
            mailer.enviarMail({
                direccion: admin.correo,
                titulo: "Doomi | Nueva contraseña generada",
                mensaje: mailp({
                    cuerpo: cuerpo,
                    titulo: "Doomi | Nueva contraseña generada"
                })
            });
            return  AR.enviarDatos({r: true, msj: "Verifique el correo que se ha enviado" }, res);
          } else {
            return  AR.enviarDatos({r: false, msj: "El correo no ha sido encontrado."}, res);
          }
        } else {
          return  AR.enviarDatos({r: false, msj: "El correo no ha sido encontrado."}, res);
        }
      }).catch(err => {
        return  AR.enviarDatos({r: false, msj: "El correo no ha sido encontrado."}, res);
      });
});

router.post("/recuperar_app", upload.none(), async (req, res, next) => {
  data = req.body;


  let errr = false;
  // inicio validaciones
  if (!data.correo) errr = "Ingrese el correo";

  //fin validaciones
  if(errr) return  res.send({r: false, msj: errr}, res);

   const ced = await Repartidor.query()
      .where({ correo: data.correo })
      .skipUndefined()
      .then(async resp => {
        if (resp[0]) {
          admin = resp[0];
          admin.token  = uuid.v1();
          await Repartidor.query().updateAndFetchById(admin.id, {token:admin.token, pass: bcrypt.hashSync(admin.token.substr(0,10), 8)});
          var cuerpo = `Hola,
            <br> Se ha generado una nueva contraseña en nuestra plataforma, tus datos de acceso ahora
              son: <br>
            <b>Correo: </b> ${admin.correo} <br><b>Contraseña: </b> ${admin.token.substr(0,10)}
          `;
          mailer.enviarMail({
              direccion: admin.correo,
              titulo: "Doomi | Nueva contraseña generada",
              mensaje: mailp({
                  cuerpo: cuerpo,
                  titulo: "Doomi | Nueva contraseña generada"
              })
          });
          await Notificaciones.query()
            .insert({
                contenido: '¡Haz recuperado tu contraseña!',
                fecha: new Date(),
                estatus: 0,
                tipo_noti: 0,
                id_receptor: admin.id,
                id_emisor: admin.id,
                tabla_usuario_r: 'repartidores',
                tabla_usuario_e: 'repartidores'
            }).catch(async(err)=>{console.log(err);});
          return  AR.enviarDatos({r: true, msj: " Se ha generado una nueva contraseña, verifique el correo que se ha enviado" }, res);
        } else {
          return  AR.enviarDatos({r: false, msj: "No existe un usuario con esta información."}, res);
        }
      }).catch(err => {
        return  AR.enviarDatos({r: false, msj: "No existe un usuario con esta información."}, res);
      });
});

router.post("/recuperar_app_cli", upload.none(), async (req, res, next) => {
  data = req.body;


  let errr = false;
  // inicio validaciones
  if (!data.correo) errr = "Ingrese el correo";

  //fin validaciones
  if(errr) return  res.send({r: false, msj: errr}, res);

          const ced = await Cliente.query()
            .where({ correo: data.correo })
            .skipUndefined()
            .then(async resp => {
              if (resp[0]) {
                admin = resp[0];
                admin.token  = uuid.v1();
                await Cliente.query().updateAndFetchById(admin.id, {token:admin.token, pass: bcrypt.hashSync(admin.token.substr(0,10), 8)});
                var cuerpo = `Hola,
                  <br> Se ha generado una nueva contraseña en nuestra plataforma, tus datos de acceso ahora
                  son: <br>
                  <b>Correo: </b> ${admin.correo} <br><b>Contraseña: </b> ${admin.token.substr(0,10)}
                `;
                mailer.enviarMail({
                    direccion: admin.correo,
                    titulo: "Doomi | Nueva contraseña generada",
                    mensaje: mailp({
                        cuerpo: cuerpo,
                        titulo: "Doomi | Nueva contraseña generada"
                    })
                });
                await Notificaciones.query()
                  .insert({
                      contenido: '¡Haz recuperado tu contraseña!',
                      fecha: new Date(),
                      estatus: 0,
                      tipo_noti: 0,
                      id_receptor: admin.id,
                      id_emisor: admin.id,
                      tabla_usuario_r: 'clientes',
                      tabla_usuario_e: 'clientes'
                  }).catch(async(err)=>{console.log(err);});
                return  AR.enviarDatos({r: true, msj: "Verifique el correo que se ha enviado" }, res);
              } else {
                return  AR.enviarDatos({r: false, msj: "No existe un usuario con esta información."}, res);
              }
            }).catch(err => {
              return  AR.enviarDatos({r: false, msj: "No existe un usuario con esta información."}, res);
            });
});
// Verificar si el token sigue siendo valido.
router.post("/app/validar_token", upload.none(), async(req, res)=>{
  let token = req.body.token || req.query.token || req.headers.token;
  let tipo_usuario = req.body.tipo_usuario || req.query.tipo_usuario || req.headers.tipo_usuario;
  let tipo_cliente = req.headers['tipo-cliente'];
  let query;

  if(!token || tipo_cliente!='app' || !tipo_usuario && tipo_usuario.length == 0)
    return res.sendStatus(403);

  switch(tipo_usuario){
    case 'cliente': query = Cliente.query(); break;
    case 'repartidor': query = Repartidor.query(); break;
    default: return res.sendStatus(403);
  }

  await query.where("token", token)
      .then(usuario=>{
          if(usuario.length>0)
              return res.sendStatus(204); // Exito, nada se devuelve.
          else
              return res.sendStatus(403);
      })
      .catch(err=>{
          return res.sendStatus(403);
      });
});

// Hacer login - app recuerdame.
router.post("/app/recordar", upload.none(), sesi.validar_app, async(req, res) => {
  const refresh_token = req.body.refresh_token || req.body.token;
  const correo = req.body.correo;

  if(!refresh_token || !correo){
    return res.sendStatus(401);
  }

  const dataall = [];
  let access = false;

  const cli = await Cliente.query().findOne({correo, refresh_token}).catch(console.error);
  if(cli) {
    cli.token = uuid.v1();
    cli.refresh_token = uuid.v4(); // Renovar el token de refresco.
    cli.r = true;
    cli.tipo_usu = 'cliente';
    cli.text_tipo_usu = 'Cliente';
    dataall.push(cli);
    access = true;
  }

  const rep = await Repartidor.query().findOne({correo, refresh_token}).catch(console.error);
  if(rep){
    rep.token = uuid.v1();
    rep.refresh_token = uuid.v4(); // Renovar el token de refresco.
    rep.r = true;
    rep.tipo_usu = 'repartidor';
    rep.text_tipo_usu = 'Repartidor';

    let ped = await Pedido.query()
      .select('id')
      .where({id_repartidor: rep.id})
      .whereIn('id_estatus', [6,7,8,9,10,12])
      .first();

    rep.pedido_actual = ped ? ped.id : null;


    dataall.push(rep);
    access = true;
  }

  if(access) {
    if(cli)
      await Cliente.query().patchAndFetchById(cli.id, {token: cli.token, refresh_token: cli.refresh_token}).catch(console.error);
    else if(rep)
      await Repartidor.query().patchAndFetchById(rep.id, {token: rep.token, refresh_token: rep.refresh_token}).catch(console.error);

    return AR.enviarDatos(dataall, res);
  }

  res.sendStatus(401);
});

// Olvidar usuario app.
router.delete("/app/recordar", upload.none(), sesi.validar_app, async(req, res) => {
  const refresh_token = req.body.refresh_token || req.body.token;
  const correo = req.body.correo;
  if(!refresh_token || !correo){
    return res.sendStatus(400);
  }

  await Cliente.query().where({correo, refresh_token}).patch({refresh_token:null}).catch(console.error);
  await Repartidor.query().where({correo, refresh_token}).patch({refresh_token:null}).catch(console.error);

  res.sendStatus(204);
});

// Cerrar sesion.
router.delete("/app", upload.none(), async(req, res) => {
  const token = req.body.token || req.query.token || req.headers.token || '';
  const tipo_usuario = req.body.tipo_usuario || req.query.tipo_usuario || req.headers.tipo_usuario || '';

  if(tipo_usuario === 'cliente')
    await Cliente.query().patch({token:null}).where({token}).catch(console.error);
  else if(tipo_usuario === 'repartidor')
    await Repartidor.query().patch({token:null}).where({token}).catch(console.error);

  res.sendStatus(200);
});

module.exports = router;
