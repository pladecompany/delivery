//llamar al modelo de Usuarios
const Estatus = require("../models/Estatus");

//Llamar otras librerias
const AR = require("../ApiResponser");
const express = require("express");
const jwt = require("jsonwebtoken");
const config = require("../config");
var sesion = require("./sesion");
var sesi = new sesion();
var archivos = require("../Archivos");

const validarMail = require("../functions/validateMail");
//Obligatorio para trabajar con FormData
const multer = require("multer");
const upload = multer();

const moment = require("moment");

var router = express.Router();
 

// Obtener estatus posibles.
router.get('/', upload.none(), async (req, res, next) => {
		const estatus = await Estatus.query()
		.catch(err => {
			res.send(err);
		});
	AR.enviarDatos(estatus, res);
});

router.get("/:id", archivos.none(), async (req, res) => {
	const estatus = await Estatus.query()
		.where({id:req.params.id})
		.catch(err => {
			res.send(err);
		});

	AR.enviarDatos(estatus, res);
});


module.exports = router;
