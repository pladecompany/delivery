//llamar al modelo
const { raw, ref } = require('objection');
const Pedido = require("../models/Pedido");
const PagoPedido = require("../models/PagoPedido");
const CarritoItems = require("../models/CarritoItems");
const Admin = require("../models/Admin");
const Restaurante = require("../models/Restaurante");
const Notificaciones = require("../models/Notificacion");
var Firebase = require("../routes/firebase");
Firebase = new Firebase();

//Llamar otras librerias
const fs = require("fs-extra");
const AR = require("../ApiResponser");
const path = require("path");
const archivos = require("../Archivos");
const sesi = new (require("./sesion"))();
const upload = require("multer")();
const router = require("express").Router();
const config = require("../config");
const HTML_PDF = require("html-pdf");
const moment = require("moment");


// Crear pedido.
router.post("/pago_doomi", upload.none(), sesi.validar_general, async (req, res, next) => {
    data = req.body;
    let errr = false;
    // inicio validaciones
    if (!data.cuenta) errr = "Selecciona la cuenta";
    else if (!data.referencia) errr = "Ingresa la referencia";
    else if (data.referencia.length<4) errr = "La referencia debe ser mayor a 4 digitos";
    else if (!data.id_res) errr = "Ingresa id del doomialiado";
    else if (!data.valor) errr = "Error al guardar valor en $ a pagar";
    else if (!data.tasa) errr = "Error al guardar valor en bsf a pagar";
    else if (!data.pedidos) errr = "Debes tener al menos un pedido para pagar";

    //fin validaciones
    if(errr) return AR.enviarDatos({r: false, msj: errr}, res);

    var datos = {
      referencia: data.referencia,
      id_restaurante: data.id_res,
      id_cuenta: data.cuenta,
      pago_del: data.valor,
      tasa: data.tasa,
      pago_datos_doomi: data.datos_pago,
      fecha_pago: new Date()
    };

    await PagoPedido.query()
    .insert(datos)
    .then(async resp => {
    	for (var i = 0; i < data.pedidos.length; i++) {
    		await Pedido.query().updateAndFetchById(data.pedidos[i].id_pedido, {id_pago:resp.id}).catch(err => { console.log(err) });
    	}
    	const ADMIN = await Admin.query();
    	var enviar_n = true;
		for (var i = 0; i < ADMIN.length; i++) {
			await Notificaciones.query()
			    .insert({
				     contenido: "El Doomiliado "+data.nom_res+" ha realizado un pago (Ref. "+data.referencia+"), cuyo monto es "+data.monto,
					 fecha: new Date(),
				     estatus: 0,
				     tipo_noti: 0,
				     id_receptor: ADMIN[i].id,
				     id_emisor: resp.id_restaurante,
				     tabla_usuario_r: 'administradores',
				     tabla_usuario_e: 'restaurantes'
				     }).then(async noti => {
				        IO.sockets.emit("ActualizarPagosAliado", resp);
				        // IO.sockets.emit('admin:'+noti.id_receptor, {titulo:'Pago por doomialiado #'+noti.id_receptor, cuerpo:noti.contenido});
				        IO.sockets.emit('doomi:'+noti.id_emisor, {titulo:'Pago por doomialiado #'+noti.id_receptor, cuerpo:noti.contenido});
							// Solo hacer emits una sola vez, ya que cuando hay mas admins se duplicaran los emits innecesariamente
							if (enviar_n) {
								IO.sockets.emit("RecibirNotificacion", noti);
								enviar_n = false;
							}

							await Firebase.enviarPushNotification({
								id_usuario: noti.id_receptor,
								title: 'Pago realizado',
								body: noti.contenido,
								tablausuario: noti.tabla_usuario_r
							}, function(datan) { console.log(datan); });

					   }).catch(async(err)=>{console.log(err);});

		}
      	resp.msj = "Pago al doomi registrado";
      	resp.r = true;
      	AR.enviarDatos(resp, res);

    })
    .catch(err => {
      console.log(err)
      err.r = false;
      err.msj = 'Error al registrar pago al doomi';
      res.send(err);
    });

});

router.post("/pago_repartidor", upload.none(), sesi.validar_general, async (req, res, next) => {
    data = req.body;
    const tipo_usuario = req.headers['tipo_usuario'];
    let errr = false;
    // inicio validaciones
    if (!data.pedido) errr = "Selecciona el pedido";
    else if (!data.referencia) errr = "Ingresa la referencia";
    else if (data.referencia.length<4) errr = "La referencia debe ser mayor a 4 digitos";
    //else if (data.accion == 'disputar' && !data.motivo)
    //fin validaciones
    if(errr) return AR.enviarDatos({r: false, msj: errr}, res);

    var datos = {
      ref_pago_del: data.referencia,
      delivery_pagado: 1
    };

    if (data.accion == 'disputar')
      	datos.delivery_pagado = 2;
  	else
  		datos.fecha_pago_del = new Date();
    if (data.motivo)
    	datos.pago_motivo = data.motivo;
    if (data.datos_pago)
    	datos.pago_datos = data.datos_pago;


    await Pedido.query().updateAndFetchById(data.pedido, datos)
    .then(async resp => {
		if(!data.monto)
			data.monto = '$' + (+resp.pago_del || 0).toFixed(2).replace('.',',');
    	const datos_noti = {
			contenido: "El costo del Delivery del pedido #"+resp.id+" ha sido pagado (Ref. "+resp.ref_pago_del+"), cuyo monto es "+data.monto,
			fecha: new Date(),
			estatus: 0,
			tipo_noti: 0,
			id_usable: resp.id,
			id_receptor: resp.id_repartidor,
			id_emisor: resp.id_restaurante,
			tabla_usuario_r: 'repartidores',
			tabla_usuario_e: 'restaurantes'
		}
		Notificaciones.query().insert(datos_noti)
			.then(async noti => {
	        	//IO.sockets.emit("RecibirNotificacionApp", noti);
	        	IO.sockets.emit("ActualizarPagosAliado", resp);
	        	IO.sockets.emit("ActualizarPagoRep", resp);
	        	IO.sockets.emit('doomi:'+noti.id_receptor, {titulo:'Costo de delivery pagado', cuerpo:noti.contenido});
	        	IO.sockets.emit('admin', {titulo:'Costo de delivery pagado', cuerpo:noti.contenido});
	        	var datap = {
		            id_usuario: noti.id_receptor,
		            title: noti.contenido,
		            body: noti.contenido,
		            id_usable: `${noti.id_usable}`,
		            tipo_noti: `${noti.tipo_noti}`,
		            ruta: noti.redireccionar_noti || '',
		            tablausuario: noti.tabla_usuario_r
		        };
		        await Firebase.enviarPushNotification(datap, function(datan) { console.log(datan); });
		        if (tipo_usuario=='administrador') {
			        await Firebase.enviarPushNotification({
			        	...datap,
			        	tablausuario: 'restaurantes',
						id_usuario: resp.id_restaurante,
			        }, function(datan) { console.log(datan); });
			    }
	        })
			.catch(console.error)
    	//
      resp.msj = "Pago al repartidor realizado correctamente";
      resp.r = true;
      AR.enviarDatos(resp, res);

    })
    .catch(err => {
      console.log(err)
      err.r = false;
      err.msj = 'Error al registrar pago al repartidor';
      res.send(err);
    });

});


// Listar pagos de pedidos
router.get("/pagos_pendientes/repartidor/:id(\\d+)", sesi.validar_general, async(req,res) => {
	const id_doomi = req.params.id;
	const filtros = req.query;

	const Query = Pedido.query()
	// .select('pedidos.*', 'restaurantes.nombre AS nombre_restaurante')
  .select('pedidos.*', 'restaurantes.nombre AS nombre_restaurante',raw('DATE_FORMAT(creado, "%Y-%m-%d %H:%i:%s")').as('creado'),raw('DATE_FORMAT(fecha_pago_del, "%Y-%m-%d %H:%i:%s")').as('fecha_pago_del'))
	.leftJoin('restaurantes', 'restaurantes.id', 'pedidos.id_restaurante')
	.where('id_restaurante',id_doomi)
	//.where('delivery_pagado',0)
	.whereRaw('(id_estatus=11 || id_estatus=12)')
  	.whereRaw('id_repartidor IS NOT NULL')
  	.orderByRaw('pedidos.fecha_pago_del DESC, pedidos.creado DESC')
	//.orderBy("pedidos.creado", "desc");

	if(filtros.desde) {
  		let desde = new Date(filtros.desde).toISOString();
  		desde = moment(desde).format('YYYY-MM-DD')
  		Query.whereRaw(' ((pedidos.creado>="'+desde+' 00:00:00" AND pedidos.delivery_pagado=0) OR (pedidos.fecha_pago_del>="'+desde+' 00:00:00" AND pedidos.delivery_pagado<>0))')
		//Query.where('pedidos.creado', '>=', desde+' 00:00:00');
	}

	if(filtros.hasta) {
		let hasta = new Date(filtros.hasta).toISOString();
		hasta = moment(hasta).format('YYYY-MM-DD')
		Query.whereRaw(' ((pedidos.creado<="'+hasta+' 23:59:59" AND pedidos.delivery_pagado=0) OR (pedidos.fecha_pago_del<="'+hasta+' 23:59:59" AND pedidos.delivery_pagado<>0))')
		//Query.where('pedidos.creado', '<=', hasta+' 23:59:59');
	}
	await Query.then(resp => AR.enviarDatos(resp, res)).catch(console.log);
});

//SELECT c.*, SUM((c.cantidad*c.precio_usd)*(c.ganancia/100)) AS tot, (SELECT SUM((a.cantidad*a.precio_usd)*(c.ganancia/100)) from pedidos_acompañantes a WHERE a.id_item=c.id) AS tot_acom, (SELECT SUM((ad.cantidad*ad.precio_usd)*(c.ganancia/100)) from pedidos_adicionales ad WHERE ad.id_item=c.id) AS tot_acom FROM carrito_items c, pedidos p WHERE c.ganancia IS NOT NULL AND c.id_pedido=p.id AND p.id_restaurante=1043 GROUP BY c.id
router.get("/pagos_pendientes/doomialiado/:id(\\d+)", sesi.validar_general, async(req,res) => {
	const id_doomi = req.params.id;
	const filtros = req.query;
	const Query = CarritoItems.query().join('pedidos','pedidos.id','carrito_items.id_pedido').join('clientes','clientes.id','pedidos.id_cliente')
	.where('pedidos.id_restaurante',id_doomi)
	.whereRaw('(pedidos.id_estatus=11 || pedidos.id_estatus=12)')
	.whereRaw('pedidos.id_pago IS NULL')
	.whereRaw('carrito_items.ganancia IS NOT NULL')
	.select('carrito_items.*',raw('DATE_FORMAT(pedidos.creado, "%Y-%m-%d %H:%i:%s")').as('creado'),'clientes.nombre as nom_cli','clientes.apellido as ape_cli','clientes.foto as img_cli',
		raw('SUM((carrito_items.cantidad*carrito_items.precio_usd)*(carrito_items.ganancia/100))').as('tot'),
		raw('(SELECT SUM((a.cantidad*a.precio_usd)*(c.ganancia/100)) from pedidos_acompañantes a, carrito_items c, pedidos p WHERE a.id_item=c.id AND c.id_pedido=p.id AND p.id=pedidos.id AND p.id_pago IS NULL)').as('tot_acom'),
		raw('(SELECT SUM((ad.cantidad*ad.precio_usd)*(c.ganancia/100)) from pedidos_adicionales ad, carrito_items c, pedidos p WHERE ad.id_item=c.id AND c.id_pedido=p.id AND p.id=pedidos.id AND p.id_pago IS NULL)').as('tot_adi'),
		raw('(SELECT pre_tasa from restaurantes r WHERE r.id=pedidos.id_restaurante)').as('pre_tasa'),
		raw('(SELECT nombre from restaurantes r WHERE r.id=pedidos.id_restaurante)').as('nombre_doomi'),
		raw('(SELECT id from restaurantes r WHERE r.id=pedidos.id_restaurante)').as('id_doomi'),
		//raw('DATE_FORMAT(pedidos.creado, "%Y-%m-%d %H:%i:%s")').as('creado'),raw('SUM((carrito_items.cantidad*carrito_items.precio_usd)*(carrito_items.ganancia/100))').as('tot'),raw('(SELECT SUM((a.cantidad*a.precio_usd)*(carrito_items.ganancia/100)) from pedidos_acompañantes a WHERE a.id_item=carrito_items.id)').as('tot_acom'),raw('(SELECT SUM((ad.cantidad*ad.precio_usd)*(carrito_items.ganancia/100)) from pedidos_adicionales ad WHERE ad.id_item=carrito_items.id)').as('tot_adi'))
	).groupBy("carrito_items.id_pedido")
	.orderBy("pedidos.creado", "desc");
	if(filtros.desde) {
  		let desde = new Date(filtros.desde).toISOString();
  		desde = moment(desde).format('YYYY-MM-DD')
		Query.where('pedidos.creado', '>=', desde+' 00:00:00');
	}

	if(filtros.hasta) {
		let hasta = new Date(filtros.hasta).toISOString();
		hasta = moment(hasta).format('YYYY-MM-DD')
		Query.where('pedidos.creado', '<=', hasta+' 23:59:59');
	}

	await Query.then(resp => AR.enviarDatos(resp, res)).catch(console.log);
});

router.get("/ver_pago_realizado/doomialiado/:id(\\d+)", sesi.validar_general, async(req,res) => {
	const id_doomi = req.params.id;
	const filtros = req.query;
	const Query = CarritoItems.query().join('pedidos','pedidos.id','carrito_items.id_pedido').join('clientes','clientes.id','pedidos.id_cliente')
	.where('pedidos.id_restaurante',id_doomi)
	.whereRaw('(pedidos.id_estatus=11 || pedidos.id_estatus=12)')
	.where('pedidos.id_pago', filtros.idpago)
	.whereRaw('carrito_items.ganancia IS NOT NULL')
	.select('carrito_items.*',raw('DATE_FORMAT(pedidos.creado, "%Y-%m-%d %H:%i:%s")').as('creado'),'clientes.nombre as nom_cli','clientes.apellido as ape_cli','clientes.foto as img_cli',
		raw('SUM((carrito_items.cantidad*carrito_items.precio_usd)*(carrito_items.ganancia/100))').as('tot'),
		raw('(SELECT SUM((a.cantidad*a.precio_usd)*(c.ganancia/100)) from pedidos_acompañantes a, carrito_items c, pedidos p WHERE a.id_item=c.id AND c.id_pedido=p.id AND p.id=pedidos.id AND p.id_pago='+filtros.idpago+')').as('tot_acom'),
		raw('(SELECT SUM((ad.cantidad*ad.precio_usd)*(c.ganancia/100)) from pedidos_adicionales ad, carrito_items c, pedidos p WHERE ad.id_item=c.id AND c.id_pedido=p.id AND p.id=pedidos.id AND p.id_pago='+filtros.idpago+')').as('tot_adi'),
		raw('(SELECT pre_tasa from restaurantes r WHERE r.id=pedidos.id_restaurante)').as('pre_tasa'),
		raw('(SELECT nombre from restaurantes r WHERE r.id=pedidos.id_restaurante)').as('nombre_doomi'),
		raw('(SELECT id from restaurantes r WHERE r.id=pedidos.id_restaurante)').as('id_doomi'),
		//raw('DATE_FORMAT(pedidos.creado, "%Y-%m-%d %H:%i:%s")').as('creado'),raw('SUM((carrito_items.cantidad*carrito_items.precio_usd)*(carrito_items.ganancia/100))').as('tot'),raw('(SELECT SUM((a.cantidad*a.precio_usd)*(carrito_items.ganancia/100)) from pedidos_acompañantes a WHERE a.id_item=carrito_items.id)').as('tot_acom'),raw('(SELECT SUM((ad.cantidad*ad.precio_usd)*(carrito_items.ganancia/100)) from pedidos_adicionales ad WHERE ad.id_item=carrito_items.id)').as('tot_adi'))
	).groupBy("carrito_items.id_pedido")
	.orderBy("pedidos.creado", "desc");
	if(filtros.desde) {
  		let desde = new Date(filtros.desde).toISOString();
  		desde = moment(desde).format('YYYY-MM-DD')
		Query.where('pedidos.creado', '>=', desde+' 00:00:00');
	}

	if(filtros.hasta) {
		let hasta = new Date(filtros.hasta).toISOString();
		hasta = moment(hasta).format('YYYY-MM-DD')
		Query.where('pedidos.creado', '<=', hasta+' 23:59:59');
	}

	await Query.then(resp => AR.enviarDatos(resp, res)).catch(console.log);
});

router.get("/pagos_realizados/doomialiado/:id(\\d+)", sesi.validar_general, async(req,res) => {
	const id_doomi = req.params.id;
	const filtros = req.query;
	const Query = PagoPedido.query()
	.select(
		'pagar_pedidos.*',
		'restaurantes.nombre as nombre_restaurante',
		raw('DATE_FORMAT(fecha_pago, "%Y-%m-%d %H:%i:%s")').as('fecha_pago'),
		b => b.count().from('pedidos').where('pedidos.id_pago', ref('pagar_pedidos.id')).as('numero_pedidos')
	)
	.join('restaurantes', 'restaurantes.id', 'pagar_pedidos.id_restaurante')
	.where('id_restaurante',id_doomi)
	.orderBy("fecha_pago", "desc");
	if(filtros.desde) {
  		let desde = new Date(filtros.desde).toISOString();
  		desde = moment(desde).format('YYYY-MM-DD')
		Query.where('fecha_pago', '>=', desde+' 00:00:00');
	}

	if(filtros.hasta) {
		let hasta = new Date(filtros.hasta).toISOString();
		hasta = moment(hasta).format('YYYY-MM-DD')
		Query.where('fecha_pago', '<=', hasta+' 23:59:59');
	}

	if (+filtros.id_pago) {
		Query.where('pagar_pedidos.id', filtros.id_pago);
	}

	await Query.then(resp => AR.enviarDatos(resp, res)).catch(console.log);
});

router.get("/totales/:id(\\d+)", sesi.validar_restaurante, async(req,res) => {
	const id_doomi = req.params.id;
	let tot_p = 0;
	let tot_f = 0;
	let tot_d = 0;
	let tot_d_bs = 0;
	/*const info = await Restaurante.query().findById(id_doomi).catch(console.log);
	//console.log(info.pre_tasa);
	const Query = await CarritoItems.query()
		.join('pedidos','pedidos.id','carrito_items.id_pedido')
	.where('pedidos.id_restaurante',id_doomi)
	.whereRaw('pedidos.id_pago IS NULL')
	.whereRaw('(pedidos.id_estatus=11 || pedidos.id_estatus=12)')
  	//.whereRaw('(pedidos.id_estatus=11 || pedidos.id_estatus=12)')
  	.whereRaw('carrito_items.ganancia IS NOT NULL')
	.select('carrito_items.*',
		raw('SUM((carrito_items.cantidad*carrito_items.precio_usd)*(carrito_items.ganancia/100))').as('tot'),
		raw('SUM(((carrito_items.cantidad*carrito_items.precio_usd)*(carrito_items.ganancia/100))*'+info.pre_tasa+')').as('tot_bs'),
		raw('(SELECT SUM((a.cantidad*a.precio_usd)*(c.ganancia/100)) from pedidos_acompañantes a, carrito_items c, pedidos p WHERE a.id_item=c.id AND c.id_pedido=p.id AND p.id_restaurante=pedidos.id_restaurante AND p.id_pago IS NULL)').as('tot_acom'),
		raw('(SELECT SUM((ad.cantidad*ad.precio_usd)*(c.ganancia/100)) from pedidos_adicionales ad, carrito_items c, pedidos p WHERE ad.id_item=c.id AND c.id_pedido=p.id AND p.id_restaurante=pedidos.id_restaurante AND p.id_pago IS NULL)').as('tot_adi'),
		raw('(SELECT SUM(((a.cantidad*a.precio_usd)*(c.ganancia/100))*'+info.pre_tasa+') from pedidos_acompañantes a, carrito_items c, pedidos p WHERE a.id_item=c.id AND c.id_pedido=p.id AND p.id_restaurante=pedidos.id_restaurante AND p.id_pago IS NULL)').as('tot_acom_bs'),
		raw('(SELECT SUM(((ad.cantidad*ad.precio_usd)*(c.ganancia/100))*'+info.pre_tasa+') from pedidos_adicionales ad, carrito_items c, pedidos p WHERE ad.id_item=c.id AND c.id_pedido=p.id AND p.id_restaurante=pedidos.id_restaurante AND p.id_pago IS NULL)').as('tot_adi_bs'),
		//raw('(SELECT SUM(((a.cantidad*a.precio_usd)*(carrito_items.ganancia/100))*pedidos.tasa) from pedidos_acompañantes a WHERE a.id_item=carrito_items.id)').as('tot_acom_bs'),
		//raw('(SELECT SUM(((ad.cantidad*ad.precio_usd)*(carrito_items.ganancia/100))*pedidos.tasa) from pedidos_adicionales ad WHERE ad.id_item=carrito_items.id)').as('tot_adi_bs'))
	).groupBy("pedidos.id_restaurante");
	if(Query[0]){
		tot_d = Query[0].tot;
		if(Query[0].tot_acom)
			tot_d = tot_d + Query[0].tot_acom;
		if(Query[0].tot_adi)
			tot_d = tot_d + Query[0].tot_adi;

		tot_d_bs = Query[0].tot_bs;
		if(Query[0].tot_acom)
			tot_d_bs = tot_d_bs + Query[0].tot_acom_bs;
		if(Query[0].tot_adi)
			tot_d_bs = tot_d_bs + Query[0].tot_adi_bs;
	}*/

  const Query2 = await Pedido.query()
  .where('id_restaurante',id_doomi)
  .where('id_estatus','<>',3)
  .where('id_estatus','<>',97)
  .where('id_estatus','<>',98)
  .where('id_estatus','<>',99)
  //.whereRaw('(id_estatus=11 || id_estatus=12)')
  .select(raw('count(*)').as('tot_ped'),raw('SUM(subtotal+COALESCE(pago_del,0)+subtotal_adicionales+subtotal_acompañantes)').as('tot'))
  .groupBy("id_restaurante");
  //console.log(Query2);
  if(Query2[0]){
    if(Query2[0].tot_ped)
      tot_p = Query2[0].tot_ped;
    if(Query2[0].tot)
      tot_f = Query2[0].tot;
  }
	AR.enviarDatos({t1: tot_p, t2: tot_f, t3: tot_d, t3b: tot_d_bs}, res)

});

router.get("/pagos/repartidor/:id(\\d+)", sesi.validar_general, async(req,res) => {
	const id_rep = req.params.id;
	const filtros = req.query;
	const Query = Pedido.query()
	.select('pedidos.*', 'restaurantes.nombre AS nombre_restaurante', 'restaurantes.img AS img_restaurante',raw('DATE_FORMAT(creado, "%Y-%m-%d %H:%i:%s")').as('creado'),raw('DATE_FORMAT(fecha_pago_del, "%Y-%m-%d %H:%i:%s")').as('fecha_pago_del'))
	.leftJoin('restaurantes', 'restaurantes.id', 'pedidos.id_restaurante')
	.where('id_repartidor',id_rep)
	.whereRaw('(id_estatus=11 || id_estatus=12)')
  	.orderBy("pedidos.creado", "desc");

  	if(filtros.desde) {
  		let desde = new Date(filtros.desde).toISOString();
  		desde = moment(desde).format('YYYY-MM-DD')
  		//Query.whereRaw(' (pedidos.creado>="'+desde+' 00:00:00" OR pedidos.fecha_pago_del>="'+desde+' 00:00:00" )')
		Query.whereRaw(' ((pedidos.creado>="'+desde+' 00:00:00" AND pedidos.delivery_pagado=0) OR (pedidos.fecha_pago_del>="'+desde+' 00:00:00" AND pedidos.delivery_pagado<>0))')
		//Query.where('pedidos.creado', '>=', desde+' 00:00:00');
	}

	if(filtros.hasta) {
		let hasta = new Date(filtros.hasta).toISOString();
		hasta = moment(hasta).format('YYYY-MM-DD')
		//Query.whereRaw(' (pedidos.creado<="'+hasta+' 23:59:59" OR pedidos.fecha_pago_del<="'+hasta+' 23:59:59" )')
		Query.whereRaw(' ((pedidos.creado<="'+hasta+' 23:59:59" AND pedidos.delivery_pagado=0) OR (pedidos.fecha_pago_del<="'+hasta+' 23:59:59" AND pedidos.delivery_pagado<>0))')
		//Query.where('pedidos.creado', '<=', hasta+' 23:59:59');
	}

	if(filtros.delivery_pagado) {
		Query.where('pedidos.delivery_pagado', filtros.delivery_pagado);
	}

	await Query.then(resp => AR.enviarDatos(resp, res)).catch(console.log);
});

router.get("/buscar_pago/repartidor/:id(\\d+)", sesi.validar_general, async(req,res) => {
	const id_ped = req.params.id;
	const filtros = req.query;
	const Query = Pedido.query()
	.select('pedidos.*', 'restaurantes.nombre AS nombre_restaurante', 'restaurantes.img AS img_restaurante',raw('DATE_FORMAT(creado, "%Y-%m-%d %H:%i:%s")').as('creado'),raw('DATE_FORMAT(fecha_pago_del, "%Y-%m-%d %H:%i:%s")').as('fecha_pago_del'))
	.leftJoin('restaurantes', 'restaurantes.id', 'pedidos.id_restaurante')
	.where('pedidos.id',id_ped)
	.whereRaw('(id_estatus=11 || id_estatus=12)')
  	.orderBy("pedidos.creado", "desc");

	await Query.then(resp => AR.enviarDatos(resp, res)).catch(console.log);
});

router.get("/pagos/repartidor_total/:id(\\d+)", sesi.validar_general, async(req,res) => {
	const id_rep = req.params.id;
	const Query = Pedido.query()
	.select(raw('count(*)').as('tot_ped'),raw('SUM(pago_del)').as('tot'))
	.where('id_repartidor',id_rep)
	.where('delivery_pagado','<>',3)
	//.where('delivery_pagado','<>',0)
	.whereRaw('(id_estatus=11 || id_estatus=12)')
  	.groupBy("id_repartidor");
	await Query.then(resp => AR.enviarDatos(resp, res)).catch(console.log);
});

router.post("/disputar_pago", upload.none(), sesi.validar_general, async (req, res, next) => {
    data = req.body;
    let errr = false;
    // inicio validaciones
    if (!data.pedido) errr = "Selecciona el pedido";
    else if (!data.motivo) errr = "Ingresa el motivo";
    else if (data.motivo.length<4) errr = "El motivo debe ser mayor a 4 digitos";
    //fin validaciones
    if(errr) return AR.enviarDatos({r: false, msj: errr}, res);

    var datos = {
      pago_motivo: data.motivo,
      delivery_pagado: 2
    };

    await Pedido.query().updateAndFetchById(data.pedido, datos)
    .then(async resp => {
    	const datos_noti = {
			contenido: "El pago del Delivery del pedido #"+resp.id+" está en disputa (Motivo: "+resp.pago_motivo+")",
			fecha: new Date(),
			estatus: 0,
			tipo_noti: 0,
			id_usable: resp.id,
			id_receptor: resp.id_restaurante,
			id_emisor: resp.id_repartidor,
			tabla_usuario_r: 'restaurantes',
			tabla_usuario_e: 'repartidores'
		}
		Notificaciones.query().insert(datos_noti)
			.then(async noti => {
	        	//IO.sockets.emit("RecibirNotificacionApp", noti);
	        	IO.sockets.emit("ActualizarPagosAliado", resp);
	        	IO.sockets.emit("ActualizarPagoRep", resp);
	        	IO.sockets.emit('doomi:'+noti.id_receptor, {titulo:noti.contenido, cuerpo:noti.contenido});
	        	IO.sockets.emit('admin', {titulo:noti.contenido, cuerpo:noti.contenido});
	        	var datap = {
		            id_usuario: noti.id_receptor,
		            title: noti.contenido,
		            body: noti.contenido,
		            id_usable: noti.id_usable,
		            tipo_noti: noti.tipo_noti,
		            ruta: noti.redireccionar_noti,
		            tablausuario: noti.tabla_usuario_r
		        };
		        await Firebase.enviarPushNotification(datap, function(datan) { console.log(datan); });
		        if (req.headers['tipo_usuario']=='administrador') {
		        	await Firebase.enviarPushNotification({
		        		...datap,
		        		tablausuario: 'repartidores',
		        		id_usuario: resp.id_repartidor,
		        	}, function(datan) { console.log(datan); });
		        }
	        })
			.catch(console.error)
    	//
      resp.msj = "Pago puesto en disputa correctamente";
      resp.r = true;
      AR.enviarDatos(resp, res);

    })
    .catch(err => {
      console.log(err)
      err.r = false;
      err.msj = 'Error al poner en disputa el pago';
      res.send(err);
    });

});

router.post("/finalizar_pago", upload.none(), sesi.validar_general, async (req, res, next) => {
    data = req.body;
    let errr = false;
    // inicio validaciones
    if (!data.pedido) errr = "Selecciona el pedido";
    //fin validaciones
    if(errr) return AR.enviarDatos({r: false, msj: errr}, res);

    var datos = {
      delivery_pagado: 3
    };

    await Pedido.query().updateAndFetchById(data.pedido, datos)
    .then(async resp => {
    	const datos_noti = {
			contenido: "El pago del Delivery del pedido #"+resp.id+" ha sido aceptado correctamente",
			fecha: new Date(),
			estatus: 0,
			tipo_noti: 0,
			id_usable: resp.id,
			id_receptor: resp.id_restaurante,
			id_emisor: resp.id_repartidor,
			tabla_usuario_r: 'restaurantes',
			tabla_usuario_e: 'repartidores'
		}
		Notificaciones.query().insert(datos_noti)
			.then(async noti => {
	        	//IO.sockets.emit("RecibirNotificacionApp", noti);
	        	IO.sockets.emit("ActualizarPagoRep", resp);
	        	IO.sockets.emit("ActualizarPagosAliado", resp);
	        	IO.sockets.emit('doomi:'+noti.id_receptor, {titulo:noti.contenido, cuerpo:noti.contenido});
	        	IO.sockets.emit('admin', {titulo:noti.contenido, cuerpo:noti.contenido});
	        	var datap = {
		            id_usuario: noti.id_receptor,
		            title: noti.contenido,
		            body: noti.contenido,
		            id_usable: noti.id_usable,
		            tipo_noti: noti.tipo_noti,
		            ruta: noti.redireccionar_noti,
		            tablausuario: noti.tabla_usuario_r
		        };
		        await Firebase.enviarPushNotification(datap, function(datan) { console.log(datan); });
	        })
			.catch(console.error)
    	//
      resp.msj = "Pago aceptado correctamente";
      resp.r = true;
      AR.enviarDatos(resp, res);

    })
    .catch(err => {
      console.log(err)
      err.r = false;
      err.msj = 'Error al aceptar el pago';
      res.send(err);
    });
});

router.post("/confirmacion_pago_doomi", upload.none(), sesi.validar_admin, async(req,res) => {
	const id_pago = req.body.id_pago;
	const confirmado = req.body.estatus ? 1 : 0;

	await PagoPedido.query()
		.patchAndFetchById(id_pago, {confirmado})
		.then(resp => {
			// IO.sockets.emit("ActualizarPagosAliado", resp);

			const datos_noti = {
				contenido: `Administración ha aceptado un pago de pedidos`,
				fecha: new Date(),
				estatus: 0,
				tipo_noti: 0,
				id_usable: resp.id,
				id_receptor: resp.id_restaurante,
				// id_emisor: resp.,
				tabla_usuario_r: 'restaurantes',
				tabla_usuario_e: 'administradores'
			}

			Notificaciones.query().insert(datos_noti)
				.then(async noti => {
		        	IO.sockets.emit("RecibirNotificacionApp", noti);
		        	IO.sockets.emit("ActualizarPagosAliado", resp);
		        	IO.sockets.emit('doomi:'+noti.id_receptor, {titulo:noti.contenido, cuerpo:noti.contenido});
		        	const datap = {
			            id_usuario: noti.id_receptor,
			            title: noti.contenido,
			            body: noti.contenido,
			            id_usable: noti.id_usable,
			            tipo_noti: noti.tipo_noti,
			            ruta: noti.redireccionar_noti,
			            tablausuario: noti.tabla_usuario_r
			        };
			        await Firebase.enviarPushNotification(datap, function(datan) { console.log(datan); });
		        })
				.catch(console.error)

			AR.enviarDatos(resp, res);
		})
		.catch(err => {
			console.error(err);
			res.sendStatus(500);
		});
});


function crearPush(config) {
	return new Promise((resolve, reject) => {
		const datos_noti = {
			contenido: `Administración ha aceptado un pago para pedidos`,
			fecha: new Date(),
			estatus: 0,
			tipo_noti: 0,
			id_usable: config.id,
			id_receptor: config.id_restaurante,
			// id_emisor: config.,
			tabla_usuario_r: 'restaurantes',
			tabla_usuario_e: 'administradores'
		}

		Notificaciones.query().insert(config)
			.then(async noti => {
	        	const datap = {
		            id_usuario: `${noti.id_receptor}`,
		            title: `${noti.contenido}`,
		            body: `${noti.contenido}`,
		            id_usable: `${noti.id_usable}`,
		            tipo_noti: `${noti.tipo_noti}`,
		            ruta: `${noti.redireccionar_noti}`,
		            tablausuario: `${noti.tabla_usuario_r}`
		        };
		        await Firebase.enviarPushNotification(datap, function(datan) { console.log(datan); });
		        resolve(noti);
	        })
			.catch(console.error)
		});
}


module.exports = router;
