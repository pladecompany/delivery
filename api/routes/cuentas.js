//llamar al modelo
const { raw } = require('objection');
const Cuentas = require("../models/Cuentas");
const CuentaswhitDetails = require("../models/CuentaswhitDetails");
const Metodos_en_cuentas = require("../models/Metodos_en_cuentas");
const Restaurante = require("../models/Restaurante");
const Pedido = require("../models/Pedido");

//Llamar otras librerias
const AR = require("../ApiResponser");
const express = require("express");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const config = require("../config");
var uuid = require("uuid");
var sesion = require("./sesion");
var sesi = new sesion();


//Obligatorio para trabajar con FormData
const multer = require("multer");
const upload = multer();

//Manejador de Errores
const { AuthError, ValidationError } = require("../Errores");

var router = express.Router();

function Quitarcoma(value) {
  if(value){
    for (var i = 0; i < value.split(".").length+10; i++) {
      value = value.replace(".", "");
    }
    value = value.replace(",", ".");
  }
  if(value.length=="3" || value.length=="2"){
    if(value.split(".").length==0)
      value = "0."+""+value;
    else
    value = "0"+""+value;
  }
  return parseFloat(value);
}

//registrar un aula
router.post("/add", upload.none(), sesi.validar_general, async (req, res, next) => {
    data = req.body;
    //console.log(data);
    let errr = false;
    // inicio validaciones
    if (!data.nombre) errr = "Ingresa un nombre";
    if (data.retorno && !data.app && parseFloat(Quitarcoma(data.porcentaje))>100) errr = "El porcentaje no debe ser mayor a 100";
    if (data.retorno && data.app && parseFloat(data.porcentaje)>100) errr = "El porcentaje no debe ser mayor a 100";
    //if (!data.metodos) errr = "debe elegir al menos un método de pago";
    if (!data.id_res && data.tipo_usu!='administrador') errr = "Ingresa id del restaurante";

    //fin validaciones
    if(errr) return AR.enviarDatos({r: false, msj: errr}, res);

    var datos = {
      nombre: data.nombre,
      //numero: data.numero,
      //tipo: data.tipo,
      detalles: data.ext
    };
    if(data.retorno){
      datos.retorno = true;
      if(!data.app)
        datos.porcentaje = parseFloat(Quitarcoma(data.porcentaje));
      else
        datos.porcentaje = parseFloat(data.porcentaje);
    }
    if(data.tipo_usu!='administrador')
      datos.id_restaurante = data.id_res;
    await Cuentas.query()
    .insert(datos)
    .then(async resp => {

      /*for (var i = 0; i < data.metodos.length; i++) {
        await Metodos_en_cuentas.query().insert({ id_cuenta: resp.id, id_metodo: data.metodos[i] }).catch(err => { });
      };*/
      resp.msj = "Cuenta registrada";
      resp.r = true;
      AR.enviarDatos(resp, res);

    })
    .catch(err => {
      console.log(err)
      err.r = false;
      err.msj = 'Error al registrar Cuenta';
      res.send(err);
    });

});
router.put("/edit", upload.none(), sesi.validar_general, async (req, res, next) => {
    data = req.body;
    let errr = false;

    // inicio validaciones
    if (!data.nombre) errr = "Ingresa un nombre";
    if (data.retorno && !data.app && parseFloat(Quitarcoma(data.porcentaje))>100) errr = "El porcentaje no debe ser mayor a 100";
    if (data.retorno && data.app && parseFloat(data.porcentaje)>100) errr = "El porcentaje no debe ser mayor a 100";
    //if (!data.metodos) errr = "debe elegir al menos un método de pago";
    if (!data.iden) errr = "Ingresa la id";
    //fin validaciones
    if(errr) return AR.enviarDatos({r: false, msj: errr}, res);
    var datos = {
      nombre: data.nombre,
      //numero: data.numero,
      //tipo: data.tipo,
      detalles: data.ext,
      retorno: data.retorno
    };
    if(!data.app)
      datos.porcentaje = parseFloat(Quitarcoma(data.porcentaje));
    else
      datos.porcentaje = parseFloat(data.porcentaje);
    if(!data.retorno)
      datos.retorno=0;

   const aula = await Cuentas.query()
    .updateAndFetchById(data.iden, datos)
    .then(async resp => {

      /*await Metodos_en_cuentas.query().delete().where({ id_cuenta: data.iden }).catch(err => { console.log(err); });

      for (var i = 0; i < data.metodos.length; i++) {
        await Metodos_en_cuentas.query().insert({ id_cuenta: data.iden, id_metodo: data.metodos[i] }).catch(err => { });
      };*/

      resp.msj = "Cuenta actualizada";
      resp.r = true;
      AR.enviarDatos(resp, res);

    })
    .catch(err => {
      console.log(err)
      err.r = false;
      err.msj = 'Error al editar Cuenta';
      res.send(err);
    });

});

//editar Estatus
router.put("/edit_sts", upload.none(), sesi.validar_general, async (req, res, next) => {
  data = req.body;
  let errr = false;

  // inicio validaciones
  //if (!data.sts) errr = "Ingresa el estatus";
  if (!data.iden) errr = "Ingresa la id de la cuenta";

  //fin validaciones
  if(errr){
    return AR.enviarDatos({r: false, msj: errr}, res);
  }

  var datos = {
    estatus: data.sts
  }


  await Cuentas.query().updateAndFetchById(data.iden, datos)
  .then(async resp => {
    resp.msj = "Estatus actualizado con éxito";
    resp.r = true;
    AR.enviarDatos(resp, res);
  })
  .catch(async err => {
    console.log(err)
    err.r = false;
    err.msj = 'Error al actualizar estatus';
    res.send(err);
  });

});

router.delete("/delete/:id", upload.none(), sesi.validar_general, async(req, res) => {
      data = req.query;

      const buscar_en_pedido = await Pedido.query().where('id_cuenta',req.params.id).catch(err => { console.log(err); });
      if(buscar_en_pedido[0])
        return AR.enviarDatos({ r:false, msj: "No se pudo eliminar, información usada" }, res);
      //await Metodos_en_cuentas.query().delete().where({ id_cuenta: req.params.id }).catch(err => { console.log(err); });
        const eliminado = await Cuentas.query()
            .delete()
            .where({ id: req.params.id })
            .catch(err => {
                console.log(err);
                res.send(err);
            });
        if (eliminado){AR.enviarDatos({ r:true, msj: "Registro eliminado exitosamente" }, res);}
        else{ AR.enviarDatos({r:false, msj: "No se pudo eliminar, información usada"}, res);}
});

router.get("/", async(req, res) => {
    data = req.query;
    if(data.tipo_usu=='administrador'){
      await Cuentas.query().where("id_restaurante",null)
          .then(cuentas => {
          AR.enviarDatos(cuentas, res);
      });
    }else{
      await Cuentas.query().where("id_restaurante",data.id_res).orderBy("retorno", "desc")
          .then(cuentas => {
          AR.enviarDatos(cuentas, res);
      });
    }
});

router.get("/app", upload.none(), sesi.validar_cliente, async(req, res) => {
  const data = req.query;
  const id_res = req.query.id_res || req.query.id_doomi;

  if(!data || !data.id_res)
    return AR.enviarDatos({r:false}, res);

  const rest = await Restaurante.query().findById(id_res)
    .catch(e => console.log(e));

  const Query = Cuentas.query()

  if(rest && rest.estatus)
    Query.where("id_restaurante", id_res)
  else
    Query.where("id_restaurante", null)

  Query.where("estatus", 1)
  await Query.then(cuentas => AR.enviarDatos(cuentas, res))
    .catch(e => console.log(e));
});

// Verificar que hayan cuentas.
router.search("/", upload.none(), sesi.validar_cliente, async(req, res) => {
  let r = false;
  const id_restaurante = req.body.id_restaurante || req.headers.id_restaurante;

  const query = await Cuentas.query()
      // .count('id_restaurante AS num_res')
      .select(
        raw('SUM(IF(id_restaurante=?, 1, 0))', id_restaurante).as('cuentas_doomi'),
        raw('SUM(IF(id_restaurante, 0, 1))').as('cuentas_admin'),
        )
      .first()
      .catch(e => console.log(e));

  if(query.cuentas_admin > 0 && query.cuentas_doomi > 0)
    r = true;

  return AR.enviarDatos({r}, res);
});


router.get("/:id", async(req, res) => {
    await CuentaswhitDetails.query()
        .findById(req.params.id)
        .then(cuentas => {
        AR.enviarDatos(cuentas, res);
    });
});


module.exports = router;