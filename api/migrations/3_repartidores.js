exports.up = knex => {
    return knex.schema.createTable("repartidores", table => {
        table.increments("id").primary();
        table.integer("id_repartidor").unique().unsigned();
        table.string("nombre");
        table.string("apellido");
        table.string("identificacion", 2);
        table.string("cedula", 20).unique();
        table.string("usuario", 50).unique();
        table.string("correo", 150).unique();
        table.text("pass");
        table.string("telefono",20).unique();
        table.string("whatsapp",20).unique();
        table.string("sexo",1);
        table.text("img");
        table.text("img_rif");
        table.text("foto_1");
        table.text("foto_2");
        table.text("foto_3");
        table.string("token", 150).unique();
        table.string("refresh_token", 150).unique();
        table.integer("id_tipo_vehiculo").unsigned().references("id").inTable("tipovehiculo");
        table.string("lat", 100).defaultTo(null);
        table.string("lon", 100).defaultTo(null);
        table.boolean("estatus").defaultTo(false);
        table.boolean("aprobado").defaultTo(false);
        table.text("observaciones");
        table.text("domicilio");
        table.boolean("trabajar").defaultTo(true);
    }).raw('ALTER TABLE repartidores AUTO_INCREMENT = 1001');
};

exports.down = knex => {
    return knex.schema.dropTableIfExists("repartidores");
};
