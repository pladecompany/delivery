exports.up = knex => {
    return knex.schema.createTable("pedidos", table => {
        table.increments("id").primary();
        table.string("direccion");
        table.string("punto_referencia");
        table.string("lat_dir_p", 100).defaultTo(null);
        table.string("lon_dir_p", 100).defaultTo(null);
        table.datetime("fecha_pro");
        table.string("modalidad");
        table.string("nota");
        table.string("nota_rep");
        table.integer("id_restaurante").unsigned().references('id').inTable('restaurantes');
        table.integer("id_cliente").unsigned().references('id').inTable('clientes');
        table.integer("id_estatus").unsigned().references('id').inTable('pedidos_estatus');
        table.integer("id_repartidor").unsigned().references('id').inTable('repartidores');
        table.timestamp("creado").defaultTo(knex.fn.now());
        table.integer("nr_pago").unsigned();
        table.float('pago_del', 100, 2);
        table.text("motivo");
        table.text("obs_rep");
        // Al realizar el pago:
        // table.integer("id_metodo").unsigned().references("id").inTable("metodos_de_pago");
        //table.integer("id_metodos_en_cuentas").unsigned().references("id").inTable("metodos_en_cuentas");
        table.integer("id_cuenta").unsigned().references('id').inTable('cuentas');
        table.float('tasa', 100, 2);
        table.string("img_pago");
        table.string("referencia");
        table.float("subtotal").unsigned();
        table.float("subtotal_adicionales").unsigned();
        table.float("subtotal_acompañantes").unsigned();
    }).raw('ALTER TABLE pedidos AUTO_INCREMENT = 1001');
};

exports.down = knex => {
    return knex.schema.dropTableIfExists("pedidos");
};