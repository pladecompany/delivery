exports.up = function(knex, Promise) {
    return knex.schema.alterTable("pedidos", table => {
        table.boolean("preparando_programado").defaultTo(null);
    });
};

exports.down = function(knex, Promise) {
    // return knex.schema.dropTableIfExists("pedidos");
    return knex.schema.table("pedidos", function(table) {
		table.dropColumn("preparando_programado");
    });


};
