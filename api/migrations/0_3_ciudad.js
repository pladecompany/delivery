exports.up = knex => {
    return knex.schema.createTable("ciudad", table => {
      table.increments("id").primary();
      table.string("nombre");
      table.integer("id_estado").unsigned().references("id").inTable("estado");
    });
  };
  
exports.down = knex => {
    return knex.schema.dropTableIfExists("ciudad");
};