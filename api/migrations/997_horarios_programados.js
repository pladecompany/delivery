exports.up = knex => {
    return knex.schema.createTable("horarios_programados", table => {
        table.increments("id").primary();
        table.integer("id_restaurante").unsigned().references('id').inTable('restaurantes');
        table.datetime("inicio");
        table.datetime("fin");
        table.enu("modalidad", ["pickup", "programado"]);
    });
};

exports.down = knex => {
    return knex.schema.dropTableIfExists("horarios_programados");
};
