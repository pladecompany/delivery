exports.up = knex => {
	return knex.schema.createTable("preguntas_frecuentes", table => {
		table.increments("id").primary();
		table.string("pregunta");
		table.text("respuesta");
		table.boolean("estatus").defaultTo(1);
	});
};

exports.down = knex => {
	return knex.schema.dropTableIfExists("preguntas_frecuentes");
};
