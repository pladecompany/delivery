exports.up = function(knex, Promise) {
    return knex.schema.alterTable("carrito_items", table => {
        table.float("precio_usd").unsigned().alter();
    });
};

exports.down = function(knex, Promise) {
	return knex.schema.alterTable("carrito_items", table => {
		table.integer("precio_usd").unsigned().alter();
	});
    // return knex.schema.dropTableIfExists("carrito_items");
};
