exports.up = function(knex, Promise) {
    return knex.schema.alterTable("pedidos", table => {
        table.boolean("buscar_favoritos").defaultTo(false);
        table.datetime("fecha_buscar_fav");
    });
};

exports.down = function(knex, Promise) {
    // return knex.schema.dropTableIfExists("pedidos");
    return knex.schema.table("pedidos", function(table) {
		table.dropColumn("buscar_favoritos");
		table.dropColumn("fecha_buscar_fav");
    });


};
