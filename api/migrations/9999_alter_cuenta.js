exports.up = function(knex, Promise) {
    return knex.schema.alterTable("cuentas", table => {
    	table.boolean("estatus").defaultTo(true);
    });
};

exports.down = function(knex, Promise) {
    return knex.schema.table("cuentas", function(table) {
		table.dropColumn("estatus");
	});
};
