exports.up = knex => {
    return knex.schema.createTable("usuarios", table => {
        table.increments("id").primary();
        table.string("nombre_apellido");
        table.string("cedula", 50).unique();
        table.string("correo", 150).unique();
        table.string("img");
        table.string("telefono",20).unique();
        table.string("whatsapp",20).unique();
        table.text("pass");
        table.string("token", 150).unique();
        table.integer("id_restaurante").unsigned().references("id").inTable("restaurantes").onDelete('CASCADE');
    });
};

exports.down = knex => {
    return knex.schema.dropTableIfExists("usuarios");
};
