exports.up = function(knex, Promise) {
    return knex.schema.alterTable("pagar_pedidos", table => {
        table.boolean("confirmado");
    });
};

exports.down = function(knex, Promise) {
    // return knex.schema.dropTableIfExists("pedidos");
    return knex.schema.table("pagar_pedidos", function(table) {
        table.dropColumn('confirmado');
    });

};
