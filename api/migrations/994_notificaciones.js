exports.up = knex => {
    return knex.schema.createTable("notificacion", table => {
        table.increments("id").primary();
        table.text("contenido");
        table.datetime("fecha");
        table.integer("estatus").defaultTo(0); //1 activo // o inactivo
        table.integer("tipo_noti"); // 0 sinredireccionar // 1 redireccionar // 2 redireccionar con identificador 
        table.string("redireccionar_noti"); 
        table.integer("id_usable");
        table.integer('id_receptor');
        table.integer('id_emisor');
        table.string("tabla_usuario_r");
        table.string("tabla_usuario_e");
    });
  };
  
exports.down = knex => {
    return knex.schema.dropTableIfExists("notificacion");

};
  
  /*tipo_noti:
    1(admin):retirarse,
    2(admin):se retiro,
    3(est):se retiro,
    4(est):pago servicio interno
    5(admin):pagos cuotas
    6(est):cambio de estatus a un pago
    7(est):inscrito
    8(pro):asignar a grupo
    9(pro):asignar estudiante a un servicio interno
    10(est):postulado
    11(pro):servicio interno pagado

    12(est):pago vencido
    13(est):comienzo del grupo
    14(pro):comienzo del grupo
    15(admin):bienvenido admin
    16-17(pro):completar planificacion
    18(admin):multimedia grupo,
    19(est):multimedia grupo*/