exports.up = knex => {
    return knex.schema.createTable("pagar_pedidos", table => {
        table.increments("id").primary();
        table.datetime("fecha_pago");
        table.integer("id_restaurante").unsigned().references('id').inTable('restaurantes');
        table.integer("id_cuenta").unsigned().references('id').inTable('cuentas');
        table.float('pago_del', 100, 2);
        table.float('tasa', 100, 2);
        table.string("referencia",150);
    });
};

exports.down = knex => {
    return knex.schema.dropTableIfExists("pagar_pedidos");
};