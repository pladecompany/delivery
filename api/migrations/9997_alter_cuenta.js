exports.up = function(knex, Promise) {
    return knex.schema.alterTable("cuentas", table => {
    	table.boolean("retorno").defaultTo(false);
    	table.float('porcentaje', 100, 2);
    });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTableIfExists("cuentas");
};
