exports.up = knex => {
	return knex.schema.createTable("carrito", table => {
		table.increments("id").primary();
		table.integer("id_cliente").unsigned().unique().references('id').inTable('clientes');
		table.integer("id_restaurante").unsigned().references("id").inTable("restaurantes");
	});
};

exports.down = knex => {
	return knex.schema.dropTableIfExists("carrito");
};