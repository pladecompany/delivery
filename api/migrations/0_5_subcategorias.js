exports.up = knex => {
	return knex.schema.createTable("subcategorias", table => {
		table.increments("id").primary();
		table.string("nombre", 100);
		table.integer("veces_usada").unsigned().defaultTo(0);
		table.integer("id_categoria").unsigned().references('id').inTable('categorias').onDelete("CASCADE");
	});
};

exports.down = knex => {
	return knex.schema.dropTableIfExists("subcategorias");
};