exports.up = knex => {
    return knex.schema.createTable("restaurantes_subcategorias", table => {
    	table.integer("id_restaurante").unsigned().references("id").inTable("restaurantes").onDelete("CASCADE")
        table.integer("id_categoria").unsigned().references('id').inTable('categorias').onDelete("CASCADE");
        table.integer("id_subcategoria").unsigned().references('id').inTable('subcategorias').onDelete("CASCADE");
    });
};

exports.down = knex => {
    return knex.schema.dropTableIfExists("restaurantes_subcategorias");
};
