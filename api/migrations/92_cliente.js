exports.up = knex => {
    return knex.schema.createTable("clientes", table => {
        table.increments("id").primary();
        table.string("nombre");
        table.string("apellido");
        table.enu("sexo", ["M", 'F', "O"]);
        table.string("cedula", 20).unique();
        table.string("identificacion", 2);
        table.string("correo", 150).unique();
        table.string("usuario", 50).unique();
        table.string("telefono", 20);
        table.string("whatsapp",20).unique();
        table.string("direccion");
        table.string("facebook");
        table.string("instagram");
        table.string("punto_referencia").defaultTo(null);
        table.string("foto");
        table.string("lat", 100).defaultTo(null);
        table.string("lon", 100).defaultTo(null);
        table.integer("id_pais").unsigned().references('id').inTable('pais');
        table.integer("id_estado").unsigned().references('id').inTable('estado');
        table.integer("id_ciudad").unsigned().references('id').inTable('ciudad');
        table.text("pass");
        table.string("token", 150).unique();
        table.string("refresh_token", 150).unique();
        table.boolean("activado").defaultTo(false);
        table.timestamp("fecha_registro").defaultTo(knex.fn.now());
    }).raw('ALTER TABLE clientes AUTO_INCREMENT = 1001');
};

exports.down = knex => {
    return knex.schema.dropTableIfExists("clientes");
};
