exports.up = function(knex, Promise) {
    return knex.schema.alterTable("pedidos", table => {
        // table.boolean("delivery_pagado").defaultTo(false);
        table.string("ref_pago_del",150);
        table.integer("id_pago").unsigned().references('id').inTable('pagar_pedidos');
    });
};

exports.down = function(knex, Promise) {
    // return knex.schema.dropTableIfExists("pedidos");
    return knex.schema.table("pedidos", function(table) {
		// table.dropColumn("delivery_pagado");
		table.dropColumn("ref_pago_del");
		table.dropColumn("id_pago");
    });


};
