exports.up = knex => {
	return knex.schema.createTable("menus", table => {
		table.increments("id").primary();
		table.string("nombre", 100);
		table.string("descripcion");
		table.float("precio_usd").unsigned();
		table.integer("existencias").unsigned().defaultTo(0);
		table.text("img");
		table.boolean("estatus");
		table.integer("id_categoria").unsigned().references('id').inTable('categorias_menu');
		table.integer("id_restaurante").unsigned().references('id').inTable('restaurantes').onDelete("CASCADE");
	});
};

exports.down = knex => {
	return knex.schema.dropTableIfExists("menus");
};