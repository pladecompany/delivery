exports.up = knex => {
	return knex.schema.createTable("repartidores_favoritos", table => {
		table.increments("id").primary();
		table.integer("id_repartidor").unsigned().references('id').inTable('repartidores').onDelete("CASCADE");
		table.integer("id_restaurante").unsigned().references('id').inTable('restaurantes').onDelete("CASCADE");
	});
};

exports.down = knex => {
	return knex.schema.dropTableIfExists("repartidores_favoritos");
};