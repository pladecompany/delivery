exports.up = knex => {
    return knex.schema.createTable("restaurantes", table => {
        table.increments("id").primary();
        table.string("nombre");
        table.text("descripcion");
        table.text("ubicacion");
        table.string("lat", 100).defaultTo(null);
        table.string("lon", 100).defaultTo(null);
        table.text("img");
        table.text("img_portada");
        table.string("telefono", 15);
        table.string("facebook").defaultTo(null);
        table.string("instagram").defaultTo(null);
        table.enu("rif_letra", ["J", "G", "V", "E", "P"]);
        table.integer("rif_numero").unsigned();
        table.tinyint("rif_chequeo");
        table.integer("id_categoria").unsigned().references('id').inTable('categorias');
        table.integer("id_subcategoria").unsigned().references('id').inTable('subcategorias');
        table.integer("id_pais").unsigned().references('id').inTable('pais');
        table.integer("id_estado").unsigned().references('id').inTable('estado');
        table.integer("id_ciudad").unsigned().references('id').inTable('ciudad');
        table.float('pre_tasa', 100, 2);
        table.boolean("estatus").defaultTo(true);
        table.boolean("estatus_cuenta").defaultTo(true);
        table.boolean("laborando").defaultTo(true);
        table.boolean("modalidad_delivery");
        table.boolean("modalidad_pickup");
        table.boolean("modalidad_programados");
        table.integer("modalidad_programados_cantidad").unsigned();
        table.tinyint("pasos").unsigned().defaultTo(0);
        table.boolean("vacaciones").defaultTo(false);
        table.string("dia_cerrado", 15).defaultTo(0);
        // table.enu("pedidos_programados", ["A", "D"]); // A:ahora D:despues
        // table.string("pedidos_programados_dias", 10).defaultTo(null);
    }).raw('ALTER TABLE restaurantes AUTO_INCREMENT = 1001');
};

exports.down = knex => {
    return knex.schema.dropTableIfExists("restaurantes");
};
