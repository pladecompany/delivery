exports.up = function(knex, Promise) {
    return knex.schema.alterTable("pedidos", table => {
        //0: pendiente, 1: confirmado, 2:en disputa
        // table.enu("delivery_pagado", ['0', '1', '2']).defaultTo('0').alter();
        // table.tinyint('delivery_pagado').defaultTo(0).alter();
        table.tinyint('delivery_pagado').defaultTo(0);
        table.string('pago_motivo');
        table.string('pago_datos');
    });
};

exports.down = function(knex, Promise) {
    // return knex.schema.dropTableIfExists("pedidos");
    return knex.schema.table("pedidos", function(table) {
        table.boolean("delivery_pagado").defaultTo(0);
        table.dropColumn('pago_motivo');
        table.dropColumn('pago_datos');
    });

};
