exports.up = knex => {
	return knex.schema.createTable("carrito_items", table => {
		table.increments("id").primary();
		table.integer("cantidad").unsigned();
		table.string("notas");
		table.integer("id_menu").unsigned();//.references("id").inTable("menus");
		table.integer("id_carrito").unsigned().references("id").inTable("carrito");

		table.integer("id_pedido").unsigned().references("id").inTable("pedidos");

		table.integer("precio_usd").unsigned();
		table.string("nombre");
	});
};

exports.down = knex => {
	return knex.schema.dropTableIfExists("carrito_items");
};