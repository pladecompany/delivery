exports.up = knex => {
    return knex.schema.createTable("estado", table => {
      table.increments("id").primary();
      table.string("nombre");
      table.integer("id_pais").unsigned().references("id").inTable("pais");
	});
  };
  
exports.down = knex => {
    return knex.schema.dropTableIfExists("estado");
};