exports.up = function(knex, Promise) {
    return knex.schema.alterTable("pedidos", table => {
        table.integer("tiempo_buscar").defaultTo(0);
    });
};

exports.down = function(knex, Promise) {
    // return knex.schema.dropTableIfExists("pedidos");
    return knex.schema.table("pedidos", function(table) {
		table.dropColumn("tiempo_buscar");
	});


};
