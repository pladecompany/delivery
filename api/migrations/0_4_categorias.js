exports.up = knex => {
	return knex.schema.createTable("categorias", table => {
		table.increments("id").primary();
		table.string("nombre", 100);
		table.integer("veces_usada").unsigned().defaultTo(0);
	});
};

exports.down = knex => {
	return knex.schema.dropTableIfExists("categorias");
};