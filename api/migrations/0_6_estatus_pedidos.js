exports.up = knex => {
    return knex.schema.createTable("pedidos_estatus", table => {
        table.increments("id").primary();
        table.string("nombre");
        table.string("descripcion");
        table.boolean("detalles").defaultTo(true);
    });
};

exports.down = knex => {
    return knex.schema.dropTableIfExists("pedidos_estatus");
};
