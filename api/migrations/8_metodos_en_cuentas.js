
exports.up = function(knex, Promise) {
    return knex.schema.createTable("metodos_en_cuentas", table => {
        table.increments("id").primary();
        table.integer("id_cuenta").unsigned().references('id').inTable('cuentas');
        table.integer("id_metodo").unsigned().references('id').inTable('metodos_de_pago');
    });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTableIfExists("metodos_en_cuentas");
};
