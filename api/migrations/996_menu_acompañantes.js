exports.up = knex => {
	return knex.schema.createTable("menus_acompañantes", table => {
		table.increments("id").primary();
		table.integer("id_menu").unsigned().references("menus.id").onDelete("CASCADE");
		table.integer("id_menu_acompañante").unsigned().references("menus.id").onDelete("CASCADE");
	});
};

exports.down = knex => {
	return knex.schema.dropTableIfExists("menus_acompañantes");
};
