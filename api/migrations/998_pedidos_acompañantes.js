exports.up = knex => {
    return knex.schema.createTable("pedidos_acompañantes", table => {
        table.increments("id").primary();
        table.integer("id_item").unsigned().references('carrito_items.id').onDelete('CASCADE');
        table.integer("id_acompañante").unsigned().references('menus_acompañantes.id');
        table.integer("cantidad").unsigned();
        table.string("nombre");
        table.float("precio_usd").unsigned();
    });
};

exports.down = knex => {
    return knex.schema.dropTableIfExists("pedidos_acompañantes");
};
