exports.up = knex => {
    return knex.schema.createTable("pedidos_ordenes", table => {
        table.increments("id").primary();
        table.integer("id_menu").unsigned().references('menus.id');
        table.integer("id_pedido").unsigned().references('pedidos.id');
        table.integer("cantidad").unsigned();
        table.string("nota");
    });
};

exports.down = knex => {
    return knex.schema.dropTableIfExists("pedidos_ordenes");
};