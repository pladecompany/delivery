import axios from 'axios'
import config from '../config'
import { LocalStorage } from 'quasar'

// Crear instancia de axios.
const axiosAPI = axios.create({
  baseURL: config.apiURL,
})

// Configurar los headers globalmente.
Object.assign(axiosAPI.defaults.headers.common, config.headers)

// Incluir token en cada peticion.
axiosAPI.interceptors.request.use(
  config => {
    const token = LocalStorage.getItem('TOKEN')
    const id = LocalStorage.getItem('IDUSUARIO')

    if (token) {
      config.headers['token'] = token
    }
    if (id) {
      config.headers['id_doomi'] = id;
    }

    config.headers['tipo_usuario'] = 'restaurante';
    return config
  },
  error => { Promise.reject(error) }
)

// Para que este disponible en los componentes y en store.
export default ({ store, Vue }) => {
  Vue.prototype.$axios = axios
  Vue.prototype.$api = axiosAPI
  store.$axios = axios
  store.$api = axiosAPI


  // Interceptar codigos 401.
  const UNAUTHORIZED = 401;

  axiosAPI.interceptors.response.use(
    response => response,
    error => {
      const { status } = error.response || {};

      if (status === UNAUTHORIZED) {
        store.dispatch('auth/salir').then(() => {
          if(document.location.hash.toLowerCase() != '#/login')
            document.location.href = '#/login';
        });
      }
      return Promise.reject(error);
    }
  );

}

