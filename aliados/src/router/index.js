import Vue from 'vue'
import VueRouter from 'vue-router'

import routes from './routes'
// import { auth } from '../api'

Vue.use(VueRouter)

export const EventBus = new Vue();

export default function ({ store, ssrContext }) {
  const Router = new VueRouter({
    scrollBehavior: () => ({ x: 0, y: 0 }),
    routes,

    // Leave these as they are and change in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  })

  // Viligar rutas que requieren login.
  Router.beforeEach((to, from, next) => {
    if(to.matched.some(record => record.meta.autenticar) && !store.getters['auth/logueado']) {
      return next('/login')
    }
    else {
      next()
    }
  })

  return Router
}
