
const routes = [
  // Redirigir al login.
  {
    path: '/',
    beforeEnter: (to, from, next) => next('/login')
  },

  {
    path: '/login',
    name: 'login',
    component: () => import('pages/Iniciodesesion.vue'),
  },

  // Anidar rutas para vigilar autenticacion
  {
    path: '/',
    component: () => import('layouts/Empty.vue'),
    // meta: { autenticar: true },
    children: [
      { name: 'home', path: '/home', component: () => import('pages/Home.vue') },
      { name: 'pedidos', path: '/pedidos', component: () => import('pages/Pedidos.vue') },
      { name: 'menu_configuraciones', path: '/menu_configuraciones', component: () => import('pages/Menu_configuraciones.vue') },
      { name: 'Detalles', path: '/Detalles/:id', component: () => import('pages/Detallescliente.vue') },
      { name: 'Nuevo_plato', path: '/Nuevo_plato/:id?', component: () => import('pages/Nuevo_plato.vue') },
      { name: 'Menus', path: '/Menus', component: () => import('pages/Menudoomialiadohome.vue') },
      { name: 'Favoritos', path: '/Favoritos', component: () => import('pages/Repartidoresfavoritos.vue') },
      { name: 'Datosbancarios', path: '/Datosbancarios', component: () => import('pages/Datosbancarios.vue') },
      { name: 'Listacategorias', path: '/Listacategorias', component: () => import('pages/Listacategorias.vue') },
      { name: 'Historial', path: '/Historial', component: () => import('pages/Historial.vue') },
      { name: 'Historial2', path: '/Historial/:id?', component: () => import('pages/Historial.vue') },
      { name: 'Perfil', path: '/Perfil', component: () => import('pages/ProfileRepartidor.vue') },
      { name: 'Cambiarcontraseña', path: '/Cambiar', component: () => import('pages/Cambiarcontraseña.vue') },
      { name: 'Misdirecciones', path: '/Misdirecciones', component: () => import('pages/Misdirecciones.vue') },
      { name: 'Horarios', path: '/Horarios', component: () => import('pages/Horarios.vue') },
      { name: 'Programados', path: '/Programados', component: () => import('pages/Programados.vue') },
    ]
  },

  // Rutas sin layout.
  {
    path: '/',
    component: () => import('layouts/Empty.vue'),
    meta: { autenticar: true },
    children: [
    ]
  },

];



// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
