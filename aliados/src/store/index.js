import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import Swal from 'sweetalert2';

// import example from './module-example'
// import productos from './modules/productos'
// import cliente from './modules/cliente'
import doomi from './modules/doomi'
// import carrito from './modules/carrito'
// import ordenes from './modules/ordenes'
// import notificaciones from './modules/notificaciones'
// import repartidor from './modules/repartidor'
import auth from './modules/auth'
// import { auth, general, config } from '../api'


Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      // productos,
      // cliente,
      doomi,
      // carrito,
      // ordenes,
      // notificaciones,
      // repartidor,
      auth,
    },

    state: {
      titleCurrenView: 'Doomi',
      leftDrawerState: false,
      Fav:0,
      Pedido:0,
      swalConfig: {
        showClass: {popup: 'animate__animated animate__fadeIn'},
        hideClass: {popup: 'animate__animated animate__fadeOut'},
      },
    },


    getters: {
      leftDrawerState(state){
        return state.leftDrawerState;
      },
    },


    mutations: {
      toggleLeftDrawer(state){
        state.leftDrawerState = !state.leftDrawerState;
      },
      openLeftDrawer(state){
        state.leftDrawerState = true;
      },
      closeLeftDrawer(state){
        state.leftDrawerState = false;
      },
      setLeftDrawer(state, v){
        state.leftDrawerState = Boolean(v);
      },
    },


    actions: {
      actualizarTodo(ctx) {
        ctx.dispatch('doomi/actualizar');
      },

      _swal(ctx, conf){
        return new Promise(function(resolve){
          Swal.fire({
            // icon: 'info',
            // cancelButtonText: 'Cerrar',
            confirmButtonText: 'OK',
            ...ctx.state.swalConfig,
            ...conf,
          })
          .then(resolve);
        });
      },

      swalSuccess(ctx, conf){
        return new Promise((resolve) => {
          if(typeof conf == 'string')
            conf = {text: conf};

          ctx.dispatch('_swal', {icon:'success', ...conf}).then(resolve);
        });
      },

      swalInfo(ctx, conf){
        return new Promise((resolve) => {
          if(typeof conf == 'string')
            conf = {text: conf};

          ctx.dispatch('_swal', {icon:'info', ...conf}).then(resolve);
        });
      },

      swalOtroDoomi(context, conf){
        Swal.fire({
          icon: 'warning',
          showCancelButton: true,
          cancelButtonText: 'Ir al doomialiado',
          confirmButtonText: 'Cancelar orden anterior',
          ...context.state.swalConfig,
          ...conf,
        })
        .then(res => {
          if(res.isConfirmed) {
            context.dispatch('carrito/vaciar');
          }
          else if(res.isDismissed && res.dismiss == 'cancel')
            this.$router.push({path: conf.path});
        });
      },

    },

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEV
  })

  return Store
}
