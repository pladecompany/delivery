import { LocalStorage } from 'quasar'


function defaultState() {
  return {
    token: LocalStorage.getItem('TOKEN'),
    id: LocalStorage.getItem('IDUSUARIO'),
    doomi: null,
    usuario: null,
  };
}


const getters = {
  id: state => state.id || state.usuario?.id,
  token: state => state.token,
  doomi: state => state.doomi,
  usuario: state => state.usuario,
  logueado: state => Boolean(state.token && state.id),
};


const mutations = {
  // SET_ID: (state, id) => {
  //   state.id = id;
  //   LocalStorage.set('IDUSUARIO', id);
  // },
  SET_TOKEN: (state, token) => {
    state.token = token;
    LocalStorage.set('TOKEN', token);
  },
  SET_DOOMI: (state, doomi) => {
    state.doomi = doomi;
  },
  SET_USUARIO: (state, usuario) => {
    state.usuario = usuario;
    LocalStorage.set('IDUSUARIO', usuario.id);
  },

  LIMPIAR: state => {
    LocalStorage.remove('TOKEN');
    LocalStorage.remove('IDUSUARIO');
    Object.assign(state, defaultState())
  },
};


const actions = {
  login(ctx, info){
    return new Promise((resolve, reject) => {
      const correo = info.correo || info.email;
      const pass = info.pass || info.password || info['contraseña'];

      const data = {correo, pass};

      this.$api.post('/login', data)
        .then(resp => {
          if (!resp.data || !resp.data[0]) return reject(resp);

          const usuario = resp.data[0];
          const doomi = usuario.restaurante[0];

          // ctx.commit('SET_DOOMI', doomi);
          ctx.commit('SET_USUARIO', usuario);
          ctx.commit('SET_TOKEN', usuario.token);
          ctx.commit('doomi/SET', doomi, {root:true});

          return resolve(resp);
        })
        .catch(reject);
    });
  },

  actualizarUsuario(ctx) {
    return new Promise((resolve, reject) => {
      const id_usu = ctx.getters['id'];
      this.$api
        .get('/perfiles/encargado/'+id_usu)
        .then(res => { ctx.commit('SET_USUARIO', res.data); })
        .catch(reject)
    });
  },

  salir(ctx, callback){
    return new Promise(function(resolve, reject) {
      ctx.commit('LIMPIAR');
      ctx.commit('doomi/LIMPIAR', null, {root:true});
      resolve(callback ? callback() : true);
    });
  },
};




export default {
  namespaced: true,
  state: defaultState,
  getters,
  mutations,
  actions,
}
