import Vue from 'vue'
import axios from 'axios'
import estatus_ordenes from '../estatus-ordenes'
import { utils, general, auth, config } from '../../api'

export default
{
  namespaced: true,

  state: () => ({
    pedidos: {},
    estatus: [],
    busquedaPedidos: [],
  }),


  getters:
  {
    all: state => Object.values(state.pedidos),

    estatus: state => state.estatus,

    items: state => Object.keys(state.pedidos).length,

    porDoomialiado: (state) => (id) => state.pedidos[id],

    porId: (state) => (id) => state.pedidos[id],

    total: (state) => (idOrden) => {
      const orden = state.pedidos[idOrden];

      return orden.pedidos.reduce((num,p) => utils.sumarDinero(num, p.TOTAL), 0);
    },

    subtotalAdicionales: (state) => (idOrden) => {
      const orden = state.pedidos[idOrden];

      return orden.pedidos.reduce((num, p) =>
          utils.sumarDinero(num, p.SUBTOTAL_ADICIONALES), 0);
    },

    subtotal: (state) => (idOrden) => {
      const orden = state.pedidos[idOrden];

      return orden.pedidos.reduce((num,p) => utils.sumarDinero(num, p.SUBTOTAL), 0)
    },

    busquedaPedidos: state => state.busquedaPedidos,
  },


  mutations:
  {
    SET_PEDIDO: (state, payload) => Vue.set(state.pedidos, payload[0], payload[1]),
    DEL_PEDIDO: (state, id) => Vue.delete(state.pedidos, id),
    GUARDAR_ESTATUS: (state, estatus) => state.estatus = estatus,

    SET_BUSQUEDA_PEDIDOS: (state, payload) => state.busquedaPedidos = payload,
  },


  actions:
  {
    _afterCheckout ({commit}, datos) {
      commit('SET_PEDIDO', [datos.id_restaurante, datos]);
      return new Promise((res,rej) => res('yay'));
    },

    cancelar({commit}, id){
      return new Promise(function(resolve, reject) {
        axios({
          url: config.apiURL + 'pedidos/'+id,
          method: 'DELETE',
        })
        .then(resp => {
          if(resp.data && resp.data.r == true)
            resolve(resp.data.msj);
          else if(resp.data && resp.data.r == false)
            resolve('Ha ocurrido un error');
        });
        commit('DEL_PEDIDO', id);
      });
    },

    actualizarEstatus({commit}, id){
      general.recuperarData('/pedidos/estatus')
        .then(data => commit('GUARDAR_ESTATUS', data));
    },

    actualizarPedidos(ctx){
      if(auth.obtenerTipoUsuario() == 'repartidor'){
        general.recuperarData('/pedidos/repartidor')
          .then(pedidos => {
            pedidos.forEach(p => ctx.commit('SET_PEDIDO', [p.id, p]))
          })
      }
    },
  },




};
