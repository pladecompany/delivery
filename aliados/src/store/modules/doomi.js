import { LocalStorage } from 'quasar'


function defaultState() {
  return {
    id: LocalStorage.getItem('IDDOOMI'),
    data: null,
  };
}


const getters = {
  id: state => state.id,
  nombre: state => state.data?.nombre,
  calificacion: state => state.data?.calificacion,
  img: state => state.data?.img,
  imgPortada: state => state.data?.img_portada,
  datos: state => state.data,
  categoria: state => state.data?.categoria?.nombre,
  subcategorias: state => state.data?.subcategorias?.map(s => s.nombre),
};


const mutations = {
  SET: (state, doomi) => {
    LocalStorage.set('IDDOOMI', doomi.id);
    state.data = doomi;
    state.id = doomi.id;
  },

  LIMPIAR: (state) => {
    LocalStorage.remove('IDDOOMI');
    Object.assign(state, defaultState());
  },
};


const actions = {
  actualizar(ctx) {
    return new Promise((resolve, reject) => {
      const id_doomi = ctx.getters['id'];

      if (!id_doomi) return reject('NO ID');

      this.$api
        .get('/restaurantes/'+id_doomi)
        .then(r => {
          ctx.commit('SET', r.data);
          resolve(r.data);
        })
        .catch(reject);
    });
  },


  set(ctx, doomi) {
    ctx.commit('SET', doomi);
  },
};


export default {
  namespaced: true,
  state: defaultState,
  getters,
  mutations,
  actions,
}
