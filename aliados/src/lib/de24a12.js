/*
 * Convierte de formato 24h a 12h
 * '16:00:00' -> '04:00PM'
 */


function de24a12(hora) {
  // Verificar formato correcto de la hora y separar en componentes.
  hora = hora.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [hora];

  if (hora.length > 1) { // If hora format correct
    hora = hora.slice (1);  // Remove full string match value
    hora[5] = +hora[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
    hora[0] = +hora[0] % 12 || 12; // Adjust hours
  }
  hora = hora.filter(Boolean);
  if (hora.length === 5)
    delete hora[3]; // Quitar segundos si los tiene.

  return hora.join (''); // return adjusted time or original string
}


export default de24a12;
