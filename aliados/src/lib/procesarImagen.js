
/*
 * Recibe el evento nativo de input: @input.native=""
 */
export default function procesarImagen(e) {
  return new Promise((resolve, reject) => {
    let files = e.target.files || e.dataTransfer.files;
    let name = e.target.name;
    if (!files.length) return;
    let file = files[0];
    if (file.size / 1024 > 10240) {
      console.log('La imagen es muy grande!')
      resolve({});
    }
    else {
      processImageFile(file, name)
        .then(result => resolve(result));
    }

  });
}


function processImageFile(file, name) {
  return new Promise((resolve, reject) => {
    if (!file || !window.FileReader) return false;
    if (/^image/.test(file.type)) {
      // Create a reader.
      let reader = new FileReader();
      // Convert the image to base64 format.
      reader.readAsDataURL(file);
      // Read the callback after successful.
      reader.onloadend = function() {
        let result = this.result;
        let img = new Image();
        img.src = result;
        resolve(result);
        // vue_context[name] = result;
      }
    }
  });
}


// export default procesarImagen;