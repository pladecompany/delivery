
const config = Object.freeze({
  // apiURL:'http://localhost:1210/',
  // apiURL:'https://inthecompanies.com:1210/',
  apiURL:'https://restaurante.doomi.app:2222/',
  headers: {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Headers': '*',
    'tipo-cliente': 'app',
    },

});

export default config;
export const apiURL = config.apiURL;
export const headers = config.headers;
