function iniciojs(){
    //listar
    if(userDOOMI.tipo_usu=='administrador')
        contadores();
    else {
        contadores_res();
        pasos_res();
    }

    function contadores() {
        data = {}
        $.ajax({
            url: dominio + 'administradores/escritorio/',
            type: 'GET',
            headers: {
                'token':userDOOMI.token,
                'tipo_usuario': userDOOMI.tipo_usu
            },
            data: data,
            success: function(data) {
                //console.log(data);
                $(".conta-rep").html("("+data.repartidores+")");
                $(".conta-cli").html("("+data.clientes+")");
                $(".conta-res").html("("+data.restaurantes+")");
                $(".conta-sol").html("("+data.solicitudes+")");
            }
        })
    }

    function contadores_res() {
        data = {id_res:userDOOMI.restaurante[0].id}
        $.ajax({
            url: dominio + 'restaurantes/escritorio/',
            type: 'GET',
            headers: {
                'token':userDOOMI.token,
                'tipo_usuario': userDOOMI.tipo_usu
            },
            data: data,
            success: function(data) {
                //console.log(data);
                $(".conta-men").html("("+data.menu+")");
                $(".conta-cat").html("("+data.categorias+")");
                $(".conta-ped").html("("+data.pedidos+")");
            }
        })
    }

    function pasos_res(){
        const id_res = userDOOMI.restaurante[0].id;
        const pasos = userDOOMI.restaurante[0].pasos;

        if(pasos==63){
            $('#listado-pasos-index').hide();
        }

        ajaxGET(`/restaurantes/${id_res}/pasos`, data => {
            if(!data || !Array.isArray(data))
                return console.log('err pasos');

            data.forEach(paso => {
                $(`#progress-paso-${paso}`).css('width', '100%');
            });
        });
    }


}
