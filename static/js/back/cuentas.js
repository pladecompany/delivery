function cuentasjs(){

    //listar
    add_cuenta = true;
    cuentas();
    //metodos_pagos();
    $(".cuenta").mask("9999-9999-9999-9999-9999");

    function cuentas() {
        if(userDOOMI.tipo_usu=='administrador'){
            data = {
                tipo_usu: userDOOMI.tipo_usu
            }
        }else{
            data = {
                id_res: userDOOMI.restaurante[0].id,
                tipo_usu: userDOOMI.tipo_usu
            }
        }
        $.ajax({
            url: dominio + 'cuentas/',
            headers: {
                'token':userDOOMI.token,
                'tipo_usuario': userDOOMI.tipo_usu
            },
            type: 'GET',
            data: data,
            success: function(data) {
                //console.log(data);
                actualizar_tabla_cuentas(data);
            }
        })
    }

    function actualizar_tabla_cuentas(data) {

        var table = $('.table').DataTable({
            "language": {
                "url": "../static/<lib></lib>/JTables/Spanish.json"
            }
        });
        count = 0
        table.clear().draw();

        for (var i = 0; i < data.length; i++) {
            datos = data[i];
            var options = 'N/A';

            if(!datos.retorno){
                options = `
                    <a href="#!" id="${datos.id}" class="btn btn-primary btn-floating ver-info tooltipped" data-position="bottom" data-delay="50" data-tooltip="Ver información de la cuenta"><i class="fa fa-eye"></i></a>

                    <a href="#!" id="${datos.id}" class="btn btn-primary btn-floating edit-info tooltipped" data-position="bottom" data-delay="50" data-tooltip="Editar información de la cuenta"><i class="fa fa-edit"></i></a>

                    <a href="#!" id="${datos.id}" class="waves-effect btn btn-primary btn-floating tooltipped delete" data-position="bottom" data-delay="50" data-tooltip="Eliminar cuenta"><i class="fa fa-times"></i></a>
                `;
            }

            estatusSwitch = `<div class="switch">
                            <small class="clr_primary" style="font-size: 10px;">Deshabilitado</small>
                            <label>

                            <input type="checkbox" retorno="${datos.retorno}" id="${datos.id}" ${datos.estatus?'checked="true"':''} class="btn btn-primary btn-floating tooltipped estatus-switch-c-${datos.id} estatus-switch-c">
                                <span class="lever"></span> <small class="clr_primary" style="font-size: 10px;">Habilitado</small>
                            </label>
                        </div>`;

            var row = [count += 1, datos.nombre, estatusSwitch, options];
            table.row.add(row).draw().node();
        }
        $('.tooltipped').tooltip();
    }

    $("table").on('change', '.estatus-switch-c', async function() {
        let data = {
            iden:this.id,
            sts: this.checked ? 1 : 0
        };
        if($(this).attr("retorno")==1){
            if(this.checked){
                $(".estatus-switch-c-"+this.id).prop('checked',false);
                var toastContent = '<span>Los pedidos en efectivo tendrán 50% de recargo del delivery al cliente</span><br><button class="btn-flat toast-action conf_si_sc" id="'+this.id+'">Agregar</button><button class="btn-flat toast-action" onclick=" $(\'.toast\').hide(); ">No</button>';
                Materialize.toast(toastContent, 4000);
            }else{
                guardarcambioestatus(data,1)
            }
        }else
            guardarcambioestatus(data,1)


    });

    $(document).on('click', '.conf_si_sc', async function() {
        let data = {
            iden:this.id,
            sts: 1
        };
        $('.toast').hide();
        guardarcambioestatus(data,2)
    });
    function guardarcambioestatus(data,tipo) {
        $.ajax({
            url: dominio + "cuentas/edit_sts"
            ,headers: {token:userDOOMI.token,tipo_usuario: userDOOMI.tipo_usu}
            ,data: data
            ,type:"PUT"
            ,success: function(data) {
                Materialize.toast(data.msj, 3000)
                if(data.r && tipo==2)
                    $(".estatus-switch-c-"+data.id).prop('checked',true);
            }
            ,error: function(xhr, status, errorThrown){
                console.log(errorThrown);
            }
        });
    }

    function metodos_pagos() {
        if(userDOOMI.tipo_usu=='administrador'){
            data = {
                tipo_usu: userDOOMI.tipo_usu
            }
        }else{
            data = {
                id_res: userDOOMI.restaurante[0].id,
                tipo_usu: userDOOMI.tipo_usu
            }
        }
        $.ajax({
            url: dominio + 'metodos_pagos/',
            headers: {
                'token':userDOOMI.token,
                'tipo_usuario': userDOOMI.tipo_usu
            },
            type: 'GET',
            data: data,
            success: function(data) {
                //console.log(data);
                actualizar_tabla_metodos_pagos(data)
            }
        })
    }

    function actualizar_tabla_metodos_pagos(data) {
        count = 0;
        if(data.length==0){
            add_cuenta = false;
        }
        for (var i = 0; i < data.length; i++) {
            datos = data[i];
            var options = `
                <div class="col s12 text-center">
                    <p>
                        <input class="with-gap check-all" name="metodos[]" type="checkbox" id="__metodo_${datos.id}" value="${datos.id}">
                        <label for="__metodo_${datos.id}"></label>
                    </p>
                </div>
            `;
            var tr = `
                    <tr>
                        <td>${count += 1}</td>
                        <td>${datos.nombre}</td>
                        <td>${options}</td>
                    </tr>
            `;
            $('#list-metodos').append(tr);
        }
    }

    $("#form_registrar_cuenta").on('submit', function(e) {

        e.preventDefault();

        /*validaciones*/
        var errr = false;
        var msj = false;

        if ( $("#nombre").val().trim() == "" ){ errr = true; msj = "Ingresa un nombre para la cuenta";}
        if ( $("#retorno").prop("checked") && $("#porcentaje").val().trim() == "" ){ errr = true; msj = "Ingresa el porcentaje para el retorno";}
        /*fin validaciones*/
        if (errr) {
            Materialize.toast(msj, 4000); return false;
        }
        //data a enviar
        var data = new FormData(this);
        data.append("tipo_usu", userDOOMI.tipo_usu );
        if(userDOOMI.tipo_usu!='administrador')
            data.append("id_res", userDOOMI.restaurante[0].id );
        if($("#iden").val() != ''){
            url = "cuentas/edit";
            tipo = "PUT";
        }else{
            url = "cuentas/add";
            tipo = "POST";
        }
        $(".bt_save").prop("disabled", true);
        $(".fileprogress").removeClass("hide");
        $(".fileprogress").find("div")[0].style.width = "0%";
        $.ajax({
            url: dominio + url,
            headers: {
                'token':userDOOMI.token,
                'tipo_usuario': userDOOMI.tipo_usu
            },
            type: tipo,
            dataType: "json",
            cache: false,
            data: data,
            contentType: false,
            processData: false,
            xhr: function(){
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", function(e){
                    if (e.lengthComputable) {
                      var percentComplete = parseInt( (e.loaded / e.total * 100), 10);
                      $(".fileprogress").find("div")[0].style.width = `${percentComplete}%`;
                      if(percentComplete>=100){
                        $(".fileprogress").addClass("hide");
                        $(".fileprogress").find("div")[0].style.width = "0%";
                      }
                    }
                }, false);
                xhr.addEventListener("progress", function(e){
                    if (e.lengthComputable) {
                        var percentComplete = parseInt( (e.loaded / e.total * 100), 10);
                        $(".fileprogress").find("div")[0].style.width = `${percentComplete}%`;
                        if(percentComplete>=100){
                            $(".fileprogress").addClass("hide");
                            $(".fileprogress").find("div")[0].style.width = "0%";
                        }
                    }
                }, false);
                return xhr;
            },
            success: function(res) {
                Materialize.toast(res.msj, 4000)
                if (res.r == true) {
                    $("#form_registrar_cuenta")[0].reset();
                    cuentas();
                    $("#md-nuevaCuenta").modal('close');
                }
                //botones del form
                $(".bt_save").prop("disabled", false);
                completarPaso(4);
            },
            error: function(xhr, status, errorThrown) {
                console.log(errorThrown);
                $(".fileprogress").addClass("hide");
                $(".fileprogress").find("div")[0].style.width = "0%";
                Materialize.toast(errorThrown.msj, 4000)

                //botones del form
                $(".bt_save").prop("disabled", false)
            }
        });
    });
    $(document).on('click', '.verificar', function() {
        if(add_cuenta){
            $("#form_registrar_cuenta")[0].reset();
            $("#iden").val("");
            $(".div_por").hide();
            $("#retorno").prop("checked", false);
            $("#porcentaje").val('');
            $(".bt_save").html("Guardar");
            $("#tit-mod").html("Nueva cuenta");
            $(".check-all").prop("checked", false);
            $("#md-nuevaCuenta").modal("open");
        }else{
            Materialize.toast("Debes agregar métodos de pago", 10000);
        }
    });
    $(document).on('click', '.ver-info', function() {
        var idv = this.id;
        buscar_data(idv,'ver')
    });
    $(document).on('click', '.edit-info', function() {
        var idv = this.id;
        buscar_data(idv,'editar')
    });
    function buscar_data(idb,modal){
        data = {
        }
        $.ajax({
            url: dominio + 'cuentas/'+idb,
            headers: {
                'token':userDOOMI.token,
                'tipo_usuario': userDOOMI.tipo_usu
            },
            type: 'GET',
            data: data,
            success: function(data) {

                 //console.log(data);
                imprimir_data_credencial(data,modal)

            }, error: function(xhr, status, errorThrown) {
                console.log(errorThrown);
            }

        })
    }
    function imprimir_data_credencial(data,modal){
        if(modal=='ver'){
            $(".nom-view").html(data.nombre);
            //$(".num-view").html(data.numeno);
            //$(".tip-view").html(data.tipo);
            $(".des-view").html(data.detalles.replace(/\n/g, "<br>") );
            //if(data.retorno)
            //    $(".ret-view").html('Si ('+data.porcentaje+'%)')
            //else
            //    $(".ret-view").html('No')
            /*$('#list-view-metodos').html('<tr><th class="text-center">#</th><th>Nombre</th></tr>');
            if(data.metodos[0]){
                count = 0;
                for (var i = 0; i < data.metodos.length; i++) {
                   var tr = `
                        <tr>
                            <td class="text-center">${count += 1}</td>
                            <td>${data.metodos[i].nombre}</td>
                        </tr>
                `;
                $('#list-view-metodos').append(tr);

                };
            }*/
            $("#md-verCuenta").modal("open");
        }else if(modal=='editar'){
            $("#nombre").val(data.nombre);
            //$("#numero").val(data.numeno);
            //$("#tipo").val(data.tipo);
            $("#ext").val(data.detalles);
            $("#iden").val(data.id);
            /*$(".check-all").prop("checked", false);
            if(data.metodos[0]){
                for (var i = 0; i < data.metodos.length; i++) {
                    $("#__metodo_"+data.metodos[i].id_metodo).prop("checked", true);
                };
            }*/
            /*console.log(data.retorno);
            if(data.retorno){
                $(".div_por").show();
                $("#retorno").prop("checked", true);
            }else{
                $(".div_por").hide();
                $("#retorno").prop("checked", false);
            }*/
            //$("#porcentaje").val(formato.precio(data.porcentaje));
            $("#tit-mod").html("Editar cuenta");
            $(".bt_save").html('Guardar cambios');
            $("#md-nuevaCuenta").modal("open");
        }
    }

    $(document).on('click', '.delete', function() {
        var idv = this.id;
        var toastContent = '<span>¿Desea eliminar éste registro?</span><br><button class="btn-flat toast-action conf_si" id="'+idv+'">Si</button><button class="btn-flat toast-action" onclick=" $(\'.toast\').hide(); ">No</button>';

        Materialize.toast(toastContent, 4000);
    });

    $(document).on('click', '.conf_si', function() {
        id = this.id
        var data = {};

        $('.toast').hide();
        $.ajax({
            url: dominio + 'cuentas/delete/'+id,
            headers: {
                'token':userDOOMI.token,
                'tipo_usuario': userDOOMI.tipo_usu
            },
            type: 'DELETE',
            data: data,
            success: function(data) {
                if (data.r) {
                    Materialize.toast(data.msj, 10000);
                    cuentas();
                } else {
                    Materialize.toast(data.msj, 10000);
                }
            }
        })

    });

    $(document).on('click', '.sorting_1', function() {
        $('.tooltipped').tooltip();
    })

    $(document).on('change', "#retorno", function(){
        if($("#retorno").prop("checked"))
            $(".div_por").show();
        else
            $(".div_por").hide();
    });

}