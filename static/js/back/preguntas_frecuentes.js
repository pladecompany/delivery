function preguntasfrecuentesjs()
{
  const char_limit = 50;
  ajaxPreguntas();

  function ajaxPreguntas()
  {
    ajaxGET('/FAQ', preguntas => {
      const table = $('.table').DataTable({
        'language': { 'url': '../static/<lib></lib>/JTables/Spanish.json'}
      });

      table.clear().draw();

      for (let i = 0; i < preguntas.length; i++) {
        let {id, pregunta, respuesta, estatus} = preguntas[i];

        let options = `
          <a id="${id}" class="ver-faq btn btn-floating btn-primary modal-trigger tooltipped" data-position="bottom" data-delay="50" data-tooltip="Ver información de la pregunta"><i class="fa fa-eye"></i></a>
          <a id="${id}" class="edi-faq btn btn-floating btn-primary modal-trigger tooltipped" data-position="bottom" data-delay="50" data-tooltip="Editar información de la pregunta"><i class="fa fa-edit"></i></a>
          <a id="${id}" class="del-faq waves-effect btn btn-primary btn-floating tooltipped" data-position="bottom" data-delay="50" data-tooltip="Eliminar pregunta"><i class="fa fa-trash"></i></a>
        `;

        let est_switch = `
          <div class="switch">
            <small class="clr_primary" style="font-size: 10px;">Desactivo</small>
            <label>
              <input type="checkbox" ${estatus?'checked':''} class="cambiar_sts sts1" id="${id}">
              <span class="lever"></span>
              <small class="clr_primary" style="font-size: 10px;">Activo</small>
            </label>
          </div>
        `;

        table.row.add([
          i+1,
          pregunta.length>char_limit ? pregunta.substr(0, char_limit)+'...' : pregunta,
          respuesta.length>char_limit ? respuesta.substr(0, char_limit)+'...' : respuesta,
          est_switch,
          options
        ]).draw().node();
      }
        $('.tooltipped').tooltip();
    });
  };

  $('#form-faq').on('submit', function(e){
    e.preventDefault();
    const data = new FormData(this);

    if(!data.get('pregunta'))
      return Materialize.toast('Ingrese la pregunta');
    else if(!data.get('respuesta'))
      return Materialize.toast('Ingrese la respuesta');

    $('#btn-guardar').attr('disabled', true);

    if( $('#btn-guardar').attr('modo') == 'editar' ){
      ruta = '/FAQ/'+ $('#iden').val();
      tipo = 'PUT';
    }
    else{
      ruta = '/FAQ';
      tipo = 'POST';
    }

    enviarForm({ruta, tipo, data,
      success: res => {
        ajaxPreguntas();
        $('#btn-guardar').attr('disabled', false);
        Materialize.toast(res.msj, 2500);
        $('#md-preguntasfrecuentes').modal('close');
      },
      error: (xhr, status, errorThrown) => {
        Materialize.toast('Error al crear pregunta', 2500);
        $('#btn-guardar').attr('disabled', false);
        console.error(errorThrown);
      }
    });

    return false;
  });

  function cargar_modal(faq, modo){
    if(modo=='ver'){
      disabled = true;
      $('#btn-guardar').attr('modo', 'ver').hide();
    }
    else{
      disabled = false;
      $('#btn-guardar').attr('modo', 'editar').show();
      $('#iden').val(faq.id);
    }
    $('#pregunta').val(faq.pregunta).attr('disabled', disabled);
    $('#respuesta').val(faq.respuesta).attr('disabled', disabled);
    $('#md-preguntasfrecuentes').modal('open');
  };

  $(document).on('click', '.del-faq', function() {
    const id = this.id;
    Materialize.toast(`
      <span>¿Desea eliminar éste registro?</span>
      <a id="${id}" class="btn-flat del-faq-si green-text">Sí<a>
      <a class="btn-flat blue-text" onclick="$('.toast').hide();">No<a>
    `, 5000)
  });

  $(document).on('click', '.del-faq-si', function() {
    const id = this.id;
    $('.toast').hide();

    $.ajax({
      url: dominio + 'FAQ/'+id,
      type: 'DELETE',
      headers: {
        'token': userDOOMI.token,
        'tipo_usuario': userDOOMI.tipo_usu
      },
      success: () => {
        Materialize.toast('Pregunta eliminada', 2500);
        ajaxPreguntas();
      },
      error: (xhr, status, errorThrown) => {
        Materialize.toast('Error al eliminar pregunta', 2500);
      }
    });
  });

  $(document).on('click', '.cambiar_sts', function(){
    const id = this.id;

    $.ajax({
      url: dominio+`FAQ/${id}/estatus`,
      type: 'PATCH',
      data: {
        'estatus': $(this).prop('checked') ? '1' : '',
        'token': userDOOMI.token,
        'tipo_usuario': userDOOMI.tipo_usu
      },
      success: resp => {
        $(`input[type=checkbox][id=${id}]`).prop('checked', resp.estatus);
        Materialize.toast('Estatus actualizado', 2500);
      },
      error: () => Materialize.toast('Error al actualizar estatus', 2500),
    })
  });

  $(document).on('click', '.ver-faq', function() {
    const id = this.id;
    ajaxGET('/FAQ/'+id, data => cargar_modal(data, 'ver'));
  });

  $(document).on('click', '.edi-faq', function() {
    const id = this.id;
    ajaxGET('/FAQ/'+id, data => cargar_modal(data, 'editar'));
  })

  function resetForm() {
    $('#pregunta').val('').attr('disabled', false);
    $('#respuesta').val('').attr('disabled', false);
    $('#btn-guardar').attr('modo', '').show();
  }

  $('#btn-nuevo').on('click', resetForm);
}
