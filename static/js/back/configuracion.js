function configuracionjs(){
    listconfi();
    function listconfi(){
        $.ajax({
            url : dominio+'configuracion/',
            type: 'GET',
            headers: {
                'token':userDOOMI.token,
                'tipo_usuario': userDOOMI.tipo_usu
            },
            data:{},
            success:function(data){
                //console.log(data);
                actualizar_confi(data)
            },
            error: function(xhr, status, errorThrown){
                console.log(xhr);
            }
        });
    }
    function actualizar_confi(data){
        $("#tar_min").val(formato.precio(data.minimo));
        $("#ran_km").val(formato.precio(data.km));
        $("#ran_de").val(formato.precio(data.kmd));
        $("#pre_km").val(formato.precio(data.precio));
        $("#min_fav").val(data.minutos);
        $("#whatsapp").val(data.whatsapp_soporte).trigger('input');
        if($("#porcen")[0])
            $("#porcen").val(formato.precio1(data.ganancia));
    }
    $("#form-config").on('submit', function(e) {
        e.preventDefault();
        let data = new FormData(this);

        // Limpiar mascara.
        data.set("whatsapp", extraerDigitos(data.get("whatsapp")));

        $('.save-con').attr('disabled', true);
        $.ajax({
            url: dominio + "configuracion/",
            type: "POST",
            headers: {
                'token':userDOOMI.token,
                'tipo_usuario': userDOOMI.tipo_usu
            },
            data: data,
            contentType: false,
            processData: false,
            success: function(data) {
                Materialize.toast( data.msg , 5000);
                if(data.r){
                    $("#md-configuracion").modal('close');
                    $("#tar_min").val(formato.precio($("#tar_min").val()));
                    $("#pre_km").val(formato.precio($("#pre_km").val()));
                }
                $('.save-con').attr('disabled', false);
            },
            error: function(xhr, status, errorThrown) {
                console.log("Errr : ");
                $('.save-con').attr('disabled', false);
            }
        });
    });

    $("#form-config-porcen").on('submit', function(e) {
        e.preventDefault();
        var data = new FormData(this);
        $('.save-por').attr('disabled', true);
        $.ajax({
            url: dominio + "configuracion/ganancia",
            type: "POST",
            headers: {
                'token':userDOOMI.token,
                'tipo_usuario': userDOOMI.tipo_usu
            },
            data: data,
            contentType: false,
            processData: false,
            success: function(data) {
                Materialize.toast( data.msg , 5000);
                restaurantes();
                $('.save-por').attr('disabled', false);
            },
            error: function(xhr, status, errorThrown) {
                console.log("Errr : ");
                $('.save-por').attr('disabled', false);
            }
        });
    });
}
