function pedidos_paneljs() {
  var minutos_pedido = 5;
  var seleccionar_programado = true;
  $(".txt-min").html(minutos_pedido);
  const
    MSJ_SIN_NOMBRE_CARD = '<i>Nombre no configurado</i>',
    MSJ_SIN_NOMBRE_DETALLE = '<i>Nombre no configurado por el usuario</i>'

  const socket = io(dominio);

  const HOY = moment().format('YYYY-MM-DD');
  $('#buscar-desde').val(HOY);

  let ID_DOOMI_SELECCIONADO = null;


  // Listar restaurantes en el admin.
  if (userDOOMI.tipo_usu === 'administrador') {
    $('#buscar-doomi-wrapper').removeClass('hide');

    // Cambiar icono accion admin.
    $('.btn-next i').removeClass('fa-angle-right').addClass('fa-cog');
    $('.btn-next[name="btn-next-detalle"]').tooltip('remove');

    ajaxGET('/pedidos/estatus', data => {
      let html = '';

      for (let estatus of data) {
        if(estatus.detalles)
          html += `<option value="${estatus.id}">${estatus.nombre}</option>`;
      }

      $('#admin_estatus').html(html);
    });


    $('#buscar-doomi-wrapper .select2-chosen').html('Todos').trigger('change');
    $('#buscar-doomi').val(0);
    ID_DOOMI_SELECCIONADO = 0;
  }


  function actualizarTodo() {
    // const id_res = ID_DOOMI_SELECCIONADO || userDOOMI.id_restaurante;
    const id_res = (ID_DOOMI_SELECCIONADO || ID_DOOMI_SELECCIONADO === 0) ?
      ID_DOOMI_SELECCIONADO : userDOOMI.id_restaurante;

    if (!id_res && id_res !== 0) return desactivarBusquedas(false);

    let ruta = '/pedidos/restaurante/'+id_res;

    if ($('#buscar-desde').val() == "") {
      // ruta += '?fecd=' + HOY;
    }
    else {
      ruta += '?fecd=' + $('#buscar-desde').val();
    }

    desactivarBusquedas(true);
    ajaxGET(ruta, pedidos => {
      desactivarBusquedas(false);
      actualizarConteos(pedidos)
      actualizarCards(pedidos)
    }, () => desactivarBusquedas(false));
  }
  if (userDOOMI.tipo_usu === 'restaurante') {
    actualizarTodo();
  }


  async function actualizarConteos(pedidos) {
    // Vaciar conteo.
    for (let col of document.getElementsByClassName('not_col')) {
      col.innerHTML = 0;
    }

    for (let [col, conteo] of Object.entries(contarPedidosPorColumna(pedidos))) {
      if (!col) continue;
      document.getElementById('num-' + col).innerHTML = conteo;
    }
  }

  async function actualizarCards(pedidos) {
    // Inhabilitados al final.
    pedidos.sort((a, b) => a.id_estatus == 97 ? 1 : -1);

    // Vaciar columnas.
    for (let col of document.getElementsByClassName('body_col')) {
      col.innerHTML = null;
    }

    for (let pedido of pedidos) {
      // Nombre de columna.
      let colName = nombreDeColumna(pedido, pedido.id_estatus, pedido.modalidad, pedido.fecha_pro);
      if(pedido.modalidad=='Programado' && pedido.id_estatus==6 && !pedido.preparando_programado)
        pedido.estatus.nombre = 'Programados';
      else if(pedido.modalidad=='Programado' && pedido.id_estatus==6 && pedido.preparando_programado)
        pedido.estatus.nombre = 'Preparando';
      const card = createCard({
        modalidad: pedido.modalidad,
        spans: [{
            name: 'cliente',
            content: pedido.cliente.nombre || MSJ_SIN_NOMBRE_CARD
          },
          {
            name: 'estatus',
            content: pedido.estatus.nombre
          },
          {
            name: 'fecha_emitido',
            content: format_date(pedido.creado)
          },
          {
            name: 'fecha_entrega',
            content: format_date(pedido.fecha_pro)
          },
          {
            name: 'pedido',
            content: pedido.id
          },
          {
            name: 'fecha',
            content: format_date(pedido.creado)
          },
        ]
      });


      // Boton siguente.
      const btnNext = card.querySelector('.btn-next');

      // Ocultar boton.
      if (debeOcultarBotonSiguiente(pedido))
      {
          btnNext.remove();
      } else { // Info del pedido.
        btnNext.setAttribute('id_pedido', pedido.id);
        btnNext.setAttribute('id_estatus', pedido.id_estatus);
        btnNext.setAttribute('modalidad', pedido.modalidad);

        // Cambiar icono en admin.
        if (userDOOMI.tipo_usu === 'administrador') {
          let icon = btnNext.querySelector('i');
          if (icon) icon.classList.replace('fa-angle-right', 'fa-cog');
        }
      }

      // Boton ver detalle.
      const btnDetalle = card.querySelector('.btn-deta');
      btnDetalle.setAttribute('id_pedido', pedido.id);
      btnDetalle.setAttribute('id_estatus', pedido.id_estatus);
      btnDetalle.setAttribute('modalidad', pedido.modalidad);


      // Solo programado tiene fecha de entrega.
      if (pedido.modalidad != 'Programado') {
        let fe = card.querySelector('.fecha_entrega');
        if (fe) fe.remove();
      }
      // Si no está moroso o rechazado.
      if (pedido.id_estatus != 97 && pedido.id_estatus != 99) {
        let im = card.querySelector('.icon_moroso');
        if (userDOOMI.tipo_usu === 'restaurante' && pedido.id_estatus == 3 && userDOOMI.restaurante[0].estatus==0)
          im = false;
        if (im) im.remove();
      } else {
        btnDetalle.parentElement.classList.add('alert_col');
      }


      let clase_card, clase_icon;

      // Icono y clase por modalidad.
      if (pedido.modalidad === 'Delivery') {
        clase_card = 'card_delivery';
        clase_icon = 'fa-bicycle';
      }
      else if (pedido.modalidad === 'Pickup') {
        clase_card = 'card_pickup';
        clase_icon = 'fa-shopping-basket';
      }
      else if (pedido.modalidad === 'Programado') {
        clase_card = 'card_programado';
        clase_icon = 'fa-clock';
      }

      // Si el pedido se pago al admin.
      if (!pedido.cuenta.idres)
        clase_card = 'card_moroso';

      card.classList.add(clase_card);
      card.querySelector('.icon_modalidad').classList.add(clase_icon);
      card.querySelector('.icon_modalidad').setAttribute('data-tooltip', pedido.modalidad);
      appendCard('#cards-' + colName, card);
    }

    if (userDOOMI.tipo_usu === 'administrador') {
      $('.btn-next').attr('data-tooltip', 'Liberar y cambiar de estatus')
    }
    $('.tooltipped').tooltip();
  }



  /*** EVENTOS ***/

  //EVENTO CUANDO SE RECIBE EMIT DESDE EL API.
  socket.on('ActualizarPedidosAdmin', function(data) {
    if (data.id_restaurante_emisor == userDOOMI.id_restaurante)
      actualizarTodo();
  });

  socket.on('Recibirpedido', function(data) {
    if (userDOOMI.tipo_usu == 'administrador' || (userDOOMI.tipo_usu != 'administrador' && userDOOMI.id_restaurante == data.id_restaurante))
      actualizarTodo();
  });

  if (userDOOMI.tipo_usu == 'administrador') {
    socket.on('ActualizarPedidosAdmin', function(data) {
      if (ID_DOOMI_SELECCIONADO===0 ||
          ID_DOOMI_SELECCIONADO===null ||
          (data.id_restaurante_emisor && data.id_restaurante_emisor == ID_DOOMI_SELECCIONADO))
        actualizarTodo();
    });
  }

  socket.on('Recibirpedido', function(data) {
    if (userDOOMI.tipo_usu == 'administrador' || (userDOOMI.tipo_usu != 'administrador' && userDOOMI.id_restaurante == data.id_restaurante))
      actualizarTodo();
  });

  // Si el admin cambia el estatus del doomi a moroso.
  socket.on('ActualizarEstatusRestaurante', function(data) {
    if (userDOOMI.tipo_usu !== 'restaurante' && data.id != userDOOMI.id_restaurante) return;

    const doomi = JSON.parse(localStorage.getItem('userDOOMI'));
    doomi.restaurante[0].estatus = data.estatus;
    localStorage.setItem("userDOOMI", JSON.stringify(doomi));
    userDOOMI.restaurante[0].estatus = data.estatus;

    actualizarTodo();
  });

  // Click buscar.
  $('#btn_buscar').on('click', function() {
    if (userDOOMI.tipo_usu==='administrador' && !$('#buscar-doomi').val())
      return Materialize.toast('Seleccione un doomialiado', 2000)

    const id_restaurante = userDOOMI.tipo_usu === 'restaurante' ?
      userDOOMI.id_restaurante : $('#buscar-doomi').val();

    const data = {
      modalidad: $('#buscar-modalidad').val(),
      fecd: $('#buscar-desde').val(),
      fech: $('#buscar-hasta').val(),
    };

    desactivarBusquedas(true);
    $.ajax({
      url: dominio + 'pedidos/restaurante/' + id_restaurante,
      headers: {
        'token': userDOOMI.token,
        'tipo_usuario': userDOOMI.tipo_usu
      },
      data: data,
      success: pedidos => {
        desactivarBusquedas(false);
        actualizarCards(pedidos);
        actualizarConteos(pedidos);
      },
      error: (xhr, status, error) => {
        console.error('ERROR:', error);
        desactivarBusquedas(false);
      }
    });
  });

  // Click boton siguente.
  $(document).on('click', '.btn-next', function() {
    const id_pedido = this.getAttribute('id_pedido');
    const id_estatus = this.getAttribute('id_estatus');
    const modalidad = this.getAttribute('modalidad');

    // if (userDOOMI.tipo_usu === 'administrador' && $(this).attr('name') != 'btn-next-detalle')
    if (userDOOMI.tipo_usu === 'administrador')
      return abrir_modal_admin(id_pedido);

    if (id_estatus==8 || (id_estatus==10 && modalidad=='Pickup'))
      return imprimir_data_entregar(id_pedido);

    const toastContent = `<span>¿Mover pedido a la siguiente etapa?</span>
      <button id_pedido="${id_pedido}" id_estatus="${id_estatus}" modalidad="${modalidad}" class="btn-flat toast-action btn-next-si" style="padding:0px;margin:0px;width:40px;">
        Si
      </button>
      <button class="btn-flat toast-action" onclick="$('.toast').hide();" style="padding:0px;margin:0px;width:40px;">
        No
      </button>`;

    Materialize.toast(toastContent, 4000);
  });

  // Confirmado boton siguiente.
  $(document).on('click', '.btn-next-si', function() {
    const id_pedido = this.getAttribute('id_pedido');
    // const id_estatus = this.getAttribute('id_estatus');
    // const modalidad = this.getAttribute('modalidad');
    var tipobtn = $(this).attr('btnmodal');
    if(!tipobtn)
      seleccionar_programado = true;
    avanzarPedido(id_pedido);
  });

  // Ver detalles del pedido.
  $(document).on('click', '.btn-deta', function() {
    $(this).html('<img src="../../static/img/favicon.png" style="position: absolute;padding-top:5px;" class="fa-spin" width="25">');
    $(".btn-deta").css('cursor','not-allowed');
    $(".btn-deta").css('pointer-events','none');
    const id_pedido = this.getAttribute('id_pedido');
    imprimir_data(id_pedido);
  });

  // Click aceptar o rechazar pedido.
  $(document).on('click', '#btap, #btrp', function() {
    const btn = this.id;
    const id_est_actual = $('#md-proPedido-pago').attr('id_estatus');
    let accion = btn === 'btap' ? 'aceptar' : 'rechazar';

    if ($("#motivo_ped_p").val().trim().length < 5 && btn === 'btrp')
      return Materialize.toast('Debes agregar una nota para el cliente', 4000);

    const toastContent = `<span>
        ¿Desea ${accion} el pago de éste pedido?
      </span><br>
      <button btn="${btn}" class="btn-flat toast-action conf_si_p">
        Si
      </button>
      <button class="btn-flat toast-action" onclick="$('.toast').hide();">
        No
      </button>`;
    Materialize.toast(toastContent, 4000);
  });
  // Confirmar aceptar o rechazar pedido.
  $(document).on('click', '.conf_si_p', function() {
    const id_usu = userDOOMI.tipo_usu == 'administrador' ? userDOOMI.id : userDOOMI.id_restaurante;
    const btn = this.getAttribute('btn');
    const id_pedido = $("#md-proPedido-pago").attr('id_pedido');
    const id_est_actual = $('#md-proPedido-pago').attr('id_estatus');
    let nuevo_sts, accion;


    // Al aceptar.
    if (btn === 'btap') {
      accion = 'aceptar';
      nuevo_sts = 6; //Preparando.
    }

    // Al rechazar.
    else {
      accion = 'rechazar';
      nuevo_sts = 97; //Inhabilitado.
    }


    const data = {
      sts: nuevo_sts,
      nota: $("#motivo_ped_p").val(),
      iden: id_pedido,
      id_usu: id_usu,
      tipo_usu: userDOOMI.tipo_usu,
      minutos: minutos_pedido
    };

    $('.toast').hide();
    $.ajax({
      url: dominio + 'pedidos/edit_sts/',
      headers: {
        'token': userDOOMI.token,
        'tipo_usuario': userDOOMI.tipo_usu
      },
      type: 'PUT',
      data: data,
      success: function(data) {
        Materialize.toast(data.msj, 3000);
        $("#md-proPedido-pago, #md-verPedido").modal('close');
        $("#motivo_ped").val('');
        actualizarTodo();
        // imprimir_data_btn(id_pedido);
      },
    })
  });

  // Ver calificacion.
  $(document).on('click', '.btn-ver-calificacion', function() {
    let id_ped = this.id;
    $('.txt-cli-cal, .txt-nota-cal, .txt-obs-cal').html('');
    document.getElementById("img-cli-cali").src = "../../static/img/favicon.png";
    if (userDOOMI.tipo_usu == 'administrador') {
      // document.getElementById("img-domi-cali").src = "../../static/img/logo.jpg";
      // document.getElementById("img-rep-cali").src = "../../static/img/logo.jpg";
      $('.txt-domi-cal').html("");
    }
    ajaxGET(`pedidos/${id_ped}`, data => {
      const datos = data[0] || data;
      // console.log('rate', datos);
      if (datos.cliente.foto)
        document.getElementById("img-cli-cali").src = datos.cliente.foto;
      if (userDOOMI.tipo_usu == 'administrador') {
        if (datos.restaurante[0].img)
          // document.getElementById("img-domi-cali").src = datos.restaurante[0].img;
        $('.txt-domi-cal').html("#" + datos.restaurante[0].id + " " + datos.restaurante[0].nombre);
        if (datos.modalidad == 'Delivery') {
          $('.txt-nota-cal-rep').html(datos.nota_rep);
          //$('.txt-obs-cal-rep').html('"'+datos.obs_rep+'"');
          let estrellas = '<a href="#" class=""><i class="fa fa-star fa-2x"></i></a>';
          if (parseFloat(datos.nota_rep) >= 2)
            estrellas += '<a href="#" class=""><i class="fa fa-star fa-2x"></i></a>';
          else
            estrellas += '<a href="#" class=""><i class="far fa-star fa-2x"></i></a>';
          if (parseFloat(datos.nota_rep) >= 3)
            estrellas += '<a href="#" class=""><i class="fa fa-star fa-2x"></i></a>';
          else
            estrellas += '<a href="#" class=""><i class="far fa-star fa-2x"></i></a>';
          if (parseFloat(datos.nota_rep) >= 4)
            estrellas += '<a href="#" class=""><i class="fa fa-star fa-2x"></i></a>';
          else
            estrellas += '<a href="#" class=""><i class="far fa-star fa-2x"></i></a>';
          if (parseFloat(datos.nota_rep) == 5)
            estrellas += '<a href="#" class=""><i class="fa fa-star fa-2x"></i></a>';
          else
            estrellas += '<a href="#" class=""><i class="far fa-star fa-2x"></i></a>';
          $('.ver-est-rep').html(estrellas);
          if (datos.repartidor.img)
            document.getElementById("img-rep-cali").src = datos.repartidor.img;
          $('.txt-rep-cal').html("#" + datos.repartidor.id + " " + datos.repartidor.nombre + " " + datos.repartidor.apellido);
          $(".panel_domi_cal").removeClass('s8');
          $(".panel_domi_cal").addClass('s4');
          $(".ver_repartidor").show();
        } else {
          $(".panel_domi_cal").removeClass('s4');
          $(".panel_domi_cal").addClass('s8');
          $(".ver_repartidor").hide();
        }

      }
      $('.txt-cli-cal').html("#" + datos.cliente.id + " " + (datos.cliente.nombre||'') + " " + (datos.cliente.apellido||''));
      $('.txt-nota-cal').html(datos.nota);
      //$('.txt-obs-cal').html('"'+datos.motivo+'"');
      let estrellas = '<a href="#" class=""><i class="fa fa-star fa-2x"></i></a>';
      if (parseFloat(datos.nota) >= 2)
        estrellas += '<a href="#" class=""><i class="fa fa-star fa-2x"></i></a>';
      else
        estrellas += '<a href="#" class=""><i class="far fa-star fa-2x"></i></a>';
      if (parseFloat(datos.nota) >= 3)
        estrellas += '<a href="#" class=""><i class="fa fa-star fa-2x"></i></a>';
      else
        estrellas += '<a href="#" class=""><i class="far fa-star fa-2x"></i></a>';
      if (parseFloat(datos.nota) >= 4)
        estrellas += '<a href="#" class=""><i class="fa fa-star fa-2x"></i></a>';
      else
        estrellas += '<a href="#" class=""><i class="far fa-star fa-2x"></i></a>';
      if (parseFloat(datos.nota) == 5)
        estrellas += '<a href="#" class=""><i class="fa fa-star fa-2x"></i></a>';
      else
        estrellas += '<a href="#" class=""><i class="far fa-star fa-2x"></i></a>';
      $('.ver-est').html(estrellas);
      $("#md-calificacion").modal('open');
    });
  });

  // Buscador pedidos, enter en input.
  $('.input_busc').on('keyup', function(evt) {
    const id_ped = parseInt(this.value, 10);
    if (evt.key !== 'Enter') return;
    pedidos_por_id(id_ped);
  });
  // Buscar pedidos, click en lupa.
  $('.icon_busc').on('click', function() {
    const id_ped = parseInt($('.input_busc').val(), 10);
    pedidos_por_id(id_ped);
  });

  // Recordar doomi seleccionado.
  $('#buscar-doomi').on('input change propertychange', function() {
    ID_DOOMI_SELECCIONADO = parseInt(this.value, 10);
  });

  // Click archivar pedido.
  $('.accion-rechazado').on('click', function() {
    const id_pedido = this.getAttribute('id_pedido');
    const btn = this.id;

    let msj = '';
    if (btn == 'btn-archivar')
      msj = '¿Archivar pedido?'
    else
      msj = '¿Devolver pedido?';

    const toastContent = `
      <span>
        ${msj}
      </span><br>
      <button id_pedido="${id_pedido}" class="btn-flat toast-action conf_si_${btn}">
        Si
      </button>
      <button class="btn-flat toast-action" onclick="$('.toast').hide();">
        No
      </button>`;
    Materialize.toast(toastContent, 4000);
  });

  // Confirmar archivar.
  $(document).on('click', '.conf_si_btn-archivar', function() {
    const id_pedido = this.getAttribute('id_pedido');

    const data = {
      sts: 98, // Archivado.
      nota: $("#motivo_ped_p").val(),
      iden: id_pedido,
      id_usu: userDOOMI.tipo_usu == 'administrador' ? userDOOMI.id : userDOOMI.id_restaurante,
      tipo_usu: userDOOMI.tipo_usu
    };

    $('.toast').hide();
    $.ajax({
      url: dominio + 'pedidos/edit_sts/',
      type: 'PUT',
      headers: {
        'token': userDOOMI.token,
        'tipo_usuario': userDOOMI.tipo_usu
      },
      data: data,
      success: function(data) {
        Materialize.toast(data.msj, 3000);
        $("#md-verPedido").modal('close');
        $("#motivo_ped").val('');
        actualizarTodo();

      },
      error: (xhr, status, error) => {
        Materialize.toast('Ocurrió un error', 2000);
      }
    })
  });

  // Confirmar reciclar.
  $(document).on('click', '.conf_si_btn-devolver', function() {
    const id_pedido = this.getAttribute('id_pedido');

    const data = {
      // sts: 97, // Rechazado (Inhabilitado).
      nota: $("#motivo_ped_p").val(),
      iden: id_pedido,
      id_usu: userDOOMI.id,
      tipo_usu: userDOOMI.tipo_usu
    };

    $('.toast').hide();
    $.ajax({
      url: dominio + 'pedidos/devolver',
      type: 'PUT',
      headers: {
        'token': userDOOMI.token,
        'tipo_usuario': userDOOMI.tipo_usu
      },
      data: data,
      success: function(data) {
        Materialize.toast(data.msj, 3000);
        $("#md-verPedido").modal('close');
        $("#motivo_ped").val('');
        actualizarTodo();

      },
      error: (xhr, status, error) => {
        Materialize.toast('Ocurrió un error', 2000);
      }
    })
  });

  // Cambio admin.
  $('#btn-guardar-admin').on('click', function() {
    const id_pedido = $(this).attr('id_pedido');
    const modalidad = $(this).attr('modalidad');

    const toastContent = `
      <span>
        ¿Guardar cambios?
      </span><br>
      <button id_pedido="${id_pedido}" modalidad="${modalidad}" class="btn-flat toast-action guardar_si_admin">
        Si
      </button>
      <button class="btn-flat toast-action" onclick="$('.toast').hide();">
        No
      </button>`;
    Materialize.toast(toastContent, 4000);
  });

  // Confirmar cambio admin.
  $(document).on('click', '.guardar_si_admin', function() {
    $('.toast').hide();
    const id_pedido = $(this).attr('id_pedido');
    const modalidad = $(this).attr('modalidad');
    const data = { id_pedido };

    let cambios = false;

    if ($('#admin_estatus').val() != $('#admin_estatus').attr('valor_original')) {
      data.id_estatus = $('#admin_estatus').val();
      cambios = true;
    }

    if ($('#admin_motivo').attr('valor_original') != $('#admin_motivo').val()) {
      data.motivo = $('#admin_motivo').val();
      cambios = true;
    }

    if ($('#check_liberar').prop('checked')) {
      data.liberar_repartidor = true;
      cambios = true;
    }


    if (!cambios)
      return $('#md-admin').modal('close');

    if (modalidad == 'Pickup' && data.id_estatus) {
      // Por entregar.
      if (data.id_estatus == 8) {
        avanzarPedido(id_pedido);
        $('#md-admin').modal('close');
        return;
      }

      // Entregado al cliente.
      if (data.id_estatus == 12) {
        imprimir_data_entregar(id_pedido);
        $('#md-admin').modal('close');
        return;
      }
    }

    $('#btn-guardar-admin').attr('disabled', true);
    $.ajax({
      url: dominio + 'pedidos/admin-edit',
      type: 'PUT',
      headers: {
        'token': userDOOMI.token,
        'tipo_usuario': userDOOMI.tipo_usu,
        'id_usuario': userDOOMI.id
      },
      data: data,
      success: resp => {
        actualizarTodo();
        $('#btn-guardar-admin').attr('disabled', false);
        $('#md-admin').modal('close');
        Materialize.toast('Datos actualizados exitosamente', 2000);
      },
      error: resp => {
        actualizarTodo();
        $('#btn-guardar-admin').attr('disabled', false);
        Materialize.toast('Ocurrió un error', 2000);
      },
    });
  });

  $(document).on('input change', '.input_busc', function() {
    this.value = extraerDigitos(this.value);
  });

  /*** HELPER FUNCTIONS ***/
  function createCard(options) {
    /*
     ** Crear elemento desde el template
     */
    const modo = options.modalidad;
    // const template = document.querySelector('#template-card-' + modo.toLowerCase());
    const template = document.querySelector('#template-card');
    const card = template.content.firstElementChild.cloneNode(true);

    if (options.spans)
      for (let span of options.spans) {
        for (let el of card.querySelectorAll(`span[name="${span.name}"]`)) {
          el.innerHTML = span.content;
        }
      }

    return card;
  }

  function appendCard(qs, cardElem) {
    const parent = document.querySelector(qs);
    if (parent) parent.append(cardElem);
  }

  function format_date(date) {
    return moment(date).format('hh:mmA DD-MM-YYYY');
  }

  function fechaEsHoy(fecha) {
    const hoy = new Date();
    fecha = new Date(fecha);
    return (fecha.getDate() === hoy.getDate() &&
      fecha.getMonth() === hoy.getMonth() &&
      fecha.getFullYear() === hoy.getFullYear())
  }

  function esFechaFutura(fecha) {
    const hoy = new Date();
    fecha = new Date(fecha);
    return fecha > hoy;
  }

  function nombreDeColumna(pedido) {
    /*
    ** Obtener el nombre de la columna.
    */
    let { modalidad, id_estatus, fecha_pro } = pedido;

    modalidad = modalidad.toLowerCase();
    id_estatus = parseInt(id_estatus, 10);

    // Por Confirmar
    if ([3, 97].includes(id_estatus))
      return 'confirmar';

    // Preparando y Programado
    if ([6].includes(id_estatus)) {
      if (modalidad == 'programado' && !pedido.preparando_programado) {
        return 'programado';
      }
      return 'preparando';
    }

    // Por recoger
    if ([7, 8].includes(id_estatus))
      return 'recoger';

    // En transito
    if ([9, 10].includes(id_estatus))
      return 'transito';

    // Entregado
    if ([12].includes(id_estatus))
      return 'entregado';

    // Completado
    if ([11].includes(id_estatus))
      return 'completado';

    /* ADMIN */
    if (userDOOMI.tipo_usu !== 'administrador') return;

    // Rechazado.
    if ([99].includes(id_estatus))
      return 'confirmar';
  }

  function contarPedidosPorColumna(pedidos) {
    const columnas = {};
    let col;
    for (let pedido of pedidos) {
      col = nombreDeColumna(pedido, pedido.id_estatus, pedido.modalidad, pedido.fecha_pro);
      if (!col) continue;

      columnas[col] ? columnas[col]++ : columnas[col] = 1;
    }

    return columnas;
  }

  function debeOcultarBotonSiguiente(pedido) {
    if (userDOOMI.tipo_usu === 'administrador') return false;

    const colName = nombreDeColumna(pedido, pedido.id_estatus, pedido.modalidad, pedido.fecha_pro)

    let r = pedido.id_estatus == 8 && !pedido.id_repartidor ||
      pedido.id_estatus > 8 && pedido.id_repartidor ||
      pedido.id_estatus == 6 && pedido.modalidad === 'Pickup' ||
      pedido.id_estatus == 12 && pedido.modalidad === 'Pickup' ||
      pedido.id_estatus == 6 && (pedido.modalidad != 'Pickup' && pedido.modalidad != 'Programado') ||
      pedido.id_estatus == 6 && (colName == 'preparando' && pedido.modalidad != 'Pickup') ||
      (pedido.id_estatus == 6 && colName == 'programado') ||
      pedido.id_estatus == 97 || pedido.id_estatus == 11 ||
      (userDOOMI.tipo_usu === 'administrador' && pedido.id_estatus != 3) ||
      ((userDOOMI.restaurante || [{estatus:1}])[0].estatus == 0 && pedido.id_estatus == 3)

    // Mostrar en columna programado.
    if (colName === 'programado' && !pedido.preparando_programado)
      r = false;

    // if (pedido.id_estatus == 6 && colName === 'preparando')
    //   r = false;

    if (pedido.modalidad == 'Programado' && pedido.id_estatus == 12)
      r = true;

    if (colName=='programado' && esFechaFutura(pedido.fecha_pro) && !fechaEsHoy(pedido.fecha_pro))
      r = true;

    if ((colName=='transito' || colName=='entregado') &&  !pedido.id_repartidor)
      r = true;

    if (colName == 'transito' && pedido.modalidad == 'Pickup')
      r = false;


    return r;
  }

  function avanzarPedido(id_pedido) {
    $('.toast').hide();
    ajaxGET('/pedidos/' + id_pedido, data => {
      const pedido = data[0] || data;

      // Aceptar el pago.
      if (pedido.id_estatus == 3) {
        $(".tit-det-pedi").html('Procesar pago del pedido');
        $('.txt-id-pro-pago').html(id_pedido);
        $('.txt_pago_m, .txt_pago_c, .txt_pago_r').html('');
        $('.li-referencia').hide();
        $('.img_pago').hide();
        $('#motivo_ped_p').val('');
        $('.txt_pago_c').html(pedido.cuenta.cuenta);
        $('.txt_pago_r').html(pedido.referencia);
        
        if(pedido.referencia!='')
          $('.li-referencia').show();
        if (pedido.img_pago) {
          $(".img_pago_i").attr("src", pedido.img_pago);
          $('.img_pago').show();
        }
        $(".div-tiempo-pro").hide();
        $(".div-det-pago").show();
        if(pedido.modalidad=='Programado')
          $(".div-tiempo").hide();
        else
          $(".div-tiempo").show();
        $("#md-proPedido-pago")
          .attr('id_pedido', pedido.id)
          .attr('id_estatus', pedido.id_estatus)
          .modal('open');

        return;
      }
      if (pedido.id_estatus == 6 && pedido.modalidad=='Programado' && !pedido.preparando_programado && seleccionar_programado) {
        seleccionar_programado = false;
        $(".tit-det-pedi").html('Seleccionar tiempo');
        $('.txt-id-pro-pago').html(id_pedido);
        $('.txt_pago_m, .txt_pago_c, .txt_pago_r').html('');
        $('.img_pago').hide();

        $(".div-det-pago").hide();
        $(".div-tiempo , .div-tiempo-pro").show();
        $("#bta_ti")
          .attr('id_pedido', pedido.id)
          .attr('id_estatus', pedido.id_estatus)
          .attr('modalidad', pedido.modalidad)

        $("#md-proPedido-pago")
          .attr('id_pedido', pedido.id)
          .attr('id_estatus', pedido.id_estatus)
          .modal('open');

        return;
      }

      let url = dominio;
      let type;
      if (pedido.id_estatus==10 && pedido.modalidad == 'Pickup') {
        url += 'pedidos/repartidor/entregar';
        type = 'PATCH'
        data.id_pedido = data.iden;
      }
      else {
        url += 'pedidos/edit_sts';
        type = 'PUT'
      }


      let nuevo_sts;
      let min_ped = 0;
      switch (parseInt(pedido.id_estatus, 10)) {
        // Preparando.
        case 6: {
          nuevo_sts = 8;

          if (pedido.modalidad == 'Programado' && !pedido.preparando_programado){
            nuevo_sts = 6;
            url += '?ini_pro=1';
            min_ped = minutos_pedido;
          }
          break;
        }

        // Finalizado.
        case 7: {
          nuevo_sts = 8;
          //if (pedido.modalidad == 'Programado' && !pedido.preparando_programado){
            //nuevo_sts = 6;
          //  url += '?ini_pro=1';
          //}
          break;
        }

        // Por entregar.
        case 8: {
          nuevo_sts = 9;
          //nuevo_sts = 10;
          break;
        }

        // En transito (pickup)
        case 10: {
          nuevo_sts = 12;
          break;
        }
      }

      data = {
        sts: nuevo_sts,
        nota: $("#motivo_ped_p").val(),
        iden: id_pedido,
        id_usu: userDOOMI.tipo_usu == 'administrador' ? userDOOMI.id : userDOOMI.id_restaurante,
        tipo_usu: userDOOMI.tipo_usu
      };
      if(min_ped>0)
        data.minutos = min_ped;


      $.ajax({
        url: url,
        headers: {
          'token': userDOOMI.token,
          'tipo_usuario': userDOOMI.tipo_usu
        },
        type: type,
        data: data,
        success: function(data) {
          if (data.msj) {
            Materialize.toast(data.msj, 10000);
            $("#md-proPedido-pago, #md-verPedido").modal('close');
            $("#motivo_ped").val('');
            actualizarTodo();
            // imprimir_data_btn(id_pedido);
          } else {
            Materialize.toast(data.msj, 10000);
          }
        },
      })

    });
  }

  function desactivarBusquedas(v) {
    $('#btn_buscar, .input_busc, .icon_busc').attr('disabled', Boolean(v));
    if(v)
      $(".txt-cargando").show();
    else
      $(".txt-cargando").hide();
  }

  function pedidos_por_id(id) {
    if (!id)
      return actualizarTodo();

    desactivarBusquedas(true);

    // init_doomi_select();

    ajaxGET('/pedidos/'+id, pedido => {
      if (userDOOMI.id_restaurante != pedido.id_restaurante && userDOOMI.tipo_usu != 'administrador'){
        desactivarBusquedas(false);
        return Materialize.toast(`Este pedido no te pertenece`, 3000);
      }
      desactivarBusquedas(false);
      $('.input_busc').focus();

      if (pedido.id_estatus == 98)
        return Materialize.toast(`El pedido ${pedido.id} está archivado`, 2000);


      $('#buscar-doomi-wrapper .select2-chosen').html(pedido.restaurante[0].nombre).trigger('change');
      $('#buscar-doomi').val(pedido.restaurante[0].id)
      ID_DOOMI_SELECCIONADO = pedido.restaurante[0].id;

      const pedidos = [pedido];
      actualizarConteos(pedidos);
      actualizarCards(pedidos);
    },
    (xhr, status, error) => {
      desactivarBusquedas(false);
      $('.input_busc').focus();
      if (xhr && xhr.status == 404)
        return Materialize.toast('Pedido no encontrado', 2000);
    });
  }


  function abrir_modal_admin(id_pedido) {
    ajaxGET('/pedidos/'+id_pedido, pedido => {
      $('.txt-id-pro-pago').html(pedido.id);
        $('.txt_pago_c').html(pedido.cuenta.cuenta);
        $('.txt_pago_r').html(pedido.referencia);
        $('.li-referencia').hide();
        if(pedido.referencia!='')
          $('.li-referencia').show();
        $('.txt-fec').html(format_date(pedido.creado));
        $('#check_liberar').prop('checked', false)

        // Datos cliente.
        $('.txt-id-c').html(pedido.cliente.id);
        $('.txt-cli').html( (pedido.cliente.nombre||'') +' '+ (pedido.cliente.apellido||'') );
        $('.txt-tlf').html(pedido.cliente.telefono);

        if (!pedido.id_repartidor || ![6,7,8,9,10,12].includes(+pedido.id_estatus))
          $('.hide_no_rep').hide();
        else
          $('.hide_no_rep').show();

        // if (pedido.id_repartidor) {
        //   // Datos repartidor.
        //   $('txt-id-r').html(pedido.cliente.id);
        //   $('.txt-rep').html( (pedido.cliente.nombre||'') +' '+ (pedido.cliente.apellido||'') );
        //   $('.txt-tlf-r').html(pedido.cliente.telefono);
        // }

        pedido.motivo = pedido.motivo || '';
        $('#admin_estatus').attr('valor_original', pedido.id_estatus).val(pedido.id_estatus).trigger('change');
        $('#admin_motivo').attr('valor_original', pedido.motivo).val(pedido.motivo);

        if (pedido.img_pago) {
          $(".img_pago_i").attr("src", pedido.img_pago);
          $('.img_pago').show();
        }

        $('#btn-guardar-admin').attr('id_pedido', pedido.id);
        $('#btn-guardar-admin').attr('modalidad', pedido.modalidad);
        $('#md-admin').modal('open');
    });
  }

  // Modal detalles.
  function imprimir_data(id_ped, admin_cancelando) {
    $('.limpiar-data').html('');
    $('.limpiar-data-2').val('');
    $(".btn-accion").html('');
    ajaxGET(`pedidos/${id_ped}`, data => {
      const datos = data[0] || data;
      if (userDOOMI.id_restaurante != datos.id_restaurante && userDOOMI.tipo_usu != 'administrador'){
        $(".btn-deta").text('VER DETALLES');
        $(".btn-deta").css('cursor','pointer');
        $(".btn-deta").css('pointer-events','auto');
        return Materialize.toast(`Este pedido no te pertenece`, 3000);
      }
      //console.log(datos);
      $('.txt-id').html(datos.id);
      $('.txt-id-c').html(datos.cliente.id);
      if (datos.modalidad == 'Pickup') {
        $('#fecha-moda , .panel-rep').hide();
      } else {
        $('#fecha-moda , .panel-rep').show();
      }
      $('.txt-fec').html(moment(datos.creado).format('DD/MM/YYYY, h:mm:ss a'));
      let cliente_ced = datos.cliente.cedula ? datos.cliente.identificacion + '-' + datos.cliente.cedula : '';
      let cliente_nom = datos.cliente.nombre ? datos.cliente.nombre + " " + datos.cliente.apellido : '';
      if (!datos.cliente.nombre && !datos.cliente.apellido) {
        cliente_nom = MSJ_SIN_NOMBRE_DETALLE;
      }
      $('.txt-cli').html(cliente_ced + " - " + cliente_nom);
      $('.txt-tlf').html(datos.cliente.telefono);
      $(".txt-tasa").html(formato.precio(datos.tasa) + " Bs");
      let cuerpo_orden = '';
      let acum = 0;
      let tot_ord = 0;
      let tot_adi = 0;
      let tot_aco = 0;
      let ganancia = 1;
      for (pedidos_pro of datos.ordenes) {
        acum++;
        let adi_ext = 0;
        let txt_adi = '';
        let txt_aco = '';
        let ganancia = (pedidos_pro.ganancia||0) / 100;
        let precio_porcentaje = ((pedidos_pro.precio_usd * 100 * ganancia) + pedidos_pro.precio_usd*100) / 100;
        if (pedidos_pro.adicionales.length > 0) {
          for (adicionales_pedido of pedidos_pro.adicionales) {
            // adi_ext = parseFloat(adi_ext) + (parseFloat(adicionales_pedido.precio_usd) * parseFloat(adicionales_pedido.cantidad));
            let precio_adi = parseFloat(adicionales_pedido.precio_usd * ganancia + adicionales_pedido.precio_usd);
            adi_ext = parseFloat(adi_ext) + (precio_adi * parseFloat(adicionales_pedido.cantidad));
            if (txt_adi == '')
              txt_adi = adicionales_pedido.cantidad + ' ' + adicionales_pedido.nombre + '(' + formato.precio(precio_adi) + ' $)';
            else
              txt_adi += '<br> ' + adicionales_pedido.cantidad + ' ' + adicionales_pedido.nombre + '(' + formato.precio(precio_adi) + ' $)';
          }
        }
        if (pedidos_pro['acompañantes'].length > 0) {
          for (acompanantes_pedido of pedidos_pro['acompañantes']) {
            let precio_aco = parseFloat((acompanantes_pedido.precio_usd * ganancia) + acompanantes_pedido.precio_usd);
            adi_ext = parseFloat(adi_ext) + (precio_aco * parseFloat(acompanantes_pedido.cantidad));

            //adi_ext = parseFloat(adi_ext) + (parseFloat(acompanantes_pedido.precio_usd) * parseFloat(acompanantes_pedido.cantidad));
            if (txt_aco == '')
              txt_aco = acompanantes_pedido.cantidad + ' ' + acompanantes_pedido.nombre + '(' + formato.precio(precio_aco) + ' $)';
            else
              txt_aco += '<br> ' + acompanantes_pedido.cantidad + ' ' + acompanantes_pedido.nombre + '(' + formato.precio(precio_aco) + ' $)';
          }
        }
        cuerpo_orden += `
          <tr>
            <td scope='row' data-label='Cantidad'>${pedidos_pro.cantidad}</td>
            <td data-label='Nombre'>${pedidos_pro.nombre}</td>
            <td data-label='Precio' class='text-right'><small>${ formato.precio(precio_porcentaje) } $ <br>${formato.precio(precio_porcentaje*datos.tasa)}&nbsp;Bs </small></td>
            <td data-label='Adicionales'><small>${txt_adi}</small></td>
            <td data-label='Acompañantes'><small>${txt_aco}</small></td>
            <td data-label='Notas'>${pedidos_pro.notas || ''}</td>
          </tr>
        `;
        // tot_ord = parseFloat(tot_ord) + parseFloat(parseFloat(pedidos_pro.precio_usd) * parseFloat(pedidos_pro.cantidad));
        tot_adi = parseFloat(tot_adi) + parseFloat(adi_ext);

        // tot_ord = precio_porcentaje * 100 * pedidos_pro.cantidad / 100;
        tot_ord = datos.subtotal;
        tot_adi = datos.subtotal_adicionales;
        tot_aco = datos.subtotal_acompañantes;
      };
      let pago_d = datos.pago_del || 0;
      $('#list-pedido').html(cuerpo_orden);
      $('.txt-subt').html(formato.precio(tot_ord) + ' $');
      $('.txt-adit').html(formato.precio(tot_adi) + ' $');
      $('.txt-acom').html(formato.precio(tot_aco) + ' $');
      // $('.txt-tot').html(formato.precio(parseFloat(tot_ord) + parseFloat(tot_adi) + parseFloat(pago_d)) + ' $');
      $('.txt-tot').html(formato.precio( datos.total ) + ' $');

      $('.txt-subt-b').html(formato.precio((tot_ord) * parseFloat(datos.tasa)) + ' Bs');
      $('.txt-adit-b').html(formato.precio((tot_adi) * parseFloat(datos.tasa)) + ' Bs');
      $('.txt-acom-b').html(formato.precio((tot_aco) * parseFloat(datos.tasa)) + ' Bs');
      $('.txt-tot-b').html(formato.precio( (datos.total) * parseFloat(datos.tasa) ) + ' Bs');
      //$('.txt-tot-b').html(formato.precio((parseFloat(tot_ord) + parseFloat(tot_adi) + parseFloat(pago_d)) * parseFloat(datos.tasa)) + ' Bs');

      $('#sts_ped').html(datos.estatus.nombre + " (" + datos.estatus.descripcion + ")");
      //$('#sts_ped').val(datos.estatus.nombre+" ("+datos.estatus.descripcion+")");
      if (datos.estatus.id == 11)
        $(".vista-nota").hide();
      else
        $(".vista-nota").show();
      $('#motivo').val(datos.motivo || '');
      $('.txt-mod').html(datos.modalidad);
      $(".pre_del, .ley_mapa").hide();
      if (datos.modalidad == 'Delivery' || datos.modalidad == 'Programado') {
        if (typeof google != 'undefined') {
          DSERU = new google.maps.DirectionsService();
          DIRDISU = new google.maps.DirectionsRenderer({
            suppressMarkers: true
          });
        }
        $(".pre_del, .ley_mapa").show();
        $('.txt-delt').html(formato.precio(pago_d) + ' $');
        $('.txt-delt-b').html(formato.precio((pago_d) * parseFloat(datos.tasa)) + ' Bs');
        let data_rep = '';

        if (datos.repartidor && datos.estatus.id >= 5 && datos.estatus.id < 97) {
          //console.log(datos.repartidor);
          $('.txt-id-r').html(datos.repartidor.id);
          data_rep = `<li class="collection-item"><b>Repartidor:</b> ${datos.repartidor.nombre} ${datos.repartidor.apellido} <img width="25px" style="position: absolute;margin-left: 10px;margin-top: -3px;" src="../../static/img/transporte/${datos.repartidor.vehiculo[0].icono_veh}"></li>`;
          data_rep += `<li class="collection-item"><b>Teléfono:</b> ${datos.repartidor.telefono }</li>`;
          data_rep += `<li class="collection-item"><b>Whatsapp:</b> ${datos.repartidor.whatsapp }</li>`;
          data_rep += `<li class="collection-item"><b>Metodo de pago:</b> ${datos.repartidor.metodos_pagos || 'Información no especificada'}</li>`;
        } else if (datos.estatus.id >= 5 && datos.estatus.id < 97) {
          $('.txt-id-r').html('N/A');
          data_rep = `<li class="collection-item"><b>Repartidor:</b> Buscando</li>`;
        }else if (datos.modalidad == 'Delivery' || datos.modalidad == 'Programado') {
          $(".panel-rep").hide();
        }
        let data_fec = '';
        if (datos.modalidad == 'Programado') {
          data_fec =`<li class="collection-item"><b>Fecha entrega:</b> ${moment(datos.fecha_pro).format('DD/MM/YYYY, h:mm:ss a')} </li>`;
        }
        $('.descrip-moda').html(`
          <li class="collection-item"><b>Dirección:</b> ${datos.direccion} </li>
          <li class="collection-item"><b>Punto de referencia:</b> ${datos.punto_referencia} </li>
          <li class="collection-item"><b>Doomialiado:</b> ${data.restaurante[0].nombre} </li>
          ${data_fec}
        `);
        $('.descrip-moda-rep').html(`
          ${data_rep}
        `);
        $('.ver-mapa').html('<div style="height: 50vh;" id="mapaver" style="width:100%;"></div>');
        let map;
        let styles = [{
            featureType: "poi.business",
            elementType: "labels",
            stylers: [{
              visibility: "off"
            }]
          },
          {
            elementType: 'labels.text.fill',
            stylers: [{
              color: '#294dce'
            }]
          }
        ];
        if (!typeof google) {
          return true;
        }
        let styledMap = new google.maps.StyledMapType(styles, {
          name: "Styled Map"
        });

        let options = {
          zoom: 16,
          center: new google.maps.LatLng(9.038338434704668, -69.7499810171737),
          mapTypeControl: false,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          backgroundColor: 'transparent'
        };

        mapa = new google.maps.Map(document.getElementById('mapaver'), options);
        mapa.mapTypes.set('map_style', styledMap);
        mapa.setMapTypeId('map_style');

        //$(document).on('click', '.gm-fullscreen-control', function() {
        //    screen_map('mapaver');
        //});
        DIRDISU.setMap(mapa);
        let data_map = {
          origin: datos.restaurante[0].lat + "," + datos.restaurante[0].lon,
          destination: datos.lat_dir_p + "," + datos.lon_dir_p,
          optimizeWaypoints: true,
          travelMode: 'DRIVING'
        };
        DSERU.route(data_map, function(response, status) {
          if (status === 'OK') {
            DIRDISU.setDirections(response);
            let image = {
              url: "../../static/img/map-verde.png",
              size: new google.maps.Size(81, 81),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(40, 71),
              scaledSize: new google.maps.Size(75, 75)
            };
            let image2 = {
              url: "../../static/img/map-azul.png",
              size: new google.maps.Size(81, 81),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(40, 71),
              scaledSize: new google.maps.Size(75, 75)
            };

            let marker_n = new google.maps.Marker({
              position: {
                lat: parseFloat(response.request.origin.location.lat()),
                lng: parseFloat(response.request.origin.location.lng())
              },
              icon: image
            });
            let marker_n2 = new google.maps.Marker({
              position: {
                lat: parseFloat(response.request.destination.location.lat()),
                lng: parseFloat(response.request.destination.location.lng())
              },
              icon: image2
            });
            marker_n.setMap(mapa);
            marker_n2.setMap(mapa);
          } else {
            onError();
          }

        });

        function onError(error) {
          Materialize.toast('Ocurrió un error accediendo a la ruta, intente más tarde', 5000);
        };
      }

      if (datos.modalidad == 'Pickup') {
        $('.descrip-moda').html(`
          <li class="collection-item"><b>Fecha:</b> ${moment(datos.fecha_pro).format('DD/MM/YYYY, h:mm:ss a')} </li>
        `);
      }
      $(".ver-pagos").hide();
      if (datos.estatus.id >= 3 && datos.estatus.id < 98 && datos.estatus.id != 4) {
        $('.img_pago_ver').hide();
        if (datos.cuenta) {
          $('.txt_c').html(datos.cuenta.cuenta);
        }
        $('.txt_r').html(datos.referencia);
        $('.li-referencia').hide();
        if(datos.referencia!='')
          $('.li-referencia').show();
        if (datos.img_pago) {
          $(".img_i").attr("src", datos.img_pago);
          $('.img_pago_ver').show();
        }
        $(".ver-pagos").show();
      }
      let dif_dias = "";
      if (datos.modalidad == "Programado") {
        fecha1_p = moment();
        fecha2_p = moment(datos.fecha_pro);
        dif_dias = fecha2_p.diff(fecha1_p, 'days');
      }
      options = '';

      let id_res_cuenta;
      if (datos.cuenta)
        id_res_cuenta = datos.cuenta.idres;
      else
        id_res_cuenta = 0;

      if (datos.estatus.id == 1 && userDOOMI.tipo_usu != 'administrador')
        options += ` <a href="#!" id="${datos.id}" class="btn-procesar-pedido btn btn-floating btn-primary tooltipped" data-position="bottom" data-delay="50" data-tooltip="Procesar pedido"><i class="fa fa-check"></i></a>`;

      // if((userDOOMI.tipo_usu!='administrador' && datos.estatus.id==3 && datos.id_restaurante==id_res_cuenta) || (userDOOMI.tipo_usu=='administrador' && datos.estatus.id==3 && id_res_cuenta==null))
      //   options += ` <a href="#!" id="${datos.id}" class="btn-procesar-pago btn btn-floating btn-primary tooltipped" data-position="bottom" data-delay="50" data-tooltip="Procesar pago del pedido"><i class="fa fa-dollar-sign"></i></a>`;
      // else  if(datos.estatus.id==3 && id_res_cuenta==null && userDOOMI.tipo_usu!='administrador')
      //   options += ` <a href="#!" style="background: #d01f2a !important;" class="btn btn-floating btn-primary tooltipped" data-position="bottom" data-delay="50" data-tooltip="Procesar pago del pedido"><i class="fa fa-dollar-sign"></i></a>`;

      // if(((datos.estatus.id==5 && (datos.modalidad == "Pickup" || datos.modalidad == "Delivery")) || (datos.estatus.id==5 && datos.modalidad == "Programado" && dif_dias==0)) && userDOOMI.tipo_usu!='administrador')
      //   options += `<a href="#!" class="btn btn-floating btn-primary tooltipped ml-1 btn_general_sts" id="${datos.id}" sts="6" data-position="bottom" data-delay="50" data-tooltip="Iniciar la prepación del pedido"><i class="fas fa-utensils"></i></a>`;
      // else if(datos.estatus.id==5 && datos.modalidad == "Programado" && userDOOMI.tipo_usu!='administrador' && dif_dias>0)
      //   options += `<a href="#!" style="background: #ffc107 !important;" class="btn btn-floating btn-primary tooltipped ml-1" data-position="bottom" data-delay="50" data-tooltip="Esperando para iniciar la prepación del pedido"><i class="fas fa-utensils"></i></a>`;

      // if(datos.estatus.id==6 && userDOOMI.tipo_usu!='administrador')
      //   options += `<a href="#!" class="btn btn-floating btn-primary tooltipped ml-1 btn_general_sts" id="${datos.id}" sts="7" data-position="bottom" data-delay="50" data-tooltip="Finalizar la prepación del pedido"><i class="fas fa-calendar-check"></i></a>`;

      // if(datos.estatus.id==8 && userDOOMI.tipo_usu!='administrador' && datos.id_repartidor != null)
      //   options += `<a href="#!" class="btn btn-floating btn-primary tooltipped ml-1 btn_entregar_pago" id="${datos.id}" sts="9" data-position="bottom" data-delay="50" data-tooltip="Entregar pedido al Repartidor"><i class="fa fa-handshake"></i></a>`;


      if (datos.estatus.id == 11)
        options += `<a href="#!" id="${datos.id}" class="btn-ver-calificacion btn btn-floating btn-primary tooltipped ml-1" data-position="bottom" data-delay="50" data-tooltip="Ver calificación del pedido"><i class="fa fa-star"></i></a>`;

      if (userDOOMI.tipo_usu == 'administrador') {
        $('.txt-nombre-doomi').html(`(${data.restaurante[0].nombre})`);
        $("#motivo").attr("disabled", true);

        if (admin_cancelando) {
          $("#motivo").attr("disabled", false);
          options += ` <a href="#!" id="${datos.id}" class="btn-cancelar-pedido-admin2 btn btn-floating btn-primary tooltipped" data-position="bottom" data-delay="50" data-tooltip="Cancelar pedido"><i class="fa fa-times"></i></a>`;
        }
      }

      // Mostrar/ocultar acciones de pedidos rechazado.
      if (userDOOMI.tipo_usu == 'administrador' && datos.id_estatus == 99) {
        $('.accion-rechazado').removeClass('hide').attr('id_pedido', datos.id);
      }
      else
        $('.accion-rechazado').addClass('hide');

      if (debeOcultarBotonSiguiente(datos))
        $('#md-verPedido .btn-next').hide();
      else {
        $('#md-verPedido .btn-next')
          .attr('id_pedido', datos.id)
          .attr('id_estatus', datos.id_estatus)
          .attr('modalidad', datos.modalidad)
          .show();
      }

      $(".btn-accion").html(options);
      $("#md-verPedido").modal('open');
      $('.materialboxed').materialbox();
      $(".btn-deta").text('VER DETALLES');
      $(".btn-deta").css('cursor','pointer');
      $(".btn-deta").css('pointer-events','auto');
    });
  }


  function imprimir_data_entregar(id_ped){
    $(".txt-id-e, #list-pedido-e").html('');
    ajaxGET(`pedidos/${id_ped}`, data => {
        const datos = data[0] || data;
        //console.log(datos);
        $('.txt-id-e').html(datos.id);
        var cuerpo_orden = '';
        var acum = 0;
        var tot_ord = 0;
        var tot_adi = 0;
        for (pedidos_pro of datos.ordenes) {
            acum++;
            var adi_ext=0;
            var txt_adi='';
            var txt_aco='';
            if(pedidos_pro.adicionales.length>0){
                for (adicionales_pedido of pedidos_pro.adicionales) {
                    if(txt_adi=='')
                        txt_adi=adicionales_pedido.cantidad+' '+adicionales_pedido.nombre;
                    else
                        txt_adi+='<br> '+adicionales_pedido.cantidad+' '+adicionales_pedido.nombre;
                }
            }
            if(pedidos_pro['acompañantes'].length>0){
                for (acompanantes_pedido of pedidos_pro['acompañantes']) {
                    if(txt_aco=='')
                        txt_aco=acompanantes_pedido.cantidad+' '+acompanantes_pedido.nombre;
                    else
                        txt_aco+='<br> '+acompanantes_pedido.cantidad+' '+acompanantes_pedido.nombre;
                }
            }
            cuerpo_orden += `
                <tr>
                    <td scope='row' data-label='Cantidad'>${pedidos_pro.cantidad}</td>
                    <td data-label='Nombre'>${pedidos_pro.menu.nombre}</td>
                    <td data-label='Adicionales'><small>${txt_adi}</small></td>
                    <td data-label='Acompañantes'><small>${txt_aco}</small></td>
                    <td data-label='Notas'>${pedidos_pro.notas || ''}</td>
                </tr>
            `;
        };
        $('#bta_e')
          .attr('id_pedido', id_ped)
          .attr('id_estatus', datos.id_estatus)
          .attr('modalidad', datos.modalidad);
        $('#list-pedido-e').html(cuerpo_orden);
        $("#md-proEntregar").modal("open");
    });
  }


  function imprimir_data_btn(id_ped) {
    $(".btn-accion").html('');
    ajaxGET(`pedidos/${id_ped}`, data => {
      const datos = data[0] || data;
      var dif_dias = "";
      if (datos.modalidad == "Programado") {
        fecha1_p = moment();
        fecha2_p = moment(datos.fecha_pro);
        dif_dias = fecha2_p.diff(fecha1_p, 'days');
      }
      options = '';
      if (datos.cuenta)
        var id_res_cuenta = datos.cuenta.idres;
      else
        var id_res_cuenta = 0;
      if (datos.estatus.id == 1 && userDOOMI.tipo_usu != 'administrador')
        options += ` <a href="#!" id="${datos.id}" class="btn-procesar-pedido btn btn-floating btn-primary tooltipped" data-position="bottom" data-delay="50" data-tooltip="Procesar pedido"><i class="fa fa-check"></i></a>`;

      if ((userDOOMI.tipo_usu != 'administrador' && datos.estatus.id == 3 && datos.id_restaurante == id_res_cuenta) || (userDOOMI.tipo_usu == 'administrador' && datos.estatus.id == 3 && id_res_cuenta == null))
        options += ` <a href="#!" id="${datos.id}" class="btn-procesar-pago btn btn-floating btn-primary tooltipped" data-position="bottom" data-delay="50" data-tooltip="Procesar pago del pedido"><i class="fa fa-dollar-sign"></i></a>`;
      else if (datos.estatus.id == 3 && id_res_cuenta == null && userDOOMI.tipo_usu != 'administrador')
        options += ` <a href="#!" style="background: #d01f2a !important;" class="btn btn-floating btn-primary tooltipped" data-position="bottom" data-delay="50" data-tooltip="Procesar pago del pedido"><i class="fa fa-dollar-sign"></i></a>`;

      if (((datos.estatus.id == 5 && (datos.modalidad == "Pickup" || datos.modalidad == "Delivery")) || (datos.estatus.id == 5 && datos.modalidad == "Programado" && dif_dias == 0)) && userDOOMI.tipo_usu != 'administrador')
        options += `<a href="#!" class="btn btn-floating btn-primary tooltipped ml-1 btn_general_sts" id="${datos.id}" sts="6" data-position="bottom" data-delay="50" data-tooltip="Iniciar la prepación del pedido"><i class="fas fa-utensils"></i></a>`;
      else if (datos.estatus.id == 5 && datos.modalidad == "Programado" && userDOOMI.tipo_usu != 'administrador' && dif_dias > 0)
        options += `<a href="#!" style="background: #ffc107 !important;" class="btn btn-floating btn-primary tooltipped ml-1" data-position="bottom" data-delay="50" data-tooltip="Esperando para iniciar la prepación del pedido"><i class="fas fa-utensils"></i></a>`;

      if (datos.estatus.id == 6 && userDOOMI.tipo_usu != 'administrador')
        options += `<a href="#!" class="btn btn-floating btn-primary tooltipped ml-1 btn_general_sts" id="${datos.id}" sts="7" data-position="bottom" data-delay="50" data-tooltip="Finalizar la prepación del pedido"><i class="fas fa-calendar-check"></i></a>`;

      if (datos.estatus.id == 8 && userDOOMI.tipo_usu != 'administrador' && datos.id_repartidor != null)
        options += `<a href="#!" class="btn btn-floating btn-primary tooltipped ml-1 btn_general_sts" id="${datos.id}" sts="9" data-position="bottom" data-delay="50" data-tooltip="Entregar pedido al Repartidor"><i class="fa fa-handshake"></i></a>`;


      if (datos.estatus.id == 11)
        options += `<a href="#!" id="${datos.id}" class="btn-ver-calificacion btn btn-floating btn-primary tooltipped ml-1" data-position="bottom" data-delay="50" data-tooltip="Ver calificación del pedido"><i class="fa fa-star"></i></a>`;

      $(".btn-accion").html(options);
    });
  }


  function init_doomi_select() {
    $("#buscar-doomi").select2({
      ajax: {
          url: dominio+"restaurantes/buscar",
          dataType: 'json',
          type: "GET",
          quietMillis: 250,
          data: function (term, page) {
            return {
              q: term,
            };
          },
          results: function (data, page) {
            data.unshift({ id:0, text:'Todos' });
            return { results: data };
          },
          cache: true
      },
    });
  }
  init_doomi_select();


  if ( getop("id") )
    imprimir_data(getop("id"));

  if (userDOOMI.tipo_usu === 'administrador') {
    $('#buscar-doomi-wrapper .select2-chosen').html('Todos').trigger('change');
    $('#buscar-doomi').val(0);
    ID_DOOMI_SELECCIONADO = 0;
    actualizarTodo();
  }
  buscarNrRepartidores();
  setInterval(async function(){ buscarNrRepartidores(); }, 15000);
  function buscarNrRepartidores() {
    ajaxGET('/repartidores/acumular-trabajando/', resul => {
      if(resul[0])
        $(".nr_disponibes_rep").html(resul[0]['count(*)']);
      else
        $(".nr_disponibes_rep").html(0);
    });
  }

  $('.btn-abrir-modal-repar').on('click', function() {
    if (userDOOMI.tipo_usu == 'administrador') {
      $("#lista_repartidores_dispo").html('<tr><td colspan="4" class="text-center"><i class="fa fa-spinner fa-spin fa-fw "></i> Cargando...</td></tr>');
      $("#md-repartidores").modal('open');
      ajaxGET('/repartidores/buscar-trabajando/', resul => {
        $(".nr_disponibes_rep").html(resul.length);
        if(resul.length>0){
          let html_rep = '';
          $("#lista_repartidores_dispo").html('');
          for (var i = 0; i < resul.length; i++) {
            const datos = resul[i];
            html_rep =`
              <tr>
                <td data-label="Nombre">${datos.nombre} ${datos.apellido}</td>
                <td data-label="ID">${datos.id}</td>
                <td data-label="Teléfono">${datos.telefono}</td>
                <td class="text-center" data-label="WhatsApp">
                  <a href="https://api.whatsapp.com/send?phone=${datos.whatsapp.replace('-', '')}&text=" id="" class="btn btn-primary btn-floating tooltipped" data-position="bottom" data-delay="50" data-tooltip="Whatsapp" target="_blank"><i class="fab fa-whatsapp"></i></a>
                </td>
              </tr>
            `;
            $("#lista_repartidores_dispo").append(html_rep);
          };
        }else
          $("#lista_repartidores_dispo").html('<tr><td colspan="4" class="text-center">No hay repartidores disponibles.</td></tr>');

      },
      (xhr, status, error) => {
        $("#lista_repartidores_dispo").html('<tr><td colspan="4" class="text-center">Ocurrió un error, intenta nuevamente.</td></tr>');
      });
    }
  });
  $('.btn_minuto').on('click', function() {
    var opc_b = $(this).attr('id');
    if(opc_b=='res_'){
      var min = parseFloat(minutos_pedido)-5;
      if(min<5)
        return;
    }else{
      var min = parseFloat(minutos_pedido)+5;
      if(min>120)
        return;
    }
    minutos_pedido = min;
    $(".txt-min").html(min);
  });
};
