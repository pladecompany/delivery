function loginjs(){

    // Sesion caducada.
    if(sessionStorage.error_autenticacion){
        delete sessionStorage.error_autenticacion;
        Materialize.toast('La sesión ha caducado', 5000);
    }

    $('.modal').modal();
    //Enviar datos por método POST
    $('#formLogin').on('submit', function(e) {

        e.preventDefault();
        /*validaciones*/
        var errr = false;
        var msj = false;

        if (errr) {
            Materialize.toast(msj, 4000); return false;
        }

        //data a enviar
        var data = new FormData();
        data.append("correo", $("#correo").val());
        data.append("pass", $("#pass").val());
        $(".BTentrar").attr("disabled", true);
        $.ajax({
            url: dominio + "login/",
            type: "POST",
            dataType: "json",
            cache: false,
            data: data,
            contentType: false,
            processData: false,
            success: function(res) {

                //console.log(res);
                if(res.length>0){
                    if (res[0].r == true) {

                        localStorage.setItem('userDOOMI',JSON.stringify(res[0]));
                        userDOOMI = JSON.parse(localStorage.getItem('userDOOMI'))

                        if(userDOOMI.tipo_usu=='restaurante'){
                            // Alertar si no posee horarios o modalidades.
                            if(userDOOMI.restaurante[0].horarios.length == 0 ||
                                !userDOOMI.restaurante[0].modalidad_delivery &&
                                !userDOOMI.restaurante[0].modalidad_pickup &&
                                !userDOOMI.restaurante[0].modalidad_programados)
                            { sessionStorage['firstToast'] = '<div style="word-wrap: normal !important; word-break: normal !important;"> Recuerda: para que '+userDOOMI.restaurante[0].nombre+' aparezca en la app, debe poseer un horario y modalidades de trabajo </div>' }

                            else if (userDOOMI.restaurante[0].estatus == 0) {
                                sessionStorage['firstToast'] = '<div style="word-wrap: normal !important; word-break: normal !important;">'+userDOOMI.restaurante[0].nombre+' ha sido suspendido, ponerse en contacto con administración</div>';
                            }

                            location.href = 'vistas/index.html?op=pedidos_panel';
                        }
                        else {
                            location.href = 'vistas/index.html?op=inicio';
                        }

                    }
                }else {
                    $(".BTentrar").attr("disabled", false);
                }
                if(res.msj)
                    Materialize.toast(res.msj, 4000);
            },
            error: function(xhr, status, errorThrown) {
                console.log(errorThrown);
                Materialize.toast(errorThrown.msj, 4000)
                $(".BTentrar").attr("disabled", false);
            }
        });

    });





}
