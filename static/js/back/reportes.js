function reportesjs()
{
  ajaxFiltros();

  const formatoFecha = datetime => moment(datetime).format('DD/MM/YYYY hh:mm a');

  function sumar(...valores) {
    return valores.reduce((tot, val) => (tot + (+val||0)), 0);
  }

  let hoy = new Date().toISOString().split('T')[0];
  $('#hasta').attr('max', hoy);
  $('#desde').attr('max', hoy);
  $("#tipo_r").on("change", function(e){
    if($(this).val()=='1'){
      $("#estatus , #reps , #modalidades").prop("disabled",false);
      generarselec2_r(false);
    } else {
      // $("#estatus , #reps , #modalidades").prop("disabled",true);
      $("#estatus, #modalidades").prop("disabled",true);
      generarselec2_r(true);
    }
    $('#doomis, #reps').val(null).trigger('change');
    $('#doomis-wrapper .select2-chosen, #reps-wrapper .select2-chosen').html('').trigger('change');
    if ($(this).val()=='4') {
      $('#doomis').val(null).trigger('change').select2('enable', false);
    }
    else {
      $('#doomis').val(null).trigger('change').select2('enable', true);
    }
  });
  $('#tipo_r, #reps, #doomis').on('change', function(e){
    const tipo = parseInt($('#tipo_r').val());
    const rep = parseInt($('#reps').val());
    const doomi = parseInt($('#doomis').val());

    if ((tipo==5 || tipo==4 || tipo==3 || tipo==2) && (rep || doomi) && !(rep && doomi) && !(tipo!=5 && doomi))
      $('#btn_detalles').show();
    else
      $('#btn_detalles').hide();

    if ((tipo==3) && doomi)
      $('#btn_detalles').show();

    if ((tipo==4) || (doomi&&rep))
      $('.hide_btn_pdf').hide();
    else
      $('.hide_btn_pdf').show();

    $('#div_tabla_repartidor, #div_tabla_doomi').hide();
  });

  function ajax_pagos() {
    const rep = parseInt($('#reps').val());
    const doomi = parseInt($('#doomis').val());
    // let ruta = 'aliados/pagos/';

    const tipo_r = $('#tipo_r').val();

    // if ((tipo_r==5 || tipo_r==3) && doomi)
    //   ruta = 'aliados/pagos_realizados/';

    let ruta = 'aliados/';

    if (doomi) {
      //Pendientes
      if (tipo_r==2) {
        ruta += 'pagos_pendientes/';
      }

      //Recibidos
      else if (tipo_r==5 || tipo_r==3) {
        ruta += 'pagos_realizados/'
      }
    }
    else if (rep) {
      if (tipo_r==2 || tipo_r==3 || tipo_r==4 || tipo_r==5)
        ruta += 'pagos/';
    }

    if (rep && !doomi)
      ruta += 'repartidor/' + rep;
    else if (doomi && !rep)
      ruta += 'doomialiado/' + doomi;


    const desde = $('#desde').val();
    const hasta = $('#hasta').val();

    const params = {};

    if (desde)
      params.desde = desde;
    if (hasta)
      params.hasta = hasta;



    $('#btn_detalles').attr('disabled', true);

    $.ajax({
      type: 'GET',
      data: params,
      url: dominio + ruta,
      headers: {
        token: userDOOMI.token,
        tipo_usuario: userDOOMI.tipo_usu,
        id_usuario: userDOOMI.id,
      },
      success: function(data) {
        if (rep) {
          if (tipo_r==2) {
            data = data.filter(e => e.delivery_pagado==0);
          }
          else if (tipo_r==3)
            data = data.filter(p => p.delivery_pagado == 1);
          else if (tipo_r==4)
            data = data.filter(p => p.delivery_pagado==2);
          else if (tipo_r==5)
            data = data.filter(p => p.delivery_pagado==3);
        }

        else {
          if (tipo_r==3)
            data = data.filter(p => p.confirmado!=1);
          if (tipo_r==5)
            data = data.filter(p => p.confirmado==1);
        }


        const $tabla = $('#data-table-' + (rep ? 'repartidor' : 'doomi'))
        .DataTable({
          "language": {
            "url": "../static/<lib></lib>/JTables/Spanish.json"
          }
        });
        count = 0;
        $tabla.clear().draw();

        let doomis = {};
        for (let ped of data) {
          if (!doomis[ped.nombre_doomi]) {
            doomis[ped.nombre_doomi] = {
              numero_pedidos: 0,
              ganancia: 0,
              id_doomi: ped.id_doomi,
              creado: ped.creado,
              nombre_doomi: ped.nombre_doomi,
              pre_tasa: ped.pre_tasa,
            };
          }

          doomis[ped.nombre_doomi].numero_pedidos++;
          doomis[ped.nombre_doomi].ganancia += ped.ganancia;
        }
        // doomis = Object.values(doomis);
        doomis = data;

        let ped, options, row, monto;

        if(rep) {
          $('#titulo-fecha-rep').html(tipo_r==2 ? 'Fecha creación' : 'Fecha pago');
          for (var i = 0; i < data.length; i++) {
            const ped = data[i];

            monto = `Bs. ${formato.precio(ped.pago_del*ped.tasa)} / $${formato.precio(ped.pago_del)}`;
            options = `
              <a class="ver-rep waves-effect waves-light modal-trigger btn btn-primary btn-floating tooltipped ml-2"
              data-position="left"
              data-delay="50"
              data-tooltip="Cambiar estatus"
              data-rep-pago="${(ped && ped.repartidor && ped.repartidor.metodos_pagos) || ''}"
              data-rep-monto="${monto}"
              data-rep-tel="${ped && ped.repartidor && ped.repartidor.telefono}"
              data-id-pedido="${ped.id}"
              data-pago-ref="${ped.ref_pago_del || ''}"
              data-pago-datos="${ped.pago_datos || ''}"
              data-pago-motivo="${ped.pago_motivo || ''}"
              data-delivery-pagado="${ped.delivery_pagado}"
              href="#!"><i class="fa fa-cog"></i></a>
            `;

            let estatus = '';
            if (ped.delivery_pagado==1 || ped.delivery_pagado==0)
              estatus = 'Pendiente';
            else if (ped.delivery_pagado==2)
              estatus = 'En disputa';
            else if (ped.delivery_pagado==3)
              estatus = 'Pagado';

            row = [
              i+1,
              ped.id,
              moment(tipo_r==1 ? ped.creado : ped.fecha_pago_del).format('DD/MM/YYYY hh:mm a'),
              ped.cliente.nombre + ' ' + ped.cliente.apellido,
              ped.nombre_restaurante,
              `${monto}`,
              ped.ref_pago_del || '',
              estatus,
              options
            ];

            $tabla.row.add(row).draw().node();
          }

        } else {
          for (var i = 0; i < doomis.length; i++) {
            const data = doomis[i];
            let txt_dl, txt_bs;

            if (tipo_r==2) {
              txt_dl = formato.precio(data.tot);
              txt_bs = formato.precio(data.pre_tasa * data.tot);
            }

            else if (tipo_r==3 || tipo_r==5) {
              txt_dl = formato.precio(data.pago_del);
              txt_bs = formato.precio(data.tasa);
            }

            row = [
              i+1,
              // data.numero_pedidos,
              data.nombre_restaurante || data.nombre_doomi,
              // data.creado ? formatoFecha(data.creado) : '--',
              `$${txt_dl} / Bs.${txt_bs}`,
              // `Bs. ${formato.precio(data.pre_tasa)} / $${formato.precio(monto)}`,
              data.fecha_pago ? formatoFecha(data.fecha_pago.split('.')[0]) : '--',
              data.referencia || '',
              data.confirmado ? 'Confirmado' : 'Pendiente',
              `<a href="#!" id_pago="${data.id}" confirmado="${data.confirmado?1:''}" id_doomi="${data.id_doomi || data.id_restaurante}" class="btn btn-primary btn-floating ver-doomi tooltipped" data-position="bottom" data-delay="50" data-tooltip="Ver información"><i class="fa fa-eye"></i></a>`
            ];
            $tabla.row.add(row).draw().node();
          }
        }
        $('#btn_detalles').attr('disabled', false);
        $('#div_tabla_' + (rep ? 'repartidor' : 'doomi')).show();
      },
      error: function(xhr, status, errorThrown) {
        $('#btn_detalles').attr('disabled', false);
      }
    });

  }

  $('#btn_detalles').on('click', ajax_pagos);

  $(document).on('click', '.ver-rep', function(){
    $('#form_pago_rep')[0].reset();
    $('#md-pagoRep').modal('open');
    $('.txt_metodo_rep').html(this.getAttribute('data-rep-pago'));
    $('.txt_monto').html(this.getAttribute('data-rep-monto'));
    $('#id_pedido').val(this.getAttribute('data-id-pedido'));
    $('#motivo').val(this.getAttribute('data-pago-motivo')).attr('disabled', true);
    $('#referencia_anterior').val(this.getAttribute('data-pago-ref'));

    if (this.getAttribute('data-rep-pago')) {
      $('#datos_pago').val(this.getAttribute('data-rep-pago'));
      $('.btn_confirmar, #referencia').attr('disabled', false);
    }
    else {
      $('#datos_pago').val(`El repartidor no tiene datos de pago configurados, éste es el número de contacto del repartidor (${this.getAttribute('data-rep-tel')})`);
      $('.btn_confirmar, #referencia').attr('disabled', true);
    }

    $('.hide_disputar_pagar').show();
    $('.hide_enviar_rep, .hide_motivo, .hide_ref, .hide_enviar_rep').hide();

    const tipo_r = $('#tipo_r').val();
    const delivery_pagado = this.getAttribute('data-delivery-pagado');
    $('.btn_confirmar').show();
    // $('.btn_confirmar, .hide_ref').show();

    $('.btn_repartidor[name="disputar"], .hide_ref_prev').show();
    if (delivery_pagado==0) {
      $('.btn_repartidor[name="disputar"], .hide_ref_prev').hide();
      $('.btn_repartidor[name="confirmar"]').show();
    }
    else if (delivery_pagado==1) {
      $('.btn_repartidor[name="confirmar"], .hide_ref').hide();
      $('.btn_repartidor[name="disputar"], .btn_repartidor[name="aceptar"], .hide_ref_prev').show();
    }
    else if (delivery_pagado==2) {
      $('.btn_repartidor[name="disputar"], .btn_repartidor[name="aceptar"]').hide();
      $('.btn_repartidor[name="confirmar"], .hide_motivo').show();
    }
    else if (delivery_pagado==3) {
      $('.btn_repartidor[name="aceptar"], .btn_repartidor[name="confirmar"]').hide();
      $('.btn_repartidor[name="disputar"]').show();
    }
    // else if (delivery_pagado!=2) {
    //   $('.btn_repartidor[name="aceptar"]').show();
    //   $('.hide_motivo, .hide_ref_prev, .btn_repartidor[name="confirmar"]').hide();
    // }
    // else
    //   $('.hide_motivo').show();


    // const ref = this.getAttribute('data-pago-ref');
    // if (ref && ref.length > 0) {
    //   $('#referencia_anterior').val(this.getAttribute('data-pago-ref'))
    //     .parent().parent().show();
    // }
    $('.tooltipped').tooltip();
  });
  $(document).on('click', '.btn_pago_rep', function(){
    const name = this.getAttribute('name');
    const idv = this.id;
    const toastContent = `<span>¿Desea ${name} el pago?</span><br><button name="${name}" class="btn-flat toast-action btn_si_rep" id="${idv}">Si</button><button class="btn-flat toast-action" onclick=" $('.toast').hide(); ">No</button>`;

    Materialize.toast(toastContent, 4000);
  });
  $(document).on('click', '.btn_si_rep', function(){
    $('.toast').hide();
    const name = this.getAttribute('name');
    const idped = this.getAttribute('idped');
    const datos = new FormData(document.getElementById('form_pago_rep'));

    // datos.set('accion', name);

    let err = false;
    // if (!$('#datos_pago').val().trim())
    //   err = 'Ingrese los datos de pago';

    if (name=='confirmar' && !$('#referencia').val().trim())
      err = 'Ingrese la referencia';
    else if (name == 'disputar' && !$('#motivo').val().trim())
      err = 'Ingrese el motivo';

    if (err)
      return Materialize.toast(err, 4000);

    let ruta = 'aliados/';
    if (name=='confirmar')
      ruta +='pago_repartidor';

    else if (name=='disputar')
      ruta += 'disputar_pago';

    else if (name=='aceptar')
      ruta += 'finalizar_pago';

    $('.btn_pago_rep').attr('disabled', true);
    $.ajax({
      type: 'POST',
      url: dominio + ruta,
      data: datos,
      headers: {
        token: userDOOMI.token,
        tipo_usuario: userDOOMI.tipo_usu,
        id_usuario: userDOOMI.id,
      },
      dataType: "json",
      cache: false,
      contentType: false,
      processData: false,
      success: (data) => {
        $('.btn_pago_rep').attr('disabled', false);
        Materialize.toast('Datos actualizados', 3000);
        ajax_pagos();
        $('#md-pagoRep').modal('close');
        $('#form_pago_rep')[0].reset();
      },
      error: (xhr, status, errorThrown) => {
        $('.btn_pago_rep').attr('disabled', false);
        Materialize.toast('Ocurrió un error', 3000);
      },
    });
  });
  $('.btn_repartidor[name="aceptar"]').on('click', function(){
    const idv = this.id;
    const name = this.getAttribute('name');
    const toastContent = `<span>¿Desea aceptar el pago?</span><br><button name="${name}" class="btn-flat toast-action btn_si_rep" id="${idv}">Si</button><button class="btn-flat toast-action" onclick=" $('.toast').hide(); ">No</button>`;

    Materialize.toast(toastContent, 4000);
  });


  $(document).on('click', '.ver-doomi', function(){
    const id_doomi = this.getAttribute('id_doomi');
    const id_pago = this.getAttribute('id_pago');
    const confirmado = this.getAttribute('confirmado');
    ajaxGET('aliados/ver_pago_realizado/doomialiado/'+id_doomi+'?idpago='+id_pago, data => {
      const $tabla = $('#data-table-pagoadmin')
      .DataTable({
        "language": {
          "url": "../static/<lib></lib>/JTables/Spanish.json"
        }
      });
      count = 0;
      $tabla.clear().draw();

      $('#id_pago').val(id_pago);
      if(confirmado)
        $('.hide_confirmar').hide();
      else
        $('.hide_confirmar').show();

      let row;
      for (var i = 0; i < data.length; i++) {
        const datos = data[i];
        let row = [
          i+1,
          datos.id_pedido,
          datos.nombre_doomi,
          moment(datos.creado).format('DD/MM/YYYY hh:mm a'),
          `$${formato.precio(sumar(datos.tot, datos.tot_adi, datos.tot_acom))}`,
        ];
        $tabla.row.add(row).draw().node();
      }
      const total = data.reduce((n,dat) => sumar(n, dat.tot, dat.tot_adi, dat.tot_acom),0);
      $('#total_doomi').html(`$${formato.precio(total)}`)
    });
    $('#md-pagoAdmin').modal('open')
  });

  $(document).on('click', '.btn_pago_doomi', function(){
    const name = this.getAttribute('name');
    const idv = this.id;
    const toastContent = `<span>¿Desea confirmar el pago?</span><br><button class="btn-flat toast-action btn_si_doomi" id="${idv}">Si</button><button class="btn-flat toast-action" onclick=" $('.toast').hide(); ">No</button>`;
    Materialize.toast(toastContent, 4000);
  });
  $(document).on('click', '.btn_si_doomi', function() {
    $('.toast').hide();

    const datos = new FormData(document.getElementById('form_registrar_admins'));
    datos.set('estatus', 1);

    $('.btn_pago_rep').attr('disabled', true);
    $.ajax({
      type: 'POST',
      url: dominio + 'aliados/confirmacion_pago_doomi',
      data: datos,
      headers: {
        token: userDOOMI.token,
        tipo_usuario: userDOOMI.tipo_usu,
        id_usuario: userDOOMI.id,
      },
      dataType: "json",
      cache: false,
      contentType: false,
      processData: false,
      success: (data) => {
        $('.btn_pago_rep').attr('disabled', false);
        Materialize.toast('Datos actualizados', 3000);
        ajax_pagos();
        $('#md-pagoRep,#md-pagoAdmin').modal('close');
        $('#form_pago_rep,#form_registrar_admins')[0].reset();
      },
      error: (xhr, status, errorThrown) => {
        $('.btn_pago_rep').attr('disabled', false);
        Materialize.toast('Ocurrió un error', 3000);
      },
    });
  });


  $('.btn_repartidor').on('click', function(evt) {
    const name = this.getAttribute('name');
    if (name != 'disputar' && name != 'confirmar')
      return;
    $('#btn_enviar').attr('name', name);
    $('.hide_disputar_pagar').hide();

    if (name=='confirmar') {
      $('#btn_enviar').attr('name', name);
      $('.hide_enviar_rep, .hide_ref').show();
    }
    else {
      $('#motivo').val('').attr('disabled', false);
      $('#btn_enviar').attr('name', name);
      $('.hide_enviar_rep, .hide_motivo').show();
    }
  });


  $("#form-reporte").on("submit", function(e){
    e.preventDefault();
    const datos = new FormData(this);

    let tipo_r = datos.get('tipo_r');
    let repartidor = datos.get('repartidor');
    if(tipo_r!=1){
      let hasta = datos.get('hasta');
      if(hasta) {
          datos.set('hasta', hasta);
      }
      let desde = datos.get('desde');
      if(desde) {
          datos.set('desde', desde);
      }
    }else{
      let hasta = datos.get('hasta');
      if(hasta) {
          hasta = new Date(hasta.split('-'));
          // Para que incluya todo el dia.
          hasta.setHours(24);
          hasta = hasta.toISOString();
          datos.set('hasta', hasta);
      }
    }

    if(userDOOMI.tipo_usu == 'restaurante'){
      datos.set("doomi", userDOOMI.restaurante[0].id);
    }

    const oReq = new XMLHttpRequest();
    if(tipo_r==2 && !repartidor)
      oReq.open('POST', dominio + 'pedidos/generar_reporte_pago_pendiente', true);
    else if (tipo_r==5 && repartidor) {
      oReq.open('POST', dominio + 'pedidos/generar_reporte_pago_recibido_repartidor', true);
      datos.set('delivery_pagado', 3);
    }
    else if (tipo_r==2 && repartidor)
      oReq.open('POST', dominio + 'pedidos/generar_reporte_pago_pendiente_repartidor', true);
    else if(tipo_r==5){
      oReq.open('POST', dominio + 'pedidos/generar_reporte_pago_recibido', true);
      datos.set('confirmado', 1);
    }
    else if(tipo_r==3 && !repartidor) {
      oReq.open('POST', dominio + 'pedidos/generar_reporte_pago_recibido', true);
      datos.set('confirmado', 0);
    }
    else if(tipo_r==3 && repartidor) {
      oReq.open('POST', dominio + 'pedidos/generar_reporte_pago_recibido_repartidor', true);
      datos.set('delivery_pagado', 1);
    }
    else
      oReq.open('POST', dominio + 'pedidos/generar_reporte', true);
    oReq.responseType = "arraybuffer";
    oReq.setRequestHeader('token', userDOOMI.token);
    oReq.setRequestHeader('tipo_usuario', userDOOMI.tipo_usu);

    oReq.onload = function(oEvent) {
      if(oReq.status==204){//Sin datos.
        if(tipo_r==2)
          Materialize.toast('No hay pagos pendientes', 5000);
        else if(tipo_r==5)
          Materialize.toast('No hay pagos recibidos', 5000);
        else
          Materialize.toast('No hay pedidos', 5000);
        $('.bt_save').prop('disabled', false);
        return;
      }
      const blob = new Blob([oReq.response], { type: "application/pdf" });
      // var win = window.open('', '_blank');
      // var URL = window.URL || window.webkitURL;
      // win.location = dataUrl;
      const dataUrl = URL.createObjectURL(blob);
      window.open(dataUrl, '_blank');
      // $('#pdf-frame').attr('src', dataUrl).show();
      $('.bt_save').prop('disabled', false); // Activar boton al finalizar.
    };

    $('.bt_save').prop('disabled', true); // Desactivar boton.
    $('#pdf-frame').attr('src', '#!').hide();
    oReq.send(datos);
  });
  /*
  $('#desde').on('input', function(){
    $('#hasta').attr('min', $(this).val());
  });
  $('#hasta').on('input', function(){
    $('#desde').attr('max', $(this).val());
  });
  */

  $("#doomis").select2({
    allowClear: true,
    placeholder:'',
    ajax: {
        url: dominio+"restaurantes/buscar",
        dataType: 'json',
        type: "GET",
        quietMillis: 250,
        data: function (term, page) {
          return {
            q: term,
          };
        },
        results: function (data, page) {
          data.push({id:'',text: 'Todo'});
          return { results: data };
        },
        cache: true
    },
  });


  $("#cuenta").select2({
    ajax: {
        url: dominio+"cuentas?tipo_usu=administrador",
        dataType: 'json',
        type: "GET",
        quietMillis: 250,
        data: function (term, page) {
          return {
            q: term,
          };
        },
        results: function (data, page) {
          // data.push({id:'',text: 'Todo'});
          // console.log('as',data);
          return {
            results: data.map(e => ({id:e.id, text:e.nombre}))
            // results: data
          };
        },
        cache: true
    },
  });
  generarselec2_r(false);
  function generarselec2_r(desactivar){
    $("#reps").select2({
      disabled: desactivar,
      ajax: {
          url: dominio+"repartidores/buscar",
          dataType: 'json',
          type: "GET",
          quietMillis: 250,
          data: function (term, page) {
            return {
              q: term,
            };
          },
          results: function (data, page) {
            data.push({id:'',text: 'Todo'});
            return { results: data };
          },
          cache: true
      },
    });
  }
  function ajaxFiltros() {
    $('#estatus, #modalidades').html('<option value="" selected>Cargando...</option>');

    ajaxGET('/pedidos/filtros_reporte', data => {
        let html;

        /*if(data.doomialiados){
          html = '<option value="" selected>Todo</option>';
          data.doomialiados.forEach(doomi => { html += `<option value="${doomi.id}">${doomi.nombre}</option>`});
          $('#doomis').html(html);
        }

        if(data.repartidores){
          html = '<option value="" selected>Todo</option>';
          data.repartidores.forEach(rep => { html += `<option value="${rep.id}">${rep.nombre} ${rep.apellido}</option>`});
          $('#reps').html(html);
        }*/

        if(data.estatus){
          html = '<option value="" selected>Todo</option>';
          data.estatus.forEach(estatus => {
            if(estatus.detalles)
              html += `<option value="${estatus.id}">${estatus.nombre}</option>`
          });
          $('#estatus').html(html);
        }

        if(data.modalidades){
          html = '<option value="" selected>Todo</option>';
          data.modalidades.forEach(moda => { html += `<option value="${moda}">${moda}</option>`});
          $('#modalidades').html(html);
        }
    });
  }
}
