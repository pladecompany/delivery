function clientesjs()
{
    const MENSAJE_SIN_NOMBRE = '<i>Sin configurar por el usuario</i>';

    recuperar_clientes();

    function recuperar_clientes(){
        ajaxGET('clientes/', actualizar_tabla_cientes)
    };

    function actualizar_tabla_cientes(data) {
        const table = $('.table').DataTable({
            "language": { "url": "../static/<lib></lib>/JTables/Spanish.json" }
        });
        let count = 1;
        table.clear().draw();

        let nombreCompleto;
        for (let cliente of data) {
            /*
            if (!cliente.nombre)
                continue;
            */
            if (cliente.nombre || cliente.apellido)
                nombreCompleto = (cliente.nombre||'') +' '+ (cliente.apellido||'');
            else
                nombreCompleto = MENSAJE_SIN_NOMBRE;


            options = `<a href="#md-verCliente" id="${cliente.id}" class="btn-ver-cliente btn btn-floating btn-primary  tooltipped" data-position="bottom" data-delay="50" data-tooltip="Ver información del cliente"><i class="fa fa-eye"></i></a>`;
            // cliente.nombre = cliente.nombre || '';
            // cliente.apellido = cliente.apellido || '';
            table.row.add([count++, nombreCompleto, options]).draw().node();
        }
        $('.tooltipped').tooltip();
    };

    $(document).on('click', '.btn-ver-cliente', function(evt){
        const id_cliente = $(this).attr('id');

        ajaxGET(`clientes/${id_cliente}`, data => {
            let sexo_cliente = '';
            const cliente = data;

            let view_tlfn = cliente.telefono ? '+' + cliente.telefono : '';
            let view_ws = cliente.whatsapp ? '+' + cliente.whatsapp : '';


            if(cliente.sexo=='M') sexo_cliente='Masculino';
            if (cliente.sexo=='F') sexo_cliente='Femenino';

            // $('#ver-cedula').html(cliente.identificacion+'-'+cliente.cedula);
            $('#ver-nombre').html(cliente.nombre || MENSAJE_SIN_NOMBRE);
            $('#ver-apellido').html(cliente.apellido || MENSAJE_SIN_NOMBRE);
            $('#ver-sexo').html(sexo_cliente);
            $('#ver-sexo').html();
            $('#ver-telefono').html(view_tlfn);
            $('#ver-correo').html(cliente.correo);
            // $('#ver-usuario').html(cliente.usuario);
            $('#ver-whatsapp').html(view_ws);

            $('#md-verCliente').modal('open');
        });
    });

};
