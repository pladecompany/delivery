function horariojs(){

    //FUNCIONES DEL LABORANDO
    imprimirhorario();
    function imprimirhorario(){

        var horario = ["LUNES", "MARTES", "MIERCOLES", "JUEVES", "VIERNES", "SABADO", "DOMINGO"];
        $("#table_horario").html(`<tr>
            <th colspan="10" class="bg_first-color text-center">
                <h6 class="m-0 clr_white">Horario de la semana <i class="fa fa-calendar"></i></h6>
            </th>
        </tr>
        <tr style="border-bottom: 1px solid #182c61;">
            <th>Dia</th>
            <th colspan="2">Horario 1</th>
            <th colspan="2">Horario 2</th>
            <th colspan="2">Horario 3</th>
        </tr>`);
        var hor = '';
        for (var i = 0; i < horario.length; i++) {
            $("#table_horario").append(`<tr style="border-bottom: 1px solid #182c61;">
                <th>${horario[i]}</th>
                <td>
                    <div class="input-field col s12">
                        <input type="time" class="form-control validate mt-2 recorrer_horario" idh="${i}" name="hora_ini_1[${i}]" id="hora_ini_${i}_1">
                        <label for="hora_ini_${i}_1" class="active">Hora inicio</label>
                    </div>
                    <div class="input-field col s12">
                        <input type="time" class="form-control validate mt-2" name="hora_fin_1[${i}]" id="hora_fin_${i}_1">
                        <label for="hora_fin_${i}_1" class="active">Hora fin</label>
                    </div>
                </td>
                <td>
                    <p>
                        <input class="with-gap check-all" name="status_h_1[${i}]" type="checkbox" id="__dia_${i}_1" value="1">
                        <label for="__dia_${i}_1"></label>
                    </p>
                </td>

                <td style="border-left: 1px solid #182c61;">
                    <div class="input-field col s12">
                        <input type="time" class="form-control validate mt-2 recorrer_horario" idh="${i}" name="hora_ini_2[${i}]" id="hora_ini_${i}_2">
                        <label for="hora_ini_${i}_2" class="active">Hora inicio</label>
                    </div>
                    <div class="input-field col s12">
                        <input type="time" class="form-control validate mt-2" name="hora_fin_2[${i}]" id="hora_fin_${i}_2">
                        <label for="hora_fin_${i}_2" class="active">Hora fin</label>
                    </div>
                </td>
                <td>
                    <p>
                        <input class="with-gap check-all" name="status_h_2[${i}]" type="checkbox" id="__dia_${i}_2" value="1">
                        <label for="__dia_${i}_2"></label>
                    </p>
                </td>

                <td style="border-left: 1px solid #182c61;">
                    <div class="input-field col s12">
                        <input type="time" class="form-control validate mt-2 recorrer_horario" idh="${i}" name="hora_ini_3[${i}]" id="hora_ini_${i}_3">
                        <label for="hora_ini_${i}_3" class="active">Hora inicio</label>
                    </div>
                    <div class="input-field col s12">
                        <input type="time" class="form-control validate mt-2" name="hora_fin_3[${i}]" id="hora_fin_${i}_3">
                        <label for="hora_fin_${i}_3" class="active">Hora fin</label>
                    </div>
                </td>
                <td>
                    <p>
                        <input class="with-gap check-all" name="status_h_3[${i}]" type="checkbox" id="__dia_${i}_3" value="1">
                        <label for="__dia_${i}_3"></label>
                    </p>
                </td>
            </tr>`);
        }
    }


    $("#form_registrar_horario").on('submit', function(e) {
        e.preventDefault();
        //data a enviar
        var data = new FormData(this);
        data.append("iden", userDOOMI.restaurante[0].id);
        $(".bt_save_hor").prop("disabled", true);
        $(".fileprogress").removeClass("hide");
        $(".fileprogress").find("div")[0].style.width = "0%";
        $.ajax({
            url: dominio + "horarios/add",
            headers: {
                'token':userDOOMI.token,
                'tipo_usuario': userDOOMI.tipo_usu
            },
            type: "POST",
            dataType: "json",
            cache: false,
            data: data,
            contentType: false,
            processData: false,
            xhr: function(){
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", function(e){
                    if (e.lengthComputable) {
                      var percentComplete = parseInt( (e.loaded / e.total * 100), 10);
                      $(".fileprogress").find("div")[0].style.width = `${percentComplete}%`;
                      if(percentComplete>=100){
                        $(".fileprogress").addClass("hide");
                        $(".fileprogress").find("div")[0].style.width = "0%";
                      }
                    }
                }, false);
                xhr.addEventListener("progress", function(e){
                    if (e.lengthComputable) {
                        var percentComplete = parseInt( (e.loaded / e.total * 100), 10);
                        $(".fileprogress").find("div")[0].style.width = `${percentComplete}%`;
                        if(percentComplete>=100){
                            $(".fileprogress").addClass("hide");
                            $(".fileprogress").find("div")[0].style.width = "0%";
                        }
                    }
                }, false);
                return xhr;
            },
            success: function(res) {
                if(res.veri_on){
                    if(res.veri_on.length>0){
                        if(!$("#sts_laborando").prop("checked")){
                            $("#sts_laborando").attr('checked',true);
                        }
                    }
                }
                $('#delivery').attr('horarios', res.horarios_activos); // Para saber en Perfil Negocio si hay horarios.
                if(res.r==false)
                    Materialize.toast(res.msj, 10000)
                else{
                    Materialize.toast(res.msj, 5000);
                    $("#md-horario").modal("close");
                }
                //botones del form
                $(".bt_save_hor").prop("disabled", false)
            },
            error: function(xhr, status, errorThrown) {
                console.log(errorThrown);
                $(".fileprogress").addClass("hide");
                $(".fileprogress").find("div")[0].style.width = "0%";
                Materialize.toast(errorThrown.msj, 4000)

                //botones del form
                $(".bt_save_hor").prop("disabled", false)
            }
        });
    });

    $(".open_hor").on('click', function(e){
        $.ajax({
            url: dominio + 'horarios/'+userDOOMI.restaurante[0].id,
            headers: {
                'token':userDOOMI.token,
                'tipo_usuario': userDOOMI.tipo_usu
            },
            type: 'GET',
            // data: data,
            success: function(data) {
                montarhorario(data);
            }, error: function(xhr, status, errorThrown) {
                console.log(errorThrown);
            }
        })
    });

    function montarhorario(data){
        var horario = {"Lunes": 0, "Martes": 1, "Miercoles": 2, "Jueves": 3, "Viernes": 4, "Sabado": 5, "Domingo": 6};
        for (var i = 0; i < data.length; i++) {
            fila = horario[data[i].dia];
            $("#hora_ini_"+fila+"_"+data[i].horario).val(data[i].inicio_hor);
            $("#hora_fin_"+fila+"_"+data[i].horario).val(data[i].fin_hor);
            if(data[i].estatus==1)
                $("#__dia_"+fila+"_"+data[i].horario).prop("checked",true);
        };
    }
}
