function listar_repartidores(){

    var nuevoID = 0;

    let hoy = new Date().toISOString().split('T')[0];
    $('#soli-hasta').attr('max', hoy);
    $('#soli-desde').attr('max', hoy);

    //listar
    repartidores();
    tipovehiculos();
    lista_doomis();

    function tipovehiculos() {
        data = {
        }
        $.ajax({
            url: dominio + 'tipovehiculo/',
            headers: {
                'token':userDOOMI.token,
                'tipo_usuario': userDOOMI.tipo_usu
            },
            type: 'GET',
            data: data,
            success: function(data) {
                //console.log(data);
                actualizar_tabla_tipovehiculos(data)
            }
        })
    }

    function actualizar_tabla_tipovehiculos(data) {
        var select = "";
        /*$("#tipo_veh").select2({
            placeholder: "Seleccione",
            allowClear: false
        });*/
        for (var i = 0; i < data.length; i++) {
            datos = data[i];
            select += `
                <option value='${datos.id}' data-icon="../../static/img/transporte/${datos.icono_veh}">${datos.nombre_veh} </option>
            `;
        }
        $("#tipo_veh").append(select);
        $('#tipo_veh').material_select();
    }

    function repartidores() {
        data = {
        }
        $.ajax({
            url: dominio + 'repartidores/',
            headers: {
                'token':userDOOMI.token,
                'tipo_usuario': userDOOMI.tipo_usu
            },
            type: 'GET',
            data: data,
            success: function(data) {
                //console.log(data);
                actualizar_tabla_repartidores(data.filter(e => e.aprobado), '#data-table-aprobados');
                actualizar_tabla_repartidores(data.filter(e => !e.aprobado), '#data-table-esperando');
            }
        })
    }

    function actualizar_tabla_repartidores(data, tabla) {

        var table = $(tabla || '.table').DataTable({
            "language": {
                "url": "../static/<lib></lib>/JTables/Spanish.json"
            }
        });
        count = 0
        table.clear().draw();

        for (var i = 0; i < data.length; i++) {
            datos = data[i];
            count++;

            var options = `
                <a href="#!" id="${datos.id}" class="btn btn-primary btn-floating ver-info tooltipped" data-position="bottom" data-delay="50" data-tooltip="Ver información del repartidor"><i class="fa fa-eye"></i></a>

                <a href="#!" id="${datos.id}" class="btn btn-primary btn-floating edit-info tooltipped" data-position="bottom" data-delay="50" data-tooltip="Editar información del repartidor"><i class="fa fa-edit"></i></a>
                `;

            options += datos.aprobado ?
                `<a href="#!" id="${datos.id}" class="btn btn-primary btn-floating ver-soli modal-trigger tooltipped" data-position="bottom" data-delay="50" data-tooltip="Solicitudes atendidas"><i class="fa fa-inbox"></i></a>`
                :
                `<a href="#!" id="${datos.id}" class="btn btn-primary btn-floating aprobar-rechazar modal-trigger tooltipped" data-position="bottom" data-delay="50" data-tooltip="Aceptar / Rechazar solicitudes"><i class="fa fa-check"></i></a>`
            /*
            if (datos.bloqueado && datos.bloqueado > 0) {
                options +=`
                <a href="#!" id="${datos.id}"
                    class="btn btn-primary btn-floating liberar modal-trigger tooltipped"
                    data-position="bottom" data-delay="50"
                    data-tooltip="Liberar repartidor"><i class="fa fa-lock-open"></i>
                </a>`
            }
            */

            veh = '';
            // img_autos = '';
            // if(datos.foto_1)
            //     img_autos += ` <img src="${datos.foto_1}" width="50px" onerror="this.src='../../static/img/logo.jpg'">`;
            // if(datos.foto_2)
            //     img_autos += ` <img src="${datos.foto_2}" width="50px" onerror="this.src='../../static/img/logo.jpg'">`;
            // if(datos.foto_3)
            //     img_autos += ` <img src="${datos.foto_3}" width="50px" onerror="this.src='../../static/img/logo.jpg'">`;
            if(datos.vehiculo[0].nombre_veh){
                img_veh = '';
                if(datos.vehiculo[0].icono_veh)
                    img_veh = `<br><img width="25px" src="../../static/img/transporte/${datos.vehiculo[0].icono_veh}">`;
                veh = datos.vehiculo[0].nombre_veh+" "+img_veh;
            }
            check = '';
            if(datos.estatus)
                check = 'checked';
            chechsts = `<div class="switch">
                            <small class="clr_primary" style="font-size: 10px;">Suspendido</small>
                            <label>

                            <input type="checkbox" ${check} class="cambiar_sts sts${datos.id}" id="${datos.id}">
                                <span class="lever"></span> <small class="clr_primary" style="font-size: 10px;">Activo</small>
                            </label>
                        </div>`;
            var row = [
                count,
                datos.identificacion+"-"+datos.cedula,
                datos.nombre+" "+datos.apellido,
                datos.id,
                veh,
                chechsts,
                options
                ];
            table.row.add(row).draw().node();

        }
        $('.tooltipped').tooltip();
    }

    var imgEdit = false;
    var imgEdit_v1 = false;
    var imgEdit_v2 = false;
    var imgEdit_v3 = false;
    var imgEdit_v4 = false;
    var img64_1 = '';
    var img64_v1 = '';
    var img64_v2 = '';
    var img64_v3 = '';
    var img64_v4 = '';

    var output = document.getElementById('imagen');
    var el = document.getElementById('resizer-demo');
    var resize = new Croppie(el, {
        viewport: { width: 180, height: 180 },
        boundary: { width: 210, height: 210 },
        showZoomer: true,
        enableResize: false,
        enableOrientation: true,
        mouseWheelZoom: 'ctrl'
    });

    var output1 = document.getElementById('imagen1');
    var el1 = document.getElementById('resizer-demo-1');
    var resize1 = new Croppie(el1, {
        viewport: { width: 180, height: 180 },
        boundary: { width: 210, height: 210 },
        showZoomer: true,
        enableResize: false,
        enableOrientation: true,
        mouseWheelZoom: 'ctrl'
    });

    var output2 = document.getElementById('imagen2');
    var el2 = document.getElementById('resizer-demo-2');
    var resize2 = new Croppie(el2, {
        viewport: { width: 180, height: 180 },
        boundary: { width: 210, height: 210 },
        showZoomer: true,
        enableResize: false,
        enableOrientation: true,
        mouseWheelZoom: 'ctrl'
    });

    var output3 = document.getElementById('imagen3');
    var el3 = document.getElementById('resizer-demo-3');
    var resize3 = new Croppie(el3, {
        viewport: { width: 180, height: 180 },
        boundary: { width: 210, height: 210 },
        showZoomer: true,
        enableResize: false,
        enableOrientation: true,
        mouseWheelZoom: 'ctrl'
    });

    var output4 = document.getElementById('imagen4');
    var el4 = document.getElementById('resizer-demo-4');
    var resize4 = new Croppie(el4, {
        viewport: { width: 180, height: 180 },
        boundary: { width: 210, height: 210 },
        showZoomer: true,
        enableResize: false,
        enableOrientation: true,
        mouseWheelZoom: 'ctrl'
    });
    /**
     * Método que abre el editor con la imagen seleccionada
     */
    abrirEditor = function abrirEditor(e,op) {
        if (e.files[0]) {
            if(op==0){
                output.src = URL.createObjectURL(event.target.files[0]);
                resize.bind({
                    url: output.src,
                });
                imgEdit = true;
            }else if(op==1){
                output1.src = URL.createObjectURL(event.target.files[0]);
                resize1.bind({
                    url: output1.src,
                });
                imgEdit_v1 = true;
            }else if(op==2){
                output2.src = URL.createObjectURL(event.target.files[0]);
                resize2.bind({
                    url: output2.src,
                });
                imgEdit_v2 = true;
            }else if(op==3){
                output3.src = URL.createObjectURL(event.target.files[0]);
                resize3.bind({
                    url: output3.src,
                });
                imgEdit_v3 = true;
            }else if(op==4){
                output4.src = URL.createObjectURL(event.target.files[0]);
                resize4.bind({
                    url: output4.src,
                });
                imgEdit_v4 = true;
            }
        }
        return;
    }

    $(document).on('click', '.ver-soli', function() {
        const id_repartidor = this.id;
        $('#soli-iden').val(id_repartidor);
        $('#md-solRepartidor').modal('open');
    });

    function lista_doomis() {
        ajaxGET('/pedidos/filtros_reporte', data => {
            let html = '<option value="" selected>Todo</option>';

            data.doomialiados.forEach(doomi => {
                html += `<option value="${doomi.id}"> ${doomi.nombre} </option>`;
            });

            $("#soli-doomi-list").html(html);
        });
    }

    $('#btn-buscar-soli').on('click', function() {
        const filtros = new FormData(document.getElementById('filtros-buscar-soli'));
        const data = {};
        filtros.forEach((val, key) => { data[key] = val });

        if(data.hasta) {
            data.hasta = new Date(data.hasta.split('-'));
            // Para que incluya todo el dia.
            data.hasta.setHours(23, 59, 59);
            data.hasta = data.hasta.toISOString();
        }

        // Texto del boton.
        $('#btn-buscar-soli').attr('disabled', true);
        $('#btn-buscar-soli > span').html('Cargando...');
        $.ajax({
            url: dominio + 'pedidos/filtrados',
            type: 'GET',
            data: data,
            contentType: false,
            headers: {
                'token':userDOOMI.token,
                'tipo_usuario': userDOOMI.tipo_usu,
            },
            success: data => {
                // Texto del boton.
                $('#btn-buscar-soli').attr('disabled', false);
                $('#btn-buscar-soli > span').html('Buscar');

                const table = $('#table-sol-rep').DataTable({
                    "language": {
                        "url": "../static/<lib></lib>/JTables/Spanish.json"
                    }
                });
                table.clear().draw();

                data.forEach((pedido, i) => {
                    let d = new Date(pedido.creado);
                    table.row.add([
                        i+1,
                        pedido.id,
                        [d.getDate(), d.getMonth()+1, d.getFullYear()].join('/'),
                        pedido.id_restaurante,
                        pedido.nombre_doomi,
                        pedido.id_cliente,
                        pedido.cliente.nombre + ' ' + pedido.cliente.apellido
                    ]).draw().node();
                });
            },
            error: () => {
                Materialize.toast('Error al buscar', 4000);
                $('#btn-buscar-soli').attr('disabled', false);
                $('#btn-buscar-soli > span').html('Buscar');
            },
        });
    });

    $(document).on('click', ".abrirmodal", function(){
        $("#form_registrar_repar")[0].reset();
        $("#tit-mod").html("Nuevo Repartidor"); actualizarTituloID();
        $("#iden").val("");
        $(".bt_save").html("Guardar");
        $(".all_img").attr("src", "../../static/img/logo.jpg");
        //$(".all_img_u").attr("src", "../../static/img/user.png");
        $("#identificacion").attr("change");
        $("#tipo_veh").material_select();
        $('#imagen').attr('src', '../../static/img/user.png');
        /*resize.bind(output.src);
        resize1.bind("../../static/img/logo.jpg");
        resize2.bind("../../static/img/logo.jpg");
        resize3.bind("../../static/img/logo.jpg");
        resize4.bind("../../static/img/logo.jpg");*/
        setTimeout(function(){
                resize.bind({url: output.src,zoom: 0});
                resize1.bind({url: "../../static/img/logo.jpg",zoom: 0});
                resize2.bind({url: "../../static/img/logo.jpg",zoom: 0});
                resize3.bind({url: "../../static/img/logo.jpg",zoom: 0});
                resize4.bind({url: "../../static/img/logo.jpg",zoom: 0});
        }, 900);
        img64_1 = '';imgEdit = false;
        img64_v1 = '';imgEdit_v1 = false;
        img64_v2 = '';imgEdit_v2 = false;
        img64_v3 = '';imgEdit_v3 = false;
        img64_v4 = '';imgEdit_v4 = false;
        $("#md-nuevoRepartidor").modal('open');
    });

    $("#form_registrar_repar").on('submit', function(e) {
        e.preventDefault();
        /*validaciones*/
        var errr = false;
        var msj = false;

        if ( $("#nombre").val().trim() == "" ){ errr = true; msj = "Ingresa un nombre";}

        else if ( $("#apellido").val().trim() == "" ){ errr = true; msj = "Ingresa un apellido";}

        else if ( $("#identificacion").val() == "" ){ errr = true; msj = "Selecciona un tipo de identificación";}

        else  if ( $("#cedula").val().trim() == "" ){ errr = true; msj = "Ingresa cédula";}

        else if ( $("#cedula").val().length < 7 || $("#cedula").val().length > 8 ){ errr = true; msj = "Campo cédula: mínimo 7 carácteres y máximo 8";}

        else if ( $("#correo").val().trim() == "" ){ errr = true; msj = "Ingresa correo";}

        else if ( $("#tipo_veh").val() == "" ){ errr = true; msj = "Seleccione el tipo de vehiculo";}

        else if ( $("#telefono").val() == "" ){ errr = true; msj = "Ingrese el teléfono";}

        else if ( $("#telefono").val().length < 10 || $("#telefono").val().length > 15 ){ errr = true; msj = "Campo teléfono: mínimo 10 carácteres y máximo 15";}

        else if ( $("#whatsapp").val() == "" ){ errr = true; msj = "Ingrese el Whatsapp";}

        else if ( $("#whatsapp").val().length < 10 || $("#whatsapp").val().length > 15 ){ errr = true; msj = "Campo whatsapp: mínimo 10 carácteres y máximo 15";}

        else if ( $("#usuario").val().trim().length < 5){ errr = true; msj = "Ingresa el usuario (mínimo 5 caracteres)";}

        else if ( $("#pass").val().trim().length < 5 && ($("#pass").val().trim() != '' || $("#iden").val() == '')){ errr = true; msj = "Ingresa una contraseña (mínimo 5 caracteres)";}

        /*fin validaciones*/
        if (errr) {
            Materialize.toast(msj, 4000); return false;
        }
        var data = new FormData(this);
        //data a enviar
        resize.result('base64').then(function(base64) {
            if(imgEdit)
                img64_1 = base64;
            resize1.result('base64').then(function(base64) {
                if(imgEdit_v1)
                    img64_v1 = base64;
                resize2.result('base64').then(function(base64) {
                    if(imgEdit_v2)
                        img64_v2 = base64;
                    resize3.result('base64').then(function(base64) {
                        if(imgEdit_v3)
                            img64_v3 = base64;
                        resize4.result('base64').then(function(base64) {
                            if(imgEdit_v4)
                                img64_v4 = base64;
                            data.append("imgweb", "1");
                            data.append("img", img64_1);
                            data.append("img1", img64_v1);
                            data.append("img2", img64_v2);
                            data.append("img3", img64_v3);
                            data.append("img4", img64_v4);
                            if($("#iden").val() != ''){
                                url = "repartidores/edit";
                                tipo = "PUT";
                            }else{
                                url = "repartidores/add";
                                tipo = "POST";
                            }
                            $(".bt_save").prop("disabled", true);
                            $(".fileprogress").removeClass("hide");
                            $(".fileprogress").find("div")[0].style.width = "0%";
                            $.ajax({
                                url: dominio + url,
                                headers: {
                                    'token':userDOOMI.token,
                                    'tipo_usuario': userDOOMI.tipo_usu
                                },
                                type: tipo,
                                dataType: "json",
                                cache: false,
                                data: data,
                                contentType: false,
                                processData: false,
                                xhr: function(){
                                    var xhr = new window.XMLHttpRequest();
                                    xhr.upload.addEventListener("progress", function(e){
                                        if (e.lengthComputable) {
                                          var percentComplete = parseInt( (e.loaded / e.total * 100), 10);
                                          $(".fileprogress").find("div")[0].style.width = `${percentComplete}%`;
                                          if(percentComplete>=100){
                                            $(".fileprogress").addClass("hide");
                                            $(".fileprogress").find("div")[0].style.width = "0%";
                                          }
                                        }
                                    }, false);
                                    xhr.addEventListener("progress", function(e){
                                        if (e.lengthComputable) {
                                            var percentComplete = parseInt( (e.loaded / e.total * 100), 10);
                                            $(".fileprogress").find("div")[0].style.width = `${percentComplete}%`;
                                            if(percentComplete>=100){
                                                $(".fileprogress").addClass("hide");
                                                $(".fileprogress").find("div")[0].style.width = "0%";
                                            }
                                        }
                                    }, false);
                                    return xhr;
                                },
                                success: function(res) {
                                    Materialize.toast(res.msj, 4000)
                                    if (res.r == true) {
                                        $("#form_registrar_repar")[0].reset();
                                        repartidores();
                                        $("#md-nuevoRepartidor").modal('close');
                                    }
                                    //botones del form
                                    $(".bt_save").prop("disabled", false)
                                },
                                error: function(xhr, status, errorThrown) {
                                    console.log(errorThrown);
                                    $(".fileprogress").addClass("hide");
                                    $(".fileprogress").find("div")[0].style.width = "0%";
                                    Materialize.toast(errorThrown.msj, 4000)

                                    //botones del form
                                    $(".bt_save").prop("disabled", false)
                                }
                            });
                        });
                    });
                });
            });
        });
    });
    $(document).on('click', '.ver-info', function() {
        var idv = this.id;
        buscar_data(idv,'ver')
    });
    $(document).on('click', '.edit-info', function() {
        var idv = this.id;
        buscar_data(idv,'editar')
    });
    function buscar_data(idb,modal,modalName){
        data = {
        }
        $.ajax({
            url: dominio + 'repartidores/'+idb,
            headers: {
                'token':userDOOMI.token,
                'tipo_usuario': userDOOMI.tipo_usu
            },
            type: 'GET',
            data: data,
            success: function(data) {
                //console.log(data);
                imprimir_data_credencial(data,modal,modalName)
            }, error: function(xhr, status, errorThrown) {
                console.log(errorThrown);
            }

        })
    }
    function imprimir_data_credencial(data,modal,modalName){
        if(modal=='ver'){
            let view_tlfn = data.telefono ? '+' + data.telefono : '';
            let view_ws = data.whatsapp ? '+' + data.whatsapp : '';
            $(".iden").val(data.id);
            $(".id-view").html(data.id_repartidor);
            $(".nom-view").html(data.nombre);
            $(".ape-view").html(data.apellido);
            $(".ced-view").html(data.identificacion+"-"+data.cedula);
            $(".cor-view").html(data.correo);
            $(".usu-view").html(data.usuario);
            $(".tel-view").html(view_tlfn);
            $(".wha-view").html(view_ws);
            $(".dom-view").html(data.domicilio);
            $(".met-view").html(data.metodos_pagos);
            $(".veh-view").html('');
            if(data.vehiculo[0].nombre_veh){
                img_veh = '';
                if(data.vehiculo[0].icono_veh)
                    img_veh = `<img width="25px" src="../../static/img/transporte/${data.vehiculo[0].icono_veh}" style="position: absolute;margin-left: 5px;">`;
                $(".veh-view").html(data.vehiculo[0].nombre_veh+" "+img_veh);
            }
            if(data.sexo=='F')
                $(".sex-view").html("Mujer");
            else if(data.sexo=='M')
                $(".sex-view").html("Hombre");
            else
                $(".sex-view").html("");
            $(".all_img_view").attr("src", "../../static/img/logo.jpg");
            $(".all_img_view_u").attr("src", "../../static/img/user.png");
            if(data.img)
                $(".img-view").attr("src",data.img);
            if(data.foto_1)
                $(".img1-view").attr("src",data.foto_1);
            if(data.foto_2)
                $(".img2-view").attr("src",data.foto_2);
            if(data.foto_3)
                $(".img3-view").attr("src",data.foto_3);
            if(data.img_rif){
                $(".rif-img-view").attr("src", data.img_rif);
                $("#rif-li-view").attr("style", "display: block;");
            }
            else{
                $("#rif-li-view").attr("style", "display: none;");
            }

            $(modalName || "#md-verRepartidor").modal("open");
        }else if(modal=='editar'){
            $("#nombre").val(data.nombre);
            $("#apellido").val(data.apellido);
            $("#cedula").val(data.cedula);
            $("#correo").val(data.correo);
            $("#identificacion").val(data.identificacion).attr("change");
            $("#usuario").val(data.usuario);
            $("#telefono").val(data.telefono);
            $("#whatsapp").val(data.whatsapp);
            $("#tipo_veh").val(data.id_tipo_vehiculo);
            $('#tipo_veh').material_select();
            $("#domicilio").val(data.domicilio);
            $("#metodo_pago").val(data.metodos_pagos);
            //$("#tipo_veh").select2({placeholder: "Seleccione",allowClear: false});
            $("#iden").val(data.id);
            $("#id_repartidor").val(data.id_repartidor);
            $("#Mujer").prop("checked", false);
            $("#Hombre").prop("checked", false);
            if(data.sexo=='F')
                $("#Mujer").prop("checked", true);
            else if(data.sexo=='M')
                $("#Hombre").prop("checked", true);
            $("#pass").val('');
            $("#tit-mod").html(`Editar repartidor #${data.id_repartidor}`);
            $(".bt_save").html('Guardar cambios');
            $(".all_img").attr("src", "../../static/img/logo.jpg");
            $(".all_img_u").attr("src", "../../static/img/user.png");
            if(data.img)
                output.src = data.img;
            if(data.foto_1)
                output1.src = data.foto_1;
            if(data.foto_2)
                output2.src = data.foto_2;
            if(data.foto_3)
                output3.src = data.foto_3;
            if(data.img_rif)
                output4.src = data.img_rif;

            setTimeout(function(){ $(".cr-image").css("transform","translate3d(15px, 15px, 0px) scale(1)"); }, 200);

            img64_1 = '';imgEdit = false;
            img64_v1 = '';imgEdit_v1 = false;
            img64_v2 = '';imgEdit_v2 = false;
            img64_v3 = '';imgEdit_v3 = false;
            img64_v4 = '';imgEdit_v4 = false;
            $("#md-nuevoRepartidor").modal("open");
            setTimeout(function(){
                resize.bind({url: output.src,zoom: 0});
                resize1.bind({url: output1.src,zoom: 0});
                resize2.bind({url: output2.src,zoom: 0});
                resize3.bind({url: output3.src,zoom: 0});
                resize4.bind({url: output4.src,zoom: 0});
            }, 1000);
        }
    }

    $(document).on('click', ".cambiar_sts", function(){
        id=$(this).attr("id");
        v=0;
        if($(".sts"+id+":checked").length>0)
          v = 1;
        var data = new FormData();
        data.append("iden", id);
        data.append("sts", v);
        $.ajax({
            method: "PUT",
            url: dominio + 'repartidores/edit_sts',
            headers: {
                'token':userDOOMI.token,
                'tipo_usuario': userDOOMI.tipo_usu
            },
            data: data,
            contentType: false,
            processData: false,
            success: function(data) {
                Materialize.toast( data.msj , 5000);
                // repartidores();
            },
            error: function(xhr, status, errorThrown) {
                console.log("Errr: ", errorThrown);
            }
        });
    });

    $(document).on('click', '.aprobar-rechazar', function(){
        id = $(this).attr('id');
        buscar_data(id, 'ver', '#md-aprRepartidor')
        $('#md-aprRepartidor').modal('open');
    });

    $(document).on('click', '.btn-aprobar-rechazar', function(){
        const val = $(this).attr('value');
        const msj = val==1 ? 'Aprobar' : 'Rechazar';

        Materialize.toast(`
        <span>¿${msj} repartidor?</span><br><button value="${val}" class="confirm-aprobar-rep btn-flat toast-action" >Si</button><button class="btn-flat toast-action" onclick=" $('.toast').hide(); ">No</button>
        `, 4000);
    });

    $(document).on('click', '.confirm-aprobar-rep', function(){
        const id = $('.iden').val();
        const val = $(this).attr('value');
        const obs = $('#observaciones').val();
        $.ajax({
            url: dominio + `repartidores/aprobacion`,
            type: 'PATCH',
            data: {
                id,
                aprobado: val,
                observaciones: obs,
                token: userDOOMI.token,
                tipo_usuario:userDOOMI.tipo_usu
            },
            success: function(data) {
                $('.toast').hide();
                $('#md-aprRepartidor').modal('close');
                Materialize.toast(data.msj , 5000);
                repartidores();
            },
            error: function(xhr, status, errorThrown) {
                console.log("Errr: ", errorThrown);
            }
        });
    });

    $(document).on('click', '.delete', function() {
        var idv = this.id;
        var toastContent = '<span>¿Desea eliminar éste registro?</span><br><button class="btn-flat toast-action conf_si" id="'+idv+'">Si</button><button class="btn-flat toast-action" onclick=" $(\'.toast\').hide(); ">No</button>';

        Materialize.toast(toastContent, 4000);
    });

    $(document).on('click', '.conf_si', function() {
        id = this.id
        var data = {};

        $('.toast').hide();
        $.ajax({
            url: dominio + 'repartidores/delete/'+id,
            headers: {
                'token':userDOOMI.token,
                'tipo_usuario': userDOOMI.tipo_usu
            },
            type: 'DELETE',
            data: data,
            success: function(data) {
                if (data.msj) {
                    Materialize.toast(data.msj, 10000);
                    repartidores();
                } else {
                    Materialize.toast(data.msj, 10000);
                }
            }
        })
    });

    $(document).on('click', '.sorting_1', function() {
        $('.tooltipped').tooltip();
    });

    $(document).on('click', '.liberar', function () {
        const idr = this.id;
        const toastContent = '<span>¿Liberar repartidor?</span><br><button class="btn-flat toast-action liberar_si" id="'+idr+'">Si</button><button class="btn-flat toast-action" onclick=" $(\'.toast\').hide(); ">No</button>';

        Materialize.toast(toastContent, 4000);
    });
    $(document).on('click', '.liberar_si', function() {
        const id = this.id;


        $('.toast').hide();
        $.ajax({
            url: dominio + 'repartidores/liberar/'+id,
            headers: {
                'token':userDOOMI.token,
                'tipo_usuario': userDOOMI.tipo_usu,
                'id_usuario': userDOOMI.id
            },
            type: 'PATCH',
            success: function(data) {
                if (data.r) {
                    Materialize.toast('Repartidor liberado', 1500);
                    repartidores();
                }
            },
            error: function() {
                Materialize.toast('Ocurrió un error', 1500);
            }
        })
    });

    actualizarTituloID = function() {
    	// Obtener el nuevo id.
    	let data = {};
        $.ajax({
            url: dominio + 'repartidores/nuevo_id',
            headers: {
                'token':userDOOMI.token,
                'tipo_usuario': userDOOMI.tipo_usu
            },
            type: 'GET',
            data: data,
            success: function(data) {
                nuevoID = data.nuevoID;
                $("#tit-mod").html(`Nuevo repartidor <span style="font-size: 15px">#${nuevoID}</span>`);
                $("#id_nuevo_repartidor").val(data.nuevoID);

            }, error: function(xhr, status, errorThrown) {
                console.log(errorThrown);
            }
        });
    }

}
