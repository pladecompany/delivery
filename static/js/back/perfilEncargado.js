function perfilEncargado(){
    $("#ciudad, #pais, #estado").select2({placeholder: "Seleccione", allowClear: false});

    recuperarPerfil();

    var imgEdit = false;
    var img64_1 = '';
    
    var output = document.getElementById('imagen');
    var el = document.getElementById('resizer-demo');
    var resize = new Croppie(el, {
        viewport: { width: 180, height: 180 },
        boundary: { width: 210, height: 210 },
        showZoomer: true,
        enableResize: false,
        enableOrientation: true,
        mouseWheelZoom: 'ctrl'
    });


    abrirEditor = function abrirEditor(e,op) {
        if (e.files[0]) {
            if(op==0){
                output.src = URL.createObjectURL(event.target.files[0]);
                resize.bind({
                    url: output.src,
                });
                imgEdit = true;
            }
        }
        return;
    }

    function recuperarPerfil() {
        data = {id_res: userDOOMI.restaurante[0].id};

        $.ajax({
            url: dominio + "perfiles/encargado/" + userDOOMI.id,
            headers: {
                "token":userDOOMI.token,
                "tipo_usuario": userDOOMI.tipo_usu
            },
            type: "GET",
            data: data,
            success: actualizar_datos_perfil
        });
    }

    function actualizar_datos_perfil(data) {
        var table = $(".table").DataTable({
            "language":{
                "url": "../static/<lib></lib>/JTables/Spanish.json"
            }
        });


        $("#iden").val(data.id);
        $("#titulo_id_encargado").html(data.id)

        if(data.img){
            output.src = data.img;
            resize.bind({url: output.src,zoom: 1});
            setTimeout(function(){ $(".cr-image").css("transform","translate3d(15px, 15px, 0px) scale(1)"); }, 200);
        }
        else{
            output.src = '../../static/img/user.png';
            resize.bind({url: output.src});
        }
        img64_1 = '';imgEdit = false;
        $("#nombre_apellido").val(data.nombre_apellido);
        $("#cedula").val(data.cedula);
        $("#correo").val(data.correo);

        $("#telefono").val(data.telefono);
        $("#whatsapp").val(data.whatsapp);
    }


    $("#form_perfil").on("submit", function(e) {
        e.preventDefault();

        /*validaciones*/
        var errr = false;
        var msj = "";

        if ($("#nombre_apellido").val().trim() == "")
            errr = true; msj = "Ingrese nombre y apellido";


        let cedula = $("#cedula").val().trim();

        if (cedula == ""){
            errr = true; msj = "Ingrese la cédula";
        }
        else if (cedula.length < 7  ||  cedula.length > 8){
            errr = true; msj = "La cedula debe tener entre 7 y 8 caracteres";
        }


        /*fin validaciones*/
        if (errr) {
            Materialize.toast(msj, 4000); return false;
        }
        //data a enviar
        var data = new FormData(this);
        data.append("id_res", userDOOMI.restaurante[0].id);
        data.append("iden", userDOOMI.id);
        
        resize.result('base64').then(function(base64) {
            if(imgEdit)
                img64_1 = base64;
            data.append("imgweb", "1");
            data.append("img", img64_1);

            enviarForm({
                ruta: 'perfiles/edit_encargado',
                tipo: 'PUT',
                data: data,
                success: function(res) {
                    Materialize.toast(res.msj, 4000);

                    // Actualizar datos locales.
                    let uD = JSON.parse(localStorage.getItem("userDOOMI"));

                    uD.cedula = res.cedula;
                    localStorage.setItem("userDOOMI", JSON.stringify(uD));

                    window.location.replace("index.html?op=inicio");
                },
                error: function(xhr, status, errorThrown) {
                    console.log(errorThrown);
                    Materialize.toast(errorThrown.msj, 4000)

                    recuperarPerfil();

                    //botones del form
                    $("#bt_guardar_cambios").addClass("hide");
                }
            });
        });
    });


    // Limpiar campos numericos.
    $("#cedula").on("input propertychange change", function() {
        this.value = extraerDigitos(this.value);
    });

    $(document).on('click', '.conf_si_2', function() {
        id = this.id
        var data = {};

        $('.toast').hide();
        $.ajax({
            url: dominio + 'perfiles/delete_usu/'+id,
            headers: {
                'token':userDOOMI.token,
                'tipo_usuario': userDOOMI.tipo_usu
            },
            type: 'DELETE',
            data: data,
            success: function(data) {
                if (data.msj) {
                    Materialize.toast(data.msj, 10000);
                    $("#tr-e"+id).remove();
                } else {
                    Materialize.toast(data.msj, 10000);
                }
            }
        })
    });

    $(document).on('click', '.delete-usu', function() {
        var idv = this.id;
        $("#tr"+idv).remove();
    });


}