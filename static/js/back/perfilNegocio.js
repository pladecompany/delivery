function perfilNegocio(){
    $("#ciudad, #pais, #estado").select2({placeholder: "Seleccione", allowClear: false});

    let hoy = new Date().toISOString().split('T')[0] + 'T00:00:00';
    $('#fecha_inicio').attr('min', hoy);
    $('#fecha_fin').attr('min', hoy);

    var id_categoria_s = false;
    var ids_subcategorias_s = false;
    var latitude = false;
    var longitude = false;

    // ajaxCategorias();
    ajaxPais();
    recuperarPerfil();

    var imgEdit = false;
    var imgEdit_v1 = false;
    var img64_1 = '';
    var img64_v1 = '';

    var output = document.getElementById('imagen');
    var el = document.getElementById('resizer-demo');
    var resize = new Croppie(el, {
        viewport: { width: 180, height: 180 },
        boundary: { width: 210, height: 210 },
        showZoomer: true,
        enableResize: false,
        enableOrientation: true,
        mouseWheelZoom: 'ctrl'
    });

    var output1 = document.getElementById('imagen1');
    var el1 = document.getElementById('resizer-demo-1');
    var resize1 = new Croppie(el1, {
        viewport: { width: 180, height: 180 },
        boundary: { width: 210, height: 210 },
        showZoomer: true,
        enableResize: false,
        enableOrientation: true,
        mouseWheelZoom: 'ctrl'
    });

    abrirEditor = function abrirEditor(e,op) {
        if (e.files[0]) {
            if(op==0){
                output.src = URL.createObjectURL(event.target.files[0]);
                resize.bind({
                    url: output.src,
                });
                imgEdit = true;
            }else if(op==1){
                output1.src = URL.createObjectURL(event.target.files[0]);
                resize1.bind({
                    url: output1.src,
                });
                imgEdit_v1 = true;
            }
        }
        return;
    }

    function recuperarPerfil() {
        ajaxGET('perfiles/?id_res=' + userDOOMI.restaurante[0].id, actualizar_datos_perfil);
    }

    function actualizar_datos_perfil(data) {
        /*var table = $(".table").DataTable({
            "language":{
                "url": "../static/<lib></lib>/JTables/Spanish.json"
            }
        });*/
        //console.log(data);

        $("#iden").val(data.id);

        if(data.img)
            output.src = data.img;
        if(data.img_portada)
            output1.src = data.img_portada;
        resize.bind({url: output.src,zoom: 1});
        resize1.bind({url: output1.src,zoom: 1});
        setTimeout(function(){ $(".cr-image").css("transform","translate3d(15px, 15px, 0px) scale(1)"); }, 200);
        img64_1 = '';imgEdit = false;
        img64_v1 = '';imgEdit_v1 = false;

        id_categoria_s = data.id_categoria;
        ids_subcategorias_s = data.id_subcategorias;
        ajaxCategorias();

        latitude = data.lat;
        longitude = data.lon;

        $("#nombre").val(data.nombre);
        $("#descripcion").val(data.descripcion);
        $("#ubicacion").val(data.ubicacion);

        //$("#sts_viajando").prop("checked", data.vacaciones).trigger('change');

        $("#rif_letra").val(data.rif_letra).trigger("change");
        $("#rif_numero").val(data.rif_numero);
        $("#rif_chequeo").val(data.rif_chequeo);

        $("#delivery").prop("checked", data.modalidad_delivery);
        $("#pickup").prop("checked", data.modalidad_pickup);
        $("#programado").prop("checked", data.modalidad_programados).trigger('change');
        $("#pedidosprogramadoscantidad").val(data.modalidad_programados_cantidad)
            .toggleClass('hide', !data.modalidad_programados);

        $("#telefono").val(data.telefono);
        $("#facebook").val(data.facebook);
        $("#instagram").val(data.instagram);

        if (data.lat !='' && data.lon !='') {
            $("#lat").val(data.lat);
            $("#lon").val(data.lon);
            iniciarMapa(data.lat,data.lon);
        }
        else {
            $("#lat").val('');
            $("#lon").val('');
            iniciarMapa();
        }
        if(data.pedidos_completados > 0){
            $(".vista_cali").html('<h5 class="text-center">'+data.calificacion.toFixed(1)+' <a href="#!" class=""><i class="fa fa-star"></i></a></h5>');
        }

/*
        $(`input[name='pedidos_programados'][value='${data.pedidos_programados}']`)
            .prop("checked", true).change();

        $('#dias').val(data.pedidos_programados_dias);

        // Fecha minima.
        let ahora = new Date();
        let dia = ("0" + (ahora.getDate()+1)).slice(-2);
        let mes = ("0" + (ahora.getMonth() + 1)).slice(-2);
        let hoy = ahora.getFullYear() + "-" + (mes) + "-" + (dia);
        $('#dias').attr('min', hoy);
*/

        $("#list-usuarios").html("");

        usus = "";
        count = 1;
        var html = "";

        for (var i = 0; i < data.usuarios.length; i++) {
            dataUsuario = data.usuarios[i];

            html += `
                <tr id="tr-e${dataUsuario.id}">
                    <td class="p-0 m-1">${count}</td>
                    <td class="p-0 m-1"><div class="input-field col s12 m-1"><input placeholder="" value="${data.usuarios[i].nombre_apellido}" name="nombres_apellidos[]" type="text" class="form-control mb-0 datausu" iden='n${trusu}'></div></td>
                    <td class="p-0 m-1"><div class="input-field col s12 m-1"><input placeholder="" value="${dataUsuario.cedula}" name="cedulas[]" type="text" class="form-control mb-0 dataced allusus" iden="${dataUsuario.id}"></div></td>
                    <td class="p-0 m-1"><div class="input-field col s12 m-1"><input placeholder="" value="${dataUsuario.correo}" name="correos[]" type="email" class="form-control mb-0 datacor allusus" iden="${dataUsuario.id}"></div></td>
                    <td class="p-0 m-1"><div class="input-field col s12 m-1"><input type="hidden" name="idusu[]" value="${dataUsuario.id}"><input placeholder="Solo si desea actualizar" name="pass[]" type="password" class="form-control mb-0 datausu2"></div></td>
                    <td class="text-center">
                        <div class="input-field m-1 p-0 col s12">
                            <a class="waves-effect btn btn-primary btn-floating delete-usu-2" id="${dataUsuario.id}"><i class="fa fa-trash"></i></a>
                        </div>
                    </td>
                </tr>
            `;
        count++;
        }
        $("#list-usuarios").append(html);
    }

    $("#form_perfil").on("submit", function(e) {
        e.preventDefault();

        /*validaciones*/
        var errr = false;
        var msj = false;
        var incompletos = 0;
        var incompletos2 = 0;
        var iguales = 0;
        var iguales_c = 0;
        $('.dataced').each(function(){
            idn = $(this).attr('iden');
            valor = $(this).val();
            if(valor.trim()=='' || valor.length < 7 || valor.length > 8)
                incompletos ++;
            if(idn){
                $('.dataced').each(function(){
                    idn2 = $(this).attr('iden');
                    if(idn!=idn2){
                        if($(this).val() == valor)
                            iguales++;
                    }
                });
            }
        });
        $('.datacor').each(function(){
            idn = $(this).attr('iden');
            valor = $(this).val();
            if(valor.trim()=='' || valor.length < 5)
                incompletos2 ++;
            $('.datacor').each(function(){
                idn2 = $(this).attr('iden');
                if(idn!=idn2){
                    if($(this).val() == valor)
                        iguales_c++;
                }
            });
        });
        $('.datausu2').each(function(){
            valor = $(this).val();
            if(valor.length > 0 && valor.length < 5)
                incompletos ++;
        });

        if ( $("#nombre").val().trim() == "" ){ errr = true; msj = "Ingresa el nombre";}

        else if ( $("#ubicacion").val().trim() == "" ){ errr = true; msj = "Ingresa la ubicación";}

        else if ( $("#lat").val() == "" || $("#lon").val() == ""){ errr = true; msj = "Selecciona la ubicación en el mapa";}

        else if ( $("#pais").val() == "" ){ errr = true; msj = "Selecciona el pais";}

        else if ( $("#estado").val() == "" ){ errr = true; msj = "Selecciona el estado";}

        else if ( $("#ciudad").val() == "" ){ errr = true; msj = "Selecciona la ciudad";}

        else if(!$("#delivery:checked").val() &&
                !$("#pickup:checked").val() &&
                !$("#pedidosprogramados:checked").val())
            {errr=true; msj="Seleccione una modalidad de trabajo"}

        else if($("#pedidosprogramados:checked").val() &&
                $("#pedidosprogramadoscantidad").val() < 1)
            {errr = true; msj="La cantidad de pedidos debe ser mayor que cero"}

        else if ( parseFloat(incompletos) > 0 ){ errr = true; msj = "Debes completar todos los campos de cédula (entre 7 y 8 caracteres)";}

        else if ( parseFloat(incompletos2) > 0 ){ errr = true; msj = "Debes completar todos los datos de los usuarios";}

        else if ( parseFloat(iguales) > 0 ){ errr = true; msj = "No debes repetir los usuarios";}

        else if ( parseFloat(iguales_c) > 0 ){ errr = true; msj = "No debes repetir los correos de usuarios";}

        else if($('#delivery').prop('checked') && $('#delivery').attr('horarios') == 0)
            {errr=true; msj='No hay horarios generales para la modalidad delivery'}

        // else if($('#pickup').prop('checked') && $('#pickup').attr('horarios') == 0)
        //     {errr=true; msj='No hay horarios para modalidad pick up'}

        else if($('#programado').prop('checked') && $('#programado').attr('horarios') == 0)
            {errr=true; msj='No hay horarios para modalidad de pedidos programados'}

        /*fin validaciones*/
        if (errr) {
            Materialize.toast(msj, 4000); return false;
        }
        //data a enviar
        var data = new FormData(this);
        //v=0;
        //if($("#sts_viajando:checked").length>0)
        //  v = 1;
        //data.append("vaca", v);
        resize.result('base64').then(function(base64) {
            if(imgEdit)
                img64_1 = base64;
            resize1.result('base64').then(function(base64) {
                if(imgEdit_v1)
                    img64_v1 = base64;
                data.append("imgweb", "1");
                data.append("img", img64_1);
                data.append("img1", img64_v1);

                // $(".bt_save").prop("disabled", true);
                // $(".fileprogress").removeClass("hide");
                // $(".fileprogress").find("div")[0].style.width = "0%";
                enviarForm({
                    ruta: 'restaurantes/edit',
                    tipo: 'PUT',
                    data: data,
                    progressBar: true,
                    success: function(res) {
                        Materialize.toast(res.msj, 4000);
                        $(".bt_save").prop("disabled", false);

                        if (!res.r) return;

                        // Actualizar datos locales.
                        let uD = JSON.parse(localStorage.getItem("userDOOMI"));

                        uD.restaurante[0] = res;
                        localStorage.setItem("userDOOMI", JSON.stringify(uD));
                        completarPaso(2);
                        window.location.replace("index.html?op=inicio");
                    },
                    error: function(xhr, status, errorThrown) {
                        console.log(errorThrown);
                        Materialize.toast(errorThrown.msj, 4000);

                        recuperarPerfil();

                        //botones del form
                        $(".bt_save").prop("disabled", false);
                        $("#bt_guardar_cambios").addClass("hide");
                    }
                });
            });
        });
    });

    trusu = 0;
    $(document).on('click', '.add-usu', function() {
        var html = `<tr id="tr-${trusu}">
                <td class="p-1"><span class="caja-new">Nuevo</span></td>
                <td class="p-1"><div class="input-field m-0 p-0 s12"><input placeholder="" name="nombres_apellidos[]" type="text" class="form-control m-0 datausu" iden='n${trusu}'></div></td>
                <td class="p-1"><div class="input-field m-0 p-0 col s12"><input placeholder="" name="cedulas[]" type="text" class="form-control m-0 dataced allusus" iden='n${trusu}'></div></td>
                <td class="p-1"><div class="input-field mb-1 mt-2 p-0 col s12"><input placeholder="" name="correos[]" type="email" class="form-control m-0 datacor" iden='n${trusu}'></div></td>
                <td class="p-1"><div class="input-field mb-1 mt-2 p-0 col s12"><input type="hidden" name="idusu[]" value="0"><input placeholder="" name="pass[]" type="password" class="form-control m-0 datausu" ></div></td>
                <td class="text-center">
                    <div class="input-field m-1 p-0 col s12">
                        <a class="waves-effect btn btn-primary btn-floating delete-usu" id="-${trusu}"><i class="fa fa-trash"></i></a>
                    </div>
                </td>
            </tr>
        `;
        trusu++;
        $("#list-usuarios").append(html);
    });


    // Eliminar usuario.
    $(document).on('click', '.delete-usu-2', function() {
        var idv = this.id;
        var toastContent = '<span>¿Desea eliminar éste usuario?</span><br><button class="btn-flat toast-action conf_si_2" id="'+idv+'">Si</button><button class="btn-flat toast-action" onclick=" $(\'.toast\').hide(); ">No</button>';

        Materialize.toast(toastContent, 4000);
    });
    $(document).on('click', '.conf_si_2', function() {
        id = this.id

        ajaxDELETE(`perfiles/delete_usu/${id}`, function(data) {
                if (data.msj) {
                    Materialize.toast(data.msj, 10000);
                    $("#tr-e"+id).remove();
                } else {
                    Materialize.toast(data.msj, 10000);
                }
            });


        $('.toast').hide();
    });

    $(document).on('click', '.delete-usu', function() {
        var idv = this.id;
        $("#tr"+idv).remove();
    });

    $('#pedidosprogramados').on('click', function(){
        let checked = $(this).prop('checked');
        $('#pedidosprogramadoscantidad').toggleClass('hide', !checked);
    });

    // Limpiar campo numerico pedidos.
    $('#pedidosprogramadostexto').on('input propertychange change', function(){
        this.value = extraerDigitos(this.value);
    });

    // Guardar horarios programados.
    $('.btn-agregar-horario').on('click', function() {
        const data = new FormData( document.getElementById('form-horario') ),
            fec_sel = new Date(data.get('fecha_ini')),
            ini = new Date(data.get('fecha_ini')+' '+data.get('hora_ini')),
            fin = new Date(data.get('fecha_ini')+' '+data.get('hora_fin'));

        /* VERIFICAR */
        let err = false;

        if ( isNaN(fec_sel.getTime()) ) err = 'Seleccione fecha';
        else if ( isNaN(ini.getTime()) ) err = 'Ingrese hora inicio';
        else if ( isNaN(fin.getTime()) ) err = 'Ingrese hora final';
        else if (ini > fin) err = 'La hora inicial no puede ser mayor que la final';

        if (err) return Materialize.toast(err, 5000);

        data.append('id_res', userDOOMI.restaurante[0].id);
        data.append('fecha_inicio', data.get('fecha_ini')+' '+data.get('hora_ini'));
        data.append('fecha_fin', data.get('fecha_ini')+' '+data.get('hora_fin'));
        $('.btn-agregar-horario').attr('disabled', true);
        ajaxPOST('/horarios/programados', data,
            (resp) => {
                $('.btn-agregar-horario').attr('disabled', false);
                if(resp.r == false)
                    Materialize.toast(resp.msj, 5000);
                ajaxHorarios();
            },
            (xhr, status, errorThrown) => {
                Materialize.toast('Ocurrió un error', 4000);
                $('.btn-agregar-horario').attr('disabled', false);
                console.error(errorThrown);
            });
    });

    // Abrir modal horarios programados.
    $('.md-horarioprogramado').on('click', function() {
        $('#hor-modalidad').val($(this).attr('modalidad'));
        $('#md-horarioprogramado-titulo').html($(this).attr('modalidad'));
        ajaxHorarios();
        $('#md-horarioprogramado').modal('open');
    });

    $(document).on('click', '.del-horario', function() {
        const id = this.id;
        Materialize.toast(`<span>¿Desea eliminar ésta horario?</span>
            <a id="${id}" class="del-horario-si btn-flat green-text" href="#!">Sí</a>
            <a class="btn-flat blue-text" href="#!" onclick="$('.toast').hide()">No<a>`, 5000);
    })

    // Eliminar horario.
    $(document).on('click', '.del-horario-si', function() {
        const id = this.id;
        $('.toast').hide();
        ajaxDELETE('/horarios/programados/'+id,
            ()=>{Materialize.toast('Horario eliminado', 4000); ajaxHorarios()},
            ()=>Materialize.toast('Error al eliminar horario', 4000)
        );
    });

    ajaxCantidadHorarios();
    // Verificar que hayan horarios si se selecciona una modalidad.
    function ajaxCantidadHorarios() {
        $.ajax({
            url: dominio+'horarios/',
            type: 'SEARCH',
            data: {
                token: userDOOMI.token,
                tipo_usuario: userDOOMI.tipo_usu,
                id_res: userDOOMI.restaurante[0].id
            },
            success: data => {
                $('#delivery').attr('horarios', data.general);
                $('#pickup').attr('horarios', data.pickup);
                $('#programado').attr('horarios', data.programado);
            },
            error: (xhr, status, errorThrown) => {},
        });
    };

    function ajaxHorarios(){
        const modo = $('#hor-modalidad').val();
        const id_res = userDOOMI.restaurante[0].id;
        const ruta = `/horarios/programados?modo=${modo}&id_res=${id_res}`;
        ajaxGET(ruta, horarios => {
            $('#'+modo).attr('horarios', horarios.length);
            const table = $('#data-table-horarios').DataTable({
                'paging': false,
                'info': false,
                'searching': false,
                // 'language':{ 'url': '../lib/JTables/Spanish.json' }
                'language': { emptyTable: 'No hay horarios' }
            });
            table.clear().draw();
            horarios.forEach((h, i) => {
                let inicio = new Date(h.fec_ini),
                    fin = new Date(h.fec_fin),
                    fecha = extraerFecha(inicio) + ' - ' + extraerFecha(fin),
                    hora = extraerHora(inicio) + ' - ' + extraerHora(fin),
                    btn_del = `<a id="${h.id}" class="del-horario waves-effect btn btn-primary btn-floating tooltipped" data-position="bottom" data-delay="50" data-tooltip="Eliminar horario"><i class="fa fa-trash"></i></a>`;
                    btn_del += ` <a id="${h.id}" fec="${moment(h.fec_ini).format('YYYY-MM-DD')}" hor_i="${moment(h.fec_ini).format('HH:mm:ss')}" hor_f="${moment(h.fec_fin).format('HH:mm:ss')}" class="rep-horario waves-effect btn btn-primary btn-floating tooltipped" data-position="bottom" data-delay="50" data-tooltip="Repetir horario"><i class="fa fa-copy"></i></a>`;
                table.row.add([i+1, fecha, hora, btn_del]).draw();
            });
        });
    }
    var fecha_pro = '';
    var hora_pro_i = '';
    var hora_pro_f = '';
    $(document).on('click', '.rep-horario', function() {
        fecha_pro = $(this).attr('fec');
        hora_pro_i = $(this).attr('hor_i');
        hora_pro_f = $(this).attr('hor_f');
        $("#md-horariorepetir").modal('open');
    });

    $("#form-horario-rep").on("submit", async function(e) {
        e.preventDefault();
        if($("#can_rep").val()=='' || $("#can_rep").val()==0){
            Materialize.toast('Debes agregar una cantidad de dias a repetir', 4000);
        }else{
            dias = $("#can_rep").val();
            var dias_acu = 0;
            while(dias_acu<dias){
                fecha_pro = moment(fecha_pro+' '+hora_pro_i).add(1, 'days').format('YYYY-MM-DD');
                const data = new FormData();
                data.append('modalidad', 'programado');
                data.append('id_res', userDOOMI.restaurante[0].id);
                data.append('fecha_inicio', fecha_pro+' '+hora_pro_i);
                data.append('fecha_fin', fecha_pro+' '+hora_pro_f);
                $('.bt_save_2').attr('disabled', true);
                await ajaxPOST('/horarios/programados', data,
                    (resp) => {
                        if(resp.id)
                            dias_acu++;
                    },(xhr, status, errorThrown) => {console.error(errorThrown);});
            };
            if(dias_acu>0)
                Materialize.toast('Horario repetido correctamente!', 4000);
            $('.bt_save_2').attr('disabled', false);
            ajaxHorarios();
            $("#can_rep").val(0)
            $("#md-horariorepetir").modal('close');
        }
    });

    function ajaxCategorias() {
        $("#categoria").html('<option value="" selected disabled>Cargando...</option>');

        ajaxGET('categorias/', function(data){
                html = '<option value="" selected>Seleccione</option>';
                for (var i = 0; i < data.length; i++) {
                    html += `<option value="${data[i].id}">${data[i].nombre}</option>`;
                }
                $("#categoria").html(html);

                if(id_categoria_s){
                    $("#categoria").val(id_categoria_s).trigger("change");
                    id_categoria_s = false;
                }
                $("#categoria").select2({placeholder: "Seleccione", allowClear: false});
            });
    }
    $("#categoria").on('change', function() {
        ajaxSubcategorias(this.value);
    });

    function ajaxSubcategorias(id){
        if (!id) {
            $('#categoria').html('<option value="" selected>Seleccione</option>');
            $('#subcategoria').html('<option value="" selected>Seleccione</option>');
            return;
        }
        $("#subcategoria").html('<option value="" selected disabled>Cargando...</option>');


        ajaxGET(`categorias/${id}/subcategorias`, function(data){
                html = '<option></option>';
                for (var i = 0; i < data.length; i++) {
                    html += `<option value="${data[i].id}">${data[i].nombre}</option>`;
                }
                $("#subcategoria").html(html);

                if(ids_subcategorias_s){
                    $("#subcategoria").val(ids_subcategorias_s).trigger("change");
                    ids_subcategorias_s = false;
                }
                $("#subcategoria").select2({allowClear: false});
            });
    }


    // Funciones de paises estado y ciudad.
    function ajaxPais() {
        $('#pais').html('<option value="" selected disabled>Cargando...</option>');
        $.ajax({
            url: dominio + 'paises/',
            type: 'GET',
            success: function(paises) {

                html = '<option value="" selected>Seleccione</option>';
                for (var i = 0; i < paises.length; i++) {
                    //html += '<option value="' + paises[i].id + '">' + paises[i].nombre + '</option>';
                    html += `<option value="${paises[i].id}"> ${paises[i].nombre} </option>`;
                }

                $("#pais").html(html);

                $("#pais").val(userDOOMI.restaurante[0].id_pais).trigger("change");
                $("#pais").select2({placeholder: "Seleccione",allowClear: false});
            }

        })
    }
    $("#pais").on('change', function() {
        ajaxEstados(this.value);
    });

    function ajaxEstados(id) {
        if (!id) {
            $('#estado').html('<option value="" selected>Seleccione</option>');
            $('#ciudad').html('<option value="" selected>Seleccione</option>');
            return;
        }
        $('#estado').html('<option value="" selected disabled>Cargando...</option>');
        $('#ciudad').html('<option value="" selected>Seleccione</option>');
        $.ajax({
            url: dominio + 'paises/estado/' + id,
            type: 'GET',
            success: function(data) {

                html = '<option value="" selected>Seleccione</option>';
                for (var i = 0; i < data.length; i++) {
                    html += '<option value="' + data[i].id + '">' + data[i].nombre + '</option>';
                }

                $("#estado").html(html);

                $("#estado").val(userDOOMI.restaurante[0].id_estado).trigger("change");
                $("#estado").select2({placeholder: "Seleccione",allowClear: false});
            }
        })
    }

    $("#estado").on('change', function() {
        ajaxCiudades(this.value);
    });

    function ajaxCiudades(id) {
        if (!id) {
            $('#ciudad').html('<option value="" selected>Seleccione</option>');
            return;
        }
        $('#ciudad').html('<option value="" selected disabled>Cargando...</option>');
        $.ajax({
            url: dominio + 'paises/ciudad/' + id,
            type: 'GET',
            success: function(data) {
                html = '<option value="" selected>Seleccione</option>';
                for (var i = 0; i < data.length; i++) {
                    html += '<option value="' + data[i].id + '">' + data[i].nombre + '</option>';
                }

                $("#ciudad").html(html);

                $("#ciudad").val(userDOOMI.restaurante[0].id_ciudad).trigger("change");
                $("#ciudad").select2({placeholder: "Seleccione",allowClear: false});
            }

        })
    }

    var lat='10.336902';
    var lon='-68.745944';
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position){
            lat = position.coords.latitude;
            lon = position.coords.longitude;
            //iniciarmapa();
        });

    }
    else {
        onError();
    }

    var map;
    function iniciarMapa(la,lo){
        if(typeof google == "undefined"){
            return;
        }
        latitude = lat;
        longitude = lon;
        if(la && lo){
            latitude = la;
            longitude = lo;
        }
        // Recibo lat y lon y losguardo en centro.
        var centro = new google.maps.LatLng(latitude,longitude);

        // Le damos el valor a los hidden.
        var propiedades = {
            center: centro,
            zoom: 15,
        };

        // Inicia mapa.
        map = new google.maps.Map(document.getElementById('map'), propiedades);

        // Agrega el marcador.
        var image = {url: "../../static/img/map-verde.png",size: new google.maps.Size(81, 81),origin: new google.maps.Point(0, 0),anchor: new google.maps.Point(40, 71),scaledSize: new google.maps.Size(75, 75)};

        var marker = new google.maps.Marker({position: centro,icon: image});
        if(la && lo)
            marker.setMap(map);

        // Posicion del marcador al hacer click.
        var geocoder = new google.maps.Geocoder();
        google.maps.event.addListener(map, 'click', function(event) {
            marker.setMap(null);
            marker = new google.maps.Marker({position: event.latLng,icon: image})
            marker.setMap(map);
            geocoder.geocode({ location: event.latLng }, (results, status) => {
                if (status === "OK") {
                    if(results[0])
                        $("#ubicacion").val(results[0].formatted_address);
                }
            });
            // Se guarda nueva posición.
            $("#lat").val(event.latLng.lat());
            $("#lon").val(event.latLng.lng());
        });
    }

    // Asegurarse de cargar el mapa.
    if (typeof google == "undefined"){
        // Create the script tag, set the appropriate attributes
        var script = document.createElement('script');
        script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBUPAHemQKCzVuIBCYTJ9AvsEtZDkfOOjY&libraries=places&region=mx&sensor=false&amp;language=es&callback=initMap';
        script.defer = true;

        // Attach your callback function to the `window` object
        window.initMap = function initMap(){iniciarMapa(latitude, longitude)};
        // Append the 'script' element to 'head'
        document.head.appendChild(script);
    }
}
