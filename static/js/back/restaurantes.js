function listar_restaurantes(){
    var id_pais_s = false;
    var id_estado_s = false;
    var id_ciudad_s = false;
    var ids_subcategorias_s = false;
    var id_categoria_s = false;
    $("#ciudad, #pais, #estado").select2({placeholder: "Seleccione", allowClear: false});

    //listar




    restaurantes = function restaurantes() {
        data = {
        }
        $.ajax({
            url: dominio + 'restaurantes/',
            headers: {
                'token':userDOOMI.token,
                'tipo_usuario': userDOOMI.tipo_usu
            },
            type: 'GET',
            data: data,
            success: function(data) {
                //console.log(data);
                actualizar_tabla_restaurantes(data)
            }
        })
    }

    restaurantes();
    ajaxPais();
    ajaxCategorias();
    function actualizar_tabla_restaurantes(data) {

        var table = $('.table').DataTable({
            "language": {
                "url": "../static/<lib></lib>/JTables/Spanish.json"
            }
        });
        count = 0
        table.clear().draw();

        for (var i = 0; i < data.length; i++) {
            datos = data[i];

            var options = `
                <a href="#!" id="${datos.id}" class="btn btn-primary btn-floating ver-info tooltipped" data-position="bottom" data-delay="50" data-tooltip="Ver información del restaurante"><i class="fa fa-eye"></i></a>

                <a href="#!" id="${datos.id}" class="btn btn-primary btn-floating edit-info tooltipped" data-position="bottom" data-delay="50" data-tooltip="Editar información del restaurante"><i class="fa fa-edit"></i></a>

                <a href="#!" id="${datos.id}" class="waves-effect btn btn-primary btn-floating tooltipped delete" data-position="bottom" data-delay="50" data-tooltip="Eliminar restaurante"><i class="fa fa-times"></i></a>

                <a href="index.html?op=listadomenu&id=${datos.id}" id="" class="waves-effect btn btn-primary btn-floating tooltipped" data-position="bottom" data-delay="50" data-tooltip="Menu"><i class="fa fa-utensil-spoon"></i></a>

            `;
            img_l = ` <img src='../../static/img/logo.jpg' class="img-table" onerror="this.src='../../static/img/logo.jpg'">`;
            if(datos.img)
                img_l = ` <img src="${datos.img}" class="img-table" onerror="this.src='../../static/img/logo.jpg'">`;
            check = '';
            check_laborando = '';
            if(datos.laborando) check_laborando = 'checked';
            if(datos.estatus) check = 'checked';

            chechsts_laborando = `<div class="switch">
                            <small class="clr_primary" style="font-size: 10px;">Cerrado</small>
                            <label>

                            <input type="checkbox" ${check_laborando} class="cambiar_sts_laborando sts_laborando${datos.id}" id="${datos.id}">
                                <span class="lever"></span> <small class="clr_primary" style="font-size: 10px;">Abierto</small>
                            </label>
                        </div>`;

            chechsts = `<div class="switch">
                            <small class="clr_primary" style="font-size: 10px;">Desactivo</small>
                            <label>

                            <input type="checkbox" ${check} class="cambiar_sts sts${datos.id}" id="${datos.id}">
                                <span class="lever"></span> <small class="clr_primary" style="font-size: 10px;">Activo</small>
                            </label>
                        </div>`;
            input_gan = '<input type="text" class="form-control mb-0 ganancia_domi" iden="'+datos.id+'" value="'+formato.precio1(datos.ganancia)+'">';
            var row = [count += 1, img_l, datos.nombre, chechsts_laborando, chechsts, input_gan, options];
            table.row.add(row).draw().node();
        }
        $('.tooltipped').tooltip();
        $('.ganancia_domi').mask('##0,0', {reverse: true});
    }

    var imgEdit = false;
    var imgEdit_v1 = false;
    var img64_1 = '';
    var img64_v1 = '';

    var output = document.getElementById('imagen');
    var el = document.getElementById('resizer-demo');
    var resize = new Croppie(el, {
        viewport: { width: 180, height: 180 },
        boundary: { width: 210, height: 210 },
        showZoomer: true,
        enableResize: false,
        enableOrientation: true,
        mouseWheelZoom: 'ctrl'
    });

    var output1 = document.getElementById('imagen1');
    var el1 = document.getElementById('resizer-demo-1');
    var resize1 = new Croppie(el1, {
        viewport: { width: 180, height: 180 },
        boundary: { width: 210, height: 210 },
        showZoomer: true,
        enableResize: false,
        enableOrientation: true,
        mouseWheelZoom: 'ctrl'
    });

    abrirEditor = function abrirEditor(e,op) {
        if (e.files[0]) {
            if(op==0){
                output.src = URL.createObjectURL(event.target.files[0]);
                resize.bind({
                    url: output.src,
                });
                imgEdit = true;
            }else if(op==1){
                output1.src = URL.createObjectURL(event.target.files[0]);
                resize1.bind({
                    url: output1.src,
                });
                imgEdit_v1 = true;
            }
        }
        return;

    }

    $(document).on('click', ".abrirmodal", function(){
        // Validar que hayan categorias.
        if ($('#categoria > option').length == 1){
            let msj = 'No hay categorías';
            if ($('#categoria').attr('existen')) {msj = 'No hay subcategorías'}
            document.getElementById('nuevoResBtn').setAttribute('href', '#');
            document.getElementById('nuevoResBtn').style.pointerEvents = 'none';
            return Materialize.toast(msj, 4000, '', function(){
                    document.getElementById('nuevoResBtn').setAttribute('href', '#md-nuevoRestaurant');
                    document.getElementById('nuevoResBtn').style.pointerEvents = 'auto';
            });
        };

        $("#form_registrar_resta")[0].reset();
        $("#iden").val("");
        $(".bt_save").html("Guardar");
        $("#tit-mod").html("Nuevo Doomialiado");
        $(".all_img").attr("src", "../../static/img/logo.jpg");
        iniciarmapa();
        $("#list-usuarios").html("");
        $("#ciudad, #pais, #estado").select2({placeholder: "Seleccione",allowClear: false});
        $("#categoria, #subcategoria").select2({placeholder:"Seleccione categoría", allowClear: false});
        // $("#categoria").select2({placeholder:"Seleccione categoría", allowClear: false});
        resize.bind("../../static/img/logo.jpg");
        resize1.bind("../../static/img/logo.jpg");
        img64_1 = '';imgEdit = false;
        img64_v1 = '';imgEdit_v1 = false;
        $("#sts_viajando").prop("checked", true).trigger('change');
        $("#md-nuevoRestaurant").modal('open');
    });

    $("#form_registrar_resta").on('submit', function(e) {
        e.preventDefault();

        /*validaciones*/
        var errr = false;
        var msj = false;
        var incompletos = 0;
        var incompletos2 = 0;
        var iguales = 0;
        var iguales_c = 0;
        $('.dataced').each(function(){
            idn = $(this).attr('iden');
            valor = $(this).val();
            if(valor.trim()=='' || valor.length < 7 || valor.length > 8)
                incompletos ++;
            if(idn){
                $('.dataced').each(function(){
                    idn2 = $(this).attr('iden');
                    if(idn!=idn2){
                        if($(this).val() == valor)
                            iguales++;
                    }
                });
            }
        });
        $('.datacor').each(function(){
            idn = $(this).attr('iden');
            valor = $(this).val();
            if(valor.trim()=='' || valor.length < 5)
                incompletos2 ++;
            $('.datacor').each(function(){
                idn2 = $(this).attr('iden');
                if(idn!=idn2){
                    if($(this).val() == valor)
                        iguales_c++;
                }
            });
        });
        $('.datausu2').each(function(){
            valor = $(this).val();
            if(valor.length > 0 && valor.length < 5)
                incompletos ++;
        });

        if ( $("#nombre").val().trim() == "" ){ errr = true; msj = "Ingresa el nombre";}

        else if ( $("#ubicacion").val().trim() == "" ){ errr = true; msj = "Ingresa la ubicación";}

        else if ($("#rif_letra").val() == "" || $("#rif_numero").val().trim() == "" || $("#rif_chequeo").val() == "")
        {
            errr = true;
            msj = "Ingrese el RIF";
        }

        else if(!verificarRIF(`${$("#rif_letra").val()}-${$("#rif_numero").val().trim()}-${$("#rif_chequeo").val()}`))
        {
            errr = true;
            msj = "El RIF no es válido";
        }

        else if ( $("#lat").val() == "" || $("#lon").val() == ""){ errr = true; msj = "Selecciona la ubicación en el mapa";}

        else if ( $("#pais").val() == "" ){ errr = true; msj = "Selecciona el pais";}

        else if ( $("#estado").val() == "" ){ errr = true; msj = "Selecciona el estado";}

        else if ( $("#ciudad").val() == "" ){ errr = true; msj = "Selecciona la ciudad";}

        else if ( $("#categoria").val() == "" ){ errr = true; msj = "Selecciona la categoría";}

        else if ( $("#subcategoria").val() == "" ){ errr = true; msj = "Selecciona la subcategoría";}

        else if ( parseFloat(incompletos) > 0 ){ errr = true; msj = "Debes completar todos los campos de cédula (entre 7 y 8 caracteres)";}

        else if ( parseFloat(incompletos2) > 0 ){ errr = true; msj = "Debes completar todos los datos de los usuarios";}

        else if ( parseFloat(iguales) > 0 ){ errr = true; msj = "No debes repetir los usuarios";}

        else if ( parseFloat(iguales_c) > 0 ){ errr = true; msj = "No debes repetir los correos de usuarios";}

        /*fin validaciones*/
        if (errr) {
            Materialize.toast(msj, 4000); return false;
        }
        //data a enviar
        var data = new FormData(this);

        resize.result('base64').then(function(base64) {
            if(imgEdit)
                img64_1 = base64;
            resize1.result('base64').then(function(base64) {
                if(imgEdit_v1)
                    img64_v1 = base64;
                data.append("imgweb", "1");
                data.append("img", img64_1);
                data.append("img1", img64_v1);
                v=1;
                if($("#sts_viajando:checked").length>0)
                  v = 0;
                data.append("vaca", v);

                if($("#iden").val() != ''){
                    url = "restaurantes/edit";
                    tipo = "PUT";
                }else{
                    url = "restaurantes/add";
                    tipo = "POST";
                }
                $(".bt_save").prop("disabled", true);
                $(".fileprogress").removeClass("hide");
                $(".fileprogress").find("div")[0].style.width = "0%";
                $.ajax({
                    url: dominio + url,
                    headers: {
                        'token':userDOOMI.token,
                        'tipo_usuario': userDOOMI.tipo_usu
                    },
                    type: tipo,
                    dataType: "json",
                    cache: false,
                    data: data,
                    contentType: false,
                    processData: false,
                    xhr: function(){
                        var xhr = new window.XMLHttpRequest();
                        xhr.upload.addEventListener("progress", function(e){
                            if (e.lengthComputable) {
                              var percentComplete = parseInt( (e.loaded / e.total * 100), 10);
                              $(".fileprogress").find("div")[0].style.width = `${percentComplete}%`;
                              if(percentComplete>=100){
                                $(".fileprogress").addClass("hide");
                                $(".fileprogress").find("div")[0].style.width = "0%";
                              }
                            }
                        }, false);
                        xhr.addEventListener("progress", function(e){
                            if (e.lengthComputable) {
                                var percentComplete = parseInt( (e.loaded / e.total * 100), 10);
                                $(".fileprogress").find("div")[0].style.width = `${percentComplete}%`;
                                if(percentComplete>=100){
                                    $(".fileprogress").addClass("hide");
                                    $(".fileprogress").find("div")[0].style.width = "0%";
                                }
                            }
                        }, false);
                        return xhr;
                    },
                    success: function(res) {
                        Materialize.toast(res.msj, 4000)
                        if (res.r == true) {
                            $("#form_registrar_resta")[0].reset();
                            restaurantes();
                            $("#md-nuevoRestaurant").modal('close');
                        }
                        //botones del form
                        $(".bt_save").prop("disabled", false)
                    },
                    error: function(xhr, status, errorThrown) {
                        console.log(errorThrown);
                        $(".fileprogress").addClass("hide");
                        $(".fileprogress").find("div")[0].style.width = "0%";
                        Materialize.toast(errorThrown.msj, 4000)

                        //botones del form
                        $(".bt_save").prop("disabled", false)
                    }
                });
            });
        });
    });
    $(document).on('click', '.ver-info', function() {
        var idv = this.id;
        buscar_data(idv,'ver')
    });
    $(document).on('click', '.edit-info', function() {
        var idv = this.id;
        buscar_data(idv,'editar')
    });
    function buscar_data(idb,modal){
        data = {
        }
        $.ajax({
            url: dominio + 'restaurantes/'+idb,
            headers: {
                'token':userDOOMI.token,
                'tipo_usuario': userDOOMI.tipo_usu
            },
            type: 'GET',
            data: data,
            success: function(data) {
                //console.log(data);
                imprimir_data_credencial(data,modal)
            }, error: function(xhr, status, errorThrown) {
                console.log(errorThrown);
            }

        })
    }
    function imprimir_data_credencial(data,modal){
        if(modal=='ver'){
            $(".cat-view").html(data.categoria.nombre);
            // $(".sub-view").html(data.subcategoria.nombre);
            let subcategorias_texto = data.subcategorias.map(s => s.nombre);
            $(".sub-view").html(subcategorias_texto.join(', '));
            $(".nom-view").html(data.nombre);
            $(".des-view").html(data.descripcion);
            $(".ubi-view").html(data.ubicacion);
            $(".rif-view").html(`${data.rif_letra}-${data.rif_numero}-${data.rif_chequeo}`);
            $(".telefono").html(data.telefono);
            $(".facebook").html(data.facebook);
            $(".instagram").html(data.instagram);
            $(".lab-view").html(data.laborando ? 'Abierto' : 'Cerrado');
            $(".sts-view").html(data.estatus ? 'Activo' : 'Desactivo');

            $(".usu-view,.pai-view,.est-view,.ciu-view").html('');
            if(data.pais[0])
                $(".pai-view").html(data.pais[0].nombre);
            if(data.estado[0])
                $(".est-view").html(data.estado[0].nombre);
            if(data.ciudad[0])
                $(".ciu-view").html(data.ciudad[0].nombre);
            if(data.usuarios[0]){
                usus = '';
                for (var i = 0; i < data.usuarios.length; i++) {
                    if(usus == '')
                        usus = data.usuarios[i].cedula+" ("+data.usuarios[i].correo+")";
                    else
                        usus += "<br> "+data.usuarios[i].cedula+" ("+data.usuarios[i].correo+")";
                };
                $(".usu-view").html("<br>"+usus);
            }
            $(".img-view").attr("src", "../../static/img/logo.jpg");
            if(data.img)
                $(".img-view").attr("src",data.img);
            if(data.img_portada)
                $(".img-view-por").attr("src",data.img_portada);
            $('#map_ver').hide();
            if(data.lat !='' && data.lon !=''){
                vermapa(data.lat,data.lon);
                $('#map_ver').show();
            }



            $("#md-verRestaurant").modal("open");
        }else if(modal=='editar'){
            $("#iden").val(data.id);
            if(data.lat !='' && data.lon !=''){
                $("#lat").val(data.lat);
                $("#lon").val(data.lon);
                iniciarmapa(data.lat,data.lon);
            }else{
                $("#lat").val('');
                $("#lon").val('');
                iniciarmapa();
            }

            id_categoria_s = data.id_categoria;
            ids_subcategorias_s = data.id_subcategorias;
            ajaxCategorias();

            $("#nombre").val(data.nombre);
            $("#descripcion").val(data.descripcion);
            $("#ubicacion").val(data.ubicacion);

            $("#rif_letra").val(data.rif_letra).trigger("change");
            $("#rif_numero").val(data.rif_numero);
            $("#rif_chequeo").val(data.rif_chequeo);

            $("#telefono").val(data.telefono);
            $("#facebook").val(data.facebook);
            $("#instagram").val(data.instagram);

            $("#sts_viajando").prop("checked", !data.vacaciones).trigger('change');

            id_pais_s = data.id_pais;
            id_estado_s = data.id_estado;
            id_ciudad_s = data.id_ciudad;

            ajaxPais();
            $("#list-usuarios").html('');
            if(data.usuarios[0]){
                usus = '';
                count=1;
                var html = '';
                for (var i = 0; i < data.usuarios.length; i++) {
                    html += `<tr id="tr-e${data.usuarios[i].id}">
                            <td class="p-1">${count}</td>
                            <td class="p-1"><div class="input-field col s12 p-0"><input placeholder="" value="${data.usuarios[i].nombre_apellido}" name="nombres_apellidos[]" type="text" class="form-control mb-1 datausu" iden='n${trusu}'></div></td>
                            <td class="p-1"><div class="input-field col s12 p-0"><input placeholder="" value="${data.usuarios[i].cedula}" name="cedulas[]" type="text" class="form-control mb-1 dataced allusus" iden='n${trusu}'></div></td>
                            <td class="p-1"><div class="input-field col s12 p-0"><input placeholder="" value="${data.usuarios[i].correo}" name="correos[]" type="email" class="form-control mb-1 datacor" iden='n${trusu}'></div></td>
                            <td class="p-1"><div class="input-field col s12 p-0"><input type="hidden" name="idusu[]" value="${data.usuarios[i].id}"><input placeholder="Solo si desea actualizar" name="pass[]" type="password" class="form-control mb-1 datausu2" ></div></td>
                            <td class="text-center">
                                <div class="input-field m-1 p-0 col s12">
                                    <a class="waves-effect btn btn-primary btn-floating delete-usu-2" id="${data.usuarios[i].id}"><i class="fa fa-trash"></i></a>
                                </div>
                            </td>
                        </tr>
                    `;
                    count++;
                };
                $("#list-usuarios").append(html);
            }
            $("#tit-mod").html("Editar doomialiado");
            $(".bt_save").html('Guardar cambios');
            $(".all_img").attr("src", "../../static/img/logo.jpg");
            if(data.img)
                output.src = data.img;
            if(data.img_portada)
                output1.src = data.img_portada;
            resize.bind({url: output.src,zoom: 1});
            resize1.bind({url: output1.src,zoom: 1});
            setTimeout(function(){ $(".cr-image").css("transform","translate3d(15px, 15px, 0px) scale(1)"); }, 200);
            img64_1 = '';imgEdit = false;
            img64_v1 = '';imgEdit_v1 = false;
            $("#md-nuevoRestaurant").modal("open");
        }
    }
    //eliminar restaurante
    $(document).on('click', '.delete', function() {
        var idv = this.id;
        var toastContent = '<span>¿Desea eliminar éste registro?</span><br><button class="btn-flat toast-action conf_si" id="'+idv+'">Si</button><button class="btn-flat toast-action" onclick=" $(\'.toast\').hide(); ">No</button>';

        Materialize.toast(toastContent, 4000);
    });

    $(document).on('click', '.conf_si', function() {
        id = this.id
        var data = {};

        $('.toast').hide();
        $.ajax({
            url: dominio + 'restaurantes/delete/'+id,
            headers: {
                'token':userDOOMI.token,
                'tipo_usuario': userDOOMI.tipo_usu
            },
            type: 'DELETE',
            data: data,
            success: function(data) {
                if (data.msj) {
                    Materialize.toast(data.msj, 10000);
                    restaurantes();
                } else {
                    Materialize.toast(data.msj, 10000);
                }
            }
        })

    });
    //fin de eliminar restaurante
    //eliminar usuario del restaurante
    $(document).on('click', '.delete-usu-2', function() {
        var idv = this.id;
        var toastContent = '<span>¿Desea eliminar éste usuario?</span><br><button class="btn-flat toast-action conf_si_2" id="'+idv+'">Si</button><button class="btn-flat toast-action" onclick=" $(\'.toast\').hide(); ">No</button>';

        Materialize.toast(toastContent, 4000);
    });


    // Limpiar campo numerico del RIF.
    $("#rif_numero").on("input propertychange change", function(){
        this.value = extraerDigitos(this.value);
    });
    $("#rif_chequeo").on("input propertychange change", function(){
        this.value = extraerDigitos(this.value);
    });

    // Limpiar campo numerico de cedula.
    $("#list-usuarios").on("input propertychange change", ".dataced", async function(){
        this.value = extraerDigitos(this.value);
    });

    // Capitalizar nombres.
    $("#nombre, #descripcion, #ubicacion").on("input propertychange change", async function(){
        this.value = capitalizar(this.value);
    });


    $(document).on('click', '.conf_si_2', function() {
        id = this.id
        var data = {};

        $('.toast').hide();
        $.ajax({
            url: dominio + 'restaurantes/delete_usu/'+id,
            headers: {
                'token':userDOOMI.token,
                'tipo_usuario': userDOOMI.tipo_usu
            },
            type: 'DELETE',
            data: data,
            success: function(data) {
                if (data.msj) {
                    Materialize.toast(data.msj, 10000);
                    $("#tr-e"+id).remove();
                } else {
                    Materialize.toast(data.msj, 10000);
                }
            }
        })

    });
    trusu = 0;
    $(document).on('click', '.add-usu', function() {
        var html = `<tr id="tr-${trusu}">
                <td class="p-1"><span class="caja-new">Nuevo</span></td>
                <td class="p-1"><div class="input-field s12"><input placeholder="" name="nombres_apellidos[]" type="text" class="form-control mb-0 datausu" iden='n${trusu}'></div></td>
                <td class="p-1"><div class="input-field s12"><input placeholder="" name="cedulas[]" type="text" class="form-control mb-0 dataced allusus" iden='n${trusu}'></div></td>
                <td class="p-1"><div class="input-field s12"><input placeholder="" name="correos[]" type="email" class="form-control mb-0 datacor" iden='n${trusu}'></div></td>
                <td class="p-1"><div class="input-field s12"><input type="hidden" name="idusu[]" value="0"><input placeholder="" name="pass[]" type="password" class="form-control mb-0 datausu" ></div></td>
                <td class="text-center">
                    <div class="input-field m-1 p-0 col s12">
                        <a class="waves-effect btn btn-primary btn-floating delete-usu" id="-${trusu}"><i class="fa fa-trash"></i></a>
                    </div>
                </td>
            </tr>
        `;
        trusu++;
        $("#list-usuarios").append(html);
    });

    $(document).on('click', '.delete-usu', function() {
        var idv = this.id;
        $("#tr"+idv).remove();
    });
    function ajaxCategorias() {
        // $("#categoria").html('<option value="" selected disabled>Cargando...</option>');
        // $("#categoria").select2({placeholder: "Cargando...", allowClear: false});

        document.getElementById('nuevoResBtn').setAttribute('href', '#');
        document.getElementById('nuevoResBtn').style.pointerEvents = 'none';
        $("#nuevoResBtn span").html("Cargando...");

        $.ajax({
            url: dominio + "categorias/?con_subcategorias=1",
            type: "GET",
            headers: {
                token: userDOOMI.token,
                tipo_usuario: userDOOMI.tipo_usu
            },
            success: function(data){
                html = '<option></option>';
                for (var i = 0; i < data.length; i++) {
                    html += `<option value="${data[i].id}">${data[i].nombre}</option>`;
                }
                $("#categoria").html(html);

                if(id_categoria_s){
                    $("#categoria").val(id_categoria_s).trigger("change");
                    id_categoria_s = false;
                }
                $("#categoria").select2({placeholder: "Seleccione categoría", allowClear: false});
            }
        });
        ajaxGET('categorias/', function(data){
            if(data.length > 0) $("#categoria").attr('existen', 1);
            document.getElementById('nuevoResBtn').setAttribute('href', '#md-nuevoRestaurant');
            document.getElementById('nuevoResBtn').style.pointerEvents = 'auto';
            $("#nuevoResBtn span").html("Nuevo");
        });
    }
    $("#categoria").on('change', function() {
        ajaxSubcategorias(this.value);
    });

    function ajaxSubcategorias(id){
        if (!id) {
            // $('#categoria').html('<option></option>');
            $('#subcategoria').html('<option></option>');
            $("#subcategoria").select2({placeholder: "Seleccione categoría", allowClear: false});
            return;
        }
        $("#subcategoria").html('<option></option>');
        $.ajax({
            url: dominio + `categorias/${id}/subcategorias`, //"subcategorias/en_categoria/" + id,
            type: "GET",
            headers: {
                token: userDOOMI.token,
                tipo_usuario: userDOOMI.tipo_usu
            },
            success: function(data){
                html = '<option></option>';
                for (var i = 0; i < data.length; i++) {
                    html += `<option value="${data[i].id}">${data[i].nombre}</option>`;
                }
                $("#subcategoria").html(html);

                if(ids_subcategorias_s){
                    $("#subcategoria").val(ids_subcategorias_s).trigger("change");
                    ids_subcategorias_s = false;
                }
                $("#subcategoria").attr('placeholder', 'Seleccione subcategoría').select2({allowClear: false});
            }
        });
    }

    //funciones de paises estado y ciudad
    function ajaxPais() {
        $('#pais').html('<option value="" selected disabled>Cargando...</option>');
        $.ajax({
            url: dominio + 'paises/',
            type: 'GET',
            success: function(data) {

                html = '<option value="" selected>Seleccione</option>';
                for (var i = 0; i < data.length; i++) {
                    html += '<option value="' + data[i].id + '">' + data[i].nombre + '</option>';
                }

                $("#pais").html(html);

                if (id_pais_s) {
                    $("#pais").val(id_pais_s).trigger("change");
                }
                $("#pais").select2({placeholder: "Seleccione",allowClear: false});

            }

        })
    }
    $("#pais").on('change', function() {
        ajaxEstados(this.value);
    });

    function ajaxEstados(id) {
        if (!id) {
            $('#estado').html('<option value="" selected>Seleccione</option>');
            $('#ciudad').html('<option value="" selected>Seleccione</option>');
            return;
        }
        $('#estado').html('<option value="" selected disabled>Cargando...</option>');
        $('#ciudad').html('<option value="" selected>Seleccione</option>');
        $.ajax({
            url: dominio + 'paises/estado/' + id,
            type: 'GET',
            success: function(data) {

                html = '<option value="" selected>Seleccione</option>';
                for (var i = 0; i < data.length; i++) {
                    html += '<option value="' + data[i].id + '">' + data[i].nombre + '</option>';
                }

                $("#estado").html(html);

                if (id_estado_s) {
                    $("#estado").val(id_estado_s).trigger("change");
                }
                $("#estado").select2({placeholder: "Seleccione",allowClear: false});
            }
        })
    }

    $("#estado").on('change', function() {
        ajaxCiudades(this.value);
    });

    function ajaxCiudades(id) {
        if (!id) {
            $('#ciudad').html('<option value="" selected>Seleccione</option>');
            return;
        }
        $('#ciudad').html('<option value="" selected disabled>Cargando...</option>');
        $.ajax({
            url: dominio + 'paises/ciudad/' + id,
            type: 'GET',
            success: function(data) {
                html = '<option value="" selected>Seleccione</option>';
                for (var i = 0; i < data.length; i++) {
                    html += '<option value="' + data[i].id + '">' + data[i].nombre + '</option>';
                }

                $("#ciudad").html(html);

                if (id_ciudad_s) {
                    $("#ciudad").val(id_ciudad_s).trigger("change");
                }
                $("#ciudad").select2({placeholder: "Seleccione",allowClear: false});
            }

        })
    }

    $(document).on('click', '.sorting_1', function() {
        $('.tooltipped').tooltip();
        $('.ganancia_domi').mask('##0,0', {reverse: true});
    })

    $(document).on('click', ".cambiar_sts", function(){
        id=$(this).attr("id");
        v=0;
        if($(".sts"+id+":checked").length>0)
          v = 1;
        var data = new FormData();
        data.append("iden", id);
        data.append("sts", v);
        $.ajax({
            method: "PUT",
            url: dominio + 'restaurantes/edit_sts',
            headers: {
                'token':userDOOMI.token,
                'tipo_usuario': userDOOMI.tipo_usu
            },
            data: data,
            contentType: false,
            processData: false,
            success: function(data) {
                Materialize.toast( data.msj , 5000);
            },
            error: function(xhr, status, errorThrown) {
                console.log("Errr : ");
            }
        });
    });

    $(document).on('click', ".cambiar_sts_laborando", function(){
        id=$(this).attr("id");
        v=0;
        if($(".sts_laborando"+id+":checked").length>0)
          v = 1;
        var data = new FormData();
        data.append("iden", id);
        data.append("sts", v);
        $.ajax({
            method: "PUT",
            url: dominio + 'restaurantes/edit_sts_laborando',
            headers: {
                'token':userDOOMI.token,
                'tipo_usuario': userDOOMI.tipo_usu
            },
            data: data,
            contentType: false,
            processData: false,
            success: function(data) {
                Materialize.toast( data.msj , 5000);
            },
            error: function(xhr, status, errorThrown) {
                console.log("Errr : ");
            }
        });
    });

    var lat='10.336902';
    var lon='-68.745944';
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position){
            lat = position.coords.latitude;
            lon = position.coords.longitude;
            //iniciarmapa();
        });

    }else {
        onError();
    }

    var map;
    iniciarmapa = function iniciarmapa(la,lo){
        if(typeof google == "undefined"){
            return;
        }
        latitude = lat;
        longitude = lon;
        if(la && lo){
            latitude = la;
            longitude = lo;
        }
        //recibo lat y lon y losguardo en centro
        var centro = new google.maps.LatLng(latitude,longitude);
        //le damos el valor a los hidden
        var propiedades={
            center: centro,
            zoom: 15,
        };
        //inicia mapa
        map = new google.maps.Map(document.getElementById('map'),propiedades);

        //Agrega el marcador
        var image = {url: "../../static/img/map-verde.png",size: new google.maps.Size(81, 81),origin: new google.maps.Point(0, 0),anchor: new google.maps.Point(40, 71),scaledSize: new google.maps.Size(75, 75)};

        var marker = new google.maps.Marker({position: centro,icon: image});
        if(la && lo)
            marker.setMap(map);


        //posicion del marcador al hacer click
        var geocoder = new google.maps.Geocoder();
        google.maps.event.addListener(map, 'click', function(event) {
            marker.setMap(null);

            marker = new google.maps.Marker({position: event.latLng,icon: image})
            marker.setMap(map);
            geocoder.geocode({ location: event.latLng }, (results, status) => {
                if (status === "OK") {
                    if(results[0])
                        $("#ubicacion").val(results[0].formatted_address);
                }
            });
            //se guarda nueva posición
            $("#lat").val(event.latLng.lat());
            $("#lon").val(event.latLng.lng());

        });
    }

    vermapa = function vermapa(la,lo){
        if(typeof google == "undefined"){
            return;
        }
        //recibo lat y lon y losguardo en centro
        var centro = new google.maps.LatLng(la,lo);
        //le damos el valor a los hidden
        var propiedades={
            center: centro,
            zoom: 15,
        };
        //inicia mapa
        map = new google.maps.Map(document.getElementById('map_ver'),propiedades);

        //Agrega el marcador
        var image = {url: "../../static/img/map-verde.png",size: new google.maps.Size(81, 81),origin: new google.maps.Point(0, 0),anchor: new google.maps.Point(40, 71),scaledSize: new google.maps.Size(75, 75)};
        var marker = new google.maps.Marker({position: centro,icon: image});
        marker.setMap(map);
    }

    $(document).on('keypress', ".ganancia_domi", function(e){
        var code = (e.keyCode ? e.keyCode : e.which);
        if(code==13){

            iden = $(this).attr("iden");
            val = $(this).val();
            var data = new FormData();
            data.append("iden", iden);
            data.append("val", Quitarcoma(val));
            $.ajax({
                method: "PUT",
                url: dominio + 'restaurantes/edit_ganancia',
                headers: {
                    'token':userDOOMI.token,
                    'tipo_usuario': userDOOMI.tipo_usu
                },
                data: data,
                contentType: false,
                processData: false,
                success: function(data) {
                    Materialize.toast( data.msj , 5000);
                },
                error: function(xhr, status, errorThrown) {
                    console.log("Errr : ");
                }
            });
        }
    });
}