function listar_notificaciones(){
    var limit2 = 5;
    var CookieArray = [];
    
    obtener_mis_notis = function obtener_mis_notis() {
        var html = '';
        tipo_usu = userDOOMI.tipo_usu;
        if(tipo_usu=='administrador'){
            tipo_usu = 'administradores';
            id_usu = userDOOMI.id;
        }else{
            tipo_usu = 'restaurantes';
            id_usu = userDOOMI.id_restaurante;
        }
        $.ajax({
            url: dominio + 'notificaciones/',
            type: 'GET',
            data: {
                tipo_usuario: tipo_usu,
                id: id_usu,
                limit: limit2
            },
            success: function(data) {
                data = data.noti;
                $(".view_more").html('<a href="#!" onclick="$(this).html(\'Cargando...\'); obtener_mis_notis(); "><i class="fa fa-plus"></i> Cargar más</a>');

                if (data.length > 0 && data.length < 5 || data.length == 0)
                    $(".view_more").addClass("hide");
                if(data.length == 0)
                   $(".vertodasnotificacion").addClass("hide");

                for (i = data.length - 1; i >= 0; i--) {
                    CargarNoti(data[i]);
                }
                if(data.length<limit2)
                    $(".view_more").addClass("hide");

                if (limit2 == 5)
                    $(".misNotificaciones").scrollTop($('.misNotificaciones')[0].scrollHeight);
                
                if (data.length == 0)
                    $(".misNotificaciones").html('<div class="boxNotificaciones pb-2"> <p class="text-center m-0"><a href="#" class="clr_black p-1">Sin notificaciones</a></p></div>');

                limit2 = (limit2 * 2);
            },
            error: function(err) {
                $(".view_more").addClass("hide");
                $(".misNotificaciones").html('<li class="boxNotificaciones"><a href="#!" class="clr_black p-1 text-center"> Sin notificaciones pendientes</a></li>');
            }
        })
    }
    if ( getop("op")=='notificaciones' )
        obtener_mis_notis();

    buscarID = function buscarID(id) {

        for (var i = 0; i < CookieArray.length; i++) {
            if (CookieArray[i].id == id)
                return true;
        }
        return false;
    }
    CargarNoti = function CargarNoti(datos) {
        //moment.locale("es");
        if (buscarID(datos.id)) {
            return false;
        }
        CookieArray.push(datos);
        var html = '';
        html += `<div class="boxNotificaciones noti_${datos.id}">
            <div>
                <a href="#!" noti="${datos.id}" tipo="${datos.tipo_noti}" url_noti="${datos.redireccionar_noti}" id_usable="${datos.id_usable}" class="clr_black row p-1 vernotificacion">
                    <div class="col s1 pt-4 text-right"><img src="${datos.img_c || datos.img_r || datos.img_re || '../../static/img/user.png'}" onerror="this.src ='../../static/img/user.png'" id="img-menu" style="border-radius: 50%;" width="40px" height="40px"></div>
                    <div class="col s11"><p class="m-0"><b>${datos.nombre_c || datos.nombre_r || datos.nombre_re || 'Administración'}</b> <br> ${datos.contenido}</p></div>
                    <div class="col s12"><small class="timeago text-right p-0 pr-2 right">${moment(datos.fecha).toNow()}</small></div>
                </a>
            </div>
        </div>`;
        

        $(".misNotificaciones").prepend(html);
    }

    $(document).on('click', '.vernotificacion', function() {
        var tipo = $(this).attr("tipo");
        var usable = $(this).attr("id_usable");
        var url = $(this).attr("url_noti");
        var id = $(this).attr("noti");
        

        $.ajax({
            url: dominio + 'notificaciones/vista/' + id,
            type: 'post',
            success: function(data) {
                
                if(tipo==2){
                    location.href = url+usable;
                }else if(tipo==1){
                    location.href = url;
                }else if(tipo==0){
                    $(".noti_"+id).remove();
                    ultimas_notis();
                }
            },
            error: function(err) {
                $(i).removeClass('');
            }
        })

    });

    $(document).on('click', '.vertodasnotificacion', function() {
        tipo_usu = userDOOMI.tipo_usu;
        if(tipo_usu=='administrador'){
            tipo_usu = 'administradores';
            id_usu = userDOOMI.id;
        }else{
            tipo_usu = 'restaurantes';
            id_usu = userDOOMI.id_restaurante;
        }
        $.ajax({
            url: dominio + 'notificaciones/vistaall/',
            type: 'GET',
            data: {
                tipo_usuario: tipo_usu,
                id: id_usu
            },
            success: function(data) {
                if(data.r){
                    Materialize.toast("Todas las notificaciones se han marcado como vistas.", 4000);
                    $(".boxNotificaciones").remove();
                    ultimas_notis();
                }
            },
            error: function(err) {
                $(i).removeClass('');
            }
        })

    });
    

  


   




   
}; 