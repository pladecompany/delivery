function recuperarjs(){
     
    //Enviar datos por método POST
    $('#formRecuperar').on('submit', function(e) {

        e.preventDefault();

        if($("#correo").val().trim() == ""){
            Materialize.toast("Ingrese un correo válido.", 5000);
            return;
        }

        //data a enviar
        var data = new FormData(this);
       
        $(".Btrecuperar").attr("disabled",true);
        $.ajax({
            url: dominio + "login/recuperar",
            type: "POST",
            dataType: "json",
            cache: false,
            data: data,
            contentType: false,
            processData: false,
            success: function(res) {
                Materialize.toast(res.msj, 4000);
                $(".Btrecuperar").attr("disabled",false);
                if(res.r){
                    $('#formRecuperar')[0].reset();
                } 
            },
            error: function(xhr, status, errorThrown) {
                console.log(errorThrown);
                Materialize.toast(errorThrown.msj, 4000)
                $(".Btrecuperar").attr("disabled",false);
            }
        });

    });
    
}