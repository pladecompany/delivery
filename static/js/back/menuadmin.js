function listar_menus_admin() {

    if(!getop("id"))
        window.location.href="?op=doomialiados";
    else
        $("#a-list").attr("href","?op=listadomenu&id="+getop("id"));
    const route = "menu/";
    var tasa_d = 0;
    $("#cat_men, #est_men")
        .select2({placeholder: "Seleccione", allowClear: false});



    menus = function menus() {
        data = {
            id_res: getop("id")
        };
        $.ajax({
            url: dominio + route,
            headers: {
                'token': userDOOMI.token,
                'tipo_usuario': userDOOMI.tipo_usu
            },
            type: 'GET',
            data: data,
            success: function(data){
                actualizar_tabla_menus(data)
            }
        })
    }

    function buscar_doomi(){
        data = {}
        $.ajax({
            url: dominio + 'restaurantes/'+getop("id"),
            headers: {
                'token':userDOOMI.token,
                'tipo_usuario': userDOOMI.tipo_usu
            },
            type: 'GET',
            data: data,
            success: function(data) {
                //console.log(data);
                if(data.pre_tasa){
                    tasa_d = data.pre_tasa;
                    // Listar.
                    menus();
                }

            }, error: function(xhr, status, errorThrown) {
                console.log(errorThrown);
            }

        })
    }
    buscar_doomi();

    function actualizar_tabla_menus(data) {
        var table = $('.table').DataTable({
            "language": {
                "url": "../static/<lib></lib>/JTables/Spanish.json"
            }
        });
        count = 0;
        table.clear().draw();
        for (var i = 0; i < data.length; i++) {
            menu = data[i];

            let precios = `$${formato.precio(menu.precio_usd)} <br> Bs. ${formato.precio(menu.precio_usd * tasa_d)}`;
            //let estatusSwitch = `<div class="switch"><label><input type="checkbox" id="${menu.id}" ${menu.estatus?'checked="true"':''} class="btn btn-primary btn-floating tooltipped estatus-switch"><span class="lever"></span></label></div>`;


            estatusSwitch = `<div class="switch">
                            <small class="clr_primary" style="font-size: 10px;">No disponible</small>
                            <label>

                            <input type="checkbox"  disabled ${menu.estatus?'checked="true"':''} class="btn btn-primary btn-floating tooltipped">
                                <span class="lever"></span> <small class="clr_primary" style="font-size: 10px;">Disponible</small>
                            </label>
                        </div>`;


            var options = `
                <a href="#!" id="${menu.id}" class="btn btn-primary btn-floating edit-info tooltipped" data-position="bottom" data-delay="50" data-tooltip="Guardar"><i class="fa fa-save"></i></a>
                <a href="#!" id="${menu.id}" class="btn btn-primary btn-floating ver-info tooltipped" data-position="bottom" data-delay="50" data-tooltip="Ver adicionales del menú"><i class="fa fa-eye"></i></a>
            `;
            let ganancia = menu.precio_usd * (menu.ganancia/100);
            let tot_gan = menu.precio_usd+ganancia;
            let precios_tot = `$${formato.precio(tot_gan)} <br> Bs. ${formato.precio(tot_gan * tasa_d)}`;
            var row = [count+=1
                ,menu.nombre
                ,menu.descripcion
                ,precios
                ,menu.categoria.nombre
                ,'<input id="menu_'+menu.id+'" iden="'+menu.id+'" type="text" placeholder="%" class="form-control mb-0 ganancia-all" value="'+formato.precio1(menu.ganancia)+'">'
                ,estatusSwitch
                ,precios_tot
                ,options
            ];
            table.row.add(row).draw().node();
        }
        $('.ganancia-all').mask('##0,0', {reverse: true});
        $('.tooltipped').tooltip();
    }



    function buscar_data(idb, modal){
        data = {
            id_res: getop("id")
        };
        $.ajax({
            url: dominio + "menu/"+idb,
            headers: {
                "token": userDOOMI.token,
                'tipo_usuario': userDOOMI.tipo_usu
            },
            type: "GET",
            data: data,
            success: function(data){
                imprimir_data_menu(data, modal)
            },
            error: function(xhr, status, errorThrown){
                console.log(errorThrown);
            }
        });
    };
    function imprimir_data_menu(data, modal) {
        if (modal == "ver"){
            $('.adi-view').html('<b>Adicionales: </b> Sin adicionales.').removeClass('adi-view-style');
            if(data.adicionales[0]){
                console.log(data);
                let html_adi = `
                    <table class="responsive-table mb-4">
                        <tr class="bg_gray">
                            <th colspan="4" class="text-center">ADICIONALES</th>
                        </tr>
                        <tr class="bg_gray">
                            <th>Adicionales:</th>
                            <th class="s2 m2 l2">Precio Doomi</th>
                            <th>Precio Total</th>
                            <th>Existencias</th>
                        </tr>
                    `;
                for (adicional of data.adicionales){
                    let ganancia = adicional.precio_usd * (data.ganancia/100);
                    let tot_gan = adicional.precio_usd+ganancia;
                    html_adi += `
                        <tr>
                            <td>${adicional.nombre}</td>
                            <td>$${formato.precio(adicional.precio_usd)}<br> Bs. ${formato.precio(adicional.precio_usd * tasa_d)}</td>
                            <td>$${formato.precio(tot_gan)} <br> Bs. ${formato.precio(tot_gan * tasa_d)}</td>
                            <td>${adicional.existencias}</td>
                        </tr>
                    `;
                }
                html_adi += "</table>";
                $(".adi-view").html(html_adi).addClass('adi-view-style');
            }

            $("#md-veradicionales").modal("open");
        }

    }


    tr_adi = 0;


    $(document).on("click", ".ver-info", function(){
        var idv = this.id;
        $('.tooltipped').tooltip();
        buscar_data(idv, "ver");
    });
    $(document).on("click", ".edit-info", function(){
        var idv = this.id;
        val = $("#menu_"+idv).val();
        var data = new FormData();
        data.append("iden", idv);
        data.append("val", Quitarcoma(val));
        $(".edit-info").attr("disabled",true);
        $.ajax({
                method: "PUT",
                url: dominio + 'menu/edit_ganancia',
                headers: {
                    'token':userDOOMI.token,
                    'tipo_usuario': userDOOMI.tipo_usu
                },
                data: data,
                contentType: false,
                processData: false,
                success: function(data) {
                    $(".edit-info").attr("disabled",false);
                    if(data.r)
                        menus();
                    Materialize.toast( data.msj , 5000);
                    $('.tooltipped').tooltip();
                },
                error: function(xhr, status, errorThrown) {
                    $(".edit-info").attr("disabled",false);
                    console.log("Errr : ");
                }
        });
    });

    $(document).on('keypress', ".ganancia-all", function(e){
        var code = (e.keyCode ? e.keyCode : e.which);
        if(code==13){

            iden = $(this).attr("iden");
            val = $(this).val();
            var data = new FormData();
            data.append("iden", iden);
            data.append("val", Quitarcoma(val));
            $.ajax({
                method: "PUT",
                url: dominio + 'menu/edit_ganancia',
                headers: {
                    'token':userDOOMI.token,
                    'tipo_usuario': userDOOMI.tipo_usu
                },
                data: data,
                contentType: false,
                processData: false,
                success: function(data) {
                    if(data.r)
                        menus();
                    Materialize.toast( data.msj , 5000);
                },
                error: function(xhr, status, errorThrown) {
                    console.log("Errr : ");
                }
            });
        }
    });

    $(document).on('click', '.sorting_1', function() {
        $('.tooltipped').tooltip();
        $('.ganancia-all').mask('##0,0', {reverse: true});
    });

    // Capitalizar nombres.
    $(document).on("input propertychange change", "#nom_men, #descripcion, input[name='adicionales_nombres[]']", async function(){
        this.value = capitalizar(this.value);
    });

};
