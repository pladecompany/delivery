function metodos_pagosjs(){

    //listar
    metodos_pagos();

    function metodos_pagos() {
        if(userDOOMI.tipo_usu=='administrador'){
            data = {
                tipo_usu: userDOOMI.tipo_usu
            }
        }else{
            data = {
                id_res: userDOOMI.restaurante[0].id,
                tipo_usu: userDOOMI.tipo_usu
            }
        }

        $.ajax({
            url: dominio + 'metodos_pagos/',
            headers: {
                'token':userDOOMI.token,
                'tipo_usuario': userDOOMI.tipo_usu
            },
            type: 'GET',
            data: data,
            success: function(data) {
                //console.log(data);
                actualizar_tabla_metodos_pagos(data);
            }
        })
    }

    function actualizar_tabla_metodos_pagos(data) {

        var table = $('.table').DataTable({
            "language": {
                "url": "../static/<lib></lib>/JTables/Spanish.json"
            }
        });
        count = 0
        table.clear().draw();

        for (var i = 0; i < data.length; i++) {
            datos = data[i];

            var options = `
                <a href="#!" id="${datos.id}" class="btn btn-primary btn-floating ver-info tooltipped" data-position="bottom" data-delay="50" data-tooltip="Ver información del método de pago"><i class="fa fa-eye"></i></a>

                <a href="#!" id="${datos.id}" class="btn btn-primary btn-floating edit-info tooltipped" data-position="bottom" data-delay="50" data-tooltip="Editar información del método de pago"><i class="fa fa-edit"></i></a>

                <a href="#!" id="${datos.id}" class="waves-effect btn btn-primary btn-floating tooltipped delete" data-position="bottom" data-delay="50" data-tooltip="Eliminar método de pago"><i class="fa fa-times"></i></a>
            `;

            var row = [count += 1, datos.nombre, options];
            table.row.add(row).draw().node();
        }
        $('.tooltipped').tooltip();
    }

    $("#form_registrar_metodo").on('submit', function(e) {

        e.preventDefault();

        /*validaciones*/
        var errr = false;
        var msj = false;

        if ( $("#nom_met").val().trim() == "" ){ errr = true; msj = "Ingresa el tipo de vehículo";}

        /*fin validaciones*/
        if (errr) {
            Materialize.toast(msj, 4000); return false;
        }
        //data a enviar
        var data = new FormData(this);
        data.append("tipo_usu", userDOOMI.tipo_usu );
        if(userDOOMI.tipo_usu!='administrador')
            data.append("id_res", userDOOMI.restaurante[0].id );
        if($("#iden").val() != ''){
            url = "metodos_pagos/edit";
            tipo = "PUT";
        }else{
            url = "metodos_pagos/add";
            tipo = "POST";
        }
        $(".bt_save").prop("disabled", true);
        $(".fileprogress").removeClass("hide");
        $(".fileprogress").find("div")[0].style.width = "0%";
        $.ajax({
            url: dominio + url,
            headers: {
                'token':userDOOMI.token,
                'tipo_usuario': userDOOMI.tipo_usu
            },
            type: tipo,
            dataType: "json",
            cache: false,
            data: data,
            contentType: false,
            processData: false,
            xhr: function(){
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", function(e){
                    if (e.lengthComputable) {
                      var percentComplete = parseInt( (e.loaded / e.total * 100), 10);
                      $(".fileprogress").find("div")[0].style.width = `${percentComplete}%`;
                      if(percentComplete>=100){
                        $(".fileprogress").addClass("hide");
                        $(".fileprogress").find("div")[0].style.width = "0%";
                      }
                    }
                }, false);
                xhr.addEventListener("progress", function(e){
                    if (e.lengthComputable) {
                        var percentComplete = parseInt( (e.loaded / e.total * 100), 10);
                        $(".fileprogress").find("div")[0].style.width = `${percentComplete}%`;
                        if(percentComplete>=100){
                            $(".fileprogress").addClass("hide");
                            $(".fileprogress").find("div")[0].style.width = "0%";
                        }
                    }
                }, false);
                return xhr;
            },
            success: function(res) {
                Materialize.toast(res.msj, 4000)
                if (res.r == true) {
                    $("#form_registrar_metodo")[0].reset();
                    metodos_pagos();
                    $("#md-nuevoMetododepago").modal('close');
                }
                //botones del form
                $(".bt_save").prop("disabled", false);
                completarPaso(3);
            },
            error: function(xhr, status, errorThrown) {
                console.log(errorThrown);
                $(".fileprogress").addClass("hide");
                $(".fileprogress").find("div")[0].style.width = "0%";
                Materialize.toast(errorThrown.msj, 4000)

                //botones del form
                $(".bt_save").prop("disabled", false)
            }
        });
    });
    $(document).on('click', '.ver-info', function() {
        var idv = this.id;
        buscar_data(idv,'ver')
    });
    $(document).on('click', '.edit-info', function() {
        var idv = this.id;
        buscar_data(idv,'editar')
    });
    function buscar_data(idb,modal){
        data = {
        }
        $.ajax({
            url: dominio + 'metodos_pagos/'+idb,
            headers: {
                'token':userDOOMI.token,
                'tipo_usuario': userDOOMI.tipo_usu
            },
            type: 'GET',
            data: data,
            success: function(data) {

                 //console.log(data);
                imprimir_data_credencial(data,modal)

            }, error: function(xhr, status, errorThrown) {
                console.log(errorThrown);
            }

        })
    }
    function imprimir_data_credencial(data,modal){
        if(modal=='ver'){
            $(".nom-view").html(data.nombre);
            $("#md-verCuenta").modal("open");
        }else if(modal=='editar'){
            $("#nom_met").val(data.nombre);
            $("#iden").val(data.id);
            $("#tit-mod").html("Editar método de pago");
            $(".bt_save").html('Guardar cambios');
            $("#md-nuevoMetododepago").modal("open");
        }
    }

    $(document).on('click', '.delete', function() {
        var idv = this.id;
        var toastContent = '<span>¿Desea eliminar éste registro?</span><br><button class="btn-flat toast-action conf_si" id="'+idv+'">Si</button><button class="btn-flat toast-action" onclick=" $(\'.toast\').hide(); ">No</button>';

        Materialize.toast(toastContent, 4000);
    });

    $(document).on('click', '.conf_si', function() {
        id = this.id
        var data = {};

        $('.toast').hide();
        $.ajax({
            url: dominio + 'metodos_pagos/delete/'+id,
            headers: {
                'token':userDOOMI.token,
                'tipo_usuario': userDOOMI.tipo_usu
            },
            type: 'DELETE',
            data: data,
            success: function(data) {
                if (data.msj) {
                    Materialize.toast(data.msj, 10000);
                    metodos_pagos();
                } else {
                    Materialize.toast(data.msj, 10000);
                }
            }
        })

    });

    $(document).on('click', '.sorting_1', function() {
        $('.tooltipped').tooltip();
    })

}