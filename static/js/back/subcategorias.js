function listar_subcategorias() {

    var ruta = "subcategorias/";
    var id_categoria_s = false;
    $("#categoria").select2({placeholder: "Seleccione", allowClear: false});
    ajaxCategorias();

    // Listar.
    subcategorias();


    clickNuevaSubcategoria = function clickNuevaSubcategoria(){

        if ($('#categoria > option').length == 1) {
            // Quitar enlace e interaccion con el puntero.
            document.getElementById('nuevaSubcategoriaBtn').setAttribute('href', '#');
            document.getElementById('nuevaSubcategoriaBtn').style.pointerEvents = 'none';

            Materialize.toast("No hay categorías", 4000, '', function(){
                // Regresar enlace e interaccion luego del Toast.
                document.getElementById('nuevaSubcategoriaBtn').setAttribute('href', '#md-nuevaSubcategoria');
                document.getElementById('nuevaSubcategoriaBtn').style.pointerEvents = 'auto';
            });
            return false;
        }
        $('#form_registrar_subcategoria')[0].reset();
        $('#iden').val('');
        $('.bt_save').html('Guardar');
        $('#tit-mod').html('Nueva subcategoría');
        $('#categoria').select2({placeholder: 'Seleccione', allowClear: false});
    };

    function subcategorias() {
        data = {};
        $.ajax({
            url: dominio + ruta,
            headers: {
                'token': userDOOMI.token,
                'tipo_usuario': userDOOMI.tipo_usu
            },
            type: 'GET',
            data: data,
            success: function(data){
                actualizar_tabla_subcategorias(data)
            }
        })
    }

    function actualizar_tabla_subcategorias(data) {
        var table = $('.table').DataTable({
            "language": {
                "url": "../static/<lib></lib>/JTables/Spanish.json"
            }
        });
        count = 0;
        table.clear().draw();

        for (var i = 0; i < data.length; i++) {
            subcategoria = data[i];

            var options = `
                <a href="#!" id="${subcategoria.id}" class="btn btn-primary btn-floating ver-info tooltipped" data-position="bottom" data-delay="50" data-tooltip="Ver información de la categoría"><i class="fa fa-eye"></i></a>
                <a href="#!" id="${subcategoria.id}" class="btn btn-primary btn-floating edit-info tooltipped" data-position="bottom" data-delay="50" data-tooltip="Editar información de la categoría"><i class="fa fa-edit"></i></a>
                <a href="#!" id="${subcategoria.id}" class="waves-effect btn btn-primary btn-floating tooltipped delete" data-position="bottom" data-delay="50" data-tooltip="Eliminar categoría"><i class="fa fa-times"></i></a>
            `;

            var row = [count+=1, subcategoria.categoria.nombre, subcategoria.nombre, options];
            table.row.add(row).draw().node();
        }
        $('.tooltipped').tooltip();
    }

    $("#form_registrar_subcategoria").on("submit", function(e){
        e.preventDefault();

        // Validaciones.
        var errr = false;
        var msj = false;

        if ($("#nombre").val().trim() == ""){
            errr=true;
            msj = "Ingresa el nombre de la subcategoría";
        }
        if ($("#categoria").val() == ""){
            errr=true;
            msj = "Seleccione una categoría";
        }
        // Fin de validaciones.

        if (errr) {
            Materialize.toast(msj, 4000);
            return false;
        }
        // Data a enviar.
        var data = new FormData(this);
        if($("#iden").val() != ''){
            url = ruta+"edit/";
            tipo = "PUT";
        }
        else {
            url = ruta+"add/";
            tipo = "POST";
        }

        $(".bt_save").prop("disable", true);
        $(".fileprogress").removeClass("hide");
        $(".fileprogress").find("div")[0].style.width = "0%";
        $.ajax({
            url: dominio + url,
            headers: {
                'token': userDOOMI.token,
                'tipo_usuario': userDOOMI.tipo_usu
            },
            type: tipo,
            dataType: "json",
            cache: false,
            data: data,
            contentType: false,
            processData: false,
            xhr: function(){
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", function(e){
                    if (e.lengthComputable) {
                        var percentComplete = parseInt((e.loaded/e.total*100), 10);
                        $(".fileprogress").find("div")[0].style.width = `${percentComplete}%`;
                        if (percentComplete >= 100){
                            $(".fileprogress").addClass("hide");
                            $(".fileprogress").find("div")[0].style.width ="0%";
                        }
                    }
                }, false);

                xhr.addEventListener("progress", function(e){
                    if (e.lengthComputable){
                        var percentComplete = parseInt((e.loaded/e.total*100), 10);
                        $(".fileprogress").find("div")[0].style.width = `${percentComplete}%`;
                        if(percentComplete>=100){
                            $(".fileprogress").addClass("hide");
                            $(".fileprogress").find("div")[0].style.width = "0%";
                        }
                    }
                }, false);

                return xhr;
            },
            success: function(res){
                Materialize.toast(res.msj, 4000)
                if (res.r == true){
                    $("#form_registrar_subcategoria")[0].reset();
                    subcategorias();
                    $("#md-nuevaSubcategoria").modal('close');
                }
                // Botones del form.
                $(".bt_save").prop("disable", false)
            },
            error: function(xhr, status, errorThrown){
                console.log(errorThrown);

                $(".fileprogress").addClass("hide");
                $(".fileprogress").find("div")[0].style.width = "0%";

                Materialize.toast(errorThrown.msj, 4000)

                //botones del form
                $(".bt_save").prop("disabled", false)
            }
        });
    });
    $(document).on("click", ".ver-info", function(){
        var idv = this.id;
        buscar_data(idv, "ver");
    });
    $(document).on("click", ".edit-info", function(){
        var idv = this.id;
        buscar_data(idv, "editar")
    });
    function buscar_data(idb, modal){
        data = {};
        $.ajax({
            url: dominio + ruta + idb,
            headers: {
                "token": userDOOMI.token,
                "tipo_usuario": userDOOMI.tipo_usu
            },
            type: "GET",
            data: {},
            success: function(data){
                imprimir_data_subcategoria(data, modal)
            },
            error: function(xhr, status, errorThrown){
                console.log(errorThrown);
            }
        });
    };
    function imprimir_data_subcategoria(data, modal) {
        if (modal == "ver"){
            $(".nom-view").html(data.nombre);
            $(".cat-view").html(data.categoria.nombre);
            $("#md-verSubcategoria").modal("open");
        }
        else if (modal == "editar") {
            id_categoria_s = data.id_categoria;
            ajaxCategorias();
            $("#nombre").val(data.nombre);
            $("#iden").val(data.id);
            $("#tit-mod").html("Editar subcategoría");
            $(".bt_save").html("Guardar cambios");
            $("#md-nuevaSubcategoria").modal("open");
        }
    }

    $(document).on('click', '.delete', function() {
        var idv = this.id;
        var toastContent = '<span>¿Desea eliminar esta subcategoría?</span><br><button class="btn-flat toast-action conf_si" id="'+idv+'">Si</button><button class="btn-flat toast-action" onclick=" $(\'.toast\').hide(); ">No</button>';

        Materialize.toast(toastContent, 4000);
    });
    $(document).on('click', '.conf_si', function() {
        id = this.id
        data = {};

        $('.toast').hide();
        $.ajax({
            url: dominio + ruta + 'delete/'+id,
            headers: {
                'token':userDOOMI.token
            },
            type: 'DELETE',
            data: data,
            success: function(data) {
                if (data.msj) {
                    Materialize.toast(data.msj, 10000);
                    subcategorias();
                } else {
                    Materialize.toast(data.msj, 10000);
                }
            }
        })

    });

    $(document).on('click', '.sorting_1', function() {
        $('.tooltipped').tooltip();
    });

    // Capitalizar nombres.
    $("#nombre").on("input propertychange change", async function(){
        this.value = capitalizar(this.value);
    });

    function ajaxCategorias(){
        $('#categoria').html('<option value="" selected disabled>Cargando...</option>');
        $.ajax({
            url: dominio + 'categorias/',
            headers: {
                'token': userDOOMI.token,
                'tipo_usuario': userDOOMI.tipo_usu
            },
            type: 'GET',
            data: {},
            success: function(data){
                html = '<option value="" selected>Seleccione</option>';
                for (var i = 0; i < data.length; i++) {
                    // html += '<option value="' + data[i].id + '">' + data[i].nombre + '</option>';
                    html += `<option value="${data[i].id}">${data[i].nombre}</option>`;
                }
                $("#categoria").html(html);

                if(id_categoria_s){
                    $("#categoria").val(id_categoria_s).trigger("change");
                    id_categoria_s = false;
                }
                $("#categoria").select2({placeholder: "Seleccione", allowClear: false});
            }
        });
    };
};
