function pedidosjs() {
    var socket = io(dominio);
    DSERU = "";
    DIRDISU = "";
    var buscar_fec_d = '';
    var buscar_fec_h = '';
    var buscar_cli = '';
    var buscar_sts = '';

    recuperar_clientes();
    recuperar_estatus();
    recuperar_pedidos();


    function recuperar_clientes(){
        $('#sel_cli').html('<option value="" selected disabled>Cargando...</option>');
        ajaxGET('clientes/', actualizar_tabla_cientes_p)
    };
    function actualizar_tabla_cientes_p(data) {
        var html = '<option value="" selected>Seleccione</option>';
        for (cliente of data) {
            html += '<option value="' + cliente.id + '">' + cliente.nombre + ' ' + cliente.apellido + '</option>';
        }
        $("#sel_cli").html(html);
        $("#sel_cli").select2({placeholder: "Seleccione", allowClear: false});
    };

    function recuperar_estatus(){
        $('#sel_sts').html('<option value="" selected disabled>Cargando...</option>');
        ajaxGET('pedidos/estatus/', actualizar_tabla_sts_p)
    };
    function actualizar_tabla_sts_p(data) {
        var html = '<option value="" selected>Seleccione</option>';
        for (sts_data of data) {
            //html += '<option value="' + sts_data.id + '">' + sts_data.nombre + '('+ sts_data.descripcion +')</option>';
            html += '<option value="' + sts_data.id + '">' + sts_data.nombre + '</option>';
        }
        $("#sel_sts").html(html);
        $("#sel_sts").select2({placeholder: "Seleccione", allowClear: false});
    };

    $(document).on('click', '.btn-filtro-ped', function() {
        //if($("#fec_ped_d").val()!='' && $("#fec_ped_h").val()=='' ){
        //    Materialize.toast('Debes seleccionar la fecha "Hasta"', 5000);
        //    return
        //}
        if($("#fec_ped_h").val()!='' && $("#fec_ped_d").val()=='' ){
            Materialize.toast('Debes seleccionar la fecha "Desde"', 5000);
            return
        }
        var fec_d = new Date($("#fec_ped_d").val()).getTime();
        var fec_h = new Date($("#fec_ped_h").val()).getTime();
        if ($("#fec_ped_h").val()!='' && $("#fec_ped_d").val()!='' && fec_d>fec_h){
            Materialize.toast('La fecha "Desde" no debe ser mayor a "Hasta"', 5000);
            return
        }

        buscar_fec_d = $("#fec_ped_d").val();
        buscar_fec_h = $("#fec_ped_h").val();
        buscar_cli = $("#sel_cli").val();
        buscar_sts = $("#sel_sts").val();
        recuperar_pedidos();
    });

    function recuperar_pedidos(){
        if(buscar_fec_d==''){
            buscar_fec_d = moment().format('YYYY-MM-DD');
            $("#fec_ped_d").val(moment().format('YYYY-MM-DD'));
        }
        data = {
            fecd: buscar_fec_d,
            fech: buscar_fec_h,
            cliente: buscar_cli,
            sts: buscar_sts
        }
        if(userDOOMI.tipo_usu=='administrador')
            var ruta = 'pedidos/restaurante/0';
        else
            var ruta = 'pedidos/restaurante/' + userDOOMI.restaurante[0].id;
        //ajaxGET(ruta, actualizar_tabla_cientes)
        $.ajax({
            url: dominio + ruta,
            headers: {
                'token':userDOOMI.token,
                'tipo_usuario': userDOOMI.tipo_usu
            },
            type: 'GET',
            data: data,
            success: function(data) {
                //console.log(data);
                actualizar_tabla_pedidos(data);
            }, error: function(xhr, status, errorThrown) {
                console.log(xhr,errorThrown);
            }
        })
    };

    function actualizar_tabla_pedidos(data) {
        //console.log(data);
        const table = $('.table').DataTable({
            "aaSorting": [],
            "language": { "url": "../static/lib/JTable/Spanish.json" }
        });
        let count = 1;
        table.clear().draw();

        for (datos of data) {
            options = `<a href="#!" id="${datos.id}" class="btn-ver-pedido btn btn-floating btn-primary tooltipped" data-position="bottom" data-delay="50" data-tooltip="Ver información del pedido"><i class="fa fa-eye"></i></a>`;
            if(datos.cuenta)
                var id_res_cuenta = datos.cuenta.idres;
            else
                var id_res_cuenta = 0;

            var dif_dias = "";
            if(datos.modalidad == "Programado"){
                fecha1_p = moment();
                fecha2_p = moment(datos.fecha_pro);
                dif_dias = fecha2_p.diff(fecha1_p, 'days');
            }

            if(userDOOMI.tipo_usu == 'administrador' && (datos.estatus.id==8 || datos.estatus.id==9 || datos.estatus.id==10 || datos.estatus.id==12)){
                options += ` <a href="#!" id="${datos.id}" class="btn-cancelar-pedido-admin btn btn-floating btn-primary tooltipped" data-position="bottom" data-delay="50" data-tooltip="Cancelar pedido"><i class="fa fa-times"></i></a>`;
            }

            if(datos.estatus.id==1 && userDOOMI.tipo_usu!='administrador')
                options += ` <a href="#!" id="${datos.id}" class="btn-procesar-pedido btn btn-floating btn-primary tooltipped" data-position="bottom" data-delay="50" data-tooltip="Procesar pedido"><i class="fa fa-check"></i></a>`;

            if((userDOOMI.tipo_usu!='administrador' && datos.estatus.id==3 && datos.id_restaurante==id_res_cuenta) || (userDOOMI.tipo_usu=='administrador' && datos.estatus.id==3 && id_res_cuenta==null))
                options += ` <a href="#!" id="${datos.id}" class="btn-procesar-pago btn btn-floating btn-primary tooltipped" data-position="bottom" data-delay="50" data-tooltip="Procesar pago del pedido"><i class="fa fa-dollar-sign"></i></a>`;
            else  if(datos.estatus.id==3 && id_res_cuenta==null && userDOOMI.tipo_usu!='administrador')
                options += ` <a href="#!" style="background: #d01f2a !important;" class="btn btn-floating btn-primary tooltipped" data-position="bottom" data-delay="50" data-tooltip="Procesar pago del pedido"><i class="fa fa-dollar-sign"></i></a>`;

            if(((datos.estatus.id==5 && (datos.modalidad == "Pickup" || datos.modalidad == "Delivery")) || (datos.estatus.id==5 && datos.modalidad == "Programado" && dif_dias==0)) && userDOOMI.tipo_usu!='administrador')
                options += `<a href="#!" class="btn btn-floating btn-primary tooltipped ml-1 btn_general_sts" id="${datos.id}" sts="6" data-position="bottom" data-delay="50" data-tooltip="Iniciar la prepación del pedido"><i class="fas fa-utensils"></i></a>`;
            else if(datos.estatus.id==5 && datos.modalidad == "Programado" && userDOOMI.tipo_usu!='administrador' && dif_dias>0)
                options += `<a href="#!" style="background: #ffc107 !important;" class="btn btn-floating btn-primary tooltipped ml-1" data-position="bottom" data-delay="50" data-tooltip="Esperando para iniciar la prepación del pedido"><i class="fas fa-utensils"></i></a>`;

            if(datos.estatus.id==6 && userDOOMI.tipo_usu!='administrador')
                options += `<a href="#!" class="btn btn-floating btn-primary tooltipped ml-1 btn_general_sts" id="${datos.id}" sts="7" data-position="bottom" data-delay="50" data-tooltip="Finalizar la prepación del pedido"><i class="fas fa-calendar-check"></i></a>`;

            if(datos.estatus.id==8 && userDOOMI.tipo_usu!='administrador' && datos.id_repartidor != null)
                options += `<a href="#!" class="btn btn-floating btn-primary tooltipped ml-1 btn_entregar_pago" id="${datos.id}" sts="9" data-position="bottom" data-delay="50" data-tooltip="Entregar pedido al Repartidor"><i class="fa fa-handshake"></i></a>`;


            if(datos.estatus.id==11)
                options += `<a href="#!" id="${datos.id}" class="btn-ver-calificacion btn btn-floating btn-primary tooltipped ml-1" data-position="bottom" data-delay="50" data-tooltip="Ver calificación del pedido"><i class="fa fa-star"></i></a>`;

            let cliente_ced = datos.cliente.cedula ? datos.cliente.identificacion+"-"+datos.cliente.cedula : '';
            let cliente_nom = datos.cliente.nombre ? datos.cliente.nombre+" "+datos.cliente.apellido : '' ;
            if(userDOOMI.tipo_usu=='administrador')
                table.row.add([datos.id, moment(datos.creado).format('DD/MM/YYYY, h:mm:ss a'),datos.nom_res,cliente_ced,cliente_nom, datos.estatus.nombre,datos.modalidad, options]).draw().node();
            else
                table.row.add([datos.id, moment(datos.creado).format('DD/MM/YYYY, h:mm:ss a'),cliente_ced, cliente_nom, datos.estatus.nombre,datos.modalidad, options]).draw().node();
        }
        $('.tooltipped').tooltip();
    };

     //EVENTO CUANDO SE RECIBE EMIT DESDE EL API
    socket.on('Recibirpedido', function(data) {
        //console.log(data);
        if(userDOOMI.tipo_usu=='administrador' || (userDOOMI.tipo_usu!='administrador' && userDOOMI.id_restaurante==data.id_restaurante))
            recuperar_pedidos();
    });

    $(document).on('click', '.btn-ver-pedido', function() {
        var id_ped = this.id;
        imprimir_data(id_ped);
    });

    function imprimir_data(id_ped, admin_cancelando){
        $('.limpiar-data').html('');
        $('.limpiar-data-2').val('');
        $(".btn-accion").html('');
        ajaxGET(`pedidos/${id_ped}`, data => {
            const datos = data[0] || data;
            //console.log(datos);
            $('.txt-id').html(datos.id);
            $('.txt-id-c').html(datos.cliente.id);
            $('.txt-fec').html(moment(datos.creado).format('DD/MM/YYYY, h:mm:ss a'));
            let cliente_ced = datos.cliente.cedula ? datos.cliente.identificacion+'-'+datos.cliente.cedula : '' ;
            let cliente_nom = datos.cliente.nombre ? datos.cliente.nombre+" "+datos.cliente.apellido : '';
            $('.txt-cli').html(cliente_ced + " - " + cliente_nom);
            $('.txt-tlf').html(datos.cliente.telefono);
            $(".txt-tasa").html(formato.precio(datos.tasa)+" Bs");
            var cuerpo_orden = '';
            var acum = 0;
            var tot_ord = 0;
            var tot_adi = 0;
            for (pedidos_pro of datos.ordenes) {
                acum++;
                var adi_ext=0;
                var txt_adi='';
                var txt_aco='';
                if(pedidos_pro.adicionales.length>0){
                    for (adicionales_pedido of pedidos_pro.adicionales) {
                        adi_ext = parseFloat(adi_ext)+(parseFloat(adicionales_pedido.precio_usd)*parseFloat(adicionales_pedido.cantidad));
                        if(txt_adi=='')
                            txt_adi=adicionales_pedido.cantidad+' '+adicionales_pedido.nombre+'('+formato.precio(adicionales_pedido.precio_usd)+' $)';
                        else
                            txt_adi+='<br> '+adicionales_pedido.cantidad+' '+adicionales_pedido.nombre+'('+formato.precio(adicionales_pedido.precio_usd)+' $)';
                    }
                }
                if(pedidos_pro['acompañantes'].length>0){
                    for (acompanantes_pedido of pedidos_pro['acompañantes']) {
                        adi_ext = parseFloat(adi_ext)+(parseFloat(acompanantes_pedido.precio_usd)*parseFloat(acompanantes_pedido.cantidad));
                        if(txt_aco=='')
                            txt_aco=acompanantes_pedido.cantidad+' '+acompanantes_pedido.nombre+'('+formato.precio(acompanantes_pedido.precio_usd)+' $)';
                        else
                            txt_aco+='<br> '+acompanantes_pedido.cantidad+' '+acompanantes_pedido.nombre+'('+formato.precio(acompanantes_pedido.precio_usd)+' $)';
                    }
                }
                cuerpo_orden += `
                    <tr>
                        <td>${pedidos_pro.cantidad}</td>
                        <td>${pedidos_pro.menu.nombre}</td>
                        <td class="text-right">${formato.precio(pedidos_pro.precio_usd)} $</td>
                        <td><small>${txt_adi}</small></td>
                        <td><small>${txt_aco}</small></td>
                        <td>${pedidos_pro.notas || ''}</td>
                    </tr>
                `;
                tot_ord = parseFloat(tot_ord) + parseFloat(parseFloat(pedidos_pro.precio_usd)*parseFloat(pedidos_pro.cantidad));
                tot_adi = parseFloat(tot_adi) + parseFloat(adi_ext);
            };
            var pago_d = datos.pago_del || 0;
            $('#list-pedido').html(cuerpo_orden);
            $('.txt-subt').html(formato.precio(tot_ord) +' $');
            $('.txt-adit').html(formato.precio(tot_adi)+' $');
            $('.txt-tot').html(formato.precio(parseFloat(tot_ord) + parseFloat(tot_adi) + parseFloat(pago_d))+' $');

            $('.txt-subt-bs').html(formato.precio((tot_ord)*parseFloat(datos.tasa)) +' Bs');
            $('.txt-adit-bs').html(formato.precio((tot_adi)*parseFloat(datos.tasa))+' Bs');
            $('.txt-tot-bs').html(formato.precio((parseFloat(tot_ord) + parseFloat(tot_adi) + parseFloat(pago_d))*parseFloat(datos.tasa))+' Bs');

            $('#sts_ped').html(datos.estatus.nombre+" ("+datos.estatus.descripcion+")");
            //$('#sts_ped').val(datos.estatus.nombre+" ("+datos.estatus.descripcion+")");
            if(datos.estatus.id==11)
                $(".vista-nota").hide();
            else
                $(".vista-nota").show();
            $('#motivo').val(datos.motivo || '');
            $('.txt-mod').html(datos.modalidad);
            $(".pre_del, .ley_mapa").hide();
            if(datos.modalidad=='Delivery' || datos.modalidad=='Programado'){
                if (typeof google) {
                    DSERU = new google.maps.DirectionsService();
                    DIRDISU = new google.maps.DirectionsRenderer({suppressMarkers: true});
                }
                $(".pre_del, .ley_mapa").show();
                $('.txt-delt').html(formato.precio(pago_d) +' $');
                $('.txt-delt-bs').html(formato.precio((pago_d)*parseFloat(datos.tasa))+' Bs');
                var data_rep = '';
                if(datos.repartidor && datos.estatus.id>=5 && datos.estatus.id<97){
                    data_rep =`<li class="collection-item"><b>Repartidor:</b> #${datos.repartidor.id} ${datos.repartidor.nombre} ${datos.repartidor.apellido}</li>`;
                }else if(datos.estatus.id>=5 && datos.estatus.id<97){
                    data_rep =`<li class="collection-item"><b>Repartidor:</b> Buscando</li>`;
                }
                $('.descrip-moda').html(`
                    ${data_rep}
                    <li class="collection-item"><b>Dirección:</b> ${datos.direccion} </li>
                    <li class="collection-item"><b>Punto de referencia:</b> ${datos.punto_referencia} </li>
                `);
                $('.ver-mapa').html('<div style="height: 50vh;" id="mapaver" style="width:100%;"></div>');
                var map;
                var styles = [
                    { featureType: "poi.business", elementType: "labels", stylers: [{ visibility: "off" }] },
                    { elementType: 'labels.text.fill', stylers: [{ color: '#294dce' }] }
                ];
                if (!typeof google) {
                    return true;
                }
                var styledMap = new google.maps.StyledMapType(styles, { name: "Styled Map" });

                var options = {
                    zoom: 16,
                    center: new google.maps.LatLng(9.038338434704668, -69.7499810171737),
                    mapTypeControl: false,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    backgroundColor: 'transparent'
                };

                mapa = new google.maps.Map(document.getElementById('mapaver'), options);
                mapa.mapTypes.set('map_style', styledMap);
                mapa.setMapTypeId('map_style');

                //$(document).on('click', '.gm-fullscreen-control', function() {
                //    screen_map('mapaver');
                //});
                DIRDISU.setMap(mapa);
                var data_map = {
                    origin: datos.restaurante[0].lat + "," + datos.restaurante[0].lon,
                    destination: datos.lat_dir_p + "," + datos.lon_dir_p,
                    optimizeWaypoints: true,
                    travelMode: 'DRIVING'
                };
                DSERU.route(data_map, function(response, status) {
                    if (status === 'OK') {
                        DIRDISU.setDirections(response);
                        var image = {url: "../../static/img/map-verde.png",size: new google.maps.Size(81, 81),origin: new google.maps.Point(0, 0),anchor: new google.maps.Point(40, 71),scaledSize: new google.maps.Size(75, 75)};
                        var image2 = {url: "../../static/img/map-azul.png",size: new google.maps.Size(81, 81),origin: new google.maps.Point(0, 0),anchor: new google.maps.Point(40, 71),scaledSize: new google.maps.Size(75, 75)};

                        var marker_n = new google.maps.Marker({
                            position: {lat: parseFloat(response.request.origin.location.lat()), lng: parseFloat(response.request.origin.location.lng())},
                            icon: image
                        });
                        var marker_n2 = new google.maps.Marker({
                            position: {lat: parseFloat(response.request.destination.location.lat()), lng: parseFloat(response.request.destination.location.lng())},
                            icon: image2
                        });
                        marker_n.setMap(mapa);marker_n2.setMap(mapa);
                    } else {
                        onError();
                    }

                });
                function onError(error) {
                    Materialize.toast('Ocurrió un error accediendo a la ruta, intente más tarde', 5000);
                };
            }

            if(datos.modalidad=='Programado' || datos.modalidad=='Pickup'){
                $('.descrip-moda').html(`
                    <li class="collection-item"><b>Fecha:</b> ${moment(datos.fecha_pro).format('DD/MM/YYYY, h:mm:ss a')} </li>
                `);
            }
            $(".ver-pagos").hide();
            if(datos.estatus.id>=3 && datos.estatus.id<98 && datos.estatus.id!=4){
                $('.img_pago_ver').hide();
                if(datos.cuenta){
                    $('.txt_c').html(datos.cuenta.cuenta);
                }
                $('.txt_r').html(datos.referencia);
                if(datos.img_pago){
                    $(".img_i").attr("src",datos.img_pago);
                    $('.img_pago_ver').show();
                }
                $(".ver-pagos").show();
            }
            var dif_dias = "";
            if(datos.modalidad == "Programado"){
                fecha1_p = moment();
                fecha2_p = moment(datos.fecha_pro);
                dif_dias = fecha2_p.diff(fecha1_p, 'days');
            }
            options = '';
            if(datos.cuenta)
                var id_res_cuenta = datos.cuenta.idres;
            else
                var id_res_cuenta = 0;
            if(datos.estatus.id==1 && userDOOMI.tipo_usu!='administrador')
                options += ` <a href="#!" id="${datos.id}" class="btn-procesar-pedido btn btn-floating btn-primary tooltipped" data-position="bottom" data-delay="50" data-tooltip="Procesar pedido"><i class="fa fa-check"></i></a>`;

            if((userDOOMI.tipo_usu!='administrador' && datos.estatus.id==3 && datos.id_restaurante==id_res_cuenta) || (userDOOMI.tipo_usu=='administrador' && datos.estatus.id==3 && id_res_cuenta==null))
                options += ` <a href="#!" id="${datos.id}" class="btn-procesar-pago btn btn-floating btn-primary tooltipped" data-position="bottom" data-delay="50" data-tooltip="Procesar pago del pedido"><i class="fa fa-dollar-sign"></i></a>`;
            else  if(datos.estatus.id==3 && id_res_cuenta==null && userDOOMI.tipo_usu!='administrador')
                options += ` <a href="#!" style="background: #d01f2a !important;" class="btn btn-floating btn-primary tooltipped" data-position="bottom" data-delay="50" data-tooltip="Procesar pago del pedido"><i class="fa fa-dollar-sign"></i></a>`;

            if(((datos.estatus.id==5 && (datos.modalidad == "Pickup" || datos.modalidad == "Delivery")) || (datos.estatus.id==5 && datos.modalidad == "Programado" && dif_dias==0)) && userDOOMI.tipo_usu!='administrador')
                options += `<a href="#!" class="btn btn-floating btn-primary tooltipped ml-1 btn_general_sts" id="${datos.id}" sts="6" data-position="bottom" data-delay="50" data-tooltip="Iniciar la prepación del pedido"><i class="fas fa-utensils"></i></a>`;
            else if(datos.estatus.id==5 && datos.modalidad == "Programado" && userDOOMI.tipo_usu!='administrador' && dif_dias>0)
                options += `<a href="#!" style="background: #ffc107 !important;" class="btn btn-floating btn-primary tooltipped ml-1" data-position="bottom" data-delay="50" data-tooltip="Esperando para iniciar la prepación del pedido"><i class="fas fa-utensils"></i></a>`;

            if(datos.estatus.id==6 && userDOOMI.tipo_usu!='administrador')
                options += `<a href="#!" class="btn btn-floating btn-primary tooltipped ml-1 btn_general_sts" id="${datos.id}" sts="7" data-position="bottom" data-delay="50" data-tooltip="Finalizar la prepación del pedido"><i class="fas fa-calendar-check"></i></a>`;

            if(datos.estatus.id==8 && userDOOMI.tipo_usu!='administrador' && datos.id_repartidor != null)
                options += `<a href="#!" class="btn btn-floating btn-primary tooltipped ml-1 btn_entregar_pago" id="${datos.id}" sts="9" data-position="bottom" data-delay="50" data-tooltip="Entregar pedido al Repartidor"><i class="fa fa-handshake"></i></a>`;


            if(datos.estatus.id==11)
                options += `<a href="#!" id="${datos.id}" class="btn-ver-calificacion btn btn-floating btn-primary tooltipped ml-1" data-position="bottom" data-delay="50" data-tooltip="Ver calificación del pedido"><i class="fa fa-star"></i></a>`;

            if(userDOOMI.tipo_usu == 'administrador') {
                $("#motivo").attr("disabled", true);

                if(admin_cancelando){
                    $("#motivo").attr("disabled", false);
                    options += ` <a href="#!" id="${datos.id}" class="btn-cancelar-pedido-admin2 btn btn-floating btn-primary tooltipped" data-position="bottom" data-delay="50" data-tooltip="Cancelar pedido"><i class="fa fa-times"></i></a>`;
                }
            }

            $(".btn-accion").html(options);
            $("#md-verPedido").modal('open');
        });
    }

    function imprimir_data_btn(id_ped){
        $(".btn-accion").html('');
        ajaxGET(`pedidos/${id_ped}`, data => {
            const datos = data[0] || data;
            var dif_dias = "";
            if(datos.modalidad == "Programado"){
                fecha1_p = moment();
                fecha2_p = moment(datos.fecha_pro);
                dif_dias = fecha2_p.diff(fecha1_p, 'days');
            }
            options = '';
            if(datos.cuenta)
                var id_res_cuenta = datos.cuenta.idres;
            else
                var id_res_cuenta = 0;
            if(datos.estatus.id==1 && userDOOMI.tipo_usu!='administrador')
                options += ` <a href="#!" id="${datos.id}" class="btn-procesar-pedido btn btn-floating btn-primary tooltipped" data-position="bottom" data-delay="50" data-tooltip="Procesar pedido"><i class="fa fa-check"></i></a>`;

            if((userDOOMI.tipo_usu!='administrador' && datos.estatus.id==3 && datos.id_restaurante==id_res_cuenta) || (userDOOMI.tipo_usu=='administrador' && datos.estatus.id==3 && id_res_cuenta==null))
                options += ` <a href="#!" id="${datos.id}" class="btn-procesar-pago btn btn-floating btn-primary tooltipped" data-position="bottom" data-delay="50" data-tooltip="Procesar pago del pedido"><i class="fa fa-dollar-sign"></i></a>`;
            else  if(datos.estatus.id==3 && id_res_cuenta==null && userDOOMI.tipo_usu!='administrador')
                options += ` <a href="#!" style="background: #d01f2a !important;" class="btn btn-floating btn-primary tooltipped" data-position="bottom" data-delay="50" data-tooltip="Procesar pago del pedido"><i class="fa fa-dollar-sign"></i></a>`;

            if(((datos.estatus.id==5 && (datos.modalidad == "Pickup" || datos.modalidad == "Delivery")) || (datos.estatus.id==5 && datos.modalidad == "Programado" && dif_dias==0)) && userDOOMI.tipo_usu!='administrador')
                options += `<a href="#!" class="btn btn-floating btn-primary tooltipped ml-1 btn_general_sts" id="${datos.id}" sts="6" data-position="bottom" data-delay="50" data-tooltip="Iniciar la prepación del pedido"><i class="fas fa-utensils"></i></a>`;
            else if(datos.estatus.id==5 && datos.modalidad == "Programado" && userDOOMI.tipo_usu!='administrador' && dif_dias>0)
                options += `<a href="#!" style="background: #ffc107 !important;" class="btn btn-floating btn-primary tooltipped ml-1" data-position="bottom" data-delay="50" data-tooltip="Esperando para iniciar la prepación del pedido"><i class="fas fa-utensils"></i></a>`;

            if(datos.estatus.id==6 && userDOOMI.tipo_usu!='administrador')
                options += `<a href="#!" class="btn btn-floating btn-primary tooltipped ml-1 btn_general_sts" id="${datos.id}" sts="7" data-position="bottom" data-delay="50" data-tooltip="Finalizar la prepación del pedido"><i class="fas fa-calendar-check"></i></a>`;

            if(datos.estatus.id==8 && userDOOMI.tipo_usu!='administrador' && datos.id_repartidor != null)
                options += `<a href="#!" class="btn btn-floating btn-primary tooltipped ml-1 btn_general_sts" id="${datos.id}" sts="9" data-position="bottom" data-delay="50" data-tooltip="Entregar pedido al Repartidor"><i class="fa fa-handshake"></i></a>`;


            if(datos.estatus.id==11)
                options += `<a href="#!" id="${datos.id}" class="btn-ver-calificacion btn btn-floating btn-primary tooltipped ml-1" data-position="bottom" data-delay="50" data-tooltip="Ver calificación del pedido"><i class="fa fa-star"></i></a>`;

            $(".btn-accion").html(options);
        });
    }

    function imprimir_data_entregar(id_ped){
        $(".txt-id-e, #list-pedido-e").html('');
        ajaxGET(`pedidos/${id_ped}`, data => {
            const datos = data[0] || data;
            //console.log(datos);
            $('.txt-id-e').html(datos.id);
            var cuerpo_orden = '';
            var acum = 0;
            var tot_ord = 0;
            var tot_adi = 0;
            for (pedidos_pro of datos.ordenes) {
                acum++;
                var adi_ext=0;
                var txt_adi='';
                var txt_aco='';
                if(pedidos_pro.adicionales.length>0){
                    for (adicionales_pedido of pedidos_pro.adicionales) {
                        if(txt_adi=='')
                            txt_adi=adicionales_pedido.cantidad+' '+adicionales_pedido.nombre;
                        else
                            txt_adi+='<br> '+adicionales_pedido.cantidad+' '+adicionales_pedido.nombre;
                    }
                }
                if(pedidos_pro['acompañantes'].length>0){
                    for (acompanantes_pedido of pedidos_pro['acompañantes']) {
                        if(txt_aco=='')
                            txt_aco=acompanantes_pedido.cantidad+' '+acompanantes_pedido.nombre;
                        else
                            txt_aco+='<br> '+acompanantes_pedido.cantidad+' '+acompanantes_pedido.nombre;
                    }
                }
                cuerpo_orden += `
                    <tr>
                        <td>${pedidos_pro.cantidad}</td>
                        <td>${pedidos_pro.menu.nombre}</td>
                        <td><small>${txt_adi}</small></td>
                        <td><small>${txt_aco}</small></td>
                        <td>${pedidos_pro.notas || ''}</td>
                    </tr>
                `;
            };
            $('#list-pedido-e').html(cuerpo_orden);
            $("#md-proEntregar").modal("open");
        });
    }
    $(document).on('click', '.btn-ver-calificacion', function() {
        var id_ped = this.id;
        $('.txt-cli-cal, .txt-nota-cal, .txt-obs-cal').html('');
        document.getElementById("img-cli-cali").src = "../../static/img/favicon.png";
        if(userDOOMI.tipo_usu=='administrador'){
            document.getElementById("img-domi-cali").src = "../../static/img/logo.jpg";
            document.getElementById("img-rep-cali").src = "../../static/img/logo.jpg";
            $('.txt-domi-cal').html("");
        }
        ajaxGET(`pedidos/${id_ped}`, data => {
            const datos = data[0] || data;
            //console.log(datos);
            if(datos.cliente.foto)
                document.getElementById("img-cli-cali").src = datos.cliente.foto;
            if(userDOOMI.tipo_usu=='administrador'){
                if(datos.restaurante[0].img)
                    document.getElementById("img-domi-cali").src = datos.restaurante[0].img;
                $('.txt-domi-cal').html("#"+datos.restaurante[0].id+" "+datos.restaurante[0].nombre);
                if(datos.modalidad=='Delivery'){
                    $('.txt-nota-cal-rep').html(datos.nota_rep);
                    //$('.txt-obs-cal-rep').html('"'+datos.obs_rep+'"');
                    var estrellas = '<a href="#" class=""><i class="fa fa-star fa-2x"></i></a>';
                    if(parseFloat(datos.nota_rep)>=2)
                        estrellas += '<a href="#" class=""><i class="fa fa-star fa-2x"></i></a>';
                    else
                        estrellas += '<a href="#" class=""><i class="far fa-star fa-2x"></i></a>';
                    if(parseFloat(datos.nota_rep)>=3)
                        estrellas += '<a href="#" class=""><i class="fa fa-star fa-2x"></i></a>';
                    else
                        estrellas += '<a href="#" class=""><i class="far fa-star fa-2x"></i></a>';
                    if(parseFloat(datos.nota_rep)>=4)
                        estrellas += '<a href="#" class=""><i class="fa fa-star fa-2x"></i></a>';
                    else
                        estrellas += '<a href="#" class=""><i class="far fa-star fa-2x"></i></a>';
                    if(parseFloat(datos.nota_rep)==5)
                        estrellas += '<a href="#" class=""><i class="fa fa-star fa-2x"></i></a>';
                    else
                        estrellas += '<a href="#" class=""><i class="far fa-star fa-2x"></i></a>';
                    $('.ver-est-rep').html(estrellas);
                    if(datos.repartidor.img)
                        document.getElementById("img-rep-cali").src = datos.repartidor.img;
                    $('.txt-rep-cal').html("#"+datos.repartidor.id+" "+datos.repartidor.nombre+" "+datos.repartidor.apellido);
                    $(".panel_domi_cal").removeClass('s8');
                    $(".panel_domi_cal").addClass('s4');
                    $(".ver_repartidor").show();
                }else{
                    $(".panel_domi_cal").removeClass('s4');
                    $(".panel_domi_cal").addClass('s8');
                    $(".ver_repartidor").hide();
                }

            }
            $('.txt-cli-cal').html("#"+datos.cliente.id+" "+datos.cliente.nombre+" "+datos.cliente.apellido);
            $('.txt-nota-cal').html(datos.nota);
            //$('.txt-obs-cal').html('"'+datos.motivo+'"');
            var estrellas = '<a href="#" class=""><i class="fa fa-star fa-2x"></i></a>';
            if(parseFloat(datos.nota)>=2)
                estrellas += '<a href="#" class=""><i class="fa fa-star fa-2x"></i></a>';
            else
                estrellas += '<a href="#" class=""><i class="far fa-star fa-2x"></i></a>';
            if(parseFloat(datos.nota)>=3)
                estrellas += '<a href="#" class=""><i class="fa fa-star fa-2x"></i></a>';
            else
                estrellas += '<a href="#" class=""><i class="far fa-star fa-2x"></i></a>';
            if(parseFloat(datos.nota)>=4)
                estrellas += '<a href="#" class=""><i class="fa fa-star fa-2x"></i></a>';
            else
                estrellas += '<a href="#" class=""><i class="far fa-star fa-2x"></i></a>';
            if(parseFloat(datos.nota)==5)
                estrellas += '<a href="#" class=""><i class="fa fa-star fa-2x"></i></a>';
            else
                estrellas += '<a href="#" class=""><i class="far fa-star fa-2x"></i></a>';
            $('.ver-est').html(estrellas);
            $("#md-calificacion").modal('open');
        });
    });

    if(userDOOMI.tipo_usu == 'administrador'){
        $(document).on('click', '.btn-cancelar-pedido-admin', function() {
            const id_pedido = $(this).attr('id');
            imprimir_data(id_pedido, true);
        });
        $(document).on('click', '.btn-cancelar-pedido-admin2', function() {
            const id_pedido = $(this).attr('id');
            let toastContent = '<span>¿Desea cancelar éste pedido?</span><br><button class="btn-flat toast-action si-cancelar-pedido-admin" id="'+id_pedido+'">Si</button><button class="btn-flat toast-action" onclick=" $(\'.toast\').hide(); ">No</button>';
            Materialize.toast(toastContent, 4000);
        });


        $(document).on('click', '.si-cancelar-pedido-admin', function() {
            $('.toast').hide();
            const id_pedido = $(this).attr('id');
            const motivo = $("#motivo").val().trim();

            if(!motivo)
                return Materialize.toast('Por favor ingrese el motivo', 3000);

            $.ajax({
                url: dominio + 'pedidos/'+id_pedido,
                headers: {
                    'token':userDOOMI.token,
                    'tipo_usuario': userDOOMI.tipo_usu,
                },
                type: 'DELETE',
                data: {motivo},
                success: function(data) {
                    if (data.msj) {
                        Materialize.toast(data.msj, 4000);
                        $("#md-verPedido").modal('close');
                        $("#motivo").val('');
                        recuperar_pedidos();
                        imprimir_data_btn(id_pedido);
                    } else {
                        Materialize.toast(data.msj, 4000);
                    }
                }
            })

            // ajaxDELETE('/pedidos/'+id,
            //     resp => {let _=resp.r && Materialize.toast('Pedido cancelado',4000); recuperar_pedidos()},
            //     err => Materialize.toast('Ocurrió un error', 4000)
            // );
        })
    }

    var id_pedido = 0;
    $(document).on('click', '.btn-procesar-pedido', function() {
        id_pedido = this.id;
        $('.txt-id-pro').html(id_pedido);
        $("#md-proPedido").modal('open');
    });

    $(document).on('click', '#bta, #btr', function() {
        var tipo = this.id;
        if(tipo=='bta'){
            tipo_txt = 'Aceptar';
            ids = 4;
        }else{
            tipo_txt = 'Rechazar';
            ids = 99;
        }
        if($("#motivo_ped").val().trim().length<5 && tipo=='btr')
            Materialize.toast('Debes agregar una nota para el cliente', 4000);
        else{
            var toastContent = '<span>¿Desea '+tipo_txt+' éste pedido?</span><br><button class="btn-flat toast-action conf_si" id="'+ids+'">Si</button><button class="btn-flat toast-action" onclick=" $(\'.toast\').hide(); ">No</button>';
            Materialize.toast(toastContent, 10000);
        }
    });

    $(document).on('click', '.conf_si', function() {
        ids = this.id;
        var data = {
            sts: ids,
            nota: $("#motivo_ped").val(),
            iden: id_pedido
        };

        $('.toast').hide();
        $.ajax({
            url: dominio + 'pedidos/edit_sts/',
            headers: {
                'token':userDOOMI.token,
                'tipo_usuario': userDOOMI.tipo_usu
            },
            type: 'PUT',
            data: data,
            success: function(data) {
                if (data.msj) {
                    Materialize.toast(data.msj, 10000);
                    $("#md-proPedido").modal('close');
                    $("#motivo_ped").val('');
                    recuperar_pedidos();
                    imprimir_data_btn(id_pedido);
                } else {
                    Materialize.toast(data.msj, 10000);
                }
            }
        })

    });

    $(document).on('click', '.btn-procesar-pago', function() {
        id_pedido = this.id;
        $('.txt-id-pro-pago').html(id_pedido);
        $('.txt_pago_m, .txt_pago_c, .txt_pago_r').html('');
        $('.img_pago').hide();
        $('#motivo_ped_p').val('');
        ajaxGET(`pedidos/${id_pedido}`, data => {
            //$('.txt_pago_m').html(data.cuenta.metodo);
            $('.txt_pago_c').html(data.cuenta.cuenta);
            $('.txt_pago_r').html(data.referencia);
            if(data.img_pago){
                $(".img_pago_i").attr("src",data.img_pago);
                $('.img_pago').show();
            }
            $("#md-proPedido-pago").modal('open');
        });

    });

    $(document).on('click', '#btap, #btrp', function() {
        var tipo = this.id;
        if(tipo=='btap'){
            tipo_txt = 'Aceptar';
            //ids = 5;
            ids = 7;
        }else{
            tipo_txt = 'Rechazar';
            ids = 97;
        }
        if($("#motivo_ped_p").val().trim().length<5 && tipo_txt == 'Rechazar')
            Materialize.toast('Debes agregar una nota para el cliente', 4000);
        else{
            var toastContent = '<span>¿Desea '+tipo_txt+' el pago de éste pedido?</span><br><button class="btn-flat toast-action conf_si_p" id="'+ids+'">Si</button><button class="btn-flat toast-action" onclick=" $(\'.toast\').hide(); ">No</button>';
            Materialize.toast(toastContent, 10000);
        }
    });

    $(document).on('click', '.conf_si_p', function() {
        ids = this.id;

        if(userDOOMI.tipo_usu=='administrador')
            id_usu = userDOOMI.id;
        else
            id_usu = userDOOMI.id_restaurante;
        var data = {
            sts: ids,
            nota: $("#motivo_ped_p").val(),
            iden: id_pedido,
            id_usu: id_usu,
            tipo_usu: userDOOMI.tipo_usu
        };

        $('.toast').hide();
        $.ajax({
            url: dominio + 'pedidos/edit_sts/',
            headers: {
                'token':userDOOMI.token,
                'tipo_usuario': userDOOMI.tipo_usu
            },
            type: 'PUT',
            data: data,
            success: function(data) {
                if (data.msj) {
                    Materialize.toast(data.msj, 10000);
                    $("#md-proPedido-pago").modal('close');
                    $("#motivo_ped").val('');
                    recuperar_pedidos();
                    imprimir_data_btn(id_pedido);
                } else {
                    Materialize.toast(data.msj, 10000);
                }
            }
        })

    });

    $(document).on('click', '.btn_general_sts', function() {
        id_pedido = this.id;
        var ids = $(this).attr("sts");
        if(ids=='6')
            tipo_txt = 'Desea iniciar la preparación del pedido';
        else if(ids=='9')
            tipo_txt = 'Desea entregar el pedido al repartidor';
        else
            tipo_txt = 'Desea finalizar la preparación del pedido';
        var toastContent = '<span>¿'+tipo_txt+'?</span><br><button class="btn-flat toast-action conf_si_g" id="'+ids+'">Si</button><button class="btn-flat toast-action" onclick=" $(\'.toast\').hide(); ">No</button>';
        Materialize.toast(toastContent, 10000);
    });

    $(document).on('click', '.conf_si_g', function() {
        ids = this.id;
        var data = {
            sts: ids,
            nota: '',
            iden: id_pedido
        };

        $('.toast').hide();
        $.ajax({
            url: dominio + 'pedidos/edit_sts/',
            headers: {
                'token':userDOOMI.token,
                'tipo_usuario': userDOOMI.tipo_usu
            },
            type: 'PUT',
            data: data,
            success: function(data) {
                if (data.msj) {
                    Materialize.toast(data.msj, 10000);
                    $("#md-proPedido-pago").modal('close');
                    $("#motivo_ped").val('');
                    recuperar_pedidos();
                    imprimir_data_btn(id_pedido);
                } else {
                    Materialize.toast(data.msj, 10000);
                }
            }
        })

    });

    $(document).on('click', '.btn_entregar_pago', function() {
        id_pedido = this.id;
        imprimir_data_entregar(id_pedido);
    });
    $(document).on('click', '#btr_e', function() {
        $("#md-proEntregar").modal('close');
    });
    $(document).on('click', '.conf_si_en', function() {
        var data = {
            sts: 9,
            nota: '',
            iden: id_pedido
        };

        $('.conf_si_en').attr("disabled",true);
        $.ajax({
            url: dominio + 'pedidos/edit_sts/',
            headers: {
                'token':userDOOMI.token,
                'tipo_usuario': userDOOMI.tipo_usu
            },
            type: 'PUT',
            data: data,
            success: function(data) {
                $('.conf_si_en').attr("disabled",false);
                if (data.msj) {
                    Materialize.toast(data.msj, 10000);
                    $("#md-proEntregar").modal('close');
                    recuperar_pedidos();
                } else {
                    Materialize.toast(data.msj, 10000);
                }
            }
        })

    });

    if ( getop("id") )
        imprimir_data(getop("id"));
    $('.tooltipped').tooltip();
};
