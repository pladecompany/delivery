function repartidoresfavoritosjs()
{

  function repartidores_favoritos() {
    ajaxGET(
      `/restaurantes/${userDOOMI.id_restaurante}/repartidor-favorito`,
      data => actualizar_repartidores_favoritos(data)
    );
  };
  function select_tipo_vehiculos() {
    ajaxGET('/tipovehiculo', data => {
      const veh = $('#vehiculo').html('');
      let html = '<option value="" selected>Seleccione</option>';
      for (let tipo of data) {
        html += `<option value="${tipo.id}">${tipo.nombre_veh}</option>`;
      }
      veh.html(html);
    });
  }

  async function actualizar_repartidores_favoritos(data) {
    const table = $('.table').DataTable({
        "language": {
            "url": "../static/<lib></lib>/JTables/Spanish.json"
        }
    });

    table.clear().draw();


    let rep, row;
    let count = 0;
    for (fav of data)
    {
      table.row.add([
        ++count,
        fav.repartidor.identificacion +'-'+ fav.repartidor.cedula,
        (fav.repartidor?.nombre +' '+ fav.repartidor?.apellido) || '',
        fav.repartidor.id,
        `<a href="#!" id="${fav.id}" class="waves-effect btn btn-primary btn-floating tooltipped quitar" data-position="bottom" data-delay="50" data-tooltip="Quitar de favoritos">
            <i class="fa fa-times"></i>
        </a>`
      ]).draw().node();
    }
    $('.tooltipped').tooltip();

  };
  repartidores_favoritos();
  select_tipo_vehiculos();


  $('#btn-agregar').on('click', function(evt) {
    evt.preventDefault();

    const tipoveh = $('#vehiculo option:selected').val();
    const identificacion = $('#identificacion').val().trim();
    const cedula = $('#cedula').val().trim();

    let err = false;
    if (!tipoveh)
      err = 'Seleccione el tipo de vehículo';

    else if (!identificacion)
      err = 'Seleccione un tipo de identificación';
      // return Materialize.toast('Seleccione un tipo de identificación', 2000);

    else if (!cedula || cedula.length < 7)
      err = 'Ingrese un número válido';
      // return Materialize.toast('Ingrese un número válido', 2000);

    if (err)
      return Materialize.toast(err, 2000);

    $('#btn-agregar').attr('disabled', true);
    const datos = new FormData();
    datos.set('id_restaurante', userDOOMI.id_restaurante);
    datos.set('identificacion', identificacion);
    datos.set('cedula', cedula);
    datos.set('tipo_vehiculo', tipoveh);

    $.ajax({
      url: dominio + 'restaurantes/repartidor-favorito',

      headers: {
          'token': userDOOMI.token,
          'tipo_usuario': userDOOMI.tipo_usu
      },
      type: 'POST',
      dataType: "json",
      cache: false,
      data: datos,
      contentType: false,
      processData: false,
      success: (resp) => {
        $('#btn-agregar').attr('disabled', false);

        if (resp.r) {
          Materialize.toast('Repartidor agregado', 2000);
          $('#md-agregar').modal('close');
          repartidores_favoritos();
        }
        else if (resp.hasOwnProperty('r') && (resp.msj || resp.msg))
          Materialize.toast(resp.msj || resp.msg, 2000);
        else
          Materialize.toast('Ocurrió un error', 2000);

        $('.tooltipped').tooltip();
      },
      error: (xhr, status, error) => {
        $('#btn-agregar').attr('disabled', false);
        $('.tooltipped').tooltip();
        Materialize.toast('Ocurrió un error', 2000);
      },
    });
    return false;
  });

  $('#cedula').on('input change propertychange', function() {
    this.value = extraerDigitos(this.value);
  });

  $(document).on('click', '.quitar', function() {
    const idf = this.id;
    const toastContent = `<span>¿Quitar de favoritos?</span><br><button class="btn-flat toast-action quitar_si" id="${idf}">Si</button><button class="btn-flat toast-action" onclick=" $('.toast').hide(); ">No</button>`;
    Materialize.toast(toastContent, 4000);
  });
  $(document).on('click', '.quitar_si', function() {
    const idf = this.id;

    $('.toast').hide();
    ajaxDELETE('/restaurantes/repartidor-favorito/'+idf, (r) => {
      Materialize.toast('Datos actualizados', 2000);
      repartidores_favoritos();
    },
    (xhr, status, error) => {
      Materialize.toast('Ocurrió un error', 2000);
    });
  });



}
