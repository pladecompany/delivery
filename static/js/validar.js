formato = {

    formatear: function(num, op) {
        if (op == null || op == "undefined") {
            separador = "."; // separador para los miles
            sepDecimal = ','; // separador para los decimales
        } else {
            separador = "-"; // separador para los miles
            sepDecimal = '.'; // separador para los decimales
        }
        num += '';
        var splitStr = num.split('.');
        var splitLeft = splitStr[0];
        var splitRight = splitStr.length > 1 ? sepDecimal + splitStr[1] : '';
        var regx = /(\d+)(\d{3})/;
        while (regx.test(splitLeft)) {
            splitLeft = splitLeft.replace(regx, '$1' + separador + '$2');
        }
        return this.simbol + splitLeft + splitRight;
    },
    precio: function(num, op, simbol) {
        this.simbol = simbol || '';

        if (num == null || !num) {
            num = 0;
        }
        var num = num.toString();
        num = num.replace(/,/g, ".");

        num = Math.round(num * 100) / 100;


        return this.formatear(num.toFixed(2), op).replace(/-/g, "");

    },
    precio1: function(num, op, simbol) {
        this.simbol = simbol || '';

        if (num == null || !num) {
            num = 0;
        }
        var num = num.toString();
        num = num.replace(/,/g, ".");

        num = Math.round(num * 100) / 100;


        return this.formatear(num.toFixed(1), op).replace(/-/g, "");

    }
}

$(document).on('ready', function() {
    $(".text").validCampo("abcdefghijklmnñopqrstuvwxyz ");
    $(".text-2").validCampo('abcdefghijklmnñopqrstuvwxyz ".,!?-:;/|1234567890');
    $(".number").validCampo("1234567890");
    $(".number-2").validCampo("1234567890.");
    $(".text-number-").validCampo("1234567890abcdefghijklmnñopqrstuvwxyz");



});

function numeros(id, numero, max) {
    if (!/^([0-9])*$/.test(numero)) {
        $('#' + id + '').val('');
    } else {
        if (max <= 0)
            max = 1000;
        if (numero.length > max) {
            var num = numero.substr(0, max);
            $('#' + id + '').val(num);
        }
    }
}

function letras(id, letra) {
    if (!/^[a-zA-Z]*/.test(letra)) {
        $('#' + id + '').val('');
    }
}

var objeto2;

function decimales(objeto, e) {
    var keynum
    var keychar
    var numcheck
    if (window.event) {
        /*/ IE*/
        keynum = e.keyCode
    } else if (e.which) {
        /*/ Netscape/Firefox/Opera/*/
        keynum = e.which
    }
    if ((keynum >= 35 && keynum <= 37) || keynum == 8 || keynum == 9 || keynum == 46 || keynum == 39) {
        return true;
    }
    if (keynum == 188 || (keynum >= 95 && keynum <= 105) || (keynum >= 48 && keynum <= 57)) {
        posicion = objeto.value.indexOf(',');
        if (posicion == -1) {
            return true;
        } else {
            if (!(keynum == 188)) {
                objeto2 = objeto;
                t = setTimeout('dosDecimales()', 20);
                return true;
            } else {
                objeto2 = null;
                return false;
            }
        }
    } else {
        return false;
    }
}

function dosDecimales() {
    var objeto = objeto2;
    var posicion = objeto.value.indexOf(',');
    var decimal = 2;
    if (objeto.value.length - posicion < decimal) {
        objeto.value = objeto.value.substr(0, objeto.value.length - 1);
    } else {
        objeto.value = objeto.value.substr(0, posicion + decimal + 1);
    }
    return;
}