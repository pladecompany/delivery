var raiz = indexpanel;


//Ruta
function getop(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
//cargar mapa segun rutas especificas
if(getop("op")=='doomialiados')
    document.write('<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyBUPAHemQKCzVuIBCYTJ9AvsEtZDkfOOjY&libraries=places&region=mx&sensor=false&amp;language=es"></script>');
//document.write("<script src='"+raiz+"static/js/control.js'></script>");

var xhr = new XMLHttpRequest();
if(!getop("op")) {
    op = "inicio";
}else {
    op = getop("op");
}

xhr.open('GET', ' '+op+'.html', true);

xhr.onload = function (d) {
        document.getElementById('ruta').innerHTML=d.currentTarget.response;
        $('.table').DataTable( {
            "aaSorting": [],
            "language": {
            "url": "../../static/lib/JTable/Spanish.json"
            }
        } );
        $('.table').DataTable( {
            "aaSorting": [],
            responsive: true
        } );

        $('.modal').modal();
        $("select").css("display","block");

        $( "#openNotificaciones" ).click(function() {
            $( "#notifications-dropdown" ).toggle();
        });

        $(".button-collapse").sideNav();
        $('.tooltipped').tooltip();
        $('.tabs').tabs();


        //configuracion de nivel de usuario
        if(localStorage.getItem('userDOOMI')){
            if(userDOOMI.img){
                document.getElementById("img-menu").src = userDOOMI.img;
            }
        }
        //console.log(userDOOMI);
        if (userDOOMI.tipo_usu == "administrador") {
            $(".view-admin").removeClass('hide');
            $(".tip_niv_usu").html(userDOOMI.text_tipo_usu);
        }else if (userDOOMI.tipo_usu == "restaurante") {
            $(".view-resta").removeClass('hide');
            txt_rest = '';
            if(userDOOMI.restaurante[0]){
                $(".tasa_res").html(formato.precio(userDOOMI.restaurante[0].pre_tasa));
                txt_rest = ' ('+userDOOMI.restaurante[0].nombre+')';
                if(userDOOMI.restaurante[0].img)
                    document.getElementById("logo-menu").src = userDOOMI.restaurante[0].img;
                /*
                if(userDOOMI.restaurante[0].estatus==0){
                    $(".metodosdepago-a, .cuentas-a").attr("href","#!");
                    $(".metodosdepago-a, .cuentas-a").attr("onclick",'Materialize.toast("Modulo desactivado por el Administrador de la Aplicación", 5000);');
                }
                */
            }
            $(".tip_niv_usu").html(userDOOMI.text_tipo_usu+txt_rest+" - <b>"+userDOOMI.cedula+"</b>");
        }

        // Si hay un mensaje inicial.
        if(sessionStorage['firstToast']) {
            Materialize.toast(sessionStorage['firstToast'], 10_000);
            delete sessionStorage['firstToast'];
        }

        //listar administradores
        if(op=='administradores'){
            listar_credenciales();
        }
        //listar repartidores
        if(op=='repartidores'){
            listar_repartidores();
        }
        //listar restaurantes
        if(op=='doomialiados'){
            listar_restaurantes();
        }
        //listar tipo vehiculo
        if(op=='tipodevehiculos'){
            listar_tipovehiculo();
        }
        // Listar categorias
        if(op=='categorias'){
            if (userDOOMI.tipo_usu=="administrador")
                listar_categorias();
            if (userDOOMI.tipo_usu=="restaurante")
                listar_categorias_menu();
        }
        // Listar subcategorias.
        if(op=='subcategorias'){
            listar_subcategorias();
        }
        // Listar menus
        if(op=='menu'){
            listar_menus();
        }
        // Listar menus
        if(op=='listadomenu'){
            listar_menus_admin();
        }
        //listar metodos
        if(op=='metodosdepago'){
            /*
            if (userDOOMI.tipo_usu == "restaurante") {
                if(userDOOMI.restaurante[0]){
                    if(userDOOMI.restaurante[0].estatus==0){
                        location.href = 'index.html?op=inicio';
                    }
                }
            }
            */
            metodos_pagosjs();
        }
        //listar cuentas
        if(op=='cuentas'){
            /*
            if (userDOOMI.tipo_usu == "restaurante") {
                if(userDOOMI.restaurante[0]){
                    if(userDOOMI.restaurante[0].estatus==0){
                        location.href = 'index.html?op=inicio';
                    }
                }
            }
            */
            cuentasjs();
        }

        if (op == "inicio") {
            iniciojs();
        }

        if (op == "perfilnegocio") {
            perfilNegocio();
        }
        if (op == "pedidos_panel") {
           $('#side-out, .switch-res').hide();
           $('.cont_busc').show();
           $('.collapsible').collapsible('open', 0);
           pedidos_paneljs();
        }
        if (op == "perfilencargado") {
            perfilEncargado();
        }
        if (op == "cambiarclave") {
            cambiarclavejs();
        }
        if (op == 'clientes'){
            clientesjs();
        }
        if (op == 'pedidosrestaurant' || op == 'pedidos'){
            pedidosjs();
        }
        if (op == 'reportes'){
            reportesjs();
        }
        if (op == 'preguntasfrecuentes')
            preguntasfrecuentesjs();

        if (op == 'repartidoresfavoritos')
            repartidoresfavoritosjs();



        listar_notificaciones();
        if( getop("id") || op == "perfil") $("label").addClass("active");

        $('.Amount').maskNumber({decimal:',',thousands:'.'});
        $('.Amount2').mask('##0,0', {reverse: true});
        $(".text").validCampo("abcdefghijklmnñopqrstuvwxyz ");
        $(".text-2").validCampo('abcdefghijklmnñopqrstuvwxyz ".,!?-:;/|1234567890');
        $(".number").validCampo("1234567890");
        $(".number-2").validCampo("1234567890.");
        $(".text-number-").validCampo("1234567890abcdefghijklmnñopqrstuvwxyz");
        $(".telefono-internacional")
            .mask("+## (###) ###-####")
            .on("input change", function() {
                this.value = (this.value || '').substr(0,18); //18 caracteres, incluye mask.
            });
    //FUNCIONES PARA LA TASA EN RESTAURANTE Y TAMBIEN EL CERRAR Y ABRIR
    if (userDOOMI.tipo_usu == "restaurante") {
       tasajs();
       horariojs();
    }
    if (userDOOMI.tipo_usu == "administrador") {
       configuracionjs();
    }
    $(document).on('click', 'table.dataTable.dtr-inline.collapsed>tbody>tr>td:first-child', function() {
        $('.tooltipped').tooltip();
    });
};
xhr.send(null);
