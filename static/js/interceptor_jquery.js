/*
 * Intercepta los errores de Jquery para cerrar sesion en caso de error.
*/
$( document ).ajaxError(function( event, jqxhr, settings ) {
	if(jqxhr.status == 401 && settings.url.startsWith(dominio)){
		sessionStorage.error_autenticacion=true;
		salir();
	}
});
