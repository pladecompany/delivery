var socket = io(dominio);
//datatables
if(typeof userDOOMI == "undefined"){
    userDOOMI = {};
}
$('.table').DataTable( {
    "language": {
    "url": "../../static/lib/JTable/Spanish.json"
    }
} );
$('.table').DataTable( {
    responsive: true
} );

$('.modal').modal();
$("select").css("display","block");

$( "#openNotificaciones" ).click(function() {
  $( "#notifications-dropdown" ).dropdown();
});

$(".button-collapse").sideNav();
$('.tooltipped').tooltip();
//FIN datatables

//función para calcular edad a partir de una fecha
function calcularEdad(fecha) {

    var hoy = new Date();
    var cumpleanos = new Date(fecha);
    var edad = hoy.getFullYear() - cumpleanos.getFullYear();
    var m = hoy.getMonth() - cumpleanos.getMonth();

    if (m < 0 || (m === 0 && hoy.getDate() < cumpleanos.getDate())) {
        edad--;
    }
    return edad;

}

//retornar fecha formato yyyy--mm-dd
function retornar_fecha_ymd(fecha){
    var fecha_retornar = new Date(fecha)

    var mes = ""; var dia = "";
    if ( (fecha_retornar.getMonth()+1) < 10 ) {mes = "0"+(fecha_retornar.getMonth()+1)} else { mes = (fecha_retornar.getMonth()+1) }
    if ( ( fecha_retornar.getDate() ) < 10 ) {dia = "0"+(fecha_retornar.getDate())} else { dia = (fecha_retornar.getDate()) }

    fecha_retornar = fecha_retornar.getFullYear()+"-"+mes+"-"+dia;
    return fecha_retornar
}
//retornar fecha formato dd--mm-yyyy
function retornar_fecha_dmy(fecha, sumar_a_fecha){
    var fecha_retornar = new Date(fecha)

    //console.log(fecha_retornar);
    if (sumar_a_fecha) {
        dia = fecha_retornar.setDate(fecha_retornar.getDate() + 1)
        //dia = dias+1;
        //if(dia<10){ dia = "0"+dia; }
    }

    var mes = ""; var dia = "";
    if ( (fecha_retornar.getMonth()+1) < 10 ) {mes = "0"+(fecha_retornar.getMonth()+1)} else { mes = (fecha_retornar.getMonth()+1) }
    if ( ( fecha_retornar.getDate() ) < 10 ) {dia = "0"+(fecha_retornar.getDate())} else { dia = (fecha_retornar.getDate()) }

    fecha_retornar = dia+"-"+mes+"-"+fecha_retornar.getFullYear();
    return fecha_retornar
}

//finalizar sesión de usuarios
function salir(){
    localStorage.removeItem('userDOOMI');
    location.href = '../?op=iniciarsesion';
};

function Quitarcoma(value) {
    if(value){
        for (var i = 0; i < value.split(".").length+10; i++) {
            value = value.replace(".", "");
        }
        value = value.replace(",", ".");
    }
    if(value.length=="3" || value.length=="2"){
        if(value.split(".").length==0)
            value = "0."+""+value;
        else
        value = "0"+""+value;
    }
    return parseFloat(value);
}

function verificarRIF(rif) {
    /*
    Formato de RIF: X-00000000-N || X00000000N
    La expresion regular:

    ^             : Principio de texto.
    [JGVEPjgvep]  : La letra de la nacionalidad (X), mayuscula o minuscula.
    [-]?          : Puede o no haber un guion.
    [0-9]{7-9}    : Numeros, de 7 a 9 digitos.
    [0-9]{1}      : El digito de chequeo (N).
    $             : Fin de texto.
    */
    return (/^[JGVEPjgvep][-]?[0-9]{7,9}[-]?[0-9]{1}$/).test(rif);
}

function extraerDigitos(str) {
    // extraerDigitos('h01a mund0') -> '010'
    return (str.match(/\d/g) || []).join('');
}

function _parseAjaxSettings(args) {
    /*
    Busca los siguientes parametros y los ajusta para $.ajax

    ruta/route:  'modulo/',
    datos/data:  FormData,
    exito/success:  function(){},
    error:  function(){}
    tipo/type:  'GET' || 'POST' || 'PUT' || 'DELETE',
    */
    settings = {
        headers: {
            'token': userDOOMI.token,
            'tipo_usuario': userDOOMI.tipo_usu,
            'id_usuario': userDOOMI.id
        },
    };

    ruta = args.ruta || args.route;

    if (ruta) // Quitar '/' al principio en caso de tenerlo.
        ruta = ruta.slice(0,1)=='/' ? ruta.slice(1) : ruta;

    if(ruta) settings.url = dominio + ruta;
    if(args.tipo || args.type || args.method) settings.type = args.tipo || args.type || args.method;
    if(args.datos || args.data) {
        settings.data = args.datos || args.data;
        settings.cache = false;
        settings.dataType = 'json';
        settings.contentType = false;
        settings.processData = false;
    }

    if(args.success || args.exito) settings.success = (args.exito || args.success);
    if(args.error) settings.error = args.error;

    return settings;
}

function enviarForm(args) {
    /*
    ** NOTA: no usar funciones flecha para success y error,
    **       no mantienen el contexto del codigo.
    */

    settings = _parseAjaxSettings(args);

    settings.success = function exitoEnviandoForm(data) {
        $('.bt_save').prop('disabled', false); // Activar boton al finalizar.
        if(args.success)
            return args.success(data);
    }

    settings.error = function errorEnviandoForm(xhr, status, errorThrown) {
        $('.bt_save').prop('disabled', false); // Activar boton al finalizar.
        if (args.progressBar){
            $('.fileprogress').addClass('hide');
            $('.fileprogress').find('div')[0].style.width = '0%';
        }
        if(args.error)
            return args.error(xhr, status, errorThrown);
        else
            return console.log('Error al enviar form:', errorThrown);
    }

    if(args.progressBar){
        settings.xhr = function(){
            var xhr = new window.XMLHttpRequest();
            xhr.upload.addEventListener('progress', function(e){
                if (e.lengthComputable) {
                  var percentComplete = parseInt( (e.loaded / e.total * 100), 10);
                  $('.fileprogress').find('div')[0].style.width = `${percentComplete}%`;
                  if(percentComplete>=100){
                    $('.fileprogress').addClass('hide');
                    $('.fileprogress').find('div')[0].style.width = '0%';
                  }
                }
            }, false);
            xhr.addEventListener('progress', function(e){
                if (e.lengthComputable) {
                    var percentComplete = parseInt( (e.loaded / e.total * 100), 10);
                    $('.fileprogress').find('div')[0].style.width = `${percentComplete}%`;
                    if(percentComplete>=100){
                        $('.fileprogress').addClass('hide');
                        $('.fileprogress').find('div')[0].style.width = '0%';
                    }
                }
            }, false);
            return xhr;
        }

        $('.fileprogress').removeClass('hide');
        $('.fileprogress').find('div')[0].style.width = '0%';
    }

    // Desactivar boton del form antes de enviar.
    $('.bt_save').prop('disable', true);

    return $.ajax(settings);
}


function _ajax(args) {
    if(!args) return;

    settings = _parseAjaxSettings(args);

    if (args.success) settings.success = args.success;

    if (args.error) settings.error = args.error;
    else
        settings.error = (xhr, status, errorThrown) => {
            console.log(`ajax${settings.type} ERROR:`, errorThrown);
        }

    return $.ajax(settings);
}
// Para las funciones success y error: EVITAR USAR FUNCIONES FLECHA.
function ajaxGET(ruta, success, error){ return _ajax({tipo:'GET', ruta, success, error}) };
function ajaxDELETE(ruta, success, error){ return _ajax({tipo:'DELETE', ruta, success, error}) };
function ajaxPOST(ruta, data, success, error){ return _ajax({tipo:'POST', ruta, data, success, error}) };
function ajaxPUT(ruta, data, success, error){ return _ajax({tipo:'PUT', ruta, data, success, error}) };


$('.materialboxed').materialbox();
//$('.Amount').maskNumber();
$(document).on('blur', '.Amount', function(){
    if(this.value.trim().length<=3){
        if(this.value.trim().length==3)
            this.value = formato.precio(this.value);
        }
});
//limite = debe ser enviado en tamaño kb
subirImg = function subirImg(input,div,limite) {
    if (input.files && input.files[0]) {
        if(limite){
            lim = parseFloat(limite)*1024;
            if(lim<input.files[0].size){
                Materialize.toast("La imagen seleccionada no debe ser mayor a "+limite+" Kbs", 4000);
                input.value="";
                return;
            }
        }
        var reader = new FileReader();
        reader.onload = function(e) {
            $(div).attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

ultimas_notis = function ultimas_notis(){
    tipo_usu = userDOOMI.tipo_usu;
    if(tipo_usu=='administrador'){
        tipo_usu = 'administradores';
        id_usu = userDOOMI.id;
    }else{
        tipo_usu = 'restaurantes';
        id_usu = userDOOMI.id_restaurante;
    }
    $.ajax({
        url: dominio + "notificaciones/",
        type: "GET",
        data: {
            limit: 5,
            tipo_usuario: tipo_usu,
            id: id_usu
        },
        success: function(data){
            var html="";
            $(".notis_count").html(" "+data.count[0].count);
            for (var i = 0; i < data.noti.length; i++) {
                datos = data.noti[i];
                html += `<li class="boxNotificaciones">
                    <a href="#!" noti="${datos.id}" tipo="${datos.tipo_noti}" url_noti="${datos.redireccionar_noti}" id_usable="${datos.id_usable}" class="clr_black row p-1 vernotificacion">
                        <div class="col s2 text-right"><img src="${datos.img_c || datos.img_r || datos.img_re || '../../static/img/user.png'}" onerror="this.src ='../../static/img/user.png'" id="img-menu" style="border-radius: 50%;" width="40px" height="40px"></div>
                        <div class="col s10"><p class="m-0 info"><b>${datos.nombre_c || datos.nombre_r || datos.nombre_re || 'Administración'}</b> <br> ${datos.contenido}</p></div>
                    </a>
                    <div class="timeago text-right p-0">${moment(datos.fecha).toNow()}</div>
                </li>`;
            }
            if(data.noti.length==0){
                $(".notis_count").removeClass("alert-red");
                html += '<li class="boxNotificaciones"><a href="#!" class="clr_black p-1 text-center"> Sin notificaciones pendientes</a></li>';
            }else{
                html +='<li><a href="index.html?op=notificaciones" class="text-center"><small class="clr_first-color">Ver todas</small></a></li>';
                $(".notis_count").addClass("alert-red");
            }

            $("#notifications-dropdown").html(html);

        },
        error: function(err) {
            console.log(err);
        }
    });

}
ultimas_notis();

pruebaspush = function pruebaspush(){

    $.ajax({
        url: dominio + "notificaciones/pruebas",
        type: "POST",
        success: function(data){
            console.log(data);

        },
        error: function(err) {
            console.log(err);
        }
    });

}
//pruebaspush();
//EVENTO CUANDO SE RECIBE EMIT DESDE EL API
socket.on('RecibirNotificacion', function(data) {
    if(userDOOMI.tipo_usu=='administrador' || (userDOOMI.tipo_usu!='administrador' && userDOOMI.id_restaurante==data.id_receptor && data.tabla_usuario_r=='restaurantes')) {
        ultimas_notis();
        if (userDOOMI.tipo_usu!='administrador'){
            play_notification();
        }
    }
});


navigator.serviceWorker.register('/static/js/sw.js');
Notification.requestPermission(function(result) {
  if (result === 'granted') {
    navigator.serviceWorker.ready.then(function(registration) {
      registration.showNotification('Notification with ServiceWorker');
    });
  }
  else if (sessionStorage.getItem('alertnotif')!=1) {
    sessionStorage.setItem('alertnotif', 1);
    alert('Las notificaciones estan desactivadas');
  }
});


// Notification.requestPermission().then(function(result){
//     if (result!='granted' && sessionStorage.getItem('alertnotif')!=1) {
//         sessionStorage.setItem('alertnotif', 1);
//         alert('Las notificaciones estan desactivadas');
//     }
// });

socket.removeAllListeners('doomi:'+userDOOMI.id_restaurante);
socket.removeAllListeners('admin:'+userDOOMI.id);
socket.removeAllListeners('admin');
if (userDOOMI.tipo_usu == 'restaurante') {
    socket.on('doomi:'+userDOOMI.id_restaurante, noti => {
        spawnNotification(noti.titulo, noti.cuerpo)
            .then(evt => {
                if (noti.href) {
                    location.href = href
                }
                evt.target.close();
            });
    });
}
else if (userDOOMI.tipo_usu == 'administrador') {
    socket.on('admin', noti => {
        spawnNotification(noti.titulo, noti.cuerpo)
            .then(evt => {
                if (noti.href) {
                    location.href = href
                }
                evt.target.close();
            });
    });
    socket.on('admin:'+userDOOMI.id, noti => {
        spawnNotification(noti.titulo, noti.cuerpo)
            .then(evt => {
                if (noti.href) {
                    location.href = href
                }
                evt.target.close();
            });
    });
}

function completarPaso(paso){
    if(!+paso===+paso) return;

    // Saltar si el paso ya está completado.
    const flag = (userDOOMI.restaurante[0].pasos & 2**(paso-1)) === 2**(paso-1);
    if(flag) return;
    console.log('Paso completado:', paso);
    const data = new FormData();
    data.append('id_res', userDOOMI.restaurante[0].id);
    data.append('paso', paso);

    $.ajax({
        url: dominio + 'restaurantes/paso',
        method: 'PATCH',
        headers: {
            'token':userDOOMI.token,
            'tipo_usuario': userDOOMI.tipo_usu
        },
        data,
        contentType: false,
        processData: false,
        success: data => {
            $('#progress-paso-'+paso).css('width', '100%');
            userDOOMI.restaurante[0].pasos |= 2**(paso-1)
        },
        error: (xhr, status, errorThrown) => console.log(errorThrown),
    });
}

function pad(num, size){
  return ('000000000' + num).substr(-size);
}
function de24a12(hora) {
    /*
    Convierte de formato 24h a 12h
    '16:00:00' -> '04:00PM'
    */

    // Verificar formato correcto de la hora y separar en componentes.
    hora = hora.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [hora];

    if (hora.length > 1) { // If hora format correct
      hora = hora.slice (1);  // Remove full string match value
      hora[5] = +hora[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
      hora[0] = +hora[0] % 12 || 12; // Adjust hours
    }
    hora = hora.filter(Boolean);
    if (hora.length === 5)
      delete hora[3]; // Quitar segundos si los tiene.

    return hora.join (''); // return adjusted time or original string
}
function extraerFecha(date){
    const d = new Date(date);
    return [d.getDate(), d.getMonth()+1, d.getFullYear()].join('/');
}
function extraerHora(date){
    const d = new Date(date);
    return de24a12(pad(d.getHours(), 2) + ':' + pad(d.getMinutes(), 2));
}

function capitalizar(s){
    return s.toString().toLowerCase().replace(/(^|\s)\S/g, l => l.toUpperCase());
}

function play_notification() {
    new Audio('../../static/audio/notif.mpeg').play();
}


// function spawnNotification(titulo, cuerpo, timeout=5000) {
//     return new Promise((resolve, reject) => {
//         const options = {
//             body: cuerpo,
//             icon: '/delivery/static/img/logopush.jpg',
//         }
//         const n = new Notification(titulo,options);
//         n.onclick = resolve;
//         n.onerror = reject;
//         setTimeout(n.close.bind(n), timeout);
//         return n;
//     });
// }


function spawnNotification(titulo, cuerpo) {
  Notification.requestPermission(function(result) {
    if (result === 'granted') {
      navigator.serviceWorker.ready.then(function(registration) {
        registration.showNotification('titulo', {
          body: cuerpo,
          icon: '/delivery/static/img/logopush.jpg',
          vibrate: [200],
          tag: 'vibration-sample'
        });
      });
    }
  });
}