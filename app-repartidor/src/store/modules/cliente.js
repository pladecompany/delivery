import { general, auth } from '../../api'

export default {
  namespaced: true,


  state: () => ({
    idDoomisFavoritos: [],
    ordenes: [],
    carrito: [],
    pedidos_en_globo: 0,
    count_notif: 0,
  }),


  getters: {
    favoritos: state => state.idDoomisFavoritos,
    esFavorito: (state) => (id) => state.idDoomisFavoritos.includes(+id),
    ordenes: state => state.ordenes,
    carrito: state => state.carrito,

    doomisFavoritos: (state, getters, rootState, rootGetters) => {
      let d = rootGetters['doomis/doomialiados'].filter(doomi => state.idDoomisFavoritos.includes(doomi.id));
      return d;
    },

    pedidos_en_globo: state => state.pedidos_en_globo,

    count_notif: state => state.count_notif,
  },


  mutations: {
    AGREGAR_FAVORITO: (state, id) => state.idDoomisFavoritos.push(id),
    QUITAR_FAVORITO: (state, id) => state.idDoomisFavoritos = state.idDoomisFavoritos.filter(idFav => idFav != id),

    SET_FAVORITO: (state, args) => {
      const id = args[0], status = args[1];
      if(status && !state.idDoomisFavoritos.includes(id))
        state.idDoomisFavoritos.push(id);
      else if (!status)
        state.idDoomisFavoritos = state.idDoomisFavoritos.filter(idFav => idFav != id);
    },

    SET_PEDIDOS_EN_GLOBO(state, num){ state.pedidos_en_globo = num },

    INCREMENT_NOTIF(state){ state.count_notif++ },
  },


  actions: {
    favorito: ({ commit }, args) => {
      const idDoomi = args[0],
        status = args[1];

      const userData = auth.obtenerDatos();
      const peticion = status ? general.put : general.eliminarruta;

      peticion(`/clientes/${userData.id}/doomis_favoritos/${idDoomi}`)
        .then(r => commit('SET_FAVORITO', [idDoomi, status]));
    },

    actualizarFavoritos: ({ commit }) => {
      if(!auth.autenticado()) return;

      const userData = auth.obtenerDatos();

      general.recuperarData(`/clientes/${userData.id}/doomis_favoritos`)
        .then(function(data){
              for (let idDoomi of data.map(e => e.id_restaurante))
                commit('SET_FAVORITO', [idDoomi, true]);
        });
    },

    actualizarPedidosEnGlobo(ctx){
      const tipo_usu = auth.obtenerTipoUsuario();
      if(tipo_usu !== 'cliente') return;

      return new Promise((resolve,reject) => {
        general.recuperarData('pedidos/conteo_pedidos_por_tab')
          .then(data => {
            if(!data) return;
            let cantidad = 0;
            for (let count of data) {
              if ([3, 5, 6, 7, 8, 10, 12, 97].includes(parseInt(count.id_estatus,10))){
                cantidad += +count.count;
              }
            }
            // ctx.commit('SET_PEDIDOS_EN_GLOBO', null);
            ctx.commit('SET_PEDIDOS_EN_GLOBO', cantidad);
            ctx.commit('INCREMENT_NOTIF');
            resolve(cantidad)
          })
          .catch(reject);
      });
    },

  },
};
