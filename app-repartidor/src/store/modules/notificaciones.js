import { general, auth } from '../../api'

export default {
  namespaced: true,

  state: () => ({
    notificaciones: {noti:[]},
  }),


  getters: {
    notificaciones: state => state.notificaciones,
  },


  mutations: {
    GUARDAR_NOTIFICACIONES: (state, notificaciones) => { state.notificaciones = notificaciones || {noti:[]} },
  },


  actions: {
    actualizarNotificaciones({commit}, id){
      if(auth.obtenerTipoUsuario() == 'repartidor' || auth.obtenerTipoUsuario() == 'cliente'){
        const userData = auth.obtenerDatos();
        var tipo_usu = 'clientes';
        if(auth.obtenerTipoUsuario() == 'repartidor')
          tipo_usu = 'repartidores';
        general.recuperarDataConData('/notificaciones/',{tipo_usuario: tipo_usu,id: userData.id,limit: 'todas'})
          .then(data => commit('GUARDAR_NOTIFICACIONES', data))
      }
    },
  },
};
