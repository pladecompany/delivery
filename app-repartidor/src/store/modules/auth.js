import axios from 'axios'
import { config } from '../../api'

function defaultState() {
  return {
    token: null,
    tipoUsuario: null,
    data: {},
  };
}

const state = defaultState;


const getters = {
  token: state => state.token,
  tipoUsuario: state => state.tipoUsuario,
};


const mutations = {
  SET_ID: (state, id) => state.id = id,
  SET_TOKEN: (state, token) => state.token = token,
  SET_DATA: (state, data) => state.data = data,

  DEL_ALL: state => state.replaceState(defaultState()),
};


const actions = {
  login({ commit }, info){
    return new Promise(function(resolve, reject) {
      const correo = info.correo || info.email;
      const pass = info.pass || info.password || info['contraseña'];

      const data = {correo, pass};

      axios.post(config.apiURL+'login/app', data)
        .then(function(resp){
          let data = resp.data[0] || resp.data;
          if(data && data.r == false) return reject(data);

          commit('SET_TOKEN', data.token);
          commit('SET_ID', data.id);
          commit('SET_DATA', data);

          return resolve(data);
        })
        .catch(function(err){
          return reject(err);
        });
    });
  },

  salir({ commit }, callback){
    return new Promise(function(resolve, reject) {
      commit('DEL_ALL');
      resolve(callback ? callback() : true);
    });
  },
};




export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
}
