import { general, utils } from '../../api'

export default {
  namespaced: true,

  state: () => ({
    doomialiados: [],
    doomialiadoscercanos: [],
    categorias: [],
  }),


  getters: {
    doomialiados: state => state.doomialiados,

    doomialiadosAbiertos: state => state.doomialiados.filter(el => el.laborando),

    doomialiadosCerrados: state => state.doomialiados.filter(el => !el.laborando),

    doomialiadoscercanos: state => state.doomialiadoscercanos,

    doomialiadosAbiertosCercanos: state => state.doomialiadoscercanos.filter(el => el.laborando),

    doomialiadosCerradosCercanos: state => state.doomialiadoscercanos.filter(el => !el.laborando),

    categorias: state => state.categorias,
    
    porId: (state) => (id) => {
      let doomi = state.doomialiados.filter(el => el.id==id).pop();
      return doomi || {};
    },

    horariosDoomialiado: (state, getters) => (id) => {
      let d = getters['porId'](id);

      return (d.horarios || []).filter(h => h.estatus);
    },

    horarioDoomialadoDeHoy: (state, getters) => (id) => {
      const hoy = utils.hoy();
      const hor = getters['horariosDoomialiado'](id).filter(h => h.dia == hoy).pop();

      if(!hor) return;

      return [utils.de24a12(hor.inicio_hor), utils.de24a12(hor.fin_hor)].join('/');

    },
    horarioDoomialadoDeHoy2: (state, getters) => (id) => {
      const hoy = utils.hoy();
      const hor = getters['horariosDoomialiado'](id).filter(h => h.dia == hoy);
      if(!hor) return;
      var hr = '';
      for (var i = 0; i < hor.length; i++) {
        if(hr=='')
          hr=[utils.de24a12(hor[i].inicio_hor), utils.de24a12(hor[i].fin_hor)].join('/');
        else
          hr+=' - '+[utils.de24a12(hor[i].inicio_hor), utils.de24a12(hor[i].fin_hor)].join('/');
      }
      return hr;

    },
  },


  mutations: {
    GUARDAR_DOOMIALIADOS: (state, doomialiados) => { state.doomialiados = doomialiados || [] },
    GUARDAR_DOOMIALIADOS_CERCANOS: (state, doomialiadoscercanos) => { state.doomialiadoscercanos = doomialiadoscercanos || [] },
    GUARDAR_DOOMIALIADOS_CATEGORIAS: (state, categorias) => { state.categorias = categorias || [] },
  },


  actions: {
    actualizarDoomialiados: ({ commit }) => {
      general.recuperarData('/restaurantes/')
        .then(data => commit('GUARDAR_DOOMIALIADOS', data))
    },
    actualizarDoomialiadosCercanos: async ({ commit }) => {
      var cordenadas = await general.geo();
      general.recuperarDataConData('restaurantes/app',{lat:cordenadas.lat, lon: cordenadas.lon})
        .then(data => commit('GUARDAR_DOOMIALIADOS_CERCANOS', data))
    },
    actualizarCategorias: async ({ commit }) => {
      general.recuperarData('/categorias/')
        .then(data => commit('GUARDAR_DOOMIALIADOS_CATEGORIAS', data))
    },
  },


};
