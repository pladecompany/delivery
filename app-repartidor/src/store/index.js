import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import Swal from 'sweetalert2';

// import example from './module-example'
import productos from './modules/productos'
import cliente from './modules/cliente'
import doomis from './modules/doomis'
import carrito from './modules/carrito'
import ordenes from './modules/ordenes'
import notificaciones from './modules/notificaciones'
import repartidor from './modules/repartidor'
import { auth, general, config } from '../api'


Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      productos,
      cliente,
      doomis,
      carrito,
      ordenes,
      notificaciones,
      repartidor,
    },

    state: {
      titleCurrenView: 'Doomi',
      leftDrawerState: false,
      Ubicacion_actual: false,
      Ubicacion_nueva: false,
      setinter : null,
      Fav:0,
      Pedido:0,
      swalConfig: {
        showClass: {popup: 'animate__animated animate__fadeIn'},
        hideClass: {popup: 'animate__animated animate__fadeOut'},
      },
    },


    getters: {
      leftDrawerState(state){
        return state.leftDrawerState;
      },
      obtenersetinter(state){
        return state.setinter;
      }
    },


    mutations: {
      toggleLeftDrawer(state){
        state.leftDrawerState = !state.leftDrawerState;
      },
      openLeftDrawer(state){
        state.leftDrawerState = true;
      },
      closeLeftDrawer(state){
        state.leftDrawerState = false;
      },
      setLeftDrawer(state, v){
        state.leftDrawerState = Boolean(v);
      },
      setInterval(state, v){
        state.setinter = v;
      }
    },


    actions: {
      actualizarTodo(ctx){
        const tipo_usu = auth.obtenerTipoUsuario();
        const logueado = auth.autenticado();
        // if(auth.obtenerTipoUsuario() == 'repartidor')
        //   return context.dispatch('_actualizarTodoRepartidor');

        ctx.dispatch('doomis/actualizarDoomialiados');
        ctx.dispatch('doomis/actualizarCategorias');
        ctx.dispatch('productos/actualizarProductos');
        ctx.dispatch('doomis/actualizarDoomialiadosCercanos');
        ctx.dispatch('productos/actualizarCategorias');

        if(logueado){
          ctx.dispatch('ordenes/actualizarEstatus');
          ctx.dispatch('notificaciones/actualizarNotificaciones');
          if(tipo_usu === 'cliente'){
            ctx.dispatch('carrito/actualizar');
            ctx.dispatch('cliente/actualizarFavoritos');
          }
        }
      },

      _swal(ctx, conf){
        return new Promise(function(resolve){
          Swal.fire({
            // icon: 'info',
            // cancelButtonText: 'Cerrar',
            confirmButtonText: 'OK',
            ...ctx.state.swalConfig,
            ...conf,
          })
          .then(resolve);
        });
      },


      swalSuccess(ctx, conf){
        return new Promise((resolve) => {
          if(typeof conf == 'string')
            conf = {text: conf};

          ctx.dispatch('_swal', {icon:'success', ...conf}).then(resolve);
        });
      },

      iniciarsetinterval({ commit, getters }, callback){
        if(auth.obtenerTipoUsuario() != 'repartidor') return;
        return new Promise(function(resolve, reject) {
          clearInterval(getters.obtenersetinter);
          commit('setInterval',null);
          var inter = setInterval(async function(){
            var id_re = auth.obtenerDatos();
            if(!id_re){
              clearInterval(getters.obtenersetinter);
              commit('setInterval',null);
              return;
            }
            await general.geo().then(ubicacionActual => {
              axios({
                url: config.apiURL + 'repartidores/cambiar_ubicacion',
                data: {lat: ubicacionActual.lat,lon: ubicacionActual.lon, iden: id_re.id},
                method: 'PUT',
                headers: {
                  'token': auth.obtenerToken(),
                },
              }).catch(err => {
                console.log(err);
              });
            })
            .catch(err => {
              console.log(err);
              reject();
            });
          }, 10000);
          commit('setInterval',inter);
        });
      },
      detenersetinterval({ commit, getters }, callback){
        return new Promise(function(resolve, reject) {
          clearInterval(getters.obtenersetinter);
          commit('setInterval',null);
        });
      },
      swalInfo(ctx, conf){
        return new Promise((resolve) => {
          if(typeof conf == 'string')
            conf = {text: conf};

          ctx.dispatch('_swal', {icon:'info', ...conf}).then(resolve);
        });
        /*
        return new Promise(function(resolve, reject){
          if(typeof conf == 'string')
            conf = {text: conf};

          Swal.fire({
            icon: 'info',
            cancelButtonText: 'Cerrar',
            confirmButtonText: 'OK',
            ...context.state.swalConfig,
            ...conf,
          })
          .then(res => resolve(res));
        });
        */
      },

      swalOtroDoomi(context, conf){
        Swal.fire({
          icon: 'warning',
          showCancelButton: true,
          cancelButtonText: 'Ir al doomialiado',
          confirmButtonText: 'Cancelar orden anterior',
          ...context.state.swalConfig,
          ...conf,
        })
        .then(res => {
          if(res.isConfirmed) {
            context.dispatch('carrito/vaciar');
          }
          else if(res.isDismissed && res.dismiss == 'cancel')
            this.$router.push({path: conf.path});
        });
      },

    },

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEV
  })

  return Store
}
