import Vue from 'vue'
import VueRouter from 'vue-router'

import routes from './routes'
import { auth } from '../api'

Vue.use(VueRouter)

export const EventBus = new Vue();

export default function ({ store, ssrContext }) {
  const Router = new VueRouter({
    scrollBehavior: () => ({ x: 0, y: 0 }),
    routes,

    // Leave these as they are and change in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  })

  Router.beforeEach((to, from, next) => {
    // Mantener al repartidor dentro del pedido actual.
    let pedido_actual = store.getters['repartidor/pedido_actual']

    if(pedido_actual) {
      let entregando_pedido = store.getters['repartidor/entregando']

      if (!auth.autenticado()){
        return next();
      }

      if (!entregando_pedido && to.name !== 'RecibirDelivery'){
        console.log('redirecting:', to.name)
        return next('/RecibirDelivery/'+pedido_actual);
      }

      else if (entregando_pedido && to.name !== 'EntregarDelivery') {
        console.log('redirecting:', to.name)
        return next('/EntregarDelivery');
      }
    }

    next();
  });

  return Router
}
