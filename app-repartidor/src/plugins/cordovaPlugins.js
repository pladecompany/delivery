import axios from "axios";
import { config } from '../api'

export default {
    Firebase: function(store,this2) {
        var este = this;
        //alert(store.data.id+"-"+store.data.text_tipo_usu+"-0");
        window.FirebasePlugin.getToken(function(tokenFirebase) {
            var t_u = null;
            var idu = null;
            if(store.data){
                idu = store.data.id;
                t_u = 'clientes';
                if(store.data.text_tipo_usu!="Cliente")
                    t_u = 'repartidores';
            }
            var data = {
                id_usuario: idu,
                tokenFirebase: tokenFirebase,
                tablausuario: t_u
            };
            //alert(data.id_usuario+"-"+data.tokenFirebase+"-"+data.tablausuario+'-1');
            axios
                .post(config.apiURL+"notificaciones/agregar-eliminar-firebase", data)
                .then(resp => { console.log(resp); })
                .catch(err => {
                    console.log(err);
                });
        });

        window.FirebasePlugin.onTokenRefresh(function(tokenFirebase) {
            var t_u = null;
            var idu = null;
            if(store.data){
                idu = store.data.id;
                t_u = 'clientes';
                if(store.data.text_tipo_usu!="Cliente")
                    t_u = 'repartidores';
            }
            var data = {
                id_usuario: idu,
                tokenFirebase: tokenFirebase,
                tablausuario: t_u
            };
            //alert(data.id_usuario+"-"+data.tokenFirebase+"-"+data.tablausuario+'-2');
            axios
                .post(config.apiURL+"notificaciones/agregar-eliminar-firebase", data)
                .then(resp => { console.log(resp); })
                .catch(err => {
                    console.log(err);
                });
        });
        window.FirebasePlugin.onMessageReceived(function(data) {
            // App abierta
            //alert("onMess"+data.title); aqui entro
            if (data.tap) {
                setTimeout(function(){
                    if(data.tipo_noti=='1'){
                        this2.$router.push('/'+data.ruta);
                        //location.href = "index.html#/" + data.ruta + "";
                    }else if(data.tipo_noti=='2'){
                        this2.$router.push('/'+data.ruta+'/' + data.id_usable);
                        //location.href = "index.html#/" + data.ruta + "/"+data.id_usable;
                    }else{
                        if(data.body.includes('El costo del Delivery')){
                            var idb = data.id_usable || '';
                            this2.$router.push({ path:'/Historial_pagos/'+idb })
                        }
                    }
                }, 2000);
            }
            var googleMID = new Date().getTime();
            if (data.google.message_id) {
                googleMID = data.google.message_id;
            }

        });
    },
    Permissens: async function() {


        await cordova.plugins.locationAccuracy.canRequest(function(granted) {
                if(canRequest){
                    cordova.plugins.locationAccuracy.request(function (success){
                        //alert("Successfully requested accuracy: "+success.message);
                    }, function (error){
                       //alert("Accuracy request failed: error code="+error.code+"; error message="+error.message);
                       if(error.code !== cordova.plugins.locationAccuracy.ERROR_USER_DISAGREED){
                           if(window.confirm("No se pudo acceder a su ubicación, activar manualmente!")){
                               cordova.plugins.diagnostic.switchToLocationSettings();
                           }
                       }
                    }, cordova.plugins.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY);
                }else{
                    // request location permission and try again
                }
        });

       /*await cordova.plugins.locationAccuracy.canRequest(function(granted) {
            checkState(granted);
            canRequest = 1;
        }, handleError);*/

        function handleError(error) {
            var msg = "Error: " + error;
            console.log(msg);
        }

        function checkState(v) {
            //alert("cheking: " + v);
        }
    }
};
