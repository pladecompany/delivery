import io from 'socket.io-client';
import { config } from '../api';

export default async ({ Vue}) => {
	Vue.prototype.$socket= await io(config.apiURL) //route to the server where you started your socket (and its port)
}