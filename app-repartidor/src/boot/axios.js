import Vue from 'vue'
import axios from 'axios'
import { config, auth } from '../api'

// Configurar los headers globalmente.
Object.assign(axios.defaults.headers.common, config.headers);


// Agregar token a cada peticion si esta logeado.
axios.interceptors.request.use(
  config => {
    const token = auth.obtenerToken();
    const tipo_usuario = auth.obtenerTipoUsuario();
    const id_usuario = auth.obtenerIdUsuario();

    if (token && tipo_usuario) {
      config.headers['token'] = token;
      config.headers['tipo_usuario'] = tipo_usuario;
      config.headers['id_usuario'] = id_usuario;
    }
    return config;
  },
  error => { Promise.reject(error) }
);


// Interceptar codigos 401, verificar si el token todavia es valido.
const UNAUTHORIZED = 401;
axios.interceptors.response.use(
  response => response,
  error => {
    const {status} = error.response;
    if (status === UNAUTHORIZED) {
      auth.salir(() => {
        if(document.location.hash.toLowerCase() != '#/iniciodesesion')
          document.location.href = '#/Iniciodesesion';
      });
    }
    return Promise.reject(error);
  }
);



Vue.prototype.$axios = axios
