/*** MODULOS GENERALES ***/
export * as config from './config'
export * as utils from './utils'
export {default as auth} from './auth'


/*** MODULOS DE LA API ***/
export {default as repartidor} from './modulos/repartidor'
export {default as cliente} from './modulos/cliente'
export {default as general} from './modulos/general'
