/*
  Modulo para /clientes

  Los metodos que comienzan con 'recuperar' son llamadas al API.
  Los que comienzan con 'obtener' son locales.
*/


import Vue from 'vue'
import axios from 'axios'
import { auth, config, utils } from '..'
import { LocalStorage } from 'quasar'
// const LocalStorage = localStorage;

const apiURL = config.apiURL;
const headers = config.headers;
const STORAGE_KEY_DATA = config.STORAGE_KEY_DATA;
const STORAGE_KEY_AUTH = config.STORAGE_KEY_AUTH;
const TIPO_USUARIO = 'cliente';


const esNumerico = n => +n===+n;


export default {

  recuperarData(ruta){
    ruta = utils.normalizarRuta(ruta, false);
    return new Promise((resolve, reject) => {
      axios.get(apiURL + ruta)
      .then(resp => resp.data ? resolve(resp.data) : reject(resp))
      .catch(err => reject(err));
    });
  },
  recuperarDataConData(ruta,datos){
    ruta = utils.normalizarRuta(ruta, false);

    return new Promise((resolve, reject) => {
      axios(apiURL + ruta,{params: datos})
      .then(resp => resp.data ? resolve(resp.data) : reject(resp))
      .catch(err => reject(err));
    });
  },

  recuperarDataPorId(ruta, id_buscar){
    if(!esNumerico(id_buscar))
      throw TypeError(`recuperarDataPorId esperaba un entero, no ${typeof id_buscar}`);

    ruta = utils.normalizarRuta(ruta);

    return new Promise((resolve, reject) => {
      axios.get(apiURL + ruta  + id_buscar)
      .then(resp => resp.data ? resolve(resp.data) : reject(resp))
      .catch(err => reject(err));
    });
  },

  post(ruta, datos){
    ruta = utils.normalizarRuta(ruta);
    const data_user = auth.obtenerDatos();
    const tipo_user = auth.obtenerTipoUsuario(); //obtener el tipo de usuario logueado
    if(data_user && datos instanceof FormData){
      datos.append('usuario', tipo_user);
      datos.append('id', data_user.id);
    }
    else if (data_user){
      datos['usuario'] = tipo_user
      datos['id'] = data_user.id;
    }
    return new Promise(function(resolve, reject){
      axios({
        url: apiURL+ruta,
        data: datos,
        method: 'POST',
      })
      .then(function(resp){
        if(!resp.data.r) return reject(resp.data);
        return resolve(resp.data);
      })
      .catch(err=> reject(err));
    });
  },

  put(ruta, datos, id){
    ruta = utils.normalizarRuta(ruta);
    datos = datos ? datos : new FormData();
    const data_user = auth.obtenerDatos();
    const tipo_user = auth.obtenerTipoUsuario(); //obtener el tipo de usuario logueado
    if(data_user){
      datos.append('usuario', tipo_user);
      datos.append('id', data_user.id);
    }
    return new Promise(function(resolve, reject){
      axios({
        url: apiURL+ruta,
        data: datos,
        method: 'PUT',
      })
      .then(function(resp){
        if(!resp.data.r) return reject(resp.data);
        return resolve(resp.data);
      })
      .catch(err=> reject(err));
    });
  },

  eliminarruta(ruta){
    ruta = utils.normalizarRuta(ruta);
    return new Promise((resolve, reject) => {
      axios.delete(apiURL + ruta)
      .then(resp => resp.data ? resolve(resp.data) : reject(resp))
      .catch(err => reject(err));
    });
  },

  geo() {
    return new Promise((resolve, reject) => {
      var device = document.addEventListener("deviceready", function() {
        if (canRequest == "") {
          cordova.plugins.locationAccuracy.canRequest(canrequest, ErrorCant);
        } else {
          //canrequest();
        }
        function canrequest() {
          canRequest = 1;
          cordova.plugins.locationAccuracy.request(
            function(success) {
              navigator.geolocation.getCurrentPosition(
                  function(pos) {
                    resolve({
                        lat: pos.coords.latitude,
                        lon: pos.coords.longitude
                    });
                  },
                  function(error) {
                      reject(error);
                  }
              );
            },
            function(error) {
              console.log(error);
              navigator.geolocation.getCurrentPosition(
                function(pos) {
                  resolve({
                    lat: pos.coords.latitude,
                    lon: pos.coords.longitude
                  });
                },
                function(error) {
                  reject(error);
                }
              );
              if (
                error.code !==
                cordova.plugins.locationAccuracy.ERROR_USER_DISAGREED
              ) {
                  reject(error);
                }
            },
            cordova.plugins.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY
          );
        }
        function ErrorCant(error) {
            reject(error);
        }
      });
      if (!device) {
        navigator.geolocation.getCurrentPosition(
          function(pos) {
            resolve({
                lat: pos.coords.latitude,
                lon: pos.coords.longitude
            });
          },
          function(error) {
            reject(error);
          }
        );
      }
    })
  },

  /*** METODOS LOCALES ***/

  obtenerNombreUsuario(){
    const datos = auth.obtenerDatos();

    return datos ? datos.nombre : null;
  },

};
