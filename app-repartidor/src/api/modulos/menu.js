import { recuperarData, recuperarDataPorId } from './general'
import { config, utils } from '..'

export default {
  recuperarMenus(){
    return new Promise((resolve, reject) => {
      recuperarData('/menu/')
        .then(resp => resp.data ? resolve(resp.data) : reject(resp))
        .catch(err => reject(err));
    });


  },
};
