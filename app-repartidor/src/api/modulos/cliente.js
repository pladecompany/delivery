/*
  Modulo para /clientes

  Los metodos que comienzan con 'recuperar' son llamadas al API.
  Los que comienzan con 'obtener' son locales.
*/


import Vue from 'vue'
import axios from 'axios'
import { auth, config, utils } from '..'
import { LocalStorage } from 'quasar'
// const LocalStorage = localStorage;

const apiURL = config.apiURL;
const headers = config.headers;
const STORAGE_KEY_DATA = config.STORAGE_KEY_DATA;
const STORAGE_KEY_AUTH = config.STORAGE_KEY_AUTH;
const TIPO_USUARIO = 'cliente';


const esNumerico = n => +n===+n;

const clearAuthFromData = obj => {
  if(obj && typeof obj != 'string'){
    delete obj['token'];
    delete obj['tipo_usu'];
    delete obj['tipo_usuario'];
    delete obj['pass'];
    delete obj['password'];
  }
  return obj;
};


export default {

  recuperarPaises(){
    return new Promise((resolve, reject) => {
      axios.get(apiURL + 'paises')
      .then(resp => resp.data ? resolve(resp.data) : reject(resp))
      .catch(err => reject(err));
    });
  },

  recuperarEstados(id_pais){
    if(!esNumerico(id_pais)) throw TypeError(`recuperarEstados esperaba un entero, no ${typeof id_pais}`);

    return new Promise((resolve, reject) => {
      axios.get(apiURL + 'paises/estado/' + id_pais)
      .then(resp => resp.data ? resolve(resp.data) : reject(resp))
      .catch(err => reject(err));
    });
  },

  recuperarCiudades(id_estado){
    if(!esNumerico(id_estado)) throw TypeError(`recuperarCiudades esperaba un entero, no ${typeof id_estado}`);

    return new Promise((resolve, reject) => {
      axios.get(apiURL + 'paises/ciudad/' + id_estado)
      .then(resp => resp.data ? resolve(resp.data) : reject(resp))
      .catch(err => reject(err));
    });
  },


  recuperarDatos(){// Refrescar datos locales del cliente.
    const id = this.obtenerID();
    return new Promise(function(resolve, reject){
      axios({
        url: apiURL+ `clientes/${id}`,
        method: 'GET',
      })
      .then(function(resp){
        let data = resp.data;
        console.log('recuperarDatos.data:', data);
        if(!resp.data || !resp.data.id==id) return reject(resp);
        data = clearAuthFromData(data);
        LocalStorage.set(STORAGE_KEY_DATA, data);
        auth._recordarUsuario(data);
        return resolve(data);
      })
      .catch(err=> reject(err));
    });
  },

  registrar(datos){
    return new Promise(function(resolve, reject){
      axios({
        url: apiURL+'clientes/add',
        data: datos,
        method: 'POST',
      })
      .then(function(resp){
        if(resp.data.r == false) return reject(resp.data);
        return resolve(resp.data);
      })
      .catch(err=> reject(err));
    });
  },

  editar(datos, onUploadProgress){
    const cliente = this; // Para poder usar los metodos dentro de la promesa.

    return new Promise(function(resolve, reject){
      const id = cliente.obtenerID();
      axios({
        url: apiURL+'clientes/edit',
        data: datos,
        method: 'PUT',
        onUploadProgress,
      })
      .then(function(resp){
        // Refrescar datos locales del cliente.
        cliente.recuperarDatos();

        if(!resp.data || resp.data.r == false) return reject(resp.data);
        return resolve(resp.data);
      })
      .catch(err=> reject(err));
    });
  },

  recuperarIdCarrito(){
    return new Promise(function(resolve, reject){
      const id = auth.obtenerIdUsuario();
      const tipoUsuario = auth.obtenerTipoUsuario();
      if(tipoUsuario !== 'cliente')
        return reject(null);

      if(tipoUsuario=='cliente'){
        axios.get(apiURL + `clientes/${id}/info_carrito`)
          .then(resp => resolve(resp.data.id))
          .catch(err => reject(err));
      }
    });
  },


  /*** METODOS LOCALES ***/

  obtenerDatos(){
    const datos = LocalStorage.getItem(STORAGE_KEY_DATA);
    return datos || null;
  },

  obtenerID(){
    const datos = LocalStorage.getItem(STORAGE_KEY_DATA);
    const datosAuth = LocalStorage.getItem(STORAGE_KEY_AUTH);

    if(datos && (datosAuth.tipo_usu||datosAuth.tipo_usuario) == TIPO_USUARIO) {
      return datos.id;
    }
    return null;
  },

}