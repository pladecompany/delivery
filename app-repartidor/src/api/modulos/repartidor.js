/*
  Modulo para /repartidores

  Los metodos que comienzan con 'recuperar' son llamadas al API.
  Los que comienzan con 'obtener' son locales.
*/


import Vue from 'vue'
import axios from 'axios'
import { auth, config, utils } from '..'
import { LocalStorage } from 'quasar'
// const LocalStorage = localStorage;

const apiURL = config.apiURL;
const headers = config.headers;
const STORAGE_KEY_DATA = config.STORAGE_KEY_DATA;
const STORAGE_KEY_AUTH = config.STORAGE_KEY_AUTH;
const TIPO_USUARIO = 'repartidor';


export default {

  /*** LLAMADAS AL API ***/

  async recuperarNuevaID(contexto, propiedad) {
    let r = await axios.get(apiURL+'repartidores/nuevo_id');
    contexto[propiedad] = r.data.nuevoID;
  },

  recuperarDatos(){// Refrescar datos locales del repartidor.
    const id = this.obtenerID();
    return new Promise(function(resolve, reject){
      axios({
        url: apiURL+ `repartidores/${id}`,
        method: 'GET',
      })
      .then(function(resp){
        const data = resp.data;
        if(!resp.data || !resp.data.id==id) return reject(resp);
        LocalStorage.set(STORAGE_KEY_DATA, data);
        return resolve(data);
      })
      .catch(err=> reject(err));
    });
  },

  recuperarNuevaID(){
    return new Promise(function(resolve, reject){
      axios({
        url: apiURL+'repartidores/nuevo_id',
        method: 'GET',
      })
      .then(function(resp){
        if(!resp.data || !resp.data.nuevoID) return reject(resp);
        return resolve(resp.data.nuevoID);
      })
      .catch(err=> reject(err));
    });
  },

  recuperarTipoVehiculos(id){
    let url = apiURL+ (id ? `tipovehiculo/${id}` : 'tipovehiculo')
    return new Promise(function(resolve, reject){
      axios({
        url: url,
        method: 'GET',
      })
      .then(function(resp){
        if(!resp.data) return reject(resp);
        return resolve(resp.data);
      })
      .catch(err=> reject(err));
    });
  },

  registrar(datos, onUploadProgress){

    return new Promise(function(resolve, reject){
      axios({
        url: apiURL+'repartidores/add',
        data: datos,
        method: 'POST',
        headers: headers,
        onUploadProgress,
      })
        .then(function(resp){
          if(!resp.data || resp.data.r == false) return reject(resp.data);
          return resolve(resp.data);
        })
        .catch(err=> reject(err));
    });
  },

  editar(datos, onUploadProgress){
    const repartidor = this; // Para poder usar los metodos dentro de la promesa.

    return new Promise(function(resolve, reject){
      const id = repartidor.obtenerID();
      axios({
        url: apiURL+'repartidores/edit',
        data: datos,
        method: 'PUT',
        onUploadProgress,
      })
      .then(function(resp){
        // Refrescar datos locales del repartidor.
        repartidor.recuperarDatos();

        if(!resp.data || resp.data.r == false) return reject(resp.data);
        return resolve(resp.data);
      })
      .catch(err=> reject(err));
    });
  },




  /*** METODOS LOCALES ***/

  obtenerDatos(){
    const datos = LocalStorage.getItem(STORAGE_KEY_DATA);
    return datos || null;
  },

  obtenerID(){
    const datos = LocalStorage.getItem(STORAGE_KEY_DATA);
    const datosAuth = LocalStorage.getItem(STORAGE_KEY_AUTH);

    if(datos && (datosAuth.tipo_usu||datosAuth.tipo_usuario) == TIPO_USUARIO) {
      return datos.id;
    }
    return null;
  },
}
