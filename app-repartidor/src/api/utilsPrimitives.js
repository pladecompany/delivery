var REGEXP_VALIDAR_CORREO = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
// var REGEXP_ES_HTTP_URL = /^https?:\/\//;
// var REGEXP_ES_DATA_URL = /^data:/;
// var REGEXP_DATA_URL_PROTOCOL = /^data:.*\/.*;base64,/;
var NOMBRE_MESES = [
  'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
  'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'
];

var DIAS = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];

//Get the picture
var processImageFile = function processImageFile(file, name, vue_context) {
  // let self = this;
  // const IMAGE_COMPRESSION_RATE = 1;
  // Judging does not support FileReader.
  if (!file || !window.FileReader) return false;
  if (/^image/.test(file.type)) {
    // Create a reader.
    let reader = new FileReader();
    // Convert the image to base64 format.
    reader.readAsDataURL(file);
    // Read the callback after successful.
    reader.onloadend = function() {
      let result = this.result;
      let img = new Image();
      img.src = result;
      vue_context[name] = result;
      /*
      // img.src = URL.createObjectURL(new Blob([file], {type:'image/jpeg'}));
      console.log('******** Uncompressed image size **********');
      console.log(result.length / 1024);
      img.onload = function() {
        let data = compressImage(img, IMAGE_COMPRESSION_RATE);
        // self.uploadImg(data, name);
        vue_context[name] = data;
      }
      */
    }
  }
};

var compressImage = function compressImage(img, compressRate) {
  let canvas = document.createElement('canvas');
  let ctx = canvas.getContext('2d');
  let initSize = img.src.length;
  let width = img.width;
  let height = img.height;
  canvas.width = width;
  canvas.height = height;

  // bottom color
  ctx.fillStyle = '#fff';
  ctx.fillRect(0, 0, canvas.width, canvas.height);
  ctx.drawImage(img, 0, 0, width, height);

  // Perform minimum compression
  let ndata = canvas.toDataURL('image/jpeg', compressRate);
  return ndata;
}

var formatear = function(num, simbol) {
  simbol = simbol || '';
  let separador = "."; // separador para los miles
  let sepDecimal = ','; // separador para los decimales
  num += '';
  var splitStr = num.split('.');
  var splitLeft = splitStr[0];
  var splitRight = splitStr.length > 1 ? sepDecimal + splitStr[1] : '';
  var regx = /(\d+)(\d{3})/;
  while (regx.test(splitLeft)) {
      splitLeft = splitLeft.replace(regx, '$1' + separador + '$2');
  }
  return simbol + splitLeft + splitRight;
}

var dec2hex = function(dec) {
  /* Decimal a hexadecimal */
  return dec < 10
    ? '0' + String(dec)
    : dec.toString(16)
}

var pad = function(num, size){
  return ('000000000' + num).substr(-size);
}

var de24a12 = function(hora) {
  /*
  Convierte de formato 24h a 12h
  '16:00:00' -> '04:00PM'
  */

  // Verificar formato correcto de la hora y separar en componentes.
  hora = hora.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [hora];

  if (hora.length > 1) { // If hora format correct
    hora = hora.slice (1);  // Remove full string match value
    hora[5] = +hora[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
    hora[0] = +hora[0] % 12 || 12; // Adjust hours
  }
  hora = hora.filter(Boolean);
  if (hora.length === 5)
    delete hora[3]; // Quitar segundos si los tiene.

  return hora.join (''); // return adjusted time or original string
}

var extraerHora = function(date){
  const d = new Date(date);
  return de24a12(pad(d.getHours(), 2) + ':' + pad(d.getMinutes(), 2));
}


var getFormattedDate = function(date, prefomattedDate = false, hideYear = false) {
  const day = date.getDate();
  const month = NOMBRE_MESES[date.getMonth()];
  const year = date.getFullYear();
  const hours = date.getHours();
  let minutes = date.getMinutes();
  const laHora = extraerHora(date);

  if (minutes < 10) {
    // Adding leading zero to minutes
    minutes = `0${ minutes }`;
  }

  if (prefomattedDate) {
    // Today at 10:20
    // Yesterday at 10:20
    return `${ prefomattedDate }, a las ${ laHora }`;
  }

  if (hideYear) {
    // 10. January at 10:20
    return `${ day }. ${ month }, a las ${ laHora }`;
  }

  // 10 de Enero, 2017. a las 10:20
  return `${ day } de ${ month }, ${ year }. a las ${ laHora }`;
}

export var REGEXP_VALIDAR_CORREO = REGEXP_VALIDAR_CORREO;
// export var REGEXP_ES_HTTP_URL = REGEXP_ES_HTTP_URL;
// export var REGEXP_ES_DATA_URL = REGEXP_ES_DATA_URL;
// export var REGEXP_DATA_URL_PROTOCOL = REGEXP_DATA_URL_PROTOCOL;
export var processImageFile = processImageFile;
export var compressImage = compressImage;
export var formatear = formatear;
export var dec2hex = dec2hex;
export var dias = DIAS;
export var pad = pad;
export var getFormattedDate = getFormattedDate;
// export var compressFileImage = compressFileImage;
