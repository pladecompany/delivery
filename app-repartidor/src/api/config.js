
const config = Object.freeze({
	dominio:'http://localhost/',
	// apiURL:'http://localhost:1210/',
	// apiURL:'https://inthecompanies.com:1210/',
	apiURL:'https://restaurante.doomi.app:2222/',
	headers: {
		'Access-Control-Allow-Origin': '*',
		// 'Access-Control-Allow-Methods': '*',
		'Access-Control-Allow-Headers': '*',
		'tipo-cliente': 'app',
		},

	STORAGE_KEY_AUTH: 'authDOOMI',
	STORAGE_KEY_DATA: 'userDOOMI',
	STORAGE_KEY_USERS: 'sessionsDOOMI',
});

export default config;

export const apiURL = config.apiURL;
export const headers = config.headers;
export const dominio = config.dominio;
export const STORAGE_KEY_AUTH = config.STORAGE_KEY_AUTH;
export const STORAGE_KEY_DATA = config.STORAGE_KEY_DATA;
export const STORAGE_KEY_USERS = config.STORAGE_KEY_USERS;

