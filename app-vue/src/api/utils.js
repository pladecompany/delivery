import {
  formatear,
  processImageFile,
  REGEXP_VALIDAR_CORREO,
  dec2hex,
  dias,
  pad,
  getFormattedDate,

} from './utilsPrimitives'



const utils = {
  esNumerico: v => +v === +v,

  sumarDinero: (...valores) =>
    parseFloat(valores.reduce((a,b) => (a * 100 + b * 100) / 100, 0).toFixed(2)),

  verificarCorreo: correo => {
    let validez = true;

    if(!correo) validez = 'Por favor escribe tu correo';
    else if(!REGEXP_VALIDAR_CORREO.test(correo)) validez = 'Por favor ingresa un correo válido';

    return validez;
  },

  verificarTelefono: numero => {
    let validez = true;

    if(!numero) validez = 'Por favor ingresa tu número';
    else if (numero.length < 16 || numero.length > 17) validez = 'Mínimo 11 caracteres';

    return validez;
  },

  normalizarRuta(r, barra_fin=true) {
    r = r.startsWith('/') ? r.slice(1) : r; // Quitar barra al principio.

    if(barra_fin)
      r = r.endsWith('/') ? r : r+'/'; // Agregar barra al final.

    return r;
  },

  procesarImagen(e, vue_context) {
    let files = e.target.files || e.dataTransfer.files;
    let name = e.target.name;
    if (!files.length) return;
    let file = files[0];
    if (file.size / 1024 > 10240) {
      console.log('La imagen es muy grande!')
    }
    else {
      processImageFile(file, name, vue_context);
    }
  },

  precio: function(num, simbol) {
    simbol = simbol || '';

    if (num == null || !num)
      num = 0;

    num = num.toString().replace(/,/g, ".");

    num = Math.round(num * 100) / 100;

    return formatear(num.toFixed(2), simbol).replace(/-/g, "");
  },

  generarId (len) {
    /* Genera id hexadecimal aleatoria */
    var arr = new Uint8Array((len || 40) / 2)
    window.crypto.getRandomValues(arr)
    return Array.from(arr, dec2hex).join('')
  },

  de24a12 (hora) {
    /*
    Convierte de formato 24h a 12h
    '16:00:00' -> '04:00PM'
    */

    // Verificar formato correcto de la hora y separar en componentes.
    hora = hora.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [hora];

    if (hora.length > 1) { // If hora format correct
      hora = hora.slice (1);  // Remove full string match value
      hora[5] = +hora[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
      hora[0] = +hora[0] % 12 || 12; // Adjust hours
    }
    hora = hora.filter(Boolean);
    if (hora.length === 5)
      delete hora[3]; // Quitar segundos si los tiene.

    return hora.join (''); // return adjusted time or original string
  },

  hoy(){
    /* Nombre del dia de hoy */
    return dias[new Date().getDay()];
  },

  queDiaEs(fecha){
    /* Nombre del dia en la fecha dada */
    return dias[new Date(fecha).getDay()];
  },

  extraerHora(date){
    const d = new Date(date);
    return this.de24a12(pad(d.getHours(), 2) + ':' + pad(d.getMinutes(), 2));
  },
  extraerFecha(date){
    const d = new Date(date);
    return [d.getDate(), d.getMonth()+1, d.getFullYear()].join('/');
  },


  timeAgo(dateParam) {
    /* Cuanto tiempo ha pasado desde una fecha */
    if (!dateParam) {
      return null;
    }

    const date = typeof dateParam === 'object' ? dateParam : new Date(dateParam);
    const DAY_IN_MS = 86400000; // 24 * 60 * 60 * 1000
    const today = new Date();
    const yesterday = new Date(today - DAY_IN_MS);
    const seconds = Math.round((today - date) / 1000);
    const minutes = Math.round(seconds / 60);
    const isToday = today.toDateString() === date.toDateString();
    const isYesterday = yesterday.toDateString() === date.toDateString();
    const isThisYear = today.getFullYear() === date.getFullYear();


    if (seconds < 5) {
      return 'Ahora';
    } else if (seconds < 60) {
      return `Hace ${ seconds } segundos`;
    } else if (seconds < 90) {
      return 'hace un minuto';
    } else if (minutes < 60) {
      return `Hace ${ minutes } minutos`;
    } else if (isToday) {
      return getFormattedDate(date, 'Hoy'); // Today at 10:20
    } else if (isYesterday) {
      return getFormattedDate(date, 'Ayer'); // Yesterday at 10:20
    } else if (isThisYear) {
      return getFormattedDate(date, false, true); // 10. January at 10:20
    }

    return getFormattedDate(date); // 10. January 2017. at 10:20
  },

  fechaEnHorario(date, hor) {
    date = new Date(date);
    let res = true;
    const rango = [hor.inicio_hor, hor.fin_hor];
    const hora = pad(date.getHours(), 2) + ':' + pad(date.getMinutes(), 2);

    // Si no es el mismo dia.
    if(date.getDay() !== dias.indexOf(hor.dia)){
      res = false;
    }

    // Si no esta dentro de la hora.
    else if(!(hora >= rango[0] && hora < rango[1])){
      res = false;
    }

    return res;
  },

  horarioActual(horarios, sep){
    sep = sep || '';
    const now = new Date();
    let hor = horarios.find(hor => this.fechaEnHorario(now, hor));

    if(!hor) return false;

    return this.de24a12(hor.inicio_hor) + sep + this.de24a12(hor.fin_hor);
  },

  hash_str(str, seed = 0) {
    let h1 = 0xdeadbeef ^ seed, h2 = 0x41c6ce57 ^ seed;
    for (let i = 0, ch; i < str.length; i++) {
      ch = str.charCodeAt(i);
      h1 = Math.imul(h1 ^ ch, 2654435761);
      h2 = Math.imul(h2 ^ ch, 1597334677);
    }
    h1 = Math.imul(h1 ^ (h1>>>16), 2246822507) ^ Math.imul(h2 ^ (h2>>>13), 3266489909);
    h2 = Math.imul(h2 ^ (h2>>>16), 2246822507) ^ Math.imul(h1 ^ (h1>>>13), 3266489909);
    return 4294967296 * (2097151 & h2) + (h1>>>0);
  },

  addPercentage(precio, porcentaje) {
    /* Regresa el precio MAS porcentaje */
    const cents = precio * 100;
    return parseFloat( ((cents * (porcentaje/100) + cents) / 100).toFixed(2) );
  },
  getPercentage(precio, porcentaje) {
    /* Regresa solo el porcentaje */
    return parseFloat( (precio * 100 * (porcentaje/100) / 100).toFixed(2) );
  },

};



export default utils;
export const verificarCorreo = utils.verificarCorreo;
export const verificarTelefono = utils.verificarTelefono;
export const procesarImagen = utils.procesarImagen;
export const normalizarRuta = utils.normalizarRuta;
export const precio = utils.precio;
export const generarId = utils.generarId;
export const esNumerico = utils.esNumerico;
export const sumarDinero = utils.sumarDinero;
export const restarDinero = utils.restarDinero;
export const de24a12 = utils.de24a12;
export const hoy = utils.hoy;
export const extraerHora = utils.extraerHora;
export const extraerFecha = utils.extraerFecha;
export const timeAgo = utils.timeAgo;
export const fechaEnHorario = utils.fechaEnHorario;
export const horarioActual = utils.horarioActual;
export const hash_str = utils.hash_str;
export const queDiaEs = utils.queDiaEs;
export const addPercentage = utils.addPercentage;
export const getPercentage = utils.getPercentage;
