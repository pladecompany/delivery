/* globals LocalStorage */

// import Vue from 'vue'
// import router from '../router'
import axios from 'axios'
import { LocalStorage } from 'quasar'
import { general } from '.'
import { hash_str } from './utils'
import { apiURL, STORAGE_KEY_AUTH, STORAGE_KEY_DATA, STORAGE_KEY_USERS } from './config'

const clearAuthFromData = obj => {
  if(obj && typeof obj != 'string'){
    delete obj['token'];
    delete obj['refresh_token'];
    delete obj['pass'];
    delete obj['password'];
  }
  return obj;
};


export default {

  autenticar(correo, pass, recordar){
    const auth = this;
    const data = {correo, pass, recordar};
    return new Promise(function(resolve, reject){
      axios.post(apiURL+'login/app', data)
        .then(resp => auth._processAuth(resp, resolve, reject, recordar))
        .catch(function(err){
          auth.salir();
          return reject(err);
        });
    });
  },

  recordar(user){
    const auth = this;
    const data = {correo:user.correo, refresh_token:user.refresh_token};
    return new Promise(function(resolve, reject){
      axios.post(apiURL+'login/app/recordar', data)
        .then(resp => auth._processAuth(resp, resolve, reject, true))
        .catch(function(err){
          auth.salir();

          if(err.response && err.response.status==401){
            err.r = false; err.msj='La sesión ha caducado, por favor inicia sesión nuevamente';
            auth.olvidarUsuario(user);
          }
          return reject(err);
        });
    });
  },

  _processAuth(resp, resolve, reject, recordar){
    let data = resp.data[0] || resp.data;
    if(data && data.r == false) return reject(data);

    // Guardar datos de autenticacion.
    LocalStorage.set(STORAGE_KEY_AUTH, {
      token:data.token,
      tipo_usuario: data.tipo_usu,
      refresh_token: data.refresh_token
    });

    if(!data.refresh_token)
      this.olvidarUsuario(data);

    data = clearAuthFromData(data);
    LocalStorage.set(STORAGE_KEY_DATA, data);

    // Recordar usuario.
    if(recordar)
      this._recordarUsuario(data);

    return resolve(data);
  },

  salir(callback) {
    if(!callback)
      callback = () => {};
    return new Promise((resolve, reject) => {
      const existe = LocalStorage.has(STORAGE_KEY_AUTH) || LocalStorage.has(STORAGE_KEY_DATA);

      if(existe) {
        const token = this.obtenerToken();
        const tipo_usuario = this.obtenerTipoUsuario();
        axios({
          url: apiURL+'login/app',
          data: {token, tipo_usuario},
          method: 'DELETE'
        })
        .then(console.debug)
        LocalStorage.remove(STORAGE_KEY_AUTH);
        LocalStorage.remove(STORAGE_KEY_DATA);
        if (callback) callback();
        return resolve(true);
      }

      resolve(false);
    });
  },

  autenticado() {
    return LocalStorage.has(STORAGE_KEY_AUTH);
  },

  obtenerToken() {
    return LocalStorage.has(STORAGE_KEY_AUTH) ? (LocalStorage.getItem(STORAGE_KEY_AUTH)).token : null;
  },

  obtenerTokenRefresco() {
    return LocalStorage.has(STORAGE_KEY_AUTH) ? (LocalStorage.getItem(STORAGE_KEY_AUTH)).refresh_token : null;
  },

  obtenerDatos(){
    let datos = LocalStorage.getItem(STORAGE_KEY_DATA);
    datos = clearAuthFromData(datos);
    return datos || null;
  },

  obtenerTipoUsuario(){
    if (LocalStorage.has(STORAGE_KEY_AUTH))
    {
      let data = LocalStorage.getItem(STORAGE_KEY_AUTH);
      return data.tipo_usu || data.tipo_usuario || null;
    }
  },

  obtenerIdUsuario(){
    if (LocalStorage.has(STORAGE_KEY_DATA))
    {
      let data = LocalStorage.getItem(STORAGE_KEY_DATA);
      return data.id || null;
    }
  },

  requiereAutenticacion(to, from, next){
    if(!LocalStorage.has(STORAGE_KEY_AUTH)){
      next({
        name: 'Iniciodesesion',
        // query: { redirect: to.fullPath }
      });
    }
    else {
      next();
    }
  },

  siEstaAutenticado(to, from, next) {
    if(LocalStorage.has(STORAGE_KEY_AUTH)) {
      let data = LocalStorage.getItem(STORAGE_KEY_AUTH);
      let tipoUsuario = data.tipo_usu || data.tipo_usuario || null;

      switch (tipoUsuario){
        case 'cliente': return next({name:'home'});
        case 'repartidor': return next({name:'Pedidos'});
        default: return next();
      }
    }
    else {
      next();
    }
  },

  _recordarUsuario(user){
    const refresh_token = this.obtenerTokenRefresco();
    const tipo_usu = this.obtenerTipoUsuario();
    if(!user.id || !user.correo || !refresh_token) return;

    let data;

    if(!LocalStorage.has(STORAGE_KEY_USERS))
      data = {};
    else
      data = LocalStorage.getItem(STORAGE_KEY_USERS);

    /*
    // Para identificar a un usuario.
    // Cliente y repartidor pueden tener el mismo id.
    */
    const iden = hash_str(user.tipo_usu||tipo_usu, user.id);

    data[iden] = {
      iden, refresh_token,
      id: user.id,
      correo: user.correo,
      nombre: user.nombre || '',
      apellido: user.apellido || '',
      foto: user.foto,
    };
    LocalStorage.set(STORAGE_KEY_USERS, data);
  },

  olvidarUsuario(user){
    return new Promise((resolve, reject) => {
      // if(!LocalStorage.has(STORAGE_KEY_USERS)) return;

      if(user.refresh_token){
        axios({
          url: apiURL.concat('login/app/recordar'),
          data: {correo: user.correo, refresh_token: user.refresh_token},
          method: 'DELETE',
        })
        .then(resolve)
        .catch(reject);
      }

      if(!user.iden) user.iden = hash_str(user.tipo_usu, user.id);

      const data = LocalStorage.getItem(STORAGE_KEY_USERS) || {};
      delete data[user.iden];

      LocalStorage.set(STORAGE_KEY_USERS, data);
    });
  },

  recordarUsuarios(){
    return Object.values(LocalStorage.getItem(STORAGE_KEY_USERS) || {});
  },
}
