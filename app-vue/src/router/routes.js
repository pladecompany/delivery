import { auth } from '../api'


const routes = [
  {
    path: '/inicio',
    component: () => import('layouts/Menu.vue'),
    children: [
      { name: 'inicio', path: '', component: () => import('pages/Inicio.vue') }
    ]
  },
  {
    path: '/Home',
    component: () => import('layouts/Menu.vue'),
    children: [
      { name: 'home', path: '', component: () => import('pages/Home.vue') }
    ]
  },
  {
    path: '/Home/:id?',
    component: () => import('layouts/Menu.vue'),
    children: [
      { name: 'home', path: '', component: () => import('pages/Home.vue') }
    ],
    beforeEnter: (to, from, next) => {
      if(auth.obtenerTipoUsuario() == 'repartidor'){
        return next('/Pedidos');
      }
      next();
    },
  },
  {
    path: '/bienvenido/',
    name: 'bienvenido',
    component: () => import('layouts/Menu.vue'),
    children: [
      { path: '', component: () => import('pages/intro.vue') }
    ]
  },
  {
    path: '/business',
    name: 'business',
    component: () => import('layouts/Menu.vue'),
    children: [
      { path: '', component: () => import('pages/Business.vue') }
    ]
  },
  {
    path: '/registro/',
    name: 'registro',
    component: () => import('layouts/Empty.vue'),
    children: [
      { path: '', component: () => import('pages/Registro.vue') }
    ]
  },
  {
    path: '/registroRepartidor',
    name: 'registro',
    component: () => import('layouts/Empty.vue'),
    children: [
      { path: '', component: () => import('pages/RegistroRepartidor.vue') }
    ]
  },
  {
    path: '/profile',
    name: 'profile',
    component: () => import('layouts/Empty.vue'),
    beforeEnter: auth.requiereAutenticacion,
    children: [
      { path: '', component: () => import('pages/Profile.vue') }
    ]
  },
  {
    path: '/profileRepartidor',
    name: 'profileRepartidor',
    component: () => import('layouts/Empty.vue'),
    children: [
      { path: '', component: () => import('pages/ProfileRepartidor.vue') }
    ]
  },
  {
    path: '/notifications',
    name: 'profile',
    component: () => import('layouts/Menu.vue'),
    children: [
      { name: 'notifications', path: '', component: () => import('pages/Notifications.vue') }
    ]
  },
  {
    path: '/Cambiarcontraseña',
    name: 'Cambiarcontraseña',
    component: () => import('layouts/Empty.vue'),
    children: [
      { path: '', component: () => import('pages/Cambiarcontraseña.vue') }
    ]
  },
  {
    path: '/Iniciodesesion',
    beforeEnter: auth.siEstaAutenticado,
    component: () => import('layouts/Empty.vue'),
    children: [
      { name: 'Iniciodesesion', path: '', component: () => import('pages/Iniciodesesion.vue') }
    ]
  },
  {
    path: '/Opcionesderegistro',
    name: 'Opcionesderegistro',
    component: () => import('layouts/Menu.vue'),
    children: [
      { path: '', component: () => import('pages/Opciones.vue') }
    ]
  },
  {
    path: '/Recuperarcontraseña',
    name: 'Recuperarcontraseña',
    component: () => import('layouts/Empty.vue'),
    children: [
      { path: '', component: () => import('pages/Recuperarcontraseña.vue') }
    ]
  },
  {
    path: '/Listadirecciones',
    name: 'Misdirecciones',
    component: () => import('layouts/Menu.vue'),
    children: [
      { path: '', component: () => import('pages/Listadirecciones.vue') }
    ]
  },
  {
    path: '/Misdirecciones',
    name: 'Midireccion',
    component: () => import('layouts/Empty.vue'),
    children: [
      { path: '', component: () => import('pages/Misdirecciones.vue') }
    ]
  },
  {
    path: '/Editardireccion/:id',
    name: 'editardireccion',
    component: () => import('layouts/Empty.vue'),
    children: [
      { path: '', component: () => import('pages/Misdirecciones.vue') }
    ]
  },
  {
    path: '/Configuraciones',
    name: 'Configuraciones',
    component: () => import('layouts/Menu.vue'),
    children: [
      { path: '', component: () => import('pages/Configuraciones.vue') }
    ]
  },
  {
    path: '/Menudoomialiado',
    name: 'Menudoomialiado',
    component: () => import('layouts/Menu.vue'),
    children: [
      { path: '', props:true, component: () => import('pages/Menudoomialiado.vue') }
    ]
  },
  {
    path: '/Menudoomialiadohome/',
    component: () => import('layouts/Empty.vue'),
    children: [
      {name: 'Menudoomialiadohome', path: '', props:true, component: () => import('pages/Menudoomialiadohome.vue') }
    ]
  },
  {
    path: '/Menudoomialiadohome/:id',
    component: () => import('layouts/Empty.vue'),
    children: [
      {name: 'Menudoomialiadohome', path: '', props:true, component: () => import('pages/Menudoomialiadohome.vue') }
    ]
  },
  {
    path: '/Menudetallehome',
    component: () => import('layouts/Menu.vue'),
    children: [
      {name: 'Menudetallehome', path: '', props:true, component: () => import('pages/Menudetallehome.vue') }
    ]
  },
  {
    path: '/Menudetallehome/:id',
    component: () => import('layouts/Empty.vue'),
    children: [
      {name: 'Menudetallehome', path: '', props:true, component: () => import('pages/Menudetallehome.vue') }
    ]
  },
  {
    path: '/Pedidos',
    component: () => import('layouts/Menu.vue'),
    beforeEnter: auth.requiereAutenticacion,
    children: [
      { name: 'Pedidos', path: '', component: () => import('pages/Pedidos.vue') }
    ]
  },
   {
    path: '/Filtropedidos',
    component: () => import('layouts/Menu.vue'),
    beforeEnter: auth.requiereAutenticacion,
    children: [
      { name: 'Pedidos', path: '', component: () => import('pages/Filtropedidos.vue') }
    ]
  },
  {
    path: '/Categoria',
    component: () => import('layouts/Menu.vue'),
    children: [
      { name:'verCategoria', path:'', props:true, component: () => import('pages/Categorias.vue') }
    ]
  },
  {
    path: '/Vermas1/:id?',
    // component: () => import('layouts/Menu.vue'),
    component: () => import('layouts/Empty.vue'),
    props: true,
    children: [
      { name: 'Vermas1', props: true, path: '', component: () => import('pages/Vermas1.vue') }
    ]
  },
  {
    path: '/Sesionrepartidor',
    name: 'sesionrepartidor',
    component: () => import('layouts/Menu.vue'),
    beforeEnter: auth.siEstaAutenticado,
    children: [
      { path: '', component: () => import('pages/Sesionrepartidor.vue') }
    ]
  },
  {
    path: '/Mapa',
    name: 'mapa',
    component: () => import('layouts/Menu.vue'),
    children: [
      { path: '', component: () => import('pages/Mapa.vue') }
    ]
  },
  {
    path: '/businessrepartidor',
    name: 'bussinesrepartidor',
    component: () => import('layouts/Menu.vue'),
    children: [
      { path: '', component: () => import('pages/businessrepartidor.vue') }
    ]
  },
  {
    path: '/Notificationsrepartidor',
    name: 'notificacionesrepartidor',
    component: () => import('layouts/Menu.vue'),
    children: [
      { path: '', component: () => import('pages/Notificationsrepartidor.vue') }
    ]
  },
  {
    path: '/Loginrepartidor',
    name: 'loginrepartidor',
    component: () => import('layouts/Menu.vue'),
    children: [
      { path: '', component: () => import('pages/Loginrepartidor.vue') }
    ]
  },
  {
    path: '/Contraseñarepartidor',
    name: 'contraseñarepatidor',
    component: () => import('layouts/Menu.vue'),
    children: [
      { path: '', component: () => import('pages/Contraseñarepartidor.vue') }
    ]
  },
  {
    path: '/Historial',
    name: 'historial',
    component: () => import('layouts/Empty.vue'),
    children: [
      { path: '', component: () => import('pages/Historial.vue') }
    ]
  },
  {
    path: '/Vermashistorial1/:id?',
    // component: () => import('layouts/Menu.vue'),
    component: () => import('layouts/Empty.vue'),
    children: [
      { name:'Vermash1', props:true, path: '', component: () => import('pages/Vermashistorial1.vue') }
    ]
  },
  {
    path: '/Recuperarrepartidor',
    name: 'recurepartidor',
    component: () => import('layouts/Menu.vue'),
    children: [
      { path: '', component: () => import('pages/Recuperarrepartidor.vue') }
    ]
  },
  {
    path: '/Favoritos',
    component: () => import('layouts/Empty.vue'),
    children: [
      { name: 'favoritos', path: '', component: () => import('pages/Favoritos.vue') }
    ]
  },
  {
    path: '/Carrito',
    component: () => import('layouts/Empty.vue'),
    children: [
      { name: 'carrito', path: '', component: () => import('pages/Carrito.vue') }
    ]
  },
  {
    path: '/Formasdepago',
    component: () => import('layouts/Menu.vue'),
    children: [
      { name: 'formasdepago', path: '', props: true, component: () => import('pages/Formasdepago.vue') }
    ]
  },
  {
    path: '/Pago',
    component: () => import('layouts/Empty.vue'),
    children: [
      { name: 'pago', path: '', props: true, component: () => import('pages/Pago.vue') }
    ]
  },
  {
    path: '/Vertodos',
    name: 'Vertodo',
    component: () => import('layouts/Menu.vue'),
    children: [
      { path: '', component: () => import('pages/Vertodos.vue') }
    ]
  },
  {
    path: '/Misordenes/:en_gota?',
    props: true,
    component: () => import('layouts/Empty.vue'),
    children: [
      { name: 'Misordenes', props: true, path: '', component: () => import('pages/Misordenes.vue') }
    ]
  },
  {
    path: '/Mapahome',
    name: 'Mapahome',
    component: () => import('layouts/Empty.vue'),
    children: [
      { path: '', component: () => import('pages/MapaHome.vue') }
    ]
  },
  {
    path: '/Ventanadecarga',
    name: 'Ventanadecarga',
    component: () => import('layouts/Menu.vue'),
    children: [
      { path: '', component: () => import('pages/Ventanadecarga.vue') }
    ]
  },
  {
    path: '/Detallescliente/:id?',
    props: true,
    component: () => import('layouts/Empty.vue'),
    children: [
      {name: 'Detallescliente', props:true, path: '', component: () => import('pages/Detallescliente.vue') }
    ]
  },
  {
    path: '/Menu_configuraciones',
    props: true,
    component: () => import('layouts/Empty.vue'),
    children: [
      {name: 'Detallescliente', props:true, path: '', component: () => import('pages/Menu_configuraciones.vue') }
    ]
  },
  {
    path: '/Ayuda',
    props: true,
    component: () => import('layouts/Empty.vue'),
    children: [
      {name: 'Detallescliente', props:true, path: '', component: () => import('pages/Ayuda.vue') }
    ]
  },
  {
    path: '/Aviso/:id?',
    props: true,
    component: () => import('layouts/Empty.vue'),
    children: [
      {name: 'Aviso', props:true, path: '', component: () => import('pages/Aviso.vue') }
    ]
  },
  {
    path: '/Enconstruccion',
    props: true,
    component: () => import('layouts/Empty.vue'),
    children: [
      {name: 'Detallescliente', props:true, path: '', component: () => import('pages/Enconstruccion.vue') }
    ]
  },
  {
    path: '/Verinfodoomi',
    props: true,
    component: () => import('layouts/Empty.vue'),
    children: [
      {name: 'Detallescliente', props:true, path: '', component: () => import('pages/Verinfodoomi.vue') }
    ]
  },
  {
    path: '/Agregarubicacion',
    props: true,
    component: () => import('layouts/Empty.vue'),
    children: [
      {name: 'Detallescliente', props:true, path: '', component: () => import('pages/Agregarubicacion.vue') }
    ]
  },
  {
    path: '/Verdireccion',
    props: true,
    component: () => import('layouts/Empty.vue'),
    children: [
      {name: 'Detallescliente', props:true, path: '', component: () => import('pages/Verdireccion.vue') }
    ]
  },
  {
    path: '/Resumen',
    props: true,
    component: () => import('layouts/Empty.vue'),
    children: [
      {name: 'Detallescliente', props:true, path: '', component: () => import('pages/Resumen.vue') }
    ]
  },
  {
    path: '/RecibirDelivery/:id?',
    props: true,
    component: () => import('layouts/Empty.vue'),
    children: [
      {name: 'RecibirDelivery', props:true, path: '', component: () => import('pages/RecibirDelivery.vue') }
    ]
  },
  {
    path: '/EntregarDelivery',
    props: true,
    component: () => import('layouts/Empty.vue'),
    children: [
      {name: 'EntregarDelivery', props:true, path: '', component: () => import('pages/EntregarDelivery.vue') }
    ]
  },
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
