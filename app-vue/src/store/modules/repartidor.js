import { LocalStorage } from 'quasar'
import { auth, config } from '../../api'
import axios from 'axios'


function defaultState() {
  const id_rep = auth.obtenerIdUsuario();
  return {
    id_pedidos_rechazados: LocalStorage.getItem('PEDIDOSRECHAZADOS'+id_rep) || [],
    pedido_actual: LocalStorage.getItem('PEDIDOACTUAL'+id_rep),
    puede_empezar_delivery: LocalStorage.getItem('PUEDEEMPEZARDELIVERY'+id_rep),
    entregando: LocalStorage.getItem('ENTREGANDO'+id_rep),
    esperando_confirmacion: LocalStorage.getItem('ESPERANDOCONFIRMACION'+id_rep),
  }
}


const getters = {
  id_pedidos_rechazados: state => state.id_pedidos_rechazados,
  pedido_actual: state => state.pedido_actual,
  puede_empezar_delivery: state => state.puede_empezar_delivery,
  entregando: state => state.entregando,
  esperando_confirmacion: state => state.esperando_confirmacion,
}


const mutations = {
  ADD_PEDIDO_RECHAZADO: (state, id) => state.id_pedidos_rechazados.push(id),
  SET_PEDIDO_ACTUAL: (state, id) => { state.pedido_actual = id },
  SET_ENTREGANDO: (state, val) => { state.entregando = Boolean(val) },
  PUEDE_EMPEZAR_DELIVERY: (state, val) => { state.puede_empezar_delivery = Boolean(val) },
  SET_ESPERANDO_CONFIRMACION: (state, val) => { state.esperando_confirmacion = Boolean(val) },
  RESET_STATE: state => Object.assign(state, defaultState()),
}


const actions = {
  rechazar_pedido(ctx, id_pedido) {
    const id_rep = auth.obtenerIdUsuario()
    ctx.commit('ADD_PEDIDO_RECHAZADO', id_pedido)
    LocalStorage.set('PEDIDOSRECHAZADOS'+id_rep, ctx.getters.id_pedidos_rechazados)
  },

  tomar_pedido(ctx, id_pedido) {
    const id_rep = auth.obtenerIdUsuario()
    return new Promise((resolve, reject) => {

      axios({
        url: config.apiURL + 'pedidos/repartidor/tomar_pedido',
        method: 'PATCH',
        data: {id_pedido},
      })
      .then(resp => {
        if(resp.data.r == true) {
          ctx.commit('SET_PEDIDO_ACTUAL', id_pedido)
          LocalStorage.set('PEDIDOACTUAL'+id_rep, id_pedido)
          resolve(resp.data)
        }
        else reject(resp.data)
      })
      .catch(reject);
    });
  },

  recibir_pedido(ctx){
    const id_rep = auth.obtenerIdUsuario()
    return new Promise((resolve, reject) => {
      const data = {id_pedido: ctx.getters.pedido_actual}

      axios({
        url: config.apiURL + 'pedidos/repartidor/estatus',
        method: 'PATCH',
        data,
      }).then(resp => {
        if(resp.data && resp.data.r) {
          ctx.commit('SET_ENTREGANDO', true)
          LocalStorage.set('ENTREGANDO'+id_rep, true)
          return resolve(resp.data);
        }
        reject(resp);
      }).catch(reject);
    });
  },

  entregar_pedido(ctx){
    const id_rep = auth.obtenerIdUsuario()
    return new Promise((resolve, reject) => {
      const data = {id_pedido: ctx.getters.pedido_actual}
      axios({
        url: config.apiURL + 'pedidos/repartidor/entregar',
        method: 'PATCH',
        data
      })
      .then(resp => {
        if(resp.data && resp.data.r){
          ctx.commit('SET_ESPERANDO_CONFIRMACION', true)
          LocalStorage.set('ESPERANDOCONFIRMACION'+id_rep, true)
          return resolve(resp.data)
        }
        reject(resp)
      })
      .catch(reject)
    });
  },

  terminar_pedido(ctx){
    ctx.dispatch('reset_state')
  },

  reset_state(ctx){
    return new Promise((resolve, reject) => {
      const id_rep = auth.obtenerIdUsuario()

      LocalStorage.remove('PEDIDOACTUAL'+id_rep)
      LocalStorage.remove('PUEDEEMPEZARDELIVERY'+id_rep)
      LocalStorage.remove('ENTREGANDO'+id_rep)
      LocalStorage.remove('ESPERANDOCONFIRMACION'+id_rep)
      ctx.commit('RESET_STATE')

      resolve();
    });
  },

  set_pedido_actual(ctx, id){
    return new Promise((resolve, reject) => {
      axios.get(config.apiURL+'pedidos/repartidor/'+id)
        .then(resp => {
          if(!resp.data) return reject(resp);
          const ped = resp.data;

          if( [5,6,7,8,9,10,12].includes(parseInt(ped.id_estatus, 10)) ) {
            ctx.commit('SET_PEDIDO_ACTUAL', ped.id)
            LocalStorage.set('PEDIDOACTUAL'+ped.id_repartidor, ped.id)
          }

          if( ped.id_estatus == 10 ) {
            ctx.commit('SET_ENTREGANDO', true)
            LocalStorage.set('ENTREGANDO'+ped.id_repartidor, true)
          }

          if( ped.id_estatus == 12 ) {
            ctx.commit('SET_ESPERANDO_CONFIRMACION', true)
            LocalStorage.set('ESPERANDOCONFIRMACION'+ped.id_repartidor, true)
          }
          ctx.commit('RESET_STATE');
          resolve(ped);
        })
        .catch(reject);
    });
  },
}


export default {
  namespaced: true,
  state: defaultState,
  getters,
  mutations,
  actions
}
