import { general } from '../../api'

export default {
  namespaced: true,

  state: () => ({
    categorias: [],
    productos: [],
  }),


  getters: {
    categorias: state => state.categorias,

    productos: state => state.productos,

    doomisAbierto: (state, getters, rootState, rootGetters) => {
      const doomialiados = rootGetters.doomialiados;
      return state.productos.filter(function(producto_el){
        return doomialiados.filter(function(doomi_el){
        return doomi_el.id == producto_el.id_restaurante && !doomi_el.laborando;
        }).length == 0
      });
    },

    doomisCerrado: (state, getters, rootState, rootGetters) => {
      const doomialiados = rootGetters.doomialiados;
      return state.productos.filter(function(producto_el){
        return doomialiados.filter(function(doomi_el){
        return doomi_el.id == producto_el.id_restaurante && doomi_el.laborando;
        }).length == 0
      });
    },

    porId: (state) => (id) => state.productos.filter(el => el.id == id).pop(),

    porIdCategoria: (state) => (id) => state.productos.filter(el => el.id_categoria==id),

    porIdDoomialiado: (state) => (id) => state.productos.filter(el => el.id_restaurante==id),

    porIdDoomialiadoYCategoria: (state) => (idDoo, idCat) => {
      return state.productos.filter(el => el.id_restaurante==idDoo && el.id_categoria==idCat);
    },

    categoriasPorIdDoomialiado: (state) => (id) => state.categorias.filter(el => el.id_restaurante==id),

    adicionalesPorIdProducto: (state, getters) => (id) => {
      return getters['porId'](id).adicionales;
    },

    existenciasPorId: (state) => (id) => {
      const producto = state.productos.filter(el => el.id == id).pop();

      return producto ? producto.existencias : 0;
    },
  },


  mutations: {
    GUARDAR_CATEGORIAS: (state, categorias) => { state.categorias = categorias || [] },
    GUARDAR_PRODUCTOS: (state, productos) => { state.productos = productos || [] },
    CACHE: (state, data) => {
      if (!Array.isArray(data))
        data = [data];

      let prod, idx;
      for (prod of data) {
        if (!prod || !prod.id) continue;

        idx = state.productos.findIndex(p => p.id == prod.id);
        if (idx < 0)
          state.productos.push(prod); // Si no existe, anexar.
        else
          state.productos.splice(idx, 1, prod); // Si existe, reemplazar.
      }
    },
  },


  actions: {
    actualizarCategorias: ({ commit }) => {
      general.recuperarData('/categorias_menu/')
        .then(data => commit('GUARDAR_CATEGORIAS', data))
    },

    actualizarProductos: ({ commit }) => {
      general.recuperarData('/menu/')
        .then(data => {
          if(!data || data.r==false) data=[];
          commit('GUARDAR_PRODUCTOS', data);
        })
    },
  },
};
