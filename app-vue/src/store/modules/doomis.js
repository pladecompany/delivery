import { general, utils } from '../../api'
import { LocalStorage } from 'quasar'

export default {
  namespaced: true,

  state: () => ({
    doomialiados: [],
    doomialiadoscercanos: [],
    categorias: [],
    cargando: false,
    coordenadas_actuales: LocalStorage.getItem('DIRECCIONSELECTCORDENADA') || {},
  }),


  getters: {
    coordenadas_actuales: state => state.coordenadas_actuales,
    cargando: state => state.cargando,
    doomialiados: state => state.doomialiadoscercanos,

    doomialiadosAbiertos: state => state.doomialiadoscercanos.filter(el => el.laborando),

    doomialiadosCerrados: state => state.doomialiadoscercanos.filter(el => !el.laborando),

    doomialiadoscercanos: state => state.doomialiadoscercanos,

    doomialiadosAbiertosCercanos: state => state.doomialiadoscercanos.filter(el => el.laborando),

    doomialiadosCerradosCercanos: state => state.doomialiadoscercanos.filter(el => !el.laborando),

    abiertos: state => state.doomialiadoscercanos.filter(el => el.laborando),
    cerrados: state => state.doomialiadoscercanos.filter(el => !el.laborando),
    todos: state => state.doomialiadoscercanos,

    categorias: state => state.categorias,

    porId: (state) => (id) => {
      let doomi = state.doomialiadoscercanos.filter(el => el.id==id).pop();
      return doomi || {};
    },

    enCarrito: (state, getters, rootState, rootGetters) => {
      return rootGetters['carrito/doomi_actual']
    },

    horariosDoomialiado: (state, getters) => (id) => {
      let d = getters['porId'](id);

      return (d.horarios || []).filter(h => h.estatus);
    },

    horarioDoomialadoDeHoy: (state, getters) => (id) => {
      const hoy = utils.hoy();
      const hor = getters['horariosDoomialiado'](id).filter(h => h.dia == hoy).pop();

      if(!hor) return;

      return [utils.de24a12(hor.inicio_hor), utils.de24a12(hor.fin_hor)].join('/');

    },
    horarioDoomialadoDeHoy2: (state, getters) => (id) => {
      const hoy = utils.hoy();
      const hor = getters['horariosDoomialiado'](id).filter(h => h.dia == hoy);
      if(!hor) return;
      var hr = '';
      for (var i = 0; i < hor.length; i++) {
        if(hr=='')
          hr=[utils.de24a12(hor[i].inicio_hor), utils.de24a12(hor[i].fin_hor)].join('/');
        else
          hr+=' - '+[utils.de24a12(hor[i].inicio_hor), utils.de24a12(hor[i].fin_hor)].join('/');
      }
      return hr;

    },
  },


  mutations: {
    COORDENADAS: (state, cordenadas) => { state.coordenadas_actuales=cordenadas || {}},
    GUARDAR_DOOMIALIADOS: (state, doomialiados) => { state.doomialiados = doomialiados || [] },
    GUARDAR_DOOMIALIADOS_CERCANOS: (state, doomialiadoscercanos) => { state.doomialiadoscercanos = doomialiadoscercanos || [] },
    GUARDAR_DOOMIALIADOS_CATEGORIAS: (state, categorias) => { state.categorias = categorias || [] },
    REEMPLAZAR_DOOMIALIADO: (state, doomi) => {
      if (! doomi?.id) return;

      const idx = state.doomialiadoscercanos.findIndex(d => +d.id === doomi.id);
      if (idx < 0) return;

      // Reemplazar objeto en la lista.
      state.doomialiadoscercanos.splice(idx, 1, doomi);
    },
    CARGANDO: (state, v) => state.cargando = Boolean(v),
  },


  actions: {
    actualizarDoomialiados: ({ commit }) => {
      general.recuperarData('/restaurantes/')
        .then(data => commit('GUARDAR_DOOMIALIADOS', data))
    },
    actualizarDoomialiadosCercanos: ({ commit, getters }) => {
      return new Promise(async(resolve, reject) => {
        commit('CARGANDO', true);
        let cor = getters.coordenadas_actuales;
        let cordenada_usar = [];
        if(cor.lat && cor.lon)
          cordenada_usar = cor;
        else{
          let coords = await general.geo();
          cordenada_usar = coords;
        }
        general.recuperarDataConData('restaurantes/app', {lat: cordenada_usar.lat, lon: cordenada_usar.lon})
          .then(data => {
            commit('CARGANDO', false);
            commit('GUARDAR_DOOMIALIADOS_CERCANOS', data)
            resolve(data);
          })
          .catch(err => {
            commit('CARGANDO', false);
            reject(err);
          });
      });
    },
    actualizarCategorias: async ({ commit }) => {
      general.recuperarData('/categorias/')
        .then(data => commit('GUARDAR_DOOMIALIADOS_CATEGORIAS', data))
    },

    recuperarDoomi(ctx, id) {
      return new Promise((resolve, reject) => {
        general.recuperarDataConData('restaurantes/'+id)
          .then(doomi => {
            if(doomi && doomi.id){
              // ctx.commit('REEMPLAZAR_DOOMIALIADO', doomi);
            }
            resolve(doomi);
          })
          .catch(reject);
      });
    },
  },


};
