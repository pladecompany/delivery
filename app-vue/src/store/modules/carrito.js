import Vue from 'vue'
import Swal from 'sweetalert2'
import axios from 'axios'
import { utils, config, auth } from '../../api'


const id_length = 16;
const first_int = array => parseInt(array[0], 10); // first_int(['1', ...]) => 1
const int = x => parseInt(x, 10);
export default {
  namespaced: true,


  state: () => ({
    pedidos: {},
    idDoomialiadoActual: null, // Solo se puede tener productos de un doomi a la vez.
    modalidad: null,
    idCarrito: null,
    total: 0,
    subtotal: 0,
    subtotalAdicionales: 0,
    subtotalAcompañantes: 0,
    carrito_no_logueado: false,
    doomi_actual: null,
    // productos_actuales: null,
  }),


  getters:
  {
    // items: state => Object.keys(state.pedidos).length,
    items: state => Object.values(state.pedidos).reduce((a,b)=> a+ b.cantidad, 0),

    all: (state) => Object.values(state.pedidos),

    idDoomialiadoActual: state => state.idDoomialiadoActual,

    pedidosDetallados: (state, getters, rootState, rootGetters) => {
      const pedidos = JSON.parse(JSON.stringify(Object.values(state.pedidos)));

      // Incluir el producto.
      // pedidos.map(e => e.producto = rootGetters['productos/porId'](e.ID_PRODUCTO));

      for (let pedido of pedidos) {
        // Incluir el producto.
        pedido.producto = rootGetters['productos/porId'](pedido.ID_PRODUCTO);
        pedido.adicionales = pedido.idAdicionales;
        pedido.acompañantes = pedido.idAcompañantes || pedido.acompañantes;
        /*
        pedido.idAdicionales = pedido.idAdicionales || [];
        pedido.adicionales = [];
        let ids_adicionales = pedido.idAdicionales.map(first_int)//e => [ int(e[0]), e[1] ]);
        for (let adi of (pedido.producto.adicionales || [])) {
          let i = ids_adicionales.indexOf(adi.id);
          if(i >= 0) {
            adi.cantidad = pedido.idAdicionales[i][1];
            pedido.adicionales.push(adi);
          }
        }

        pedido.idAcompañantes = pedido.idAcompañantes || [];
        pedido.acompañantes = [];

        let ids_acompañantes = pedido.idAcompañantes.map(first_int)
        for (let aco of (pedido.producto.acompañantes || [])) {
          let i = ids_acompañantes.indexOf(aco.id);
          if(i >= 0) {
            aco.cantidad = pedido.idAcompañantes[i][1];
            pedido.acompañantes.push(aco);
          }
        }
        */
        // pedido.adicionales = pedido.producto.adicionales.filter(adi => pedido.idAdicionales.map(first_int))
      }

      /*      // Incluir adicionales.
      pedidos.map(p => {

        p.producto = rootGetters['productos/porId'](p.ID_PRODUCTO);

        p.adicionales = (p.producto.adicionales || []) // Filtrar si no estan en idAdicionales.
          .filter(function(adi){
            return this.idAdicionales ? this.idAdicionales.map(first_int).includes(parseInt(adi.id,10)) : false
          }, p) // 'p' es 'this' dentro de la funcion.


        p.acompañantes = (p.producto.acompañantes || [])
          .filter(function(aco){
            return this.acompañantes ? this.acompañantes.map(first_int).includes(parseInt(aco.id,10)) : false
          }, p) // 'p' es 'this' dentro de la funcion.
      });
      */
      return pedidos;
    },

    pedidosDetalladosNoLogueado: (state, getters, rootState, rootGetters) => {
      const pedidos = JSON.parse(JSON.stringify(Object.values(state.pedidos)));

      // Incluir el producto.
      // pedidos.map(e => e.producto = rootGetters['productos/porId'](e.ID_PRODUCTO));

      for (let pedido of pedidos) {
        // Incluir el producto.

        pedido.idAdicionales = pedido.idAdicionales || [];
        pedido.adicionales = pedido.adicionales || [];

        pedido.producto = rootGetters['productos/porId'](pedido.ID_PRODUCTO);
        // pedido.adicionales = pedido.idAdicionales;
        // pedido.acompañantes = pedido.idAcompañantes || pedido.acompañantes;

        let ids_adicionales = pedido.idAdicionales.map(first_int)//e => [ int(e[0]), e[1] ]);
        for (let adi of (pedido.producto.adicionales || [])) {
          let i = ids_adicionales.indexOf(adi.id);
          if(i >= 0) {
            adi.cantidad = pedido.idAdicionales[i][1];
            pedido.adicionales.push(adi);
          }
        }

        pedido.idAcompañantes = pedido.idAcompañantes || [];
        pedido.acompañantes = [];

        let ids_acompañantes = pedido.idAcompañantes.map(first_int)
        for (let aco of (pedido.producto.acompañantes || [])) {
          let i = ids_acompañantes.indexOf(aco.id);
          if(i >= 0) {
            aco.cantidad = pedido.idAcompañantes[i][1];
            pedido.acompañantes.push(aco);
          }
        }
      }

      return pedidos;
    },

    total: state => {
      /* Precio total del carrito con adicionales */
      if(auth.autenticado()) return state.total;
      let total = 0;
      for (let pedido of Object.values(state.pedidos)){
        total = utils.sumarDinero(total, pedido.TOTAL);
      }
      return total;
    },

    subtotal: state => {
      /* Precio total del carrito sin adicionales */
      if(auth.autenticado()) return state.subtotal;

      let subtotal = 0;
      for (let pedido of Object.values(state.pedidos)){
        // subtotal = utils.sumarDinero(subtotal, (pedido.PRECIO *100) * pedido.cantidad / 100);
        subtotal = utils.sumarDinero(subtotal, pedido.SUBTOTAL);
      }
      return subtotal;
    },

    subtotalAdicionales: state => {
      /* Precio total de todos los adicionales */

      if(auth.autenticado()) return state.subtotalAdicionales;

      let subtotal = 0;
      for (let pedido of Object.values(state.pedidos)){
        // subtotal = utils.sumarDinero(subtotal, (pedido.SUBTOTAL_ADICIONALES *100) * pedido.cantidad / 100);
        subtotal = utils.sumarDinero(subtotal, pedido.SUBTOTAL_ADICIONALES);
      }
      return subtotal;
    },

    itemPorId: (state, getters) => (id) => getters.pedidosDetallados.filter(p => p.ID==id).pop(),

    getById: (state, getters) => (id) => state.pedidos[id],

    // pedidoPorId: (state) => (idProducto) => state.pedidos[idProducto],
    pedidoPorId: (state) => (idProducto) => Object.values(state.pedidos).filter(i => i.ID_PRODUCTO==idProducto).pop(),

    cantidadPedido: (state) => (idProducto) => {
      let pedido = state.pedidos[idProducto];

      if(pedido && pedido.cantidad != undefined)
        return pedido.cantidad;

      return 0;
    },

    sync_payload: (state, getters) => Object.values(state.pedidos).map(item => ({
      // Normalizar los datos a enviar a la API.
      id_menu: item.ID_PRODUCTO,
      adicionales: item.idAdicionales,
      acompañantes: item.idAcompañantes,
      cantidad: item.cantidad,
      // id_doomialiado: getters.idDoomialiadoActual,
      notas: item.notas,
    })),

    idCarrito: state => state.idCarrito,

    carrito_no_logueado: state => state.carrito_no_logueado,

    doomi_actual: state => state.doomi_actual,
    // productos_actuales: state => state.productos_actuales,
  },


  mutations: {
    VACIAR: state => {
      state.pedidos = {};
      state.idDoomialiadoActual = null;
    },
    VACIAR_TOTALES: state => {
      state.total=0;
      state.subtotal=0;
      state.subtotalAdicionales=0;
    },
    SET_ID_DOOMIALIADO: (state, id) => state.idDoomialiadoActual = id,

    SET_ID_CARRITO: (state, id) => state.idCarrito = id,

    SET_ITEM: (state, item) => Vue.set(state.pedidos, item.ID, item),

    SET_TOTAL: (state, total) => state.total = total,
    SET_SUBTOTAL: (state, subtotal) => state.subtotal = subtotal,
    SET_SUBTOTAL_ADICIONALES: (state, sa) => state.subtotalAdicionales = sa,
    SET_SUBTOTAL_ACOMPAÑANTES: (state, sa) => state.subtotalAcompañantes = sa,

    ELIMINAR_ITEM: (state, id) =>{
      // state.carrito = state.carrito.filter(e => e.ID != id);
      Vue.delete(state.pedidos, id);
    },

    CAMBIAR_CANTIDAD_ORDEN: (state, args) => {
      const id = args[0];
      const cambio = args[1];

      const pedido = state.pedidos[id];
      pedido.cantidad = cambio;

      Vue.set(state.pedidos, id, pedido);
    },

    SET_MODALIDAD(state, modo){ state.modalidad = modo },

    SET_CARRITO_NO_LOGUEADO(state, val){ state.carrito_no_logueado = Boolean(val) },

    DOOMI_ACTUAL: (state, doomi) => { state.doomi_actual = doomi },
    // PRODUCTOS_ACTUALES: (state, data) => { state.productos_actuales = data },
    // ADD_PRODUCTO_ACTUAL: (state, data) => { state.productos_actuales = data },

  },


  actions: {

    async _populateCart(ctx, items){
      let data = {items};
      if(!items || items.length == 0){
        let resp = await axios.get(config.apiURL + 'carrito');
        data = resp.data;
        // data = resp.data;
        /*
        .then(resp => {
          let data = resp.data ? resp.data.items : [];
          commit('SET_TOTAL',resp.data.total);
          commit('SET_SUBTOTAL', resp.data.subtotal);
          commit('SET_SUBTOTAL_ADICIONALES', resp.data.subtotalAdicionales);
          commit('VACIAR');
          data.forEach(item => commit('SET_ITEM', {
            ID: item.id,
            precio: item.precio_usd,
            cantidad: item.cantidad,
            ID_PRODUCTO: item.id_menu,
            idAdicionales: item.adicionales || [],
            notas: item.notas,
            SUBTOTAL: item.subtotal,
            SUBTOTAL_ADICIONALES: item.subtotal_adicionales,
            TOTAL: item.total,
          }));
        });*/

        if(data.items) {
          ctx.commit('SET_TOTAL', data.total);
          ctx.commit('SET_SUBTOTAL', data.subtotal);
          ctx.commit('SET_SUBTOTAL_ADICIONALES', data.subtotalAdicionales);
          ctx.commit('SET_SUBTOTAL_ACOMPAÑANTES', data.subtotalAcompañantes);
        }
      }

      let menus = [];
      ctx.commit('VACIAR');

      data.items.forEach(item => {
        ctx.commit('SET_ITEM', {
            ID: item.id,
            precio: item.precio_usd,
            cantidad: item.cantidad,
            ID_PRODUCTO: item.id_menu,
            idAdicionales: item.adicionales || [],
            idAcompañantes: item.acompañantes || [],
            notas: item.notas,
            SUBTOTAL: item.subtotal,
            SUBTOTAL_ADICIONALES: item.subtotal_adicionales,
            SUBTOTAL_ACOMPAÑANTES: item.subtotal_acompañantes,
            TOTAL: item.total,
        });

        menus.push(item.menu);
      });

      ctx.commit('productos/CACHE', menus, {root:true});
      ctx.commit('SET_ID_DOOMIALIADO', data.id_restaurante);
      ctx.dispatch('doomis/recuperarDoomi', data.id_restaurante, {root:true})
        .then(doomi => ctx.commit('DOOMI_ACTUAL', doomi));

    },

    _agregarLogueado({commit, dispatch, getters, rootGetters}, item) {
      return new Promise(function(resolve, reject) {
        const pedido = {
            id_cliente: auth.obtenerIdUsuario(),
            id_producto: item.ID,
            adicionales: item.adicionales,
            acompañantes: item.acompañantes,
            cantidad: item.cantidad,
            // id_doomialiado: idDoomiActual,
            notas: item.notas,
          };

        axios({
          url: config.apiURL + "carrito/item",
          data: pedido,
          method: "POST"
        }).then(resp => {
          let data = resp.data;

          if(!data){ return console.log('no data') }

          if(data.errr){
            if(data.id_doomialiado_actual)
              commit('SET_ID_DOOMIALIADO', data.id_doomialiado_actual);

            return resolve({m: data.msj, r: data.errr, idDoomi: data.id_doomialiado_actual})
          }

          commit('SET_ID_DOOMIALIADO', data.id_doomialiado_actual);
          commit('SET_TOTAL', data.total)
          commit('SET_SUBTOTAL', data.subtotal)
          commit('SET_SUBTOTAL_ADICIONALES', data.subtotalAdicionales)
          dispatch('_populateCart')//, data.items);
          // commit('SET_ITEM', {
          //   ID: data.ultimo_item.id,
          //   notas: data.ultimo_item.notas,
          //   precio: data.ultimo_item.precio_usd,
          //   cantidad: data.ultimo_item.cantidad,
          //   ID_PRODUCTO: data.ultimo_item.id_menu,
          //   idAdicionales: data.ultimo_item.adicionales,
          //   SUBTOTAL: data.ultimo_item.subtotal,
          //   SUBTOTAL_ADICIONALES: data.ultimo_item.subtotalAdicionales,
          //   TOTAL: data.ultimo_item.total,
          // });

          resolve({m: data.msj, r:data.r});
        })
        .catch(reject);
      });
    },

    _agregarNoLogueado({commit, dispatch, getters, rootGetters}, item) {
      return new Promise(function(resolve, reject) {
          const producto = rootGetters['productos/porId'](item.ID);
          const precio_porcentaje = ((producto.precio_usd*100) * ((producto.ganancia||0) / 100) + (producto.precio_usd*100)) / 100;

          const cantidad_adis = {};
          for (let adi of item.adicionales) {
            cantidad_adis[adi[0]] = adi[1];
          }

          const cantidad_acom = {};
          for (let aco of item.acompañantes) {
            cantidad_acom[aco[0]] = aco[1];
          }

          let idAdicionales = item.adicionales.map(first_int);
          let idAcompañantes = item.acompañantes.map(first_int);

          item.SUBTOTAL = precio_porcentaje*100 * item.cantidad / 100;

          if(producto.adicionales){
            // Sumar la lista de los precios de los adicionales.
            let preciosAdicionales = producto.adicionales
                .filter(e => idAdicionales.includes(e.id))
                .map(ad => ((ad.precio_usd*100) * ((producto.ganancia||100)/100) + (ad.precio_usd*100)) * cantidad_adis[ad.id] / 100);

            item.SUBTOTAL_ADICIONALES = utils.sumarDinero(...preciosAdicionales);
          }

          if(producto.acompañantes){
            // Sumar la lista de los precios de los adicionales.
            let preciosAcompañantes = producto.acompañantes
                .filter(e => idAcompañantes.includes(e.id))
                .map(ac => ((ac.precio_usd*100) * ((producto.ganancia||100)/100) + (ac.precio_usd*100)) * cantidad_acom[ac.id] / 100);

            item.SUBTOTAL_ACOMPAÑANTES = utils.sumarDinero(...preciosAcompañantes);
          }

        // Calcular total: Subtotal + Adicionales.
        item.TOTAL = utils.sumarDinero(item.SUBTOTAL, item.SUBTOTAL_ADICIONALES, item.SUBTOTAL_ACOMPAÑANTES);

        commit('SET_ITEM', {
            ID: utils.generarId(id_length), //item.ID,
            notas: item.notas,
            precio: item.precio || item.precio_usd || producto.precio_usd,
            cantidad: item.cantidad,
            ganancia: producto.ganancia,
            ID_PRODUCTO: producto.id,
            idAdicionales: item.adicionales,
            // adicionales: item.adicionales,
            idAcompañantes: item.acompañantes || [],
            // acompañantes: item.acompañantes || [],
            SUBTOTAL: item.SUBTOTAL,
            SUBTOTAL_ADICIONALES: item.SUBTOTAL_ADICIONALES,
            SUBTOTAL_ACOMPAÑANTES: item.SUBTOTAL_ACOMPAÑANTES,
            TOTAL: item.TOTAL,
          });

        commit('SET_SUBTOTAL', utils.sumarDinero(getters.subtotal, item.SUBTOTAL))
        commit('SET_SUBTOTAL_ADICIONALES', utils.sumarDinero(getters.subtotalAdicionales, item.SUBTOTAL_ADICIONALES))
        commit('SET_TOTAL', utils.sumarDinero(getters.total, item.TOTAL))
        commit('SET_CARRITO_NO_LOGUEADO', true);
        return resolve({m: 'Agregado al carrito', r: 1});

      });
    },

    agregar({commit, dispatch, getters, rootGetters}, datos) {
      /*
      ID: Es el mismo id del menu/producto.
      PRECIO: Precio unitario del producto.
      SUBTOTAL: Precio total sin adicional.
      SUBTOTAL_ADICIONALES: Precio total de solo los adicionales.
      TOTAL: Precio completo incluyendo cantidad y adicionales.
      */

      return new Promise(function(resolve, reject){

        const item = {
          ID: datos.idProducto || datos.idMenu || datos.idItem,
          idAdicionales: (datos.idAdicionales || []).map(id => parseInt(id, 10)),
          idAcompañantes: (datos.idAcompañantes || []).map(id => parseInt(id, 10)),
          notas: datos.nota || datos.notas || '',
          cantidad: datos.cantidad || 0,
          adicionales: datos.adicionales || [],
          acompañantes: datos.acompañantes || []
        };

        const producto = rootGetters['productos/porId'](item.ID);
        const idDoomiActual = getters['idDoomialiadoActual'];
        const idDoomiProducto = producto ? producto.id_restaurante : null;

        if (idDoomiActual && idDoomiActual != producto.id_restaurante)
          return resolve({r:-1, m:'Hay productos de otro doomialiado en el carrito'});

        commit('SET_ID_DOOMIALIADO', producto.id_restaurante);

        const doomi = rootGetters['doomis/porId'](producto.id_restaurante);
        if (doomi) {
          commit('DOOMI_ACTUAL', doomi);
        }

        if(auth.autenticado()){
          dispatch('_agregarLogueado', item).then(resolve);
        }
        else {
          dispatch('_agregarNoLogueado', item).then(resolve);
        }
      });
    },

    subir({commit, dispatch, getters, rootState}){
      /* Subir carrito no logueado */
      if(getters.items < 1 || auth.obtenerTipoUsuario() != 'cliente')
        return Promise.reject();

      const data = {
        items: getters.sync_payload,
        id_doomialiado: getters.idDoomialiadoActual,
      };

      return new Promise(function(resolve, reject) {
        axios({
          url: config.apiURL + 'carrito/',
          data,
          method: 'POST',
        })
        .then(resp => {
          commit('SET_CARRITO_NO_LOGUEADO', false);
          dispatch('actualizar').then(resolve, reject);
        })
        .catch(resp => {
          return reject(resp.response);
        });
      });
    },

    cambiarCantidadOrden({ getters, rootGetters, commit, dispatch }, args){
      return new Promise(function(resolve, reject) {
        const idItem = args[0];
        const cantidad = args[1];
        const logueado = auth.autenticado();

        if(logueado){
          if (!utils.esNumerico(idItem) || !utils.esNumerico(cantidad) || cantidad==0){
            return console.log('cambiarCantidadOrden: error en id o en cantidad');
          }
        }
        else if(!idItem || idItem.length < id_length-2) {
          return console.log('cambiarCantidadOrden: error en id o en cantidad');
        }


        if(!logueado){
          let cantidad_actual = getters.cantidadPedido(idItem);
          if(cantidad > 0){
            commit('CAMBIAR_CANTIDAD_ORDEN', [idItem, cantidad]);
            // Recalcular totales locales.
            const item = JSON.parse(JSON.stringify(getters.getById(idItem)));
            const info = getters.pedidosDetalladosNoLogueado.filter(p => p.ID==idItem).pop() || {};
            const ganancia = ((item.ganancia || 0) / 100)
            // let adicionales = (pedido.adicionales || []).filter(a => item.adicionales.includes(a.id));
            // item.SUBTOTAL = item.precio * 100 * item.cantidad / 100;
            item.SUBTOTAL = ((item.precio*100) * ganancia + (item.precio*100)) * item.cantidad / 100;
            let preciosAdi = (info.adicionales||[]).map(adi => (adi.precio_usd*100 + (adi.precio_usd * 100 * ganancia)) * adi.cantidad / 100);
            let preciosAco = (info.acompañantes||[]).map(aco => (aco.precio_usd*100 + (aco.precio_usd * 100 * ganancia)) * aco.cantidad / 100);
            item.SUBTOTAL_ADICIONALES = utils.sumarDinero(...preciosAdi);
            item.SUBTOTAL_ACOMPAÑANTES = utils.sumarDinero(...preciosAco);
            item.TOTAL = utils.sumarDinero(item.SUBTOTAL, item.SUBTOTAL_ADICIONALES, item.SUBTOTAL_ACOMPAÑANTES);
            commit('SET_ITEM', item);
          }
          return;
        }


        axios({
          url: config.apiURL + 'carrito/item/cantidad',
          data: { idItem, cantidad },
          method: 'PATCH',
        })
        .then(r => {
          if(r.data && r.data.r){
            dispatch('_populateCart')
            resolve(r.data.msj);
          }
        })
        .catch(console.error);
      });
    },

    eliminar({ commit, dispatch, getters }, idItem) {
      return new Promise(function(resolve, reject){
        const logueado = auth.autenticado();

        if(logueado && (!idItem || !utils.esNumerico(idItem))){
          return resolve({r: false, m: ''});
        }
        else if(!logueado && idItem){
          commit('ELIMINAR_ITEM', idItem);
          if(getters.items == 0)
            commit('VACIAR');
          return;
        }

        axios({
          url: config.apiURL + 'carrito/item',
          data:{idItem},
          method: 'DELETE',
        })
        .then(res => {
          if(res.data && res.data.r){
            dispatch('_populateCart')
            resolve({r: true, m:res.data.msj});
          }
        })
        .catch(console.error);
      });
    },

    vaciar(context, vaciar_local=true){
      if(!auth.autenticado() && vaciar_local){
        return context.commit('VACIAR');
      }
      return new Promise(function(resolve, reject){
        axios({
          url: config.apiURL + 'carrito/',
          method: 'DELETE',
        }).then(r => {
          if(vaciar_local){
            context.commit('VACIAR');
            context.commit('VACIAR_TOTALES');
          }
          resolve(r.data);
        });
      });
    },

    modalidad: (context, modalidad) => context.commit('SET_MODALIDAD', modalidad),

    checkout({ commit, getters, dispatch }, data){
      return new Promise(function(resolve,reject){
        axios({
          url: config.apiURL + 'carrito/checkout',
          method: 'POST',
          data: data,//getters['apiPayload'],
          headers: {
            'id_res': getters['idDoomialiadoActual'],
            'id_cliente': auth.obtenerIdUsuario(),
          },
        })
        .then(resp => {
          // dispatch('ordenes/_afterCheckout', resp.data, {root:true});
          if(resp.data.errr){
            resolve(resp.data)
          }else{
            commit('VACIAR');
            commit('VACIAR_TOTALES');
            resolve('¡Orden enviada!')
          }

        })
        .catch(err => reject(err));
      });
    },

    actualizar({commit, dispatch, getters}){
      if(!auth.autenticado() || getters.carrito_no_logueado) return;
      return new Promise(function(resolve, reject){
        dispatch('_populateCart').then(resolve).catch(reject);
      });
    },


  },
};
